/*
 * =====================================================================================
 *
 *       Filename:  kill.c
 *
 *    Description:  Functions to end the simulation.
 *
 *        Version:  0.6.3
 *        Created:  12/07/13 14:58:07
 *
 *         Author:  Matthew D. Young, my304@ast.cam.ac.uk
 *        Company:  Institute of Astronomy, Cambridge, UK
 *        License:  GNU General Public License
 *      Copyright:  Copyright (c) 2014, Matthew D. Young
 *
 * =====================================================================================
 */

#include "variables.h"
#include "functions.h"


void kill(int error_code)
{
  if(error_code)
  {
    printf("[%d] Something has gone wrong!  Error code %d.\n",Task,error_code);
  }
  printf("[%d] Freeing memory and quiting.\n",Task);
  freedom();
  MPI_Finalize();
  //Terminate the code
  exit(error_code);
}


