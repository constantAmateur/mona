import h5py as h
d=h.File("test_1e6.hdf5")
dd=h.File("tst.hdf5")
#The gas
dd.create_group("Gas")
dd["Gas"]["X"]=d["PartType0"]["Coordinates"][:,0]
dd["Gas"]["Y"]=d["PartType0"]["Coordinates"][:,1]
dd["Gas"]["Z"]=d["PartType0"]["Coordinates"][:,2]
dd["Gas"]["Vel_X"]=d["PartType0"]["Velocities"][:,0]
dd["Gas"]["Vel_Y"]=d["PartType0"]["Velocities"][:,1]
dd["Gas"]["Vel_Z"]=d["PartType0"]["Velocities"][:,2]
dd["Gas"]["Acc_X"]=np.zeros(dd["Gas"]["X"].shape)
dd["Gas"]["Acc_Y"]=np.zeros(dd["Gas"]["X"].shape)
dd["Gas"]["Acc_Z"]=np.zeros(dd["Gas"]["X"].shape)
dd["Gas"]["Smoothing_Length"]=np.zeros(dd["Gas"]["X"].shape)
dd["Gas"]["Density"]=np.zeros(dd["Gas"]["X"].shape)
dd["Gas"]["Internal_Energy"]=d["PartType0"]["InternalEnergy"][:]
dd["Gas"].attrs["Mass"]=2e-7
#The sink
dd.create_group("Sink")
dd["Sink"]["X"]=d["PartType1"]["Coordinates"][:,0]
dd["Sink"]["Y"]=d["PartType1"]["Coordinates"][:,1]
dd["Sink"]["Z"]=d["PartType1"]["Coordinates"][:,2]
dd["Sink"]["Vel_X"]=d["PartType1"]["Velocities"][:,0]
dd["Sink"]["Vel_Y"]=d["PartType1"]["Velocities"][:,1]
dd["Sink"]["Vel_Z"]=d["PartType1"]["Velocities"][:,2]
dd["Sink"]["Acc_X"]=np.zeros(dd["Sink"]["X"].shape)
dd["Sink"]["Acc_Y"]=np.zeros(dd["Sink"]["X"].shape)
dd["Sink"]["Acc_Z"]=np.zeros(dd["Sink"]["X"].shape)
dd["Sink"]["Mass"]=d["PartType1"]["Masses"][:]
dd.close()
d.close()

