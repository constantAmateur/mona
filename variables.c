
#include "variables.h"
#include "functions.h"

int ParameterDefinitions_num;

int IO_properties_num;

int IO_particles_num;

int Output_num;

int MaxLVL;

PCLE_REF MaxID;

TREE_REF MaxLID;

int MaxGas;

int Task;

int NTask;

int PTask;

int NShadows;

void *Buffer;

struct gas *GBuffer;

TREE_REF *TRBuffer;

int *IBuffer;

FLOAT *FBuffer;

double *DomainWalls;

double SimBox[3][2];

int NextFreeNode;

int *NGas;

PCLE_REF *Ngbs;

int N_Export;

int *Export;

int *N_Send_Local;

int NPart[MAX_NO_PART_TYPES];

int NPartWorld[MAX_NO_PART_TYPES];

MPI_Group World;

int MaskSize;

char *NullMask;

double AccreteMom[3];

double AccreteAngMom[3];

double AccreteEnergy;

int NgbBufferSizeInts;

int BufferSizeBytes;

int BufferSizeGas;

int BufferSizeInts;

int BufferSizeFloats;

int BufferSizeTRs;

int BufferSizeDen;

int BufferSizeHydro;

int BufferSizeVisc;

int BufferSizeGravSlow;

int BufferSizeGrav;

int BufferSizeCoord;

int HalfBufferSizeBytes;

int TreeSize;

int NextID;

double PcleMass;

double COM[3];

double Mom[3];

double AngMom[3];

double Energy;

double Time;

double Dt;

int NTerm;

int LeafCtr;

int *LvlOffset;

int TreeDepth;

struct parameters

Params;

struct gas 
*G;

struct shadow 
*Shadows;

struct sink 
*S;

struct node Null_node;

struct node 
*Root,
*Trunk,
*UpperTrunk,
*Branch,
*Nodes;

union density_comm 
*DenSend,
*DenRecv;

union hydro_comm 
*HydroSend,
*HydroRecv,
*HydroBuffer;

union visc_comm 
*ViscSend,
*ViscRecv;

union grav_comm 
*GravSend,
*GravRecv;

struct slow_grav_comm 
*GravSlowSend,
*GravSlowRecv;

struct coords 
*Coords;

struct timings 
NullTimings,
Timings;
