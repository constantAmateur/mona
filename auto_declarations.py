#!/usr/bin/python
"""
Automatically generate variable declaration file variables.c from variables.h.
To automatically generate functions.h, run
"""
import re
out="""
#include "variables.h"
#include "functions.h"
"""
exception_strings=("ParameterDefinitions[]",
    "IO_properties[]",
    "IO_particles[]",
    "Output[]")

extn=re.compile("^\s*extern\s+(.+?\;)",re.M+re.DOTALL)
dat=open("variables.h").read();
#First remove any brackets
ndat=""
inbrack=0
for ch in dat:
  if ch=="{":
    inbrack=inbrack+1
  elif ch=="}":
    inbrack=inbrack-1
  elif not inbrack:
    ndat=ndat+ch

#Now find everything that is an external definition
new=extn.findall(ndat)
#Throw out any of the exceptions, but otherwise add them to the output
for dec in new:
  skip=False
  for bad in exception_strings:
    if dec.find(bad)!=-1:
      skip=True
      break
  if not skip:
    out=out+"\n"+dec+"\n"

g=open("variables.c",'w')
g.write(out)
g.close()

