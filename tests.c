/*
 * =====================================================================================
 *
 *       Filename:  tests.c
 *
 *    Description:  A series of tests of different parts of the code.
 *
 *        Version:  0.6.3
 *        Created:  23/08/13 10:34:32
 *
 *         Author:  Matthew D. Young, my304@ast.cam.ac.uk
 *        Company:  Institute of Astronomy, Cambridge, UK
 *        License:  GNU General Public License
 *      Copyright:  Copyright (c) 2014, Matthew D. Young
 *
 * =====================================================================================
 */

#include "variables.h"
#include "functions.h"


//Tests if the locally contained multipoles
//are correct (i.e., trunk is ignored)
//int test_multipoles(int verbose,double toll)
//{
//  int i,j,bad;
//  int ret;
//  double com[3];
//  double quad[3][3];
//  struct node* cell=Branch;
//  ret=0;
//  
//  while(1)
//  {
//    //Calculate them directly for this node
//    for(i=0;i<3;i++)
//    {
//      for(j=0;j<3;j++)
//      {
//        quad[i][j]=0;
//      }
//      com[i]=0;
//    }
//    //Dipole
//    for(i=cell->mob;i<cell->mob+cell->count;i++)
//    {
//      com[0]+= G[i].x;
//      com[1]+= G[i].y;
//      com[2]+= G[i].z;
//    }
//    com[0] /= cell->count;
//    com[1] /= cell->count;
//    com[2] /= cell->count;
//    //Quadrupole
//    for(i=cell->mob;i<cell->mob+cell->count;i++)
//    {
//      quad[0][0] += (com[0]-G[i].x) * (com[0]-G[i].x);
//      quad[0][1] += (com[0]-G[i].x) * (com[1]-G[i].y);
//      quad[0][2] += (com[0]-G[i].x) * (com[2]-G[i].z);
//      quad[1][0] += (com[1]-G[i].y) * (com[0]-G[i].x);
//      quad[1][1] += (com[1]-G[i].y) * (com[1]-G[i].y);
//      quad[1][2] += (com[1]-G[i].y) * (com[2]-G[i].z);
//      quad[2][0] += (com[2]-G[i].z) * (com[0]-G[i].x);
//      quad[2][1] += (com[2]-G[i].z) * (com[1]-G[i].y);
//      quad[2][2] += (com[2]-G[i].z) * (com[2]-G[i].z);
//    }
//    for(i=0;i<3;i++)
//    {
//      for(j=0;j<3;j++)
//      {
//        quad[i][j] /= cell->count;
//      }
//    }
//    //Test them against the stored value
//    bad=0;
//    for(i=0;i<3;i++)
//    {
//      for(j=0;j<3;j++)
//      {
//        //Divide by N for comparison...
//        if(fabs(quad[i][j]-cell->quad[i][j])/quad[i][j]>toll)
//        {
//          bad=1;
//          ret++;
//          break;
//        }
//      }
//      if(bad)
//        break;
//      if(fabs(com[i]-cell->com[i])/com[i]>toll)
//      {
//        bad=1;
//        ret++;
//        break;
//      }
//    }
//    if(bad && verbose && cell->count>100000)
//    {
//      printf("[%d] Bad node is ([%g,%g],[%g,%g],[%g,%g]) offset=%d,count=%d,mother=%d,sister=%d,daughter=%d,trunkity=%d,locality=%d,leafity=%d,meta=%d,owner=%d.\n",Task,cell->lows[0],cell->highs[0],cell->lows[1],cell->highs[1],cell->lows[2],cell->highs[2],(int) (cell-Nodes),cell->count,cell->mother,cell->sister,cell->daughter,cell->flag & NODE_TRUNK,cell->flag & NODE_LOCAL,cell->flag & NODE_LEAF,cell->flag & NODE_META,(cell->flag) & NODE_OWNER);
//      printf("[%d] Expected COM = (%g,%g,%g) and Quad = ((%g,%g,%g),(%g,%g,%g),(%g,%g,%g)) but got COM = (%g,%g,%g) and Quad = ((%g,%g,%g),(%g,%g,%g),(%g,%g,%g)).\n",Task,com[0],com[1],com[2],quad[0][1],quad[0][2],quad[0][3],quad[1][0],quad[1][1],quad[1][2],quad[2][0],quad[2][1],quad[2][2],cell->com[0],cell->com[1],cell->com[2],cell->quad[0][1],cell->quad[0][2],cell->quad[0][3],cell->quad[1][0],cell->quad[1][1],cell->quad[1][2],cell->quad[2][0],cell->quad[2][1],cell->quad[2][2]);
//    }
//    //OK, now time to move onto next cell
//    //If it needs opening up (and we can open it), do so
//    if((cell->flag & NODE_META) || (!(cell->flag & NODE_LEAF) && ((cell->flag & NODE_OWNER)==Task)))
//    {
//      cell = Nodes + cell->daughter;
//    }
//    else
//    {
//      //Otherwise move onto sister or finish
//      if(cell->sister==-1)
//        break;
//      cell = Nodes + cell->sister;
//    }
//  }
//  return ret;
//}


//Tests to see if the current arrangement of particles onto processors
//is consistent with the boundaries drawn in DomainWalls (remembering 
//that the convention is that lower boundaries are inclusive and upper 
//boundaries are exclusive)
//returns 1 on failure, if verbose will print all offending particles
int test_valid_decomp(int verbose)
{
  int i;
  int ret;
  ret=0;
  for(i=0;i<NPart[GAS];i++)
  {
    if(G[i].R<DomainWalls[Task] || G[i].R>=DomainWalls[Task+1])
    {
      if(verbose)
      {
        printf("[%d] Particle with G[%d].R=%g outside domain [%g,%g).\n",Task,i,G[i].R,DomainWalls[Task],DomainWalls[Task+1]);
      }
      ret = 1;
    }
  }
  return ret;
}

//Test neighbour finding routine by doing it the slow way and comparing the
//answer.
//returns 1 if the neighbour finder misses some particles
int test_ngb_finder(int ntest,double h,int verbose)
{
  double point[3];
  int fast_cnt,slow_cnt;
  double dynamic_h,static_h;
  int src,ret,i;
  int extra;
  h *= KER_SUPPORT;
  dynamic_h=!h;
  static_h = h;
  src=0;
  ret=0;
  i=0;
  //Test the first ntest particles to see if we get the right number of neighbours
  //Loop through processors doing the particles one at a time
  //for(i=0;i<NPart[GAS];i++)
  //{
  //  if(G[i].id==412877)
  //  {
  //    printf("[%d] Missed Particle of interest has x,y,z,R,phi = %g,%g,%g,%g,%g, id = %d, i=%d.\n",Task,G[i].x,G[i].y,G[i].z,G[i].R,G[i].phi,G[i].id,i);
  //    for(j=0;j<NNodes;j++)
  //    {
  //      if((Nodes[j].flag & NODE_LOCAL) && (Nodes[j].flag & NODE_LEAF) && i>=Nodes[j].mob && i<(Nodes[j].mob+Nodes[j].count))
  //      {
  //        printf("[%d] Missed particle is in leaf ([%g,%g],[%g,%g],[%g,%g]) offset=%d,count=%d,mother=%d,sister=%d,daughter=%d,trunkity=%d,locality=%d,leafity=%d,meta=%d,owner=%d,rmax=%g,com=%g,%g,%g,real_cnt=%d.\n",Task,Nodes[j].lows[0],Nodes[j].highs[0],Nodes[j].lows[1],Nodes[j].highs[1],Nodes[j].lows[2],Nodes[j].highs[2],j,Nodes[j].count,Nodes[j].mother,Nodes[j].sister,Nodes[j].daughter,Nodes[j].flag & NODE_TRUNK,Nodes[j].flag & NODE_LOCAL,Nodes[j].flag & NODE_LEAF,Nodes[j].flag & NODE_META,(Nodes[j].flag) & NODE_OWNER,Nodes[j].rmax,Nodes[j].com[0],Nodes[j].com[1],Nodes[j].com[2],i);
  //      }
  //    }
  //  }
  //}
 
  //i=0;
  while(i*NTask+src<ntest)
  {
    if(Task==src)
    {
      if(dynamic_h)
        h=G[i].hsml*KER_SUPPORT;
      else
        h=static_h;
      point[0]=G[i].x;
      point[1]=G[i].y;
      point[2]=G[i].z;
    }
    MPI_Bcast(point,3,MPI_DOUBLE,src,MPI_COMM_WORLD);
    MPI_Bcast(&h,1,MPI_DOUBLE,src,MPI_COMM_WORLD);
    MPI_Bcast(&extra,1,MPI_INT,src,MPI_COMM_WORLD);
    extra=0;
    if(extra && Task==src)
    {
      printf("[%d] Looking in detail at particle at x,y,z,R,phi = %g,%g,%g,%g,%g\n",Task,point[0],point[1],point[2],G[i].R,G[i].phi);
    }
    //printf("[%d] new extra = %d h=%g point=%g,%g,%g.\n",Task,extra,h,point[0],point[1],point[2]);
    if(extra)
      printf("[%d] Before slow point is %g,%g,%g.\n",Task,point[0],point[1],point[2]);
    slow_cnt=count_ngbs_slow(point,h,extra);
    if(extra)
      printf("[%d] Before fast point is %g,%g,%g.\n",Task,point[0],point[1],point[2]);
    fast_cnt=count_ngbs_fast(point,h,extra);
    if(extra)
      printf("[%d] After slow point is %g,%g,%g.\n",Task,point[0],point[1],point[2]);
    if(extra)
      printf("[%d] Fast found %d, slow found %d, src = %d.\n",Task,fast_cnt,slow_cnt,src);
    MPI_Allreduce(MPI_IN_PLACE,&fast_cnt,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
    MPI_Allreduce(MPI_IN_PLACE,&slow_cnt,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
    if(extra && Task==0)
      printf("[%d] Globally, fast found %d, slow found %d.\n",Task,fast_cnt,slow_cnt);
    if(fast_cnt!=slow_cnt)
    {
      ret=1;
      if(verbose && Task==src)
      {
        printf("[%d] Particle with G[%d].(x,y,z,R,phi)=(%g,%g,%g,%g,%g) should have found %d particles within %g, but instead found %d.\n",Task,i,G[i].x,G[i].y,G[i].z,G[i].R,G[i].phi,slow_cnt,h,fast_cnt);
      }
    }
    src++;
    if(src==NTask)
      i++;
    src = src % NTask;
  }
  return ret;
}

//We do this on all processors even though this
//is unnecessary
int count_ngbs_fast(double point[3],double d,int extra)
{
  int current;
  int fnd,ngbs,j,i;
  double r;
  current=0;
  fnd=0;
  do{
    //ngbs=find_ngbs(point,d,&current,0);
    ngbs=find_ngbs(point,d,&current,1);
    //printf("[%d] Ngb search found %d points/nodes current=%d.\n",Task,ngbs,current);
    for(i=0;i<ngbs;i++)
    {
      //Only count those that are locally owned.  Ignore shadows.
      if(Ngbs[i]<0 || Ngbs[i]/MaxGas!=Task)
        continue;
      j=Ngbs[i]%MaxGas;
      r = sqrt((point[0]-G[j].x)*(point[0]-G[j].x)+(point[1]-G[j].y)*(point[1]-G[j].y)+(point[2]-G[j].z)*(point[2]-G[j].z));
      if(extra)
      {
        printf("[%d] Tree provisionally identified a neighbour at %g,%g,%g,%g,%g distance %g.\n",Task,G[j].x,G[j].y,G[j].z,G[j].R,G[j].phi,r);
      }
      if(r<=d)
      {
        if(extra)
        {
          printf("[%d] Tree found %g,%g,%g ID = %d  r = %g as ngb.\n",Task,G[j].x,G[j].y,G[j].z,G[j].id,r);
        }
        fnd++;
      }
    }
  }while(current!=-1);
  return fnd;
}

//Finds all neighbours across all processors and returns how many their are
int count_ngbs_slow(double point[3],double d,int extra)
{
  double r;
  int i,fnd;
  fnd=0;
  for(i=0;i<NPart[GAS];i++)
  {
    r=sqrt((point[0]-G[i].x)*(point[0]-G[i].x)+(point[1]-G[i].y)*(point[1]-G[i].y)+(point[2]-G[i].z)*(point[2]-G[i].z));
    if(r<=d)
    {
      fnd++;
      if(extra)
      {
        printf("[%d] Exact found %g,%g,%g ID = %d idx=%d r=%g as ngb of %g,%g,%g.\n",Task,G[i].x,G[i].y,G[i].z,G[i].id,i,r,point[0],point[1],point[2]);
      }
    }
  }
  return fnd;
}

int test_tree_build(int verbose)
{
  int i,j;
  int lvls[MaxLVL];
  int leaves_lvls[MaxLVL];
  int seen[NTask];
  int bbad;
  int mob,count;
  struct node* cnode;
  struct node* daughters;
  cnode=Root;
  bbad=0;
  for(i=0;i<MaxLVL;i++)
  {
    lvls[i]=0;
    leaves_lvls[i]=0;
  }
  //Check that there are no duplicate owners in those with extra mem allocated
  for(i=0;i<NextFreeNode;i++)
  {
    if(Root[i].extra)
    {
      for(j=0;j<NTask;j++)
        seen[j]=0;
      for(j=0;j<(-1*Root[i].owner);j++)
      {
        if(seen[Root[i].extra[j].mob/MaxGas])
        {
          bbad++;
          if(verbose)
          {
            pprintf("Node lvl/lid %d/%d has a duplicate owner.\n",(int) (Root[i].lvl_lid/MaxLID),(int) (Root[i].lvl_lid%MaxLID));
          }
        }
        else
        {
          seen[Root[i].extra[j].mob/MaxGas]=1;
        }

      }
    }
  }
  //Specific check that each node's sum of daughters is the same as count
  while(1)
  {
    if(cnode->n_daughters)
    {
      j=0;
      daughters=Root+cnode->daughter;
      for(i=0;i<cnode->n_daughters;i++)
      {
        j+= daughters->count;
        daughters = Root+daughters->sister;
      }
      if(j!=cnode->count)
      {
        bbad++;
        if(verbose)
        {
          pprintf("Node with lvl=%lu and lid=%lu n_daughters=%d and owner %d has %d pcles, but daughters have %d in total.",cnode->lvl_lid/MaxLID,cnode->lvl_lid%MaxLID,cnode->n_daughters,cnode->owner,cnode->count,j);
          if(cnode->owner<0)
          {
            printf(" Owners:");
            for(j=0;j<(-cnode->owner);j++)
            {
              printf("%d ",cnode->extra[j].mob/MaxGas);
            }
          }
          printf("\n");
          daughters=Root+cnode->daughter;
          for(i=0;i<cnode->n_daughters;i++)
          {
            pprintf("Daughter number %d has lvl=%lu and lid=%lu owner %d count %d.\n",i,daughters->lvl_lid/MaxLID,daughters->lvl_lid%MaxLID,daughters->owner,daughters->count);
            daughters = Root+daughters->sister;
          }
          pprintf("The 7 preceding the daughter.\n");
          for(i=1;i<8 && cnode->daughter-i>=0;i++)
          {
            daughters=Root+cnode->daughter-i;
            pprintf("Daughter number %d has lvl=%lu and lid=%lu owner %d count %d.\n",i,daughters->lvl_lid/MaxLID,daughters->lvl_lid%MaxLID,daughters->owner,daughters->count);
          }
          pprintf("The 7 after the daughter.\n");
          for(i=1;i<8 && cnode->daughter+i<NextFreeNode;i++)
          {
            daughters=Root+cnode->daughter+i;
            pprintf("Daughter number %d has lvl=%lu and lid=%lu owner %d count %d.\n",i,daughters->lvl_lid/MaxLID,daughters->lvl_lid%MaxLID,daughters->owner,daughters->count);
          }
        }
      }
      cnode=Root+cnode->daughter;
    }
    else
    {
      if(cnode->sister==-1)
        break;
      cnode = Root+cnode->sister;
    }
  }
  cnode=Root;
  //make sure all the particles are where they say they are and the counts are correct
  while(1)
  {
    //printf("[%d] Looking at node with lvl/lid = %lu\n",Task,cnode->lvl_lid);
    //Count at levels
    lvls[(int) (cnode->lvl_lid/MaxLID)] += cnode->count;
    //Number of counts in leaves at level
    if(!cnode->n_daughters)
      leaves_lvls[(int) (cnode->lvl_lid/MaxLID)] += cnode->count;
    //If it's got some local particles, check them...
    if((cnode->owner<0 && get_bit(Task,cnode->owners)) || (cnode->owner>=0 && cnode->owner==Task))
    {
      i=0;
      do
      {
        if(cnode->owner<0)
        {
          if(cnode->extra[i].mob/MaxGas!=Task)
          {
            i++;
            continue;
          }
          mob = (int) (cnode->extra[i].mob % MaxGas);
          count = cnode->extra[i].count;
        }
        else
        {
          mob=cnode->mob;
          count = cnode->count;
        }
        for(j=mob;j<mob+count;j++)
        {
          //Test that this particle is within cell boundaries
          //Remember that upper bounds are exclusive
          if(G[j].R<cnode->lows[0] || G[j].R>=cnode->highs[0] ||
              G[j].phi < cnode->lows[1] || G[j].phi >= cnode->highs[1] ||
              G[j].z < cnode->lows[2] || G[j].z >= cnode->highs[2])
          {
            if(verbose)
            {
              pprintf("Bad node found at lvl=%lu,lid=%lu.  Particle %d (%g,%g,%g) beyond boundaries R %g,%g, phi %g,%g, z %g,%g.\n",cnode->lvl_lid/MaxLID,cnode->lvl_lid%MaxLID,j,G[j].R,G[j].phi,G[j].z,cnode->lows[0],cnode->highs[0],cnode->lows[1],cnode->highs[1],cnode->lows[2],cnode->highs[2]); 
            }
            bbad++;
          }
        }
        //Check there's no other locals that should be here...
        for(j=0;j<NPart[GAS];j++)
        {
          //Don't look at the ones we already have...
          if(j>=mob && j<mob+count)
            continue;
          if(G[j].R>=cnode->lows[0] && G[j].R<cnode->highs[0] &&
              G[j].phi >=cnode->lows[1] && G[j].phi < cnode->highs[1] &&
              G[j].z >= cnode->lows[2] && G[j].z < cnode->highs[2])
          {
            bbad++;
            if(verbose)
            {
              pprintf("Bad node found at lvl=%lu,lid=%lu.  Particle %d (%g,%g,%g) within boundaries R %g,%g, phi %g,%g, z %g,%g but not in tree.\n",cnode->lvl_lid/MaxLID,cnode->lvl_lid%MaxLID,j,G[j].R,G[j].phi,G[j].z,cnode->lows[0],cnode->highs[0],cnode->lows[1],cnode->highs[1],cnode->lows[2],cnode->highs[2]); 
            }
          }
        }
        i++;
      }while(i<(-1*(cnode->owner)));
    }
    //Go down if I can..
    if(cnode->n_daughters)
    {
      cnode = Root+cnode->daughter;
    }
    else if(cnode->sister==-1)
    {
      break;
    }
    else
    {
      cnode = Root + cnode->sister;
    }
  }
  //Output the number of particles at each level
  j=lvls[0]+leaves_lvls[0];
  count=leaves_lvls[0];
  for(i=1;i<MaxLVL && lvls[i];i++)
  {
    //if(verbose)
    //{
    //  pprintf("Have %d particles at level %d (%d in nodes, %d leaves, %d leaf particles above here).\n",lvls[i],i,lvls[i]-leaves_lvls[i],leaves_lvls[i],count);
    //}
    if(lvls[i]+count!=j)
    {
      bbad++;
      if(verbose)
      {
        pprintf("At top level have %d particles, but at level %d total of preceding leaf particles and current particles is only %d.\n",i,j,count+lvls[i]);
      }
    }
    count+=leaves_lvls[i];
  }
  //  //It is local, so test it
  //  else
  //  {
  //    //printf("[%d] Checking node which is local.\n",Task);
  //    //Check if we have the right number of counts
  //    cnt=0;
  //    for(i=0;i<NPart[GAS];i++)
  //    {
  //      if(G[i].R >= cell->lows[0] && G[i].R <= cell->highs[0] && G[i].phi >= cell->lows[1] && G[i].phi <= cell->highs[1] && G[i].z >= cell->lows[2] && G[i].z <= cell->highs[2])
  //      {
  //        cnt++;
  //        //It's not in the nodes list...
  //        if(i<cell->mob || i>=cell->mob+cell->count)
  //        {
  //          if(verbose)
  //          {
  //            printf("[%d] Bad particle is at %g,%g,%g R=%g,phi=%g, id=%d,idx=%d, mob = %d cnt=%d.\n",Task,G[i].x,G[i].y,G[i].z,G[i].R,G[i].phi,G[i].id,i,cell->mob,cell->count);
  //          }
  //          //Where is the bad one actually located
  //          for(j=0;j<NNodes;j++)
  //          {
  //            if((Nodes[j].flag & NODE_LOCAL) && (Nodes[j].flag & NODE_LEAF) && i>=Nodes[j].mob && i<(Nodes[j].mob+Nodes[j].count))
  //            {
  //              printf("[%d] Bad particle is in leaf ([%g,%g],[%g,%g],[%g,%g]) offset=%d,count=%d,mother=%d,sister=%d,daughter=%d,trunkity=%d,locality=%d,leafity=%d,meta=%d,owner=%d,rmax=%g,com=%g,%g,%g,real_cnt=%d.\n",Task,Nodes[j].lows[0],Nodes[j].highs[0],Nodes[j].lows[1],Nodes[j].highs[1],Nodes[j].lows[2],Nodes[j].highs[2],j,Nodes[j].count,Nodes[j].mother,Nodes[j].sister,Nodes[j].daughter,Nodes[j].flag & NODE_TRUNK,Nodes[j].flag & NODE_LOCAL,Nodes[j].flag & NODE_LEAF,Nodes[j].flag & NODE_META,(Nodes[j].flag) & NODE_OWNER,Nodes[j].rmax,Nodes[j].com[0],Nodes[j].com[1],Nodes[j].com[2],cnt);
  //            }
  //          }
  //        }
  //      }
  //    }
  //    if(cnt!=cell->count)
  //    {
  //      bbad++;
  //      if(verbose)
  //      {
  //        printf("[%d] Node is from ([%g,%g],[%g,%g],[%g,%g]) offset=%d,count=%d,mother=%d,sister=%d,daughter=%d,trunkity=%d,locality=%d,leafity=%d,meta=%d,owner=%d,rmax=%g,com=%g,%g,%g,real_cnt=%d.\n",Task,cell->lows[0],cell->highs[0],cell->lows[1],cell->highs[1],cell->lows[2],cell->highs[2],(int) (cell-Nodes),cell->count,cell->mother,cell->sister,cell->daughter,cell->flag & NODE_TRUNK,cell->flag & NODE_LOCAL,cell->flag & NODE_LEAF,cell->flag & NODE_META,(cell->flag) & NODE_OWNER,cell->rmax,cell->com[0],cell->com[1],cell->com[2],cnt);
  //      }
  //    }
  //    //Check that all the ones that say they're in there, are really in there
  //    if(cell->flag & NODE_LEAF)
  //    {
  //      for(i=0;i<cell->count;i++)
  //      {
  //        j=cell->mob+i;
  //        //if(G[j].id==294820)
  //        //{
  //        //  printf("[%d] Node is from ([%g,%g],[%g,%g],[%g,%g]) offset=%d,count=%d,mother=%d,sister=%d,daughter=%d,trunkity=%d,locality=%d,leafity=%d,meta=%d,owner=%d,rmax=%g,com=%g,%g,%g,real_cnt=%d.\n",Task,cell->lows[0],cell->highs[0],cell->lows[1],cell->highs[1],cell->lows[2],cell->highs[2],(int) (cell-Nodes),cell->count,cell->mother,cell->sister,cell->daughter,cell->flag & NODE_TRUNK,cell->flag & NODE_LOCAL,cell->flag & NODE_LEAF,cell->flag & NODE_META,(cell->flag) & NODE_OWNER,cell->rmax,cell->com[0],cell->com[1],cell->com[2],cnt);
  //        //}
  //        if(G[j].R < cell->lows[0] || G[j].R > cell->highs[0] || G[j].phi < cell->lows[1] || G[j].phi > cell->highs[1] || G[j].z < cell->lows[2] || G[j].z > cell->highs[2])
  //        {
  //          bbad=1;
  //          if(verbose)
  //          {
  //            printf("[%d] Node is from ([%g,%g],[%g,%g],[%g,%g]) offset=%d,count=%d,mother=%d,sister=%d,daughter=%d,trunkity=%d,locality=%d,leafity=%d,meta=%d,owner=%d,rmax=%g,com=%g,%g,%g,real_cnt=%d.\n",Task,cell->lows[0],cell->highs[0],cell->lows[1],cell->highs[1],cell->lows[2],cell->highs[2],(int) (cell-Nodes),cell->count,cell->mother,cell->sister,cell->daughter,cell->flag & NODE_TRUNK,cell->flag & NODE_LOCAL,cell->flag & NODE_LEAF,cell->flag & NODE_META,(cell->flag) & NODE_OWNER,cell->rmax,cell->com[0],cell->com[1],cell->com[2],cnt);
  //            printf("[%d] Bad particle is at %g,%g,%g R=%g,phi=%g, id=%d,idx=%d.\n",Task,G[j].x,G[j].y,G[j].z,G[j].R,G[j].phi,G[j].id,j);
  //          }
  //        }
  //      }
  //    }
  //    //Now move down,across or out
  //    if(cell->flag & NODE_LEAF)
  //    {
  //      if(cell->sister==-1)
  //        break;
  //      cell=&Nodes[cell->sister];
  //    }
  //    else
  //    {
  //      cell=&Nodes[cell->daughter];
  //    }
  //  }
  //}
  return bbad;
}


