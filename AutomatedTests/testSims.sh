#!/bin/bash
#Maximum number of parallel processes
PP=4
#Polling frequency to check for finished jobs
WTIME=5
COUNT=0
#Function to allow simple parallel processing
wait_till_free(){
  COUNT=${PP}
  #echo "Polling running jobs"
  while (( COUNT >= PP )) ; do
    sleep ${WTIME}
    COUNT=`jobs|grep "Running"|wc -l`
    #echo "Polling found ${COUNT} jobs running.."
  done
}

echo "Compiling core code."
cd ..
make clean > /dev/null; make -j ${PP} "CLINE= -DCLONLY -DDEBUG=1 -DNO_OUTPUT -DGRAV=3 -DHYDRO=3 -DNGB_LIST_LEN=60" > /dev/null
cp ./mona AutomatedTests/mona_all_debug_1
make clean > /dev/null; make -j ${PP} "CLINE= -DCLONLY -DDEBUG=1 -DNO_OUTPUT -DDOUBLE -DGRAV=3 -DHYDRO=3 -DNGB_LIST_LEN=60" > /dev/null
cp ./mona AutomatedTests/mona_all_debug_1_double
make clean > /dev/null; make -j ${PP} "CLINE= -DCLONLY -DDEBUG=2 -DNO_OUTPUT -DDOUBLE -DGRAV=3 -DHYDRO=3 -DNGB_LIST_LEN=60" > /dev/null
cp ./mona AutomatedTests/mona_all_debug_2_double
#Test that the code compiles with different allowable combinations of options
echo "Sub-configuration compilation tests"
echo "Grav=2 Hydro=3"
make clean > /dev/null ; make -j ${PP} "CLINE= -DCLONLY -DDEBUG=2 -DNO_OUTPUT -DDOUBLE -DGRAV=2 -DHYDRO=3 -DNGB_LIST_LEN=60" > /dev/null
echo "Grav=1 Hydro=3"
make clean > /dev/null ; make -j ${PP} "CLINE= -DCLONLY -DDEBUG=2 -DNO_OUTPUT -DDOUBLE -DGRAV=1 -DHYDRO=3 -DNGB_LIST_LEN=60" > /dev/null
echo "Grav=0 Hydro=3"
make clean > /dev/null ; make -j ${PP} "CLINE= -DCLONLY -DDEBUG=2 -DNO_OUTPUT -DDOUBLE -DGRAV=0 -DHYDRO=3 -DNGB_LIST_LEN=60" > /dev/null
echo "Grav=3 Hydro=2"
make clean > /dev/null ; make -j ${PP} "CLINE= -DCLONLY -DDEBUG=2 -DNO_OUTPUT -DDOUBLE -DGRAV=3 -DHYDRO=2 -DNGB_LIST_LEN=60" > /dev/null
echo "Grav=2 Hydro=2"
make clean > /dev/null ; make -j ${PP} "CLINE= -DCLONLY -DDEBUG=2 -DNO_OUTPUT -DDOUBLE -DGRAV=2 -DHYDRO=2 -DNGB_LIST_LEN=60" > /dev/null
echo "Grav=1 Hydro=2"
make clean > /dev/null ; make -j ${PP} "CLINE= -DCLONLY -DDEBUG=2 -DNO_OUTPUT -DDOUBLE -DGRAV=1 -DHYDRO=2 -DNGB_LIST_LEN=60" > /dev/null
echo "Grav=0 Hydro=2"
make clean > /dev/null ; make -j ${PP} "CLINE= -DCLONLY -DDEBUG=2 -DNO_OUTPUT -DDOUBLE -DGRAV=0 -DHYDRO=2 -DNGB_LIST_LEN=60" > /dev/null
echo "Grav=3 Hydro=1"
make clean > /dev/null ; make -j ${PP} "CLINE= -DCLONLY -DDEBUG=2 -DNO_OUTPUT -DDOUBLE -DGRAV=3 -DHYDRO=1 -DNGB_LIST_LEN=60" > /dev/null
echo "Grav=2 Hydro=1"
make clean > /dev/null ; make -j ${PP} "CLINE= -DCLONLY -DDEBUG=2 -DNO_OUTPUT -DDOUBLE -DGRAV=2 -DHYDRO=1 -DNGB_LIST_LEN=60" > /dev/null
echo "Grav=1 Hydro=1"
make clean > /dev/null ; make -j ${PP} "CLINE= -DCLONLY -DDEBUG=2 -DNO_OUTPUT -DDOUBLE -DGRAV=1 -DHYDRO=1 -DNGB_LIST_LEN=60" > /dev/null
echo "Grav=0 Hydro=1"
make clean > /dev/null ; make -j ${PP} "CLINE= -DCLONLY -DDEBUG=2 -DNO_OUTPUT -DDOUBLE -DGRAV=0 -DHYDRO=1 -DNGB_LIST_LEN=60" > /dev/null
echo "Grav=3 Hydro=0"
make clean > /dev/null ; make -j ${PP} "CLINE= -DCLONLY -DDEBUG=2 -DNO_OUTPUT -DDOUBLE -DGRAV=3 -DHYDRO=0 -DNGB_LIST_LEN=60" > /dev/null
echo "Grav=2 Hydro=0"
make clean > /dev/null ; make -j ${PP} "CLINE= -DCLONLY -DDEBUG=2 -DNO_OUTPUT -DDOUBLE -DGRAV=2 -DHYDRO=0 -DNGB_LIST_LEN=60" > /dev/null
echo "Grav=1 Hydro=0"
make clean > /dev/null ; make -j ${PP} "CLINE= -DCLONLY -DDEBUG=2 -DNO_OUTPUT -DDOUBLE -DGRAV=1 -DHYDRO=0 -DNGB_LIST_LEN=60" > /dev/null
echo "Grav=0 Hydro=0"
make clean > /dev/null ; make -j ${PP} "CLINE= -DCLONLY -DDEBUG=2 -DNO_OUTPUT -DDOUBLE -DGRAV=0 -DHYDRO=0 -DNGB_LIST_LEN=60" > /dev/null

#Now do the serious tests
cd AutomatedTests

#Test that answer doesn't change with number of processors.  This just relies on the first timestep for now.
echo "Running processor scaling tests."
echo "Running test with 1 procssor."
mpirun -np 1 ./mona_all_debug_2_double small_disc_scale_test > scale_1.txt
echo "Running test with 2 procssors."
mpirun -np 2 ./mona_all_debug_2_double small_disc_scale_test > scale_2.txt
echo "Running test with 4 procssors."
mpirun -np 4 ./mona_all_debug_2_double small_disc_scale_test > scale_4.txt
echo "Running test with 8 procssors."
mpirun -np 8 ./mona_all_debug_2_double small_disc_scale_test > scale_8.txt
echo "Running test with 16 procssors."
mpirun -np 16 ./mona_all_debug_2_double small_disc_scale_test > scale_16.txt
echo "Running test with 32 procssors."
mpirun -np 32 ./mona_all_debug_2_double small_disc_scale_test > scale_32.txt
echo "Running test with 64 procssors."
mpirun -np 64 ./mona_all_debug_2_double small_disc_scale_test > scale_64.txt
echo "Running test with 128 procssors."
mpirun -np 128 ./mona_all_debug_2_double small_disc_scale_test > scale_128.txt
#Check that they have all the same number of density loops
grep "Loop" scale_1.txt > gold_loop
grep "timestep" scale_1.txt > gold_tstep
grep "Particle.*id\=" scale_1.txt |sed 's/.*Particle\:\ //g'  > gold_ids
for file in scale_*.txt ; do
  grep "Loop" ${file} > coal_loop
  COUNT=`diff gold_loop coal_loop|wc -l`
  if (( COUNT > 0 )) ; then
    echo "ERROR! Different number of density loops when using ${file%%scale_} processors."
    exit 1
  fi
  grep "timestep" ${file} > coal_tstep
  COUNT=`diff gold_loop coal_loop|wc -l`
  if (( COUNT > 0 )) ; then
    echo "ERROR! Time steps are different from serial when using ${file%%scale_} processors."
    exit 1
  fi
  grep "Particle.*id\=" ${file} |sed 's/.*Particle\:\ //g'  > coal_ids
  COUNT=`diff gold_ids coal_ids|wc -l`
  if (( COUNT > 0 )) ; then
    echo "ERROR! Test particle has different value for ${file%%scale_} processors."
    diff gold_ids coal_ids
    exit 1
  fi
done
echo "Code produces the same results up to 128 processors"


#Test improvement of force accuracy

echo "Running gravity test with theta=0.5"
mpirun -np 1 ./mona_all_debug_2_double small_disc_gtoll_0.5 > small_disc_gtoll_0.5_double.txt &
wait_till_free

echo "Running gravity test with theta=0.2"
mpirun -np 1 ./mona_all_debug_2_double small_disc_gtoll_0.2 > small_disc_gtoll_0.2_double.txt &
wait_till_free

echo "Running gravity test with theta=0.1"
mpirun -np 1 ./mona_all_debug_2_double small_disc_gtoll_0.1 > small_disc_gtoll_0.1_double.txt &
wait_till_free

echo "Running gravity test with theta=0.05"
mpirun -np 1 ./mona_all_debug_2_double small_disc_gtoll_0.05 > small_disc_gtoll_0.05_double.txt &
wait_till_free

echo "Running gravity test with theta=0.02"
mpirun -np 1 ./mona_all_debug_2_double small_disc_gtoll_0.02 > small_disc_gtoll_0.02_double.txt &
wait_till_free

#Momentum and Angular momentum (should improve with precision)

echo "Running single precision, direct sum gravity test."
mpirun -np 1 ./mona_all_debug_1 small_disc_gtoll_0 > small_disc_gtoll_0.txt &
wait_till_free

echo "Running double precision, direct sum gravity test."
mpirun -np 1 ./mona_all_debug_1_double small_disc_gtoll_0 > small_disc_gtoll_0_double.txt &
wait_till_free

#Energy (must improve with timestep size)

echo "Running long time step test"
mpirun -np 1 ./mona_all_debug_1_double small_disc_tstep_0 > small_disc_tstep_0_double.txt &
wait_till_free

echo "Running medium time step test"
mpirun -np 1 ./mona_all_debug_1_double small_disc_tstep_1 > small_disc_tstep_1_double.txt &
wait_till_free

echo "Running short time step test"
mpirun -np 1 ./mona_all_debug_1_double small_disc_tstep_2 > small_disc_tstep_2_double.txt &
wait_till_free

echo "Running tiny time step test"
mpirun -np 1 ./mona_all_debug_1_double small_disc_tstep_3 > small_disc_tstep_3_double.txt &
wait_till_free

echo "Running shortest time step test"
mpirun -np 1 ./mona_all_debug_1_double small_disc_tstep_4 > small_disc_tstep_4_double.txt &
wait_till_free



#Finally run python script to produce some plots of the resulting output
python testRuns.py
echo "If you got here without any error messages, you've passed all our tests!"


