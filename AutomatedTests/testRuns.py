#!/usr/bin/python
import subprocess
import os
import re
import argparse
import numpy as np
from matplotlib import pyplot as plt

def momentum_test(fnom,fnom_double,toll=1e-3,plot=True):
  """
  Tests if momentum is machine accurate by checking that the maximum change
  in momentum gets smaller when we increase the precision.
  """
  dat=open(fnom).read()
  matches=[x for x in re.findall("\[Global\] Momentum is (.*),(.*),(.*)\.",dat)]
  momi=[x for x in re.findall("Initial Momentum is (.*),(.*),(.*)\.",dat)]
  mom=np.array([np.array(x,dtype=float) for x in matches])#/np.array(momi,dtype=float)
  #mom=100*(np.array(matches,dtype=float)/np.array(momi,dtype=float))
  matches=[x for x in re.findall("\[Global\] Time is (.*)\.",dat)]
  t=np.array(matches,dtype=float)
  dat=open(fnom_double).read()
  matches=[x for x in re.findall("\[Global\] Momentum is (.*),(.*),(.*)\.",dat)]
  momi=[x for x in re.findall("Initial Momentum is (.*),(.*),(.*)\.",dat)]
  dmom=np.array([np.array(x,dtype=float) for x in matches])#/np.array(momi,dtype=float)
  #dmom=100*(np.array(matches,dtype=float)/np.array(momi,dtype=float))
  matches=[x for x in re.findall("\[Global\] Time is (.*)\.",dat)]
  dt=np.array(matches,dtype=float)
  #matches=[x[1:] for x in re.findall("(\n|$)Momentum is (.*),(.*),(.*)\.",dat)]
  #momi=[x[1:] for x in re.findall("(\n|$)Initial Momentum is (.*),(.*),(.*)\.",dat)]
  #dmom=100*(np.array(matches,dtype=float)/np.array(momi,dtype=float))
  #matches=[x[1:] for x in re.findall("(\n|$)Time is (.*)\.",dat)]
  #dt=np.array(matches,dtype=float)
  diff = np.median(np.abs(mom),0)
  ddiff = np.median(np.abs(dmom),0)
  if plot:
    plt.figure()
    #plt.gca().set_yscale('log')
    plt.xlabel("Time")
    plt.ylabel("log10(Momentum error)")
    plt.title("Conservation of momentum")
    plt.plot(t,np.log10(np.abs(mom[:,0])),label="Float_x")
    plt.plot(t,np.log10(np.abs(mom[:,1])),label="Float_y")
    plt.plot(t,np.log10(np.abs(mom[:,2])),label="Float_z")
    plt.plot(dt,np.log10(np.abs(dmom[:,0])),label="Double_x")
    plt.plot(dt,np.log10(np.abs(dmom[:,1])),label="Double_y")
    plt.plot(dt,np.log10(np.abs(dmom[:,2])),label="Double_z")
    plt.legend()
  if np.any(ddiff/diff>toll):
    print "ERROR: Momentum did not become better conserved."
  return (diff,ddiff)

def angular_momentum_test(fnom,fnom_double,toll=1e-3,plot=True):
  """
  Tests if angular momentum is machine accurate by checking that the maximum change
  in angular momentum gets smaller when we increase the precision.

  It should only be machine accurate when the tree is completely opened up.
  """
  dat=open(fnom).read()
  matches=[x for x in re.findall("\[Global\] Angular momentum is (.*),(.*),(.*)\.",dat)]
  momi=[x for x in re.findall("\[Global\] Initial Angular momentum is (.*),(.*),(.*)\.",dat)]
  mom=(np.array(matches,dtype=float)/np.array(momi,dtype=float))
  matches=[x for x in re.findall("\[Global\] Time is (.*)\.",dat)]
  t=np.array(matches,dtype=float)
  dat=open(fnom_double).read()
  matches=[x for x in re.findall("\[Global\] Angular momentum is (.*),(.*),(.*)\.",dat)]
  momi=[x for x in re.findall("\[Global\] Initial Angular momentum is (.*),(.*),(.*)\.",dat)]
  dmom=(np.array(matches,dtype=float)/np.array(momi,dtype=float))
  matches=[x for x in re.findall("\[Global\] Time is (.*)\.",dat)]
  dt=np.array(matches,dtype=float)
  diff = np.median(np.abs(mom),0)
  ddiff = np.median(np.abs(dmom),0)
  if plot:
    plt.figure()
    #plt.gca().set_yscale('log')
    plt.xlabel("Time")
    plt.ylabel("log10(AngularMomentum change)")
    plt.title("Conservation of angular momentum")
    plt.plot(t,np.log10(np.abs(mom[:,0])),label="Float_x")
    plt.plot(t,np.log10(np.abs(mom[:,1])),label="Float_y")
    plt.plot(t,np.log10(np.abs(mom[:,2])),label="Float_z")
    plt.plot(dt,np.log10(np.abs(dmom[:,0])),label="Double_x")
    plt.plot(dt,np.log10(np.abs(dmom[:,1])),label="Double_y")
    plt.plot(dt,np.log10(np.abs(dmom[:,2])),label="Double_z")
    plt.legend()
  if np.any(ddiff/diff>toll):
    print "ERROR: Angular momentum did not become more accurate with increased precision."
  return (diff,ddiff)

def energy_test(files,plot=True):
  """
  Tests if the energy conservation has improved between fnom and fnom_short (shorter time step).
  """
  tsteps=[]
  energies=[]
  times=[]
  areas=[]
  for f in files:
    dat = open(f).read()
    matches=re.findall("Read parameter MaxTimeStep as (.*)",dat)
    tsteps.append(float(matches[0]))
    matches=[x for x in re.findall("\[Global\] Energy is (.*)\.",dat)]
    #Get the initial energy
    ei=float([x for x in re.findall("\[Global\] Initial Energy is (.*)\.",dat)][0])
    e=(np.array(matches,dtype=float)/ei)
    energies.append(e)
    matches=[x for x in re.findall("\[Global\] Time is (.*)\.",dat)]
    t=np.array(matches,dtype=float)
    #Integrate under curves as metric of accuracy
    dt=np.diff(t)
    area = np.sum(dt*np.abs(e)[:-1])
    #Actually use area per unit time in case some sims didn't finish
    areas.append(area/np.max(t))
    times.append(t)
  if plot:
    plt.figure()
    plt.xlabel("Time")
    plt.ylabel("Log10(Energy change)")
    plt.title("Conservation of energy")
    for i in xrange(len(files)):
      plt.plot(times[i],np.log10(np.abs(energies[i])),label="tstep=%g"%tsteps[i])
    plt.legend()
  #Make arrays into an array
  areas=np.array(areas)
  tsteps=np.array(tsteps)
  #Sort them by timesteps
  o=np.argsort(tsteps)
  areas=areas[o]
  tsteps=tsteps[o]
  #Print our summaries
  for i in xrange(len(o)):
    print "With a time step of %g energy was conserved to a relative accuracy of roughly %g"%(tsteps[i],areas[i])
  #Is it sorted by increasing area already (it should be if precision increases)
  if np.any(np.diff(areas)<=0):
    print "ERROR: Energy did not become more accurately conserved when time step was shortened."
  return (tsteps,areas)


def grav_force_test(files,toll=1e-1,plot=True):
  """
  Tests if the accuracy of the self-gravitational force calculation improves when we open the tree.
  """
  thetas=[]
  forces=[]
  times=[]
  areas=[]
  for f in files:
    dat = open(f).read()
    matches=re.findall("Read parameter GravityOpeningTheta as (.*)",dat)
    thetas.append(float(matches[0]))
    matches=[x for x in re.findall("\[Global\] Averaged Square Error in force calculation is (.*)",dat)]
    e=np.array(matches,dtype=float)
    matches=[x for x in re.findall("\[Global\] Time is (.*)\.",dat)]
    t=np.array(matches,dtype=float)
    e=e[:len(t)]
    #Integrate under curves as metric of accuracy
    dt=np.diff(t)
    area = np.sum(dt*np.abs(e)[:-1])
    #Actually use area per unit time in case some sims didn't finish
    areas.append(area/np.max(t))
    times.append(t)
    forces.append(e)
  if plot:
    plt.figure()
    plt.xlabel("Time")
    plt.ylabel("Log10(Error)")
    plt.title("Accuracy of self-gravity")
    for i in xrange(len(files)):
      plt.plot(times[i],np.log10(np.abs(forces[i])),label="theta=%g"%thetas[i])
    plt.legend()
  #Make arrays into an array
  areas=np.array(areas)
  thetas=np.array(thetas)
  #Sort them by thetas
  o=np.argsort(thetas)
  areas=areas[o]
  thetas=thetas[o]
  #Print our summaries
  for i in xrange(len(o)):
    print "With a gravitational theta of %g the force is calculated with an error of roughly %g"%(thetas[i],areas[i])
  #Is it sorted by increasing area already (it should be if precision increases)
  if np.any(np.diff(areas)<=0):
    print "ERROR: Force did not become more accurate when theta was decreased."
  return (thetas,areas)



ploting=True

#####################################
#Momentum and Angular Momentum Tests#
#####################################

diff,ddiff = momentum_test("small_disc_gtoll_0.txt","small_disc_gtoll_0_double.txt",plot=ploting)
print "Using FLOAT precision momentum was conserved to a median accuracy of (%g,%g,%g)"%(diff[0],diff[1],diff[2])
print "Using DOUBLE precision momentum was conserved to a median accuracy of (%g,%g,%g)"%(ddiff[0],ddiff[1],ddiff[2])
plt.savefig("MomentumTest.png")
plt.close('all')

diff,ddiff = angular_momentum_test("small_disc_gtoll_0.txt","small_disc_gtoll_0_double.txt",plot=ploting)
print "Using FLOAT precision angular momentum was conserved to a median relative accuracy of (%g,%g,%g)"%(diff[0],diff[1],diff[2])
print "Using DOUBLE precision angular momentum was conserved to a median relative accuracy of (%g,%g,%g)"%(ddiff[0],ddiff[1],ddiff[2])
plt.savefig("AngularMomentumTest.png")
plt.close('all')

###########################
#Energy conservation tests#
###########################

tmp=energy_test(["small_disc_tstep_0_double.txt","small_disc_tstep_1_double.txt","small_disc_tstep_2_double.txt","small_disc_tstep_3_double.txt","small_disc_tstep_4_double.txt"],plot=ploting)
plt.savefig("EnergyTest.png")
plt.close('all')

######################
#Force accuracy tests#
######################

#Does the averaged squared error get better as we open up the tree more?
grav_force_test(["small_disc_gtoll_0.5_double.txt","small_disc_gtoll_0.2_double.txt","small_disc_gtoll_0.1_double.txt","small_disc_gtoll_0.05_double.txt","small_disc_gtoll_0.02_double.txt"],plot=ploting)
plt.savefig("GravityForceTest.png")
plt.close('all')

#What about momentum and angular momentum conservation?
diff,ddiff = momentum_test("small_disc_gtoll_0.5_double.txt","small_disc_gtoll_0.02_double.txt",plot=ploting,toll=.1)
print "Using theta=0.5 momentum was conserved to a median accuracy of %g,%g,%g"%(diff[0],diff[1],diff[2])
print "Using theta=0.02 momentum was conserved to a median accuracy of %g,%g,%g"%(ddiff[0],ddiff[1],ddiff[2])
plt.savefig("MomentumGravityTest.png")
plt.close('all')


diff,ddiff = angular_momentum_test("small_disc_gtoll_0.5_double.txt","small_disc_gtoll_0.02_double.txt",plot=ploting,toll=.1)
print "Using theta=0.5 angular momentum was conserved to a median relative accuracy of (%g,%g,%g)"%(diff[0],diff[1],diff[2])
print "Using theta=0.02 angular momentum was conserved to a median relative accuracy of (%g,%g,%g)"%(ddiff[0],ddiff[1],ddiff[2])
plt.savefig("AngularMomentumGravityTest.png")
plt.close('all')
