#!/bin/bash
VERSION=0.6.3
rm functions.h
touch functions.h
for file in *.c
do
  cproto -I /usr/include/openmpi/ ${file} >> functions.h
done
#Create variables.c
python ./auto_declarations.py
#Update the headers
for file in *.c *.h
do
  sed -i "s/\*        Version:  .*/\*        Version:  ${VERSION}/g" ${file}
done
