/*
 * =====================================================================================
 *
 *       Filename:  variables.h
 *
 *    Description:  Declaration of all global variables that are used anywhere in the
 *                  simulations.  Global constant should start with a capital letter 
 *                  and by written using camel case. Pre-processor declarations should be 
 *                  in ALL CAPS.  Functions should begin with lower case letters.
 *                  functions.c is constructed automatically from this file.
 *
 *                  Any include used by the code is also written here.
 *
 *        Version:  0.6.3
 *        Created:  12/07/13 14:30:27
 *
 *         Author:  Matthew D. Young, my304@ast.cam.ac.uk
 *        Company:  Institute of Astronomy, Cambridge, UK
 *        License:  GNU General Public License
 *      Copyright:  Copyright (c) 2014, Matthew D. Young
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#ifndef MPI_FILE_NULL
  #include <mpio.h>
#endif
#include <hdf5.h>
#include <limits.h>
#include <time.h>
#include <float.h>

/*
 * These can to a certain extent considered to be "Parameters"
 */

//Sometimes we use strings.  They shouldn't be longer than this.
#define MAX_STRING_LEN  1000
//Sometimes those strings contain paths.  This is the maximum path length
#define MAX_PATH_LEN     10000
//The parameters in the parameter file are read into an array.  It should be this size
#define MAX_NO_PARAMS   50
//The maximum length in characters of a printed string.  Used for fancy printing
#define MAX_MSG_SIZE 10000

//What type to use to store floating point numbers
//not used when it is obvious that one type is preferred
#ifdef DOUBLE
#define FLOAT double
#define MFLOAT MPI_DOUBLE
#define HFLOAT H5T_NATIVE_DOUBLE
#else
#define FLOAT float
#define MFLOAT MPI_FLOAT
#define HFLOAT H5T_NATIVE_FLOAT
#endif

//Set normalisation constant for number of dims
#if NDIM==3
#define NORM_CONST 0.3183098861837907
#define H_TO_THE_D h*h*h
#define H_TO_THE_DMINUS1 h*h
#define H_TO_THE_DPLUS1 h*h*h*h
#elif NDIM==2
#define NORM_CONST 0.4547284088339867
#define H_TO_THE_D h*h
#define H_TO_THE_DMINUS1 h
#define H_TO_THE_DPLUS1 h*h*h
#else
#define NORM_CONST 0.6666666666666666
#define H_TO_THE_D h
#define H_TO_THE_DMINUS1 1.0
#define H_TO_THE_DPLUS1 h*h
#endif

//Define the type used for storing unique tree references.  Should be unsigned
#define TREE_REF unsigned long
//Define the type used to make references to particle uniquely across all processors.  Should be *signed*
#define PCLE_REF int


//Constants
#define PI M_PI
#define PI2 2*PI

//Tags for communication
#define TAG_SEND 0
#define TAG_RETURN 1
#define TAG_TERM 9

//The kernel
#define KER_SUPPORT 2

//Define error codes
//Sanity checks are performed at different points in the code.  If one of them fails,
//we have made a huge mistake and there is likely a bug and this error is raised
#define ERROR_SANITY 908
//Something is wrong with the parameters or initial conditions given
#define ERROR_SETUP 1
//There is an error to do with insufficient memory
#define ERROR_MEM 2
//There was an IO error of some variety
#define ERROR_IO 3
/*
 * Code names for different things
 */

/*
 * * * * *
WARNING!!!
 * * * * *
These are used for array indexing 
(e.g. the NPart variable).  It doesn't matter
what order they appear in as the array will
always be accessed via the symbolic name
(e.g. NPart[GAS]), but THEY MUST NOT BE ASSIGNED
SPECIAL NUMERICAL VALUES (e.g. GAS=10232).  Yes
I know this is "bad practice" but it avoids a lot
of extra crap.  This is also why we need the 
MAX_NO_PART_TYPES constant.
*/
#define MAX_NO_PART_TYPES 2
enum part_type {
  GAS,
  SINK,
};

enum part_prop {
  IO_ID,
  IO_X,
  IO_Y,
  IO_Z,
  IO_VEL_X,
  IO_VEL_Y,
  IO_VEL_Z,
  IO_ACC_X,
  IO_ACC_Y,
  IO_ACC_Z,
  IO_HSML,
  IO_DEN,
  IO_UINT,
  IO_MASS,
};

//Symbolic names for types
enum var_type {
  TYPE_FLOAT,
  TYPE_INT,
  TYPE_STRING
};

/*
 * These are special data structures that allow parameter and input/output options
 * to be simply specified in setup.c  Do not fuck with these unless you know better
 * than I do.
 */

extern struct parameter_definitions {
  char key[MAX_STRING_LEN];
  void* addr;
  enum var_type type;
} ParameterDefinitions[];

extern int ParameterDefinitions_num;

extern struct io_props {
  enum part_prop number;
  char name[MAX_STRING_LEN];
  enum var_type type;
  hid_t hdf_type;
} IO_properties[];

extern int IO_properties_num;

extern struct io_types {
  enum part_type number;
  char name[MAX_STRING_LEN];
} IO_particles[];

extern int IO_particles_num;

extern struct output {
  enum part_type type;
  enum part_prop prop;
} Output[];

extern int Output_num;

/*
 * Global variables
 */

//The tree can only be safely built to a given depth, this is it
extern int MaxLVL;  
//The largest particle ID it is possible to reference
extern PCLE_REF MaxID;
//The maximum Local ID that a tree particle can have (at MaxLVL).  Used to
//extract LVL and LID from one TREE_REF variable
extern TREE_REF MaxLID;
//The maximum number of gas particles on any given processor.  Used to 
//extract Task and gas index pairs from a single PCLE_REF variable
extern int MaxGas;
extern int Task; //The MPI rank of the processor
extern int NTask; //The number of MPI processors
extern int PTask; //Number such that 2**PTask>=NTask
extern int NShadows; //The number of shadow domains per MPI processor
extern void *Buffer; //Pointer to memory used for comm/construction
extern struct gas *GBuffer; //For filling the buffer with gas particles
extern TREE_REF *TRBuffer;  //For filling the buffer with tree references
extern int *IBuffer;        //For filling the buffer with integers
extern FLOAT *FBuffer;      //For filling the buffer with FLOATs (as defined above)
extern double *DomainWalls;  //The domain boundaries are kept here
extern double SimBox[3][2];      //The box which bounds the simulation
extern int NextFreeNode;    //The index of the next free node
extern int *NGas;           //Array containing the number of gas particles on all processors.
extern PCLE_REF *Ngbs;      //Array which points to where neighbours are stored
extern int N_Export;         //How many particles to export
extern int *Export;         //Boolean array indicating if export has happened
extern int *N_Send_Local;    //How many should be exported to where?
//The number of particles of each type stored on this processor
//MUST BE ACCESSED VIA SYMBOLIC NAME (e.g. GAS)
extern int NPart[MAX_NO_PART_TYPES]; 
//The global number of each particles across all processors
extern int NPartWorld[MAX_NO_PART_TYPES];
extern MPI_Group World;  //A copy of the global communicator.  Useful in construction new communicators.
extern int MaskSize;    //How many chars are needed to construct the ownership bitmask
extern char *NullMask;  //Bit mask which contains all zeros
extern double AccreteMom[3]; //How much momentum is lost during accretion?
extern double AccreteAngMom[3]; //How much angular momentum is lost during accretion?
extern double AccreteEnergy;  //How much energy is lost during accretion?
extern int NgbBufferSizeInts; //How many ints can we fit in the ngb buffer?
extern int BufferSizeBytes;   //How many chars can we fit in the buffer?
extern int BufferSizeGas;     //How many gas particles?
extern int BufferSizeInts;    //How many ints?
extern int BufferSizeFloats;  //How many custom definition floats?
extern int BufferSizeTRs;     //How many tree references?
extern int BufferSizeDen;     //How many density export objects can we fit in the buffer?
extern int BufferSizeHydro;    //How many hydro export objects in the buffer?
extern int BufferSizeVisc;    //How many viscosity export objects in the buffer?
extern int BufferSizeGravSlow;    //How many slow gravity export objects in the buffer?
extern int BufferSizeGrav;    //How many gravity export objects in the buffer?
extern int BufferSizeCoord;    //How many coordinate only export objects in the buffer?
extern int HalfBufferSizeBytes;  //Half the buffer, just like it says
extern int TreeSize;         //How many nodes fit in the tree?
extern int NextID; //If we create a new particle, what ID should it get?
extern double PcleMass; //Average particle mass
extern double COM[3];   //System centre of mass
extern double Mom[3];   //System total momentum
extern double AngMom[3]; //System total angular momentum
extern double Energy;  //System total energy
extern double Time;   //The global time
extern double Dt;     //The global time step
//Communication variables
extern int NTerm;     //Number of processors that have finished
//Tree stuff
extern int LeafCtr;   //Number of entries in the leaf table
extern int *LvlOffset;  //An array which stores the offset at which each lvl starts in the leaf table
extern int TreeDepth;  //What is the deepest level in the actual tree (MaxLVL is the maximum possible)

/*
 * Parameters. Stored in a structure for easy communication
 */

extern struct parameters
{
  char init_cond[MAX_PATH_LEN];  //The path to the initial conditions file
  double particle_excess_fraction;     //If there are N particles in the simulation
                                //space is reserved for N*ParticleExcessFraction/NTask
                                      //particles on each processor
  double tree_fraction;                //The code allocates N*tree_fraction nodes into
                                      //memory.
  double eta;
  double toll;                         //How accurate do we need to be?
  double sink_soft;
  double gas_soft;
  double gamma;
  double G;
  double beta_cool;
  double stop_time;
  double courant;
  double max_tstep;
  double alpha_sph;
  double grav_opening;
  double accretion_radius;
  double escape_radius;
  int buffer_size;                    //Number of MiB to allocate for the buffer
  int ngb_buffer_size;                //MiB for neighbour searches
  int pcles_per_node;                 //Stop splitting when we have this many pcles.
  int nshadows;                       //How many shadow domains does each processor get?
  int output_n_tsteps;                //Write a snapshot after this many time steps
  double output_delta;                //Snapshots are separated by this much sim time. 
  double dim_grav_soft;               //In reduce dimensions, how far do we offset the matter distribution?
}
Params;

/*
 * The following define the different particle types and what properties
 * they should have.  If you wish to change this, you should also change the
 * functions that store and write these particles, which are all contained
 * at the top of io.c
 */





//The structure where gas particles are stored
extern struct gas {
  int id;
  int nngbs;
  int nops;
  PCLE_REF ngbs[NGB_LIST_LEN];
  FLOAT x;
  FLOAT y;
  FLOAT z;
  FLOAT R;
  FLOAT phi;
  FLOAT vx;
  FLOAT vy;
  FLOAT vz;
  FLOAT vpredx;
  FLOAT vpredy;
  FLOAT vpredz;
  FLOAT grav_ax;
  FLOAT grav_ay;
  FLOAT grav_az;
  FLOAT ax;
  FLOAT ay;
  FLOAT az;
  FLOAT divv;
  FLOAT hsml;
  FLOAT density;
  FLOAT dhsml;
  FLOAT K;
  FLOAT Kpred;
  FLOAT pressure;
  FLOAT dK;
  FLOAT vsig;
  FLOAT zeta;
  FLOAT pot;
  FLOAT cost;
#ifdef ACCURATE_DIVV
  FLOAT chi[6];
  FLOAT matD[9];
#endif
}
*G;

extern struct shadow {
  int owner_rank_in_comm;
  struct gas *G;
  MPI_Comm comm;
  MPI_Group group;
}
*Shadows;

//The structure where sink particles are stored
extern struct sink {
  int id;
  FLOAT x;
  FLOAT y;
  FLOAT z;
  FLOAT m;
  FLOAT vx;
  FLOAT vy;
  FLOAT vz;
  FLOAT ax;
  FLOAT ay;
  FLOAT az;
  FLOAT pot;
}
*S;

//The tree
struct mnode {
  PCLE_REF mob;
  int count;
};

extern struct node Null_node;
extern struct node {
  FLOAT lows[3];
  FLOAT highs[3];
  FLOAT com[3];
  //FLOAT quad[3][3];
  //FLOAT coeff_0;
  //FLOAT coeff_1[3];
  //FLOAT coeff_2[3][3];
  //FLOAT coeff_3[3][3][3];
  FLOAT rmax;
  FLOAT hmax;
  int count;
  int mob;
  int owner;
  int mother;
  int sister;
  int daughter;
  int n_daughters;
  TREE_REF lvl_lid;
  //These only used on multiply owned nodes
  char *owners;
  //And this only on multiply owned leaves
  struct mnode *extra;
  //Redundant, moving towards removing
  int flag;
}
*Root,
*Trunk,
*UpperTrunk,
*Branch,
*Nodes;



//For communicating about density
extern union density_comm {
  struct density_send {
    FLOAT x;
    FLOAT y;
    FLOAT z;
    FLOAT hsml;
    int what;
    int where;
    int from;
    int nnodes;
    PCLE_REF nodes[NGB_LIST_LEN];
  } in;
  struct density_recv {
    FLOAT rho;
    FLOAT dhsml;
    FLOAT zeta;
    int what;
    int where;
    int from;
    int nops;
  } out;
}
*DenSend,
*DenRecv;

//For communicating about hydro
extern union hydro_comm {
  struct hydro_send {
    FLOAT x;
    FLOAT y;
    FLOAT z;
    FLOAT vx;
    FLOAT vy;
    FLOAT vz;
    FLOAT hsml;
    FLOAT density;
    FLOAT dhsml;
    FLOAT pressure;
    FLOAT zeta;
    int what;
    int where;
    int from;
    int nnodes;
    int nodes[NGB_LIST_LEN];
  } in;
  struct hydro_recv {
    FLOAT ax;
    FLOAT ay;
    FLOAT az;
    FLOAT divv;
#ifdef ACCURATE_DIVV
    FLOAT chi[6];
    FLOAT matD[9];
#endif
    int what;
    int where;
    int from;
    int nops;
  } out;
}
*HydroSend,
*HydroRecv,
*HydroBuffer;

//For comm in the viscosity loop
extern union visc_comm {
  struct visc_send {
    FLOAT x;
    FLOAT y;
    FLOAT z;
    FLOAT vx;
    FLOAT vy;
    FLOAT vz;
    FLOAT hsml;
    FLOAT density;
    FLOAT pressure;
    int what;
    int where;
    int from;
    int nnodes;
    int nodes[NGB_LIST_LEN];
  } in;
  struct visc_recv {
    FLOAT ax;
    FLOAT ay;
    FLOAT az;
    FLOAT dK;
    FLOAT max_vsig;
    int what;
    int where;
    int from;
    int nops;
  } out;
}
*ViscSend,
*ViscRecv;

extern union grav_comm {
  struct grav_send {
    FLOAT x;
    FLOAT y;
    FLOAT z;
    FLOAT h;
    int from;
    int where;
    int what;
    int store;
    int nodes[NGB_LIST_LEN];
    int nnodes;
  } in;
  struct grav_recv {
    FLOAT ax;
    FLOAT ay;
    FLOAT az;
    FLOAT pot;
    int from;
    int where;
    int what;
    int store;
    int nops;
  } out;
}
*GravSend,
*GravRecv;

//This is just to send coefficients, so a bit simpler...
//extern struct grav_coeff_comm {
//  FLOAT c0;
//  FLOAT c1[3];
//  FLOAT c2[3][3];
//  FLOAT c3[3][3][3];
//  int what;
//  int where;
//  int from;
//}
//*GravCoeffSend,
//*GravCoeffRecv;

extern struct slow_grav_comm {
  FLOAT x;
  FLOAT y;
  FLOAT z;
  FLOAT h;
  FLOAT ax;
  FLOAT ay;
  FLOAT az;
  FLOAT pot;
}
*GravSlowSend,
*GravSlowRecv;

extern struct coords {
  FLOAT x;
  FLOAT y;
  FLOAT z;
  FLOAT h;
}
*Coords;

//Store time spent in different parts of code
extern struct timings {
  //Time spent writing snapshots
  double io;
  //Time spent on domain decomposition and associated activities
  double domain;
  //Time spent on doing things for the shadow domains
  double shadow;
  //Total time spent for an entire timestep
  double total;
  //Time spent on tree build
  double tree_build;
  //Time spent in density loop
  double density;
  //Time spent in gravity loop
  double gravity;
  //Time spent in hydro loop
  double hydro;
  //Time spent in av loop
  double av;
  //The rest are low level quantities used to measure work-load imbalance
  //How much time do we spend calling work_function with mode 0 in the comm routine?
  double work_local;
  //How much time do we spend waiting for other to finish local work in comm routine?
  double wait_local;
  //How much time do we spend calling work_function with mode 1 in the comm routine?
  double work_remote;
  //How much time do we spend waiting for others to finish their exported work in the comm routine?
  double wait_remote;
}
NullTimings,
Timings;
