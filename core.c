/*
 * =====================================================================================
 *
 *       Filename:  core.c
 *
 *    Description:  The core of the simulation.  Everything is called from here.  In
 *                  particular the time stepping is defined here.
 *
 *        Version:  0.6.3
 *        Created:  12/07/13 14:17:11
 *
 *         Author:  Matthew D. Young, my304@ast.cam.ac.uk
 *        Company:  Institute of Astronomy, Cambridge, UK
 *        License:  GNU General Public License
 *      Copyright:  Copyright (c) 2014, Matthew D. Young
 *
 * =====================================================================================
 */

#include "variables.h"
#include "functions.h"

/* 
 * ===  FUNCTION  ======================================================================
 *  Description:  Main function that calls all others.
 * =====================================================================================
 */
int main(int argc,char **argv)
{
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &Task);
  MPI_Comm_size(MPI_COMM_WORLD, &NTask);
  int i,j;
  int cnt=0;
  int output_no=0;
  double last_output=0;
  double tstart;
#if DEBUG>=1
  double mom_start[3]={0};
  double amom_start[3]={0};
  double energy_start=0;
#endif
  /*
   * Run-time checks and warnings
   */

  if(NTask<=1)
  {
    gpprintf("You're running an MPI program with one processor.  Are you mad?!\n");
  }
  
  if(argc <2)
  {
    gpprintf("Parameter file missing.  Usage is: mpirun -np # ./mona <parameter_file>\n");
    kill(ERROR_SETUP);
  }

  /*
   * Must be run before anything else
   */

  //Populate the size of array variables.  Always run first.
  startup();

  /*
   * Read in from file
   */


  gpprintf("Reading parameter file \"%s\".\n",argv[1]);
  //Read in the parameter file
  read_parameters(argv[1]);
  //Allocate space to store particles
  alloc_buffers();
  //Actually read the particles
  read_ic();
  gpprintf("\n");
  gpprintf("Read %d Gas particles and %d Sinks.\n",NPartWorld[GAS],NPartWorld[SINK]);
  //Allocate space for the tree and anything else we might need
  alloc_misc();
  alloc_tree();
  //Calculate limitations on particle number and level due to size of reference types
  ref_limits();
  gpprintf("\n");
  gpprintf("Allocated memory (per processor):\n");
  gpprintf("%g MiB allocated for buffers (ngb and comm).\n",(NgbBufferSizeInts*sizeof(int)+BufferSizeBytes)/(1024.0*1024.0));
  gpprintf("%g MiB allocated for tree.\n",TreeSize*sizeof(struct node)/(1024.0*1024.0));
  gpprintf("%g MiB allocated for gas particles.\n",MaxGas * sizeof(struct gas)/(1024.0*1024.0));
  gpprintf("%g MiB allocated for sink particles.\n",NPartWorld[SINK]*sizeof(struct sink)/(1024.0*1024.0));
  gpprintf("%g MiB Total.\n",((NgbBufferSizeInts*sizeof(int))+BufferSizeBytes+(TreeSize*sizeof(struct node))+(MaxGas * sizeof(struct gas))+(NPartWorld[SINK]*sizeof(struct sink)))/(1024.0*1024.0));

  gpprintf("\n");
  gpprintf("Which is enough memory for:\n");
  gpprintf("%d Gas particles per processor (max).\n",MaxGas);
  gpprintf("%d Sink particles per processor.\n",NPartWorld[SINK]);
  gpprintf("%d Gas particles in each processor's buffer.\n",BufferSizeGas);
  gpprintf("%d Density exports in each processor's buffer.\n",BufferSizeDen);
  gpprintf("%d Hydro exports in each processor's buffer.\n",BufferSizeHydro);
  gpprintf("%d Particle indices in neighbour finder.\n",NgbBufferSizeInts);
  gpprintf("%d Nodes in tree.\n",TreeSize);

  gpprintf("\n");
  gpprintf("Reference types (TREE_REF and PCLE_REF) have:\n");
  gpprintf("%lu bits in TREE_REF, which allows for trees of depth %d.\n",sizeof(TREE_REF)*CHAR_BIT,MaxLVL);
  gpprintf("%lu bits in PCLE_REF, which allows for (Total number of gas particles)*(Particle excess fraction) <= %d.\n",sizeof(PCLE_REF)*CHAR_BIT,MaxID);
  gpprintf("\n");

  /*
   * Initialise variables
   */

  initialise_variables();

  /*
   * Calculate initial accelerations
   */

  //Before the first timestep, need to do these things so we can calculate K
  //and dt 
  position_dependent_accel();
  //for(i=0;i<NPart[GAS];i++)
  //  if(G[i].id<50)
  //    pprintf("Particle[%d] R=%g,phi=%g,z=%g,id=%d\n",i,G[i].R,G[i].phi,G[i].z,G[i].id);

#if HYDRO>=1
  //Convert internal energy to "entropy" now we know the density
  //Make sure we do it for the shadow domains too (such a simple
  //calculation it's not worth doing locally then broadcasting)
  for(j=0;j<NTask;j++)
  {
    if(Shadows[j].G)
    {
      for(i=0;i<NGas[j];i++)
      {
        Shadows[j].G[i].K *= (Params.gamma-1)/pow(Shadows[j].G[i].density,Params.gamma-1);
        Shadows[j].G[i].Kpred = Shadows[j].G[i].K;
      }
    }
  }
#endif

  velocity_dependent_accel();
  gather_shadows_for_kick();
 
  //The accelerations and dK are now accurate on locally owned
  //and zero in all shadow domains
 
  /*
   * Main loop
   */
  while(Time<Params.stop_time)
  {
    tstart=MPI_Wtime();
#ifndef NO_OUTPUT
    //Should we write an output file now?
    //gpprintf("Output every %d tseps or after %g time, time diff is %g and %g.\n",Params.output_n_tsteps,Params.output_delta,Time,last_output);
    if((Params.output_n_tsteps && !(cnt%Params.output_n_tsteps)) ||
       (Params.output_delta && (Time-last_output)>=Params.output_delta))
    {
      write_snap(output_no);
      last_output = Time;
      output_no++;
    }
#endif
    Timings.io+= MPI_Wtime()-tstart;
    //Calculate timestep
    compute_timestep();
#if DEBUG>=2
    MPI_Barrier(MPI_COMM_WORLD);
    for(j=0;j<50;j++)
    {
      i=0;
      while(G[i].id!=j && i<NPart[GAS])
        i++;
      if(!Task || i!=NPart[GAS])
      {
        //If we have it, send it to 0
        if(i!=NPart[GAS])
        {
          MPI_Send(G+i,sizeof(struct gas),MPI_BYTE,0,j,MPI_COMM_WORLD);
        }
        //If we're 0, receive it
        if(!Task)
        {
          MPI_Recv(Buffer,sizeof(struct gas),MPI_BYTE,MPI_ANY_SOURCE,j,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
        }
        //And 0 then prints it
        gpprintf("Particle: id=%d,x=%g,y=%g,z=%g,R=%g,phi=%g,h=%g,rho=%g,u=%g,vx=%g,vy=%g,vz=%g,ax=%g,ay=%g,az=%g,divv=%g,zeta=%g\n",GBuffer->id,GBuffer->x,GBuffer->y,GBuffer->z,GBuffer->R,GBuffer->phi,GBuffer->hsml,GBuffer->density,GBuffer->K,GBuffer->vx,GBuffer->vy,GBuffer->vz,GBuffer->ax,GBuffer->ay,GBuffer->az,GBuffer->divv,GBuffer->zeta);
      }
      MPI_Barrier(MPI_COMM_WORLD);
    }
#endif

 
    gpprintf("\n");
    gpprintf("****************************************************************\n");
    gpprintf("**** Starting timestep %d at time %g with timestep %g *****\n",cnt,Time,Dt);
    gpprintf("****************************************************************\n");
    gpprintf("\n");

    //Print out timings and reset counters
    Timings.total = MPI_Wtime()-Timings.total;
    gpprintf("total=%g, io=%g, domain=%g, shadow=%g, tree_build=%g, density=%g, gravity=%g, hydro=%g, av=%g, work_local/remote=%g/%g, wait_local/remote=%g/%g\n",Timings.total,Timings.io,Timings.domain,Timings.shadow,Timings.tree_build,Timings.density,Timings.gravity,Timings.hydro,Timings.av,Timings.work_local,Timings.work_remote,Timings.wait_local,Timings.wait_remote);
    //Reset timings to zero
    Timings = NullTimings;
    Timings.total = MPI_Wtime();

    /*
     * First Kick
     *
     * We've just been kicked (or undergone special start up calculations)
     * so there's no need to redo anything which depends on position alone 
     * (density, gravity,etc)...
     */

    gpprintf("**************\n");
    gpprintf("*****Kick*****\n");
    gpprintf("**************\n");

    //Use the accelerations from the previous kick (or the pre-calculation if cnt==0)
    for(i=0;i<NPart[GAS];i++)
    {
      G[i].ax = G[i].grav_ax;
      G[i].ay = G[i].grav_ay;
      G[i].az = G[i].grav_az;
    }

    //Calculate the velocity dependent components
    velocity_dependent_accel();

    tstart=MPI_Wtime();
    gather_shadows_for_kick();
    Timings.shadow += MPI_Wtime()-tstart;

#if DEBUG>=1
    //Calculate the conserved quantities before anything
    calc_conserved();
    //Store the starting qunatities
    if(cnt==0)
    {
      mom_start[0]=Mom[0];
      mom_start[1]=Mom[1];
      mom_start[2]=Mom[2];
      amom_start[0]=AngMom[0];
      amom_start[1]=AngMom[1];
      amom_start[2]=AngMom[2];
      energy_start = Energy;
      gpprintf("Initial Energy is %g.\n",energy_start);
      gpprintf("Initial Momentum is %g,%g,%g.\n",Mom[0],Mom[1],Mom[2]);
      gpprintf("Initial Angular momentum is %g,%g,%g.\n",AngMom[0],AngMom[1],AngMom[2]);
    }
    else
    {
      gpprintf("Time is %g.\n",Time);
      gpprintf("Energy is %g.\n",Energy-energy_start);
      gpprintf("Momentum is %g,%g,%g.\n",Mom[0]-mom_start[0],Mom[1]-mom_start[1],Mom[2]-mom_start[2]);
      gpprintf("Angular momentum is %g,%g,%g.\n",AngMom[0]-amom_start[0],AngMom[1]-amom_start[1],AngMom[2]-amom_start[2]);
    }
#endif

    kick_particles(Dt/2);

    predict_quantities(Dt/2);

    //Don't need to update kicked quantities in shadow domains
    //since we're just going to drift then do a full re-projection
 
    /*
     * Drift
     *
     * Not a lot happens here, but the fact that we've moved the
     * particles means we have to do some re-balancing between
     * processors, re-calculate any derived coordinates 
     * (e.g., cylindrical R,phi).
     */


    gpprintf("**************\n");
    gpprintf("*****Drift****\n");
    gpprintf("**************\n");

    drift_particles(Dt);

    //Some particles will have drifted out of the domain, move them to
    //the next processor.
    tstart=MPI_Wtime();
    gpprintf("Rebalancing domains.\n");
    calc_derived_coordinates();
#if defined ACCRETE || defined ESCAPE
    add_remove_particles();
    calc_derived_coordinates();
#endif
    drift_domain_walls();
    exchange_particles();
    Timings.domain += MPI_Wtime()-tstart;

    /*
     * Main Kick
     *
     * This kick is in some sense the "main" one as it usually
     * consumes the most time.  This is because the particle coordinates
     * have just changed so coordinate dependent quantities (such as 
     * gravity and density) have to be re-calculated here.
     */

    gpprintf("**************\n");
    gpprintf("*****Kick*****\n");
    gpprintf("**************\n");
    position_dependent_accel();
    velocity_dependent_accel();
    tstart=MPI_Wtime();
    gather_shadows_for_kick();
    Timings.shadow += MPI_Wtime()-tstart;
    kick_particles(Dt/2);
    predict_quantities(0);
    tstart=MPI_Wtime();
    cast_kicked_shadows();
    Timings.shadow += MPI_Wtime()-tstart;
    //Update kicked quantities in shadow domains
    Time += Dt;
    cnt++;
  }
  //Clean up and exit
  pprintf("IT IS DONE.\n");
  freedom();
  MPI_Finalize();
  return 0;
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Description:  Calculates the contribution to the acceleration of all particles which
 *                depend only on position.  That is, density and gravity.  The tree is
 *                also built here, as this should only be run when the positions of 
 *                particles have changed.
 * =====================================================================================
 */
void position_dependent_accel(void)
{
#if DEBUG>=1
  int j;
#endif
  int i;
  double tstart;
  //Reset the accelerations to zero
  for(i=0;i<NPart[GAS];i++)
  {
    G[i].nngbs=0;
    G[i].ax=0;
    G[i].ay=0;
    G[i].az=0;
    G[i].pot=0;
  }
  for(i=0;i<NPart[SINK];i++)
  {
    S[i].ax=0;
    S[i].ay=0;
    S[i].az=0;
    S[i].pot=0;
  }
  //Build the tree
  gpprintf("Building tree.\n");
  tstart=MPI_Wtime();
  build_tree();
  Timings.tree_build += MPI_Wtime()-tstart;
  //Build isn't really finished until shadows are cast
  tstart=MPI_Wtime();
  cast_shadows();
  Timings.shadow += MPI_Wtime()-tstart;

#if DEBUG>=2
  //Test the tree build
  if(test_tree_build(1))
  {
    pprintf("Tree build failed!\n");
    kill(ERROR_SANITY);
  }

  if(test_ngb_finder(NGas[0]/10,G[0].hsml,1))
  {
    pprintf("Neighbour finder failed.\n");
    kill(ERROR_SANITY);
  }
#endif

  gpprintf("Tree build done.\n");
  
#if HYDRO>=1
  //Calculate the density using the SPH equations
  gpprintf("Calculating density.\n");
  tstart=MPI_Wtime();
  density();
  Timings.density += MPI_Wtime()-tstart;
  //Cast out the updated values
  tstart=MPI_Wtime();
  cast_density_shadows();
  Timings.shadow += MPI_Wtime()-tstart;
#if DEBUG>=1
  //Check the Shadows don't have any zeros
  for(j=0;j<NTask;j++)
  {
    if(Shadows[j].G)
    {
      for(i=0;i<NGas[j];i++)
      {
        if(Shadows[j].G[i].density==0 || Shadows[j].G[i].hsml==0)
        {
          pprintf("Shadows[%d].G[%d].density/hsml = %g/%g.\n",j,i,Shadows[j].G[i].density,Shadows[j].G[i].hsml);
          kill(ERROR_SANITY);
        }
      }
    }
  }
#endif
  gpprintf("Density loop done.\n");
#endif
  //Calculate the gravitational acceleration
  gpprintf("Starting gravity calculation.\n");
  tstart = MPI_Wtime();
  gravity();
  Timings.gravity += MPI_Wtime()-tstart;
  gpprintf("Gravity calculation complete.\n");
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Description:  Calculates the contribution to all particles acceleration that
 *                depend on more than just the position.  In practice this is the
 *                usually just the hydrodynamics.
 * =====================================================================================
 */
void velocity_dependent_accel(void)
{
#if DEBUG>=1
  int i;
#endif
#if DEBUG>=2
  double new[3];
  int j;
#endif
  double tstart;
#if HYDRO>=1
  //Should have everything needed to calculate the pressure
  calc_pressure();
  //cast here if necessary
  tstart=MPI_Wtime();
  cast_pressure_shadows();
  Timings.shadow += MPI_Wtime()-tstart;
#endif

#if HYDRO>=2
  //This stuff depends on kicked variables (velocity, entropy) 
  //so we need to recalculate it
  //Now calculate the hydro accel
  tstart=MPI_Wtime();
  gpprintf("Starting hydro loop.\n"); 
  //Calculate the inviscid hydrodynamic forces
  hydro();
  Timings.hydro += MPI_Wtime()-tstart;

  tstart=MPI_Wtime();
  cast_hydro_shadows();
  Timings.shadow += MPI_Wtime()-tstart;

#if DEBUG>=2
  new[0]=new[1]=new[2]=0;
  //Test of newtons third law
  for(j=0;j<NTask;j++)
  {
    if(Shadows[j].G)
    {
      for(i=0;i<NGas[j];i++)
      {
        new[0]+=PcleMass*Shadows[j].G[i].ax;
        new[1]+=PcleMass*Shadows[j].G[i].ay;
        new[2]+=PcleMass*Shadows[j].G[i].az;
      }
    }
  }
  MPI_Allreduce(MPI_IN_PLACE,new,3,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  gpprintf("Total force after hydro is %g,%g,%g.\n",new[0],new[1],new[2]);
#endif

#if DEBUG>=1
  for(i=0;i<NPart[GAS];i++)
  {
    if(G[i].ax!=G[i].ax)
    {
      pprintf("G[%d].ax is %g after hydro.\n",i,G[i].ax);
      pprintf("G[%d].density,pressure,hsml,K,Kpred. = %g,%g,%g,%g,%g\n",i,G[i].density,G[i].pressure,G[i].hsml,G[i].K,G[i].Kpred);
      kill(ERROR_SANITY);
    }
  }
#endif

  gpprintf("Hydro loop complete.\n");
#endif
#if HYDRO>=3
  //Finally the artificial viscosity loop
  gpprintf("Starting artificial viscosity loop.\n");
  //Calculate the viscosity dependent hydrodynamic forces
  tstart=MPI_Wtime();
  viscosity();
  //Anything we need to cast...
  Timings.av += MPI_Wtime()-tstart;
#if DEBUG>=2
  new[0]=new[1]=new[2]=0;
  //Test of newtons third law
  for(j=0;j<NTask;j++)
  {
    if(Shadows[j].G)
    {
      for(i=0;i<NGas[j];i++)
      {
        new[0]+=PcleMass*Shadows[j].G[i].ax;
        new[1]+=PcleMass*Shadows[j].G[i].ay;
        new[2]+=PcleMass*Shadows[j].G[i].az;
      }
    }
  }
  MPI_Allreduce(MPI_IN_PLACE,new,3,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  gpprintf("Total force after av is %g,%g,%g.\n",new[0],new[1],new[2]);
#endif
  gpprintf("Artificial viscosity loop done.\n");
#endif
}
