/*
 * =====================================================================================
 *
 *       Filename:  comm.c
 *
 *    Description:  Defines general communication functions that can be used by any 
 *                  part of the code needing to exchange particle information.  
 *
 *        Version:  0.6.3
 *        Created:  04/02/14 16:58:07
 *
 *         Author:  Matthew D. Young, my304@ast.cam.ac.uk
 *        Company:  Institute of Astronomy, Cambridge, UK
 *        License:  GNU General Public License
 *      Copyright:  Copyright (c) 2014, Matthew D. Young
 *
 * =====================================================================================
 */
#include "variables.h"
#include "functions.h"

/*
 * The simplest comm routine possible.  This is pretty much the GADGET2
 * routine verbatim.  Will block after local work, after the exchange
 * of particles and after performing export work.
 */

/* 
 * ===  FUNCTION  ======================================================================
 *  Description:  The simplest communication routine.  Pretty much the communication 
 *                routine used by GADGET2.  Blocks after local work and after completing
 *                any received non-local work.  This makes measuring imbalance easy, but
 *                can potentially result in a lot of wasted time waiting for the slowest
 *                processor to finish compared to the other methods below.  Half the 
 *                buffer can be used for storing exported particles.
 *                *send - The location in memory to store objects for export
 *                *recv - The location in memory to store received objects (i.e., those
 *                        exported to us)
 *                object_size - The size of a single export object (send/recv objects 
 *                        assumed to be the same size)
 *                buffer_size - The number of export objects that will fit at *send and
 *                        *recv (assumed to be the same)
 *                work_function - Function that does the work that is desired on the 
 *                        object located at the first argument and stores any objects
 *                        that need exporting at N_export offset from the second
 *                        argument.  The final argument gives the mode 
 *                        (0=local,1=export)
 *                export_return - Function that takes an index to the *recv buffer which
 *                        stores the results of any object passed back to us with work
 *                        done on it.
 *                compar - qsort sorting function to sort export buffer by target task
 *                
 * =====================================================================================
 */
void ngb_search(void *send,void *recv,
    size_t object_size,int buffer_size,
    void (*work_function)(void *,void *,int), 
    void (*export_return)(void *),
    int (*compar)(const void *,const void *)
    )
{
  int i,j;
  //int n_export=0;
  int n_done;
  int n_grp;
  //int n_send_local[NTask];
  int n_send_global[NTask*NTask];
  int n_offsets[NTask];
  int n_buffer[NTask];
  int n_left;
  int maxfill;
  int remote_partner;
  int level;
  double tcost;
  i=0;
  //How many to do in total
  n_left=NPartWorld[GAS];
  do
  {

    /*
     *  The local work loop.
     */
    tcost = MPI_Wtime();

    n_done=0;
    for(j=0;j<NTask;j++)
      N_Send_Local[j]=0;

    //Do local particles until the export buffer is full up or we finish all locals
    for(N_Export=0;i<NPart[GAS] && buffer_size-N_Export > NTask;i++)
    {
      n_done++;
      if(G[i].hsml>0)
      {
        //pprintf("Doing work on particle %d.\n",i);
        //Do the calculation
        (*work_function)(G+i,send,0);
      }
    }
    Timings.work_local += MPI_Wtime()-tcost;
    //pprintf("Finished local with i=%d,n_export=%d,n_done=%d,NPart[GAS]=%d.\n",i,N_Export,n_done,NPart[GAS]);

    /*
     *  Send off newly created exports
     */

    //Sort the send buffer
    qsort(send,N_Export,object_size,compar);
    //Find the offsets for each
    for(j=1, n_offsets[0]=0;j<NTask;j++)
    {
      n_offsets[j]=n_offsets[j-1]+N_Send_Local[j-1];
    }

    //Gather the arrays of sends and measure how much time we wait for
    tcost=MPI_Wtime();
    MPI_Allgather(N_Send_Local,NTask,MPI_INT,n_send_global,NTask,MPI_INT,MPI_COMM_WORLD);
    Timings.wait_local += MPI_Wtime()-tcost;



    //Now the magical part, inspired by GADGET
    //The first loop is to ensure that we clear the send buffer.  Each loop
    //through level will do at least one send/recv for each processor.  This
    //extra loop makes sure we get everything sent in the instance when 
    //only the first send/recv executes each time
    level=1;
    do
    {
      //Initialise counter which tracks how full recv buffer gets
      for(j=0;j<NTask;j++)
      {
        n_buffer[j]=0;
      }
      //Now the magic, each loop will ensure that a processor sends and 
      //recieves from the same remote processor.  By looping over all 
      //groups we guaruntee that all pairs are found
      for(n_grp = level; n_grp < NTask; n_grp++)
      {
        //Do we have space to do all of the send/recieves in this round?
        maxfill=0;
        for(j=0;j<NTask;j++)
        {
          //This check only needed for non power of 2 tasks
          if((j ^ n_grp) < NTask)
          {
            if( maxfill < n_buffer[j] + n_send_global[(j ^ n_grp) * NTask +j])
            {
              maxfill = n_buffer[j] + n_send_global[(j ^ n_grp) * NTask +j];
            }
          }
        }
        //Don't have enough space :(
        if(maxfill >= buffer_size)
          break;

        //We do have enough space!
        //This is the partner I'm paired up with through the exclusive or bit magic
        remote_partner = Task ^ n_grp;

        if(remote_partner < NTask)
        {
          //Do we have anything to send or receive with our remote partner?
          if(N_Send_Local[remote_partner] || n_send_global[remote_partner * NTask + Task])
          {
            //It's business time!
            MPI_Sendrecv(send+n_offsets[remote_partner]*object_size,
                N_Send_Local[remote_partner]*object_size,
                MPI_BYTE,remote_partner,TAG_SEND,
                recv+n_buffer[Task]*object_size,
                n_send_global[remote_partner*NTask+Task]*object_size,
                MPI_BYTE,remote_partner,TAG_SEND,
                MPI_COMM_WORLD, MPI_STATUS_IGNORE);
          }
        }

        //Update the buffers and move on to next send/recv pair
        for(j=0;j<NTask;j++)
        {
          if((j^n_grp) < NTask)
          {
            n_buffer[j] += n_send_global[(j ^ n_grp) * NTask +j];
          }
        }
      }

      //Do the processing of exported particles
      tcost = MPI_Wtime();
      //pprintf("Doing local work on %d particles from remote.\n",n_buffer[Task]);
      for(j=0;j<n_buffer[Task];j++)
      {
        (*work_function)(recv+object_size*j,NULL,1);
      }
      Timings.work_remote += MPI_Wtime()-tcost;

      //Keep track of wasted time by waiting for sync here
      tcost = MPI_Wtime();
      MPI_Barrier(MPI_COMM_WORLD);
      Timings.wait_remote += MPI_Wtime()-tcost;

      //Now back to the magic send/recv loop
      for(j=0;j<NTask;j++)
      {
        n_buffer[j]=0;
      }
      for(n_grp = level; n_grp < NTask; n_grp++)
      {
        //Do we have space to do all of the send/recieves in this round?
        maxfill=0;
        for(j=0;j<NTask;j++)
        {
          //This check only needed for non power of 2 tasks
          if((j ^ n_grp) < NTask)
          {
            if( maxfill < n_buffer[j] + n_send_global[(j ^ n_grp) * NTask +j])
            {
              maxfill = n_buffer[j] + n_send_global[(j ^ n_grp) * NTask +j];
            }
          }
        }
        //Don't have enough space :(
        if(maxfill >= buffer_size)
          break;

        //We do have enough space!
        //This is the partner I'm paired up with through the exclusive or bit magic
        remote_partner = Task ^ n_grp;

        if(remote_partner < NTask)
        {
          //Do we have anything to return or recieve back from our remote partner?
          if(N_Send_Local[remote_partner] || n_send_global[remote_partner * NTask + Task])
          {
            //It's business time!
            MPI_Sendrecv(recv+n_buffer[Task]*object_size,
                n_send_global[remote_partner*NTask+Task]*object_size,
                MPI_BYTE,remote_partner,TAG_RETURN,
                send+n_offsets[remote_partner]*object_size,
                N_Send_Local[remote_partner]*object_size,
                MPI_BYTE,remote_partner,TAG_RETURN,
                MPI_COMM_WORLD, MPI_STATUS_IGNORE);
          }
        }

        //Store the results
        for(j=n_offsets[remote_partner];j<n_offsets[remote_partner]+N_Send_Local[remote_partner];j++)
        {
          (*export_return)(send+object_size*j);
        }

        //Update the buffers and move on to next send/recv pair
        for(j=0;j<NTask;j++)
        {
          if((j^n_grp) < NTask)
          {
            n_buffer[j] += n_send_global[(j ^ n_grp) * NTask +j];
          }
        }
      }
      //Done both sends and receives, repeat the loop if we couldn't
      //do any of them because of the receive buffer over-filling.
      level = n_grp;
    }while(level<NTask);
    //How many did we chew up this time around?
    MPI_Allreduce(MPI_IN_PLACE,&n_done,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
    n_left -= n_done;
  //Finish overall loop
  }while(n_left);
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Description:  This communication routine uses asynchronous communication to attempt
 *                to minimise the time spent waiting for processors to finish work. 
 *                This has the obvious advantage of saving computational time, but 
 *                introduces some communication overhead as small bits of information
 *                need to be passed between processors to indicate things like local
 *                completion of work.  Also makes measuring processor imbalance more
 *                challenging.  Only 1/3 of the buffer can be used for storing export
 *                objects as we need a guaranteed empty block to receive exports into
 *                and we can't know when they're going to arrive.
 *                *send - The location in memory to store objects for export
 *                *recv - The location in memory to store received objects (i.e., those
 *                        exported to us)
 *                *buffer - The location in memory to store returned work in when it 
 *                        arrives.
 *                object_size - The size of a single export object (send/recv objects 
 *                        assumed to be the same size)
 *                buffer_size - The number of export objects that will fit at *send and
 *                        *recv (assumed to be the same)
 *                work_function - Function that takes two arguments, a particle index 
 *                        and a mode flag (0=local,1=remote).  This should do the actual
 *                        calculation we are interested in performing.  Should also 
 *                        fill the export buffer if required. 
 *                export_return - Function that takes an index to the *recv buffer which
 *                        stores the results of any object passed back to us with work
 *                        done on it.
 *                compar - qsort sorting function to sort export buffer by target task
 *                
 * =====================================================================================
 */
void ngb_search_async(void *send,void *recv,void *buffer,
    size_t object_size,int buffer_size,
    void (*work_function)(void *,void *,int), 
    void (*export_return)(void *),
    int (*compar)(const void *,const void *)
    )
{
  int i,j,k;
  int flag;
  int offset;
  int msrc;
  int n_request_max = NTask*2;
  int n_block_sent = 0;
  int n_block_return = 0;
  int n_expecting = 0;
  int n_required = 0;
  int n_alive = NTask-1;
  int n_done;
  int n_recv;
  int send_len = buffer_size;
  int send_start = 0;
  int return_len = buffer_size;
  int return_start = 0;
  int allDone = 0;
  int alive = 1;
  int idxs[n_request_max];
  int n_left;
  int data_send_lens[n_request_max];
  int data_send_starts[n_request_max];
  int data_return_lens[n_request_max];
  int data_return_starts[n_request_max];
  MPI_Request data_send[n_request_max];
  MPI_Request data_return[n_request_max];
  MPI_Request termAck[NTask-1];
  MPI_Status status;
  double tcost;

  //Setup the send and return request blocks
  for(i=0;i<n_request_max;i++)
  {
    data_send[i]=MPI_REQUEST_NULL;
    data_return[i]=MPI_REQUEST_NULL;
  }
  //Send off the termination signals
  j=0;
  for(i=0;i<NTask;i++)
  {
    if(i!=Task)
    {
      MPI_Issend(0,0,MPI_INT,i,TAG_TERM,MPI_COMM_WORLD,&termAck[j]);
      j++;
    }
  }
  i=0;
  n_left=NPartWorld[GAS];
  do
  {
 

    /*
     *  The local work loop.
     */
    tcost = MPI_Wtime();

    n_done=0;
    for(j=0;j<NTask;j++)
      N_Send_Local[j]=0;

    //Do local particles until the export buffer is full up or we finish all locals
    for(N_Export=0;i<NPart[GAS] && send_len-N_Export > NTask;i++)
    {
      //Do the calculation
      (*work_function)(G+i,send,0);
      n_done++;
    }
    Timings.work_local += MPI_Wtime()-tcost;
    //printf("[%d] Finished local with i=%d,N_Export=%d,n_done=%d,NPart[GAS]=%d.\n",Task,i,N_Export,n_done,NPart[GAS]);

    /*
     *  Send off newly created exports
     */
 
    tcost=MPI_Wtime();
    //Sort the send buffer
    qsort(send+send_start*object_size,N_Export,object_size,compar);
    //Send out all the data to foreign lands
    offset=0;
    for(j=0;j<NTask;j++)
    {
      //Send it, if there's anything to send
      if(N_Send_Local[j])
      {
        //Find the next free request hole
        k=0;
        while(data_send[k]!=MPI_REQUEST_NULL)
          k++;
        //Send the data
        //printf("[%d] Exporting %d particles to %d.\n",Task,N_Send_Local[j],j);
        MPI_Isend(send+(send_start+offset)*object_size,N_Send_Local[j]*object_size,MPI_BYTE,j,TAG_SEND,MPI_COMM_WORLD,&data_send[k]);
        data_send_lens[k]=N_Send_Local[j];
        data_send_starts[k]=send_start+offset;
        //Move the pointer
        offset += N_Send_Local[j];
        //Record that these blocks exist
        n_block_sent++;
        n_expecting++;
      }
    }

    //If we still have some local particles to go...
    if(i<NPart[GAS])
    {
      //printf("[%d] Locals still pending.\n",Task);
      send_len=0;
      send_start=0;
    }
    else
    {
      //printf("[%d] No more local!\n",Task);
      allDone=1;
    }

    //Timings.hydro_comm += MPI_Wtime()-tcost;

    /*
     * The send/recv loop
     */

    tcost=MPI_Wtime();
    while(
        //If allDone, check first condition, if not, check second
        //!allDone check says keep looping until we have some memory
        //to store new exports and have enough free requests to export
        //another round of particles to each processor if needed
        //allDone check says keep looping until we have nothing left to 
        //send locally, expect nothing back and our term flag has been 
        //accepted on all other processors
        (allDone && (alive || n_alive || n_block_return || n_block_sent || n_expecting))
        ||
        (!allDone && (n_request_max - n_block_sent < NTask || !send_len))
        )
    {
      //Special death spiral tests
      if(allDone)
      {
        //Can I accept everyone else's term signals yet?
        if(alive && !n_block_return && !n_block_sent && !n_expecting)
        {
          //printf("[%d] Finished all my duties, receiving TERM packets.\n",Task);
          //I'm no longer alive
          alive=0;
          //Receive all term flags from others
          for(j=0;j<NTask;j++)
          {
            if(j!=Task)
            {
              MPI_Recv(0,0,MPI_INT,j,TAG_TERM,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
            }
          }
        }
        //Did any of my term flags get accepted?
        if(n_alive)
        {
          MPI_Testsome(NTask-1,termAck,&flag,idxs,MPI_STATUSES_IGNORE);
          if(flag)
          {
            //printf("[%d] %d of my TERM packets have been accepted.\n",Task,flag);
            n_alive -= flag;
          }
        }
      }
      //Do we have any sends to check if we're finished?
      if(n_block_sent)
      {
        //Did any of my sends finish?
        MPI_Testsome(n_request_max,data_send,&flag,idxs,MPI_STATUSES_IGNORE);
        //Don't care about memory in send buffer if all locals are done
        if(flag)
        {
          //printf("[%d] Completed %d sends.\n",Task,flag);
          //Update number of blocks still not sent
          n_block_sent -= flag;
          //Only care about the memory in the send buffer if
          //we still have locals to do
          if(!allDone)
          {
            //Completely empty, reset
            if(!n_block_sent)
            {
              send_start=0;
              send_len=buffer_size;
            }
            else
            {
              //Not empty, select biggest empty region
              for(j=0;j<flag;j++)
              {
                k = idxs[j];
                //Is it the biggest free region yet?
                if(data_send_lens[k]>send_len)
                {
                  send_len = data_send_lens[k];
                  send_start = data_send_starts[k];
                }
              }
            }
          }
        }
      }
      //Did any of my returns finish?
      if(n_block_return)
      {
        MPI_Testsome(n_request_max,data_return,&flag,idxs,MPI_STATUSES_IGNORE);
        if(flag)
        {
          //printf("[%d] Completed %d returns.\n",Task,flag);
          //Update the number of blocks still not returned
          n_block_return -= flag;
          //Find the largest empty region
          if(!n_block_return)
          {
            return_start=0;
            return_len=buffer_size;
          }
          else
          {
            //Not empty, select biggest empty region
            for(j=0;j<flag;j++)
            {
              k=idxs[j];
              if(data_return_lens[k]>return_len)
              {
                return_len = data_return_lens[k];
                return_start = data_return_starts[k];
              }
            }
          }
        }
      }
      //Should I be checking for new work?
      if(n_required < return_len && n_block_return < n_request_max)
      {
        MPI_Iprobe(MPI_ANY_SOURCE,TAG_SEND,MPI_COMM_WORLD,&flag,&status);
        if(flag)
        {
          msrc = status.MPI_SOURCE;
          //How many?
          MPI_Get_count(&status,MPI_BYTE,&n_recv);
          //printf("[%d] Attempting to get %d particles for work from %d.\n",Task,n_recv/sizeof(union hydro_comm),msrc);
          //Is there space for the new work?
          if(n_recv >= return_len*object_size)
          {
            //Nope.  Not until we've cleared out some space
            n_required=n_recv/object_size;
          }
          else
          {
            //Yep. Get the work.
            n_required=0;
            MPI_Recv(recv+return_start*object_size,n_recv,MPI_BYTE,msrc,TAG_SEND,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
            Timings.wait_remote += MPI_Wtime()-tcost;
            tcost = MPI_Wtime();
            //printf("[%d] Got %d particles for work from %d, doing local work.\n",Task,n_recv/sizeof(union hydro_comm),msrc);
            //Do stuff with it
            for(j=0;j<n_recv/object_size;j++)
            {
              (*work_function)(recv+object_size*(return_start+j),NULL,1);
            }
            //Find a spare return block
            k=0;
            while(data_return[k]!=MPI_REQUEST_NULL)
              k++;
            //Send the results back
            MPI_Isend(recv+return_start*object_size,n_recv,MPI_BYTE,msrc,TAG_RETURN,MPI_COMM_WORLD,&data_return[k]);
            data_return_lens[k]=n_recv/object_size;
            data_return_starts[k]=return_start;
            //Update counters
            n_block_return++;
            return_start += data_return_lens[k];
            return_len -= data_return_lens[k];
            //printf("[%d] Sent %d particles back to %d.\n",Task,n_recv/sizeof(union hydro_comm),msrc);
            Timings.work_remote += MPI_Wtime()-tcost;
            tcost=MPI_Wtime();
          }
        }
      }
      if(n_expecting)
      {
        //Did anyone send me any results back?
        MPI_Iprobe(MPI_ANY_SOURCE,TAG_RETURN,MPI_COMM_WORLD,&flag,&status);
        if(flag)
        {
          msrc = status.MPI_SOURCE;
          //How many?
          MPI_Get_count(&status,MPI_BYTE,&n_recv);
          //Get the work
          MPI_Recv(buffer,n_recv,MPI_BYTE,msrc,TAG_RETURN,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
          Timings.wait_remote += MPI_Wtime()-tcost;
          tcost=MPI_Wtime();
          //printf("[%d] Got %d particles for storage from %d.\n",Task,n_recv/sizeof(union hydro_comm),msrc);
          //Store it
          for(j=0;j<n_recv/object_size;j++)
          {
            (*export_return)(buffer+object_size*j);
          }
          n_expecting--;
          //printf("[%d] Store %d particles from %d.  n_expecting=%d.\n",Task,n_recv/sizeof(union hydro_comm),msrc,n_expecting);
          //Timings.hydro_work += MPI_Wtime()-tcost;
          tcost=MPI_Wtime();
        }
      }
    }
    Timings.wait_remote += MPI_Wtime()-tcost;
    //How many did we chew up this time around?
    MPI_Allreduce(MPI_IN_PLACE,&n_done,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
    n_left -= n_done;
  //Finish overall loop
  }while(n_left);
}


/* 
 * ===  FUNCTION  ======================================================================
 *  Description:  By far the most complex of the communication routines.  Work (groups 
 *                of particles) are split into chunks and processed one at a time.  If 
 *                a processor finishes early it can send of a chunk to be completed by 
 *                another processor.  However, this can only be done if the target 
 *                processor already has a copy of the local domain, so work stealing is 
 *                limited to processors that have overlapping shadow domains.  Given the 
 *                complexity of this method and its myriad of limitations, I would not 
 *                recommend it very highly.   
 *                Haven't bothered abstracting it because of the complexity of doing so 
 *                combined with the fact that my tests on the hydro routine indicated 
 *                there was no real benefit.  In fact this comm strategy can be 
 *                significantly *slower* in some circumstances.  All the functions 
 *                needed to recreate the approach are here though.  Alternatively look 
 *                at the ghostDomains branch.
 *
 * =====================================================================================
 */
/*
//int NTerm;
//int Nworking;
//int ChunkCounter;
//int NChunks;
//int Nfeeders;
//int *IsWorking;
//int *HasMe;
//int *WorkSend;
//int *RankLocal;
//MPI_Request *WorkOrders;
//int NworkOrders;
////New shit
////Number of processors that can potentially send me exported particles (i.e., number of processors I don't have a local copy of)
//int NexportFeeders;
////How many hydro export/import elements can I fit in each block in the comm buffer
//int BlockSize;
////How many export/import blocks can I fit in the comm buffer
//int Nblocks;
////How many are left for sending and receiving of local particles needing exporting
////once I've allocated space for the blocks to receive foreign exports?
//int NsendBlocks;
////A stack containing all those blocks that are available to store local particles needing export
//int *BlockStack;
////The number of free blocks available in the above stack
//int NfreeBlocks;
////The index of the currently active block to store work destined for processor i
//int *ActiveBlock;
////The array which translates index in the ExportSendRecvForeign request array into the Task number of the receiver/sender
//int *ExportSendRecvForeignTaskNo;
////The ith element of this array says if the ith element of ExportSendRecvForeign is a send(1) or a recv(-1)
//int *ExportSendRecvForeignDirection;
////Array of Requests to either send a piece of processed data back to its sender or receive some exported particles (depending on the above two arrays)
//MPI_Request *ExportSendRecvForeign;
////The array which translates index in ExportSendRecv to a Task number
//int *ExportSendRecvTaskNo;
////The array which indicates which direction each request in ExportSendRecv is going (1 is send, -1 recv)
//int *ExportSendRecvDirection;
////Array of requests to either send a block of exported particles for processing, or receive them back again after they've been processed
//MPI_Request *ExportSendRecv;
////How many blocks have I sent for processing
//int NexportSent;
////How many blocks have I sent processed data back for
//int NexportReturn;
////The array of requests that I use to know when people are finished
//MPI_Request *TerminalAck;
//
//
//void hydro(void)
//{
//  int i,j,k,l;
//  int block_len;
//  int tmp;
//  int flag;
//  int shade;
//  int done_idxs[NTask];
//  int work_order[2];
//  int work_recv[NTask*2];
//  int rank_probe[NTask];
//  int size,rank;
//  double tstart=MPI_Wtime();
//  double tcost;
//  MPI_Request recvs[NTask];
//  Ndrift=0;
//
//  WorkOrders=malloc(sizeof(MPI_Request)*NTask);
//  IsWorking=malloc(sizeof(int)*NTask);
//  HasMe=malloc(sizeof(int)*NTask);
//  RankLocal=malloc(sizeof(int)*NTask);
//  WorkSend=malloc(sizeof(int)*2*NTask);
//  TerminalAck = malloc(NTask*sizeof(MPI_Request));
//  //Initialise anything we're calculating
//  for(j=0;j<NTask;j++)
//  {
//    if(Shadows[j].G)
//    {
//      for(i=0;i<NGas[j];i++)
//      {
//        Shadows[j].G[i].divv = 0;
//        Shadows[j].G[i].divv_extra = 0;
//        Shadows[j].G[i].cost = 0;
//      }
//    }
//  }
//  NTerm=0;
//  for(i=0;i<NTask;i++)
//  {
//    HasMe[i]=0;
//    rank_probe[i]=i;
//    IsWorking[i]=1;
//    RankLocal[i]=0;
//    WorkSend[i]=0;
//    WorkOrders[i]=MPI_REQUEST_NULL;
//    recvs[i]=MPI_REQUEST_NULL;
//    TerminalAck[i]=MPI_REQUEST_NULL;
//  }
//  / *
//   * Pre-local communication initialisation
//   */
//  //Translate the local ranks into the ranks of the local group.  Afterwards,
//  //RankLocal[i]==MPI_UNDEFINED means that i is not in the local group.  That is,
//  //i does not have a copy of my domain.
//  MPI_Group_translate_ranks(World,NTask,rank_probe,Shadows[Task].group,RankLocal);
//  //Split it up into chunks
//  NChunks=imin(Params.nchunks,NPart[GAS]);
//  //Start counting time
//  Timings.hydro_misc += MPI_Wtime()-tstart;
//  tstart=MPI_Wtime();
//  //What is my rank and the size of the local group
//  MPI_Comm_size(Shadows[Task].comm,&size);
//  MPI_Comm_rank(Shadows[Task].comm,&rank);
//  //Do the first lot of sends (so the Testsome work is primed in the main loop)
//  ChunkCounter=0;
//  //The number of processors that can potentially feed me work.  Anything I have 
//  //a copy of basically.
//  Nfeeders=NShadows;
//  //The number of processors that can potentially feed me exported particles.
//  //Anything that doesn't have a copy of me.
//  NexportFeeders=NTask-size;
//  NworkOrders=0;
//  //Set off the Ack messages
//  for(i=0;i<NTask;i++)
//  {
//    if(i!=Task)
//    {
//      //Send terminal acknowledged to everyone
//      MPI_Issend(0,0,MPI_INT,i,TAG_TERM,MPI_COMM_WORLD,&TerminalAck[i]);
//      if(Shadows[i].G || RankLocal[i]!=MPI_UNDEFINED)
//      {
//        //Send the finished local acknowledged
//        MPI_Issend(0,0,MPI_INT,i,TAG_TERM_LOCAL,MPI_COMM_WORLD,&WorkOrders[i]);
//        NworkOrders++;
//        if(RankLocal[i]!=MPI_UNDEFINED)
//        {
//          HasMe[i]=1;
//        }
//      }
//    }
//  }
//  //printf("[%d] Ack packets sent.\n",Task);
//  //What should the block size be?
//  BlockSize = (2*BufferSizeHydro)/(NTask*5);
//  //Which gives me how many blocks?
//  Nblocks = (2*BufferSizeHydro)/BlockSize;
//  NsendBlocks = Nblocks-NexportFeeders;
//  //Create a free blocks stack
//  BlockStack = malloc(NsendBlocks*sizeof(int));
//  ActiveBlock = malloc(NTask*sizeof(int));
//  //The last NexportFeeders blocks are reserved for receiving and processing
//  //from the NexportFeeders processors that could potentially send me exported particles
//  NfreeBlocks = NsendBlocks;
//  //printf("[%d] NfreeBlocks=%d, NBlocks=%d,NexportFeeders = %d,Nfeeders=%d.\n",Task,NfreeBlocks,Nblocks,NexportFeeders,Nfeeders);
//  //Populate the stack with available blocks
//  for(i=0;i<NfreeBlocks;i++)
//  {
//    BlockStack[i] = i;
//  }
//  //Allocate a block to each of the processors that we might send to
//  for(i=0;i<NTask;i++)
//  {
//    if(!Shadows[i].G)
//    {
//      //I don't have a copy, so I might have to export to it
//      ActiveBlock[i]=BlockSize*BlockStack[NfreeBlocks-1];
//      NfreeBlocks--;
//    }
//    else
//    {
//      ActiveBlock[i]=0;
//    }
//  }
//  //printf("[%d] Active blocks allocated.\n",Task);
//
//  //Which means that the receivers should start where?
//  HydroRecv = HydroSend + BlockSize*NsendBlocks;
//  //Arrays which track incoming exported particles and there resending post-processing
//  ExportSendRecvForeignTaskNo = malloc(NexportFeeders*sizeof(int));
//  ExportSendRecvForeignDirection = malloc(NexportFeeders*sizeof(int));
//  ExportSendRecvForeign = malloc(NexportFeeders*sizeof(MPI_Request));
//  //Arrays which track outgoing exported particles and their receipt after work is done on them
//  ExportSendRecvTaskNo = malloc(NsendBlocks*sizeof(int));
//  ExportSendRecvDirection = malloc(NsendBlocks*sizeof(int));
//  ExportSendRecv = malloc(NsendBlocks*sizeof(MPI_Request));
//  for(i=0;i<NsendBlocks;i++)
//    ExportSendRecv[i]=MPI_REQUEST_NULL;
//  //Create the initial receives for any exported particles I need to process
//  j=0;
//  for(i=0;i<NTask;i++)
//  {
//    if(i!=Task && !HasMe[i])
//    {
//      ExportSendRecvForeignTaskNo[j]=i;
//      ExportSendRecvForeignDirection[j]=-1;
//      //Create the appropriate receive as well
//      MPI_Irecv(HydroRecv+j*BlockSize,BlockSize*sizeof(union hydro_comm),MPI_BYTE,i,TAG_EXPORT,MPI_COMM_WORLD,&ExportSendRecvForeign[j]);
//      //printf("[%d] Recv from process %d created.\n",Task,ExportSendRecvForeignTaskNo[j]);
//      j++;
//    }
//  }
//  Timings.hydro_comm += MPI_Wtime()-tstart;
//  //Numbers of active communications
//  NexportSent=0;
//  NexportReturn=0;
//  Ndrift=0;
//  tcost = MPI_Wtime();
//  //printf("[%d] About to start local computation. Done all the BS.\n",Task);
//  //printf("[%d] NexportFeeders=%d.\n",Task,NexportFeeders);
//  /*
//   * Local computation
//   */
//  while(ChunkCounter<NChunks)
//  {
//    /*
//     * Processing of foreign (i.e, particles exported to me or those sent back after I've worked on them) particles
//     */
//    //MPI_Testsome(NexportFeeders,ExportSendRecvForeign,&flag,done_idxs,MPI_STATUSES_IGNORE);
//    //for(i=0;i<flag;i++)
//    //{
//    //  printf("[%d] Foreign done.\n",Task);
//    //  j=done_idxs[i];
//    //  //Was it a send that finished?
//    //  if(ExportSendRecvForeignDirection[j]==1)
//    //  {
//    //    //Can now re-use this block, so re-set a recv on it
//    //    MPI_Irecv(HydroRecv+j*BlockSize,BlockSize*sizeof(union hydro_comm),MPI_BYTE,ExportSendRecvForeignTaskNo[j],TAG_EXPORT,MPI_COMM_WORLD,&ExportSendRecvForeign[j]);
//    //    NexportReturn--;
//    //  }
//    //  else
//    //  {
//    //    //Now it was a receive that finished.
//    //    //How many did we get?
//    //    block_len = HydroRecv[j*BlockSize].in.from;
//    //    //Do the work
//    //    for(k=0;k<block_len;k++)
//    //    {
//    //      //Need to adjust the target to point at the right recv block
//    //      hydro_ngbs(k+j*BlockSize,1);
//    //    }
//    //    //Send it back.  Can't re-use the recv block until we have completed this
//    //    MPI_Issend(HydroRecv+j*BlockSize,block_len*sizeof(union hydro_comm),MPI_BYTE,ExportSendRecvForeignTaskNo[j],TAG_RETURN,MPI_COMM_WORLD,&ExportSendRecvForeign[j]);
//    //    NexportReturn++;
//    //  }
//    //  //Swap direction of the send/recv flag for this request
//    //  ExportSendRecvForeignDirection[j]*=-1;
//    //}
//    //printf("[%d] Checking local.\n",Task);
//    /*
//     * Sending/Receiving of particles that I can't finish processing of locally.
//     */
//    if(NexportSent)
//    {
//      MPI_Testsome(NsendBlocks,ExportSendRecv,&flag,done_idxs,MPI_STATUSES_IGNORE);
//      for(i=0;i<flag;i++)
//      {
//        //printf("[%d] Local done.\n",Task);
//        j=done_idxs[i];
//        //Was it a send that finished?
//        if(ExportSendRecvDirection[j]==1)
//        {
//          //If it was, then expect a receive back again
//          MPI_Irecv(HydroSend+BlockSize*j,BlockSize*sizeof(union hydro_comm),MPI_BYTE,ExportSendRecvTaskNo[j],TAG_RETURN,MPI_COMM_WORLD,&ExportSendRecv[j]);
//          //Mark it as inbound now
//          ExportSendRecvDirection[j]=-1;
//        }
//        else
//        {
//          //A receive finished, store it...
//          tmp = j*BlockSize;
//          block_len = tmp+HydroSend[tmp].in.from;
//          for(k=tmp;k<block_len;k++)
//          {
//            shade = HydroSend[k].out.what/MaxGas;
//            l = HydroSend[k].out.what%MaxGas;
//            Shadows[shade].G[l].ax += HydroSend[k].out.ax;
//            Shadows[shade].G[l].ay += HydroSend[k].out.ay;
//            Shadows[shade].G[l].az += HydroSend[k].out.az;
//            Shadows[shade].G[l].divv += HydroSend[k].out.divv;
//          }
//          //push this block back onto the stack
//          BlockStack[NfreeBlocks]=j;
//          NfreeBlocks++;
//          //This export is done now...
//          NexportSent--;
//        }
//      }
//    }
//    /*
//     * Local work
//     */
//    work_order[0]=((ChunkCounter*NPart[GAS])/NChunks)+Task*MaxGas;
//    work_order[1]=(((ChunkCounter+1)*NPart[GAS])/NChunks)+Task*MaxGas;
//    //printf("[%d] Processing chunk %d.\n",Task,ChunkCounter);
//    process_chunk(work_order[0],work_order[1]);
//    ChunkCounter++;
//    //printf("[%d] Chunk %d processed.\n",Task,ChunkCounter);
//    /*
//     * Palming off of local work where possible
//     */
//    tstart=MPI_Wtime();
//    MPI_Testsome(NTask,WorkOrders,&flag,done_idxs,MPI_STATUSES_IGNORE);
//    for(i=0;i<flag;i++)
//    {
//      NworkOrders--;
//      j=done_idxs[i];
//      //Either a processor has finished the local loop or one of my bits
//      //of palmed off work has been received 
//      //In all instances, processor j is no longer in the local loop, so
//      //make a note of that if we haven't already
//      if(IsWorking[j])
//      {
//        IsWorking[j]=0;
//        //If we have a copy of this task, it can no longer feed us work...
//        if(Shadows[j].G)
//          Nfeeders--;
//        //printf("[%d] Processor %d finished, I now have %d feeders left.\n",Task,j,Nfeeders);
//      }
//      //If it has a copy of me, send it some work
//      if(HasMe[j] && ChunkCounter<NChunks)
//      {
//        //Send a new chunk of work
//        WorkSend[j*2]=((ChunkCounter*NPart[GAS])/NChunks)+Task*MaxGas;
//        WorkSend[j*2+1]=(((ChunkCounter+1)*NPart[GAS])/NChunks)+Task*MaxGas;
//        MPI_Issend(&WorkSend[j*2],2,MPI_INT,RankLocal[j],TAG_WORK,Shadows[Task].comm,&WorkOrders[j]);
//        //printf("[%d] Posting work to %d.\n",Task,j);
//        ChunkCounter++;
//        Ndrift+=WorkSend[j*2+1]-WorkSend[j*2];
//        NworkOrders++;
//      }
//    }
//    Timings.hydro_comm += MPI_Wtime()-tstart;
//  }
//  Timings.hydro_local += MPI_Wtime()-tcost;
//  /*
//   * Post-local work sharing
//   */
//  //Calculate how costly it was to do the local processing I did do.
//  if(CostPerPcle==0)
//  {
//    //First time since reset
//    CostPerPcle += ((MPI_Wtime()-tcost))/((double) (NPart[GAS]-Ndrift));
//  }
//  else
//  {
//    //Second time around, so average with previous cost-per-particle
//    CostPerPcle += ((MPI_Wtime()-tcost))/((double) (NPart[GAS]-Ndrift));
//    //To make this the average cost per particle over two instances
//    CostPerPcle /= 2.0;
//  }
//  printf("[%d] Finished local particles with cost-per-local %g.\n",Task,CostPerPcle*NPart[GAS]);
//  tstart=MPI_Wtime();
//  //Receive all the announcer messages.  This informs the sender that
//  //I've now finished my local work.
//  for(i=0;i<NTask;i++)
//  {
//    if(i!=Task && (Shadows[i].G || HasMe[i]))
//    {
//      MPI_Recv(0,0,MPI_INT,i,TAG_TERM_LOCAL,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
//      //printf("[%d] Received acknowledger from %d.\n",Task,i);
//    }
//  }
//  //Create Irecvs to start receiving any palmed off work from processors still
//  //working on local particles that I have a copy of
//  for(i=0;i<NTask;i++)
//  {
//    if(i!=Task && Shadows[i].G && IsWorking[i])
//    {
//      MPI_Irecv(&work_recv[i*2],2,MPI_INT,Shadows[i].owner_rank_in_comm,TAG_WORK,Shadows[i].comm,&recvs[i]);
//    }
//  }
//  Timings.hydro_comm += MPI_Wtime()-tstart;
//  //printf("[%d] Starting doing non-local work if possible.\n",Task);
//  //Now receive work until everyone is finished
//  while(Nfeeders)
//  {
//    /*
//     * Processing of foreign (i.e, particles exported to me or those sent back after I've worked on them) particles
//     */
//    MPI_Testsome(NexportFeeders,ExportSendRecvForeign,&flag,done_idxs,MPI_STATUSES_IGNORE);
//    for(i=0;i<flag;i++)
//    {
//      j=done_idxs[i];
//      //Was it a send that finished?
//      if(ExportSendRecvForeignDirection[j]==1)
//      {
//        //Can now re-use this block, so re-set a recv on it
//        MPI_Irecv(HydroRecv+j*BlockSize,BlockSize*sizeof(union hydro_comm),MPI_BYTE,ExportSendRecvForeignTaskNo[j],TAG_EXPORT,MPI_COMM_WORLD,&ExportSendRecvForeign[j]);
//        NexportReturn--;
//      }
//      else
//      {
//        //Now it was a receive that finished.
//        //How many did we get?
//        block_len = HydroRecv[j*BlockSize].in.from;
//        //Do the work
//        for(k=0;k<block_len;k++)
//        {
//          //Need to adjust the target to point at the right recv block
//          hydro_ngbs(k+j*BlockSize,1);
//        }
//        //Send it back.  Can't re-use the recv block until we have completed this
//        MPI_Issend(HydroRecv+j*BlockSize,block_len*sizeof(union hydro_comm),MPI_BYTE,ExportSendRecvForeignTaskNo[j],TAG_RETURN,MPI_COMM_WORLD,&ExportSendRecvForeign[j]);
//        NexportReturn++;
//      }
//      //Swap direction of the send/recv flag for this request
//      ExportSendRecvForeignDirection[j]*=-1;
//    }
//    /*
//     * Sending/Receiving of particles that I can't finish processing of locally.
//     */
//    if(NexportSent)
//    {
//      MPI_Testsome(NsendBlocks,ExportSendRecv,&flag,done_idxs,MPI_STATUSES_IGNORE);
//      for(i=0;i<flag;i++)
//      {
//        j=done_idxs[i];
//        //Was it a send that finished?
//        if(ExportSendRecvDirection[j]==1)
//        {
//          //If it was, then expect a receive back again
//          MPI_Irecv(HydroSend+BlockSize*j,BlockSize*sizeof(union hydro_comm),MPI_BYTE,ExportSendRecvTaskNo[j],TAG_RETURN,MPI_COMM_WORLD,&ExportSendRecv[j]);
//          //Mark it as inbound now
//          ExportSendRecvDirection[j]=-1;
//        }
//        else
//        {
//          //A receive finished, store it...
//          tmp = j*BlockSize;
//          block_len = tmp+HydroSend[tmp].in.from;
//          for(k=tmp;k<block_len;k++)
//          {
//            shade = HydroSend[k].out.what/MaxGas;
//            l = HydroSend[k].out.what%MaxGas;
//            Shadows[shade].G[l].ax += HydroSend[k].out.ax;
//            Shadows[shade].G[l].ay += HydroSend[k].out.ay;
//            Shadows[shade].G[l].az += HydroSend[k].out.az;
//            Shadows[shade].G[l].divv += HydroSend[k].out.divv;
//          }
//          //push this block back onto the stack
//          BlockStack[NfreeBlocks]=j;
//          NfreeBlocks++;
//          //This export is done now...
//          NexportSent--;
//        }
//      }
//    }
//    /*
//     * Check if any of the bits of the ack messages I sent have completed
//     * (i.e. have any other processors finished local processing too)
//     * or if a bit of local work I sent has completed.
//     * Either way I just have to note that the receiving processor is 
//     * no longer in local and can't feed me any more work
//     */
//    if(NworkOrders)
//    {
//      MPI_Testsome(NTask,WorkOrders,&flag,done_idxs,MPI_STATUSES_IGNORE);
//      for(i=0;i<flag;i++)
//      {
//        NworkOrders--;
//        j=done_idxs[i];
//        //If this message finished, this processor must be done
//        if(IsWorking[j])
//        {
//          IsWorking[j]=0;
//          if(Shadows[j].G)
//            Nfeeders--;
//        }
//        //printf("[%d] Processor %d is finished, %d feeders left.\n",Task,j,Nfeeders);
//      }
//    }
//    /*
//     * Test if anyone has sent me some of their local work for me to do
//     */
//    MPI_Testsome(NTask,recvs,&flag,done_idxs,MPI_STATUSES_IGNORE);
//    for(i=0;i<flag;i++)
//    {
//      j=done_idxs[i];
//      //Do the work
//      process_chunk(work_recv[j*2],work_recv[j*2+1]);
//      //Renew the Irecv so I can get more work from them
//      MPI_Irecv(&work_recv[j*2],2,MPI_INT,Shadows[j].owner_rank_in_comm,TAG_WORK,Shadows[j].comm,&recvs[j]);
//    }
//  }
//  /*
//   * Final export phase
//   */
//  //printf("[%d] All feeders are done now.\n",Task);
//  //Check if there was any last bits of work sent me that I didn't pick up before
//  //I noticed my ack packet had been received
//  MPI_Testsome(NTask,recvs,&flag,done_idxs,MPI_STATUSES_IGNORE);
//  for(i=0;i<flag;i++)
//  {
//    j=done_idxs[i];
//    //There is, so do the work
//    process_chunk(work_recv[j*2],work_recv[j*2+1]);
//  }
//  //printf("[%d] Finished final check for data.\n",Task);
//  tstart=MPI_Wtime();
//  //Cancel any Irecvs still live, they'll never be matched 
//  for(i=0;i<NTask;i++)
//  {
//    if(recvs[i]!=MPI_REQUEST_NULL)
//    {
//      MPI_Cancel(&recvs[i]);
//      //printf("[%d] Killing receive from %d.\n",Task,i);
//    }
//  }
//  //Wait for them to actually finish
//  MPI_Waitall(NTask,recvs,MPI_STATUSES_IGNORE);
//  //printf("[%d] About to send blocks.\n",Task);
//  //Send off all the incomplete export blocks
//  for(i=0;i<NTask;i++)
//  {
//    if(!Shadows[i].G)
//    {
//      k=ActiveBlock[i]%BlockSize;
//      if(k)
//      {
//        //Calculate the block number.  This block is not full (or it would already be sent)
//        j=ActiveBlock[i]/BlockSize;
//        //printf("[%d] Sending block %d of size %d to %d.\n",Task,j,k,i);
//        //Set the first element from to encode the block length
//        HydroSend[BlockSize*j].in.from = k;
//        //Send the block
//        MPI_Issend(HydroSend+j*BlockSize,k*sizeof(union hydro_comm),MPI_BYTE,i,TAG_EXPORT,MPI_COMM_WORLD,&ExportSendRecv[j]);
//        ExportSendRecvTaskNo[j]=i;
//        ExportSendRecvDirection[j]=1;
//        NexportSent++;
//      }
//    }
//  }
//  //Enter the holding pattern and wait for all processors to reach this point
//  death_spiral();
//  Timings.hydro_idling_post_work += MPI_Wtime()-tstart;
//  //printf("[%d] Number to export %d.\n",Task,nexport);
//  //Nothing left to do, enter final comm loop
//  tstart=MPI_Wtime();
//  //nexport=hydro_comm_loop(nsend_local,1);
//  //Need to do a reduce on these here.  But as they're not really used for anything
//  //important (just guessing next h), we'll ignore that and get it wrong for now.
//  //Finalise answers
//  printf("[%d] Ndrift=%d.\n",Task,Ndrift);
//  for(i=0;i<NPart[GAS];i++)
//  {
//    G[i].divv *= PcleMass / (G[i].hsml * G[i].hsml * G[i].hsml * G[i].hsml * G[i].dhsml);
//    G[i].divv_extra *= PcleMass / (G[i].density * G[i].hsml * G[i].hsml * G[i].hsml *G[i].hsml);
//  }
//#if DEBUG>=2
//  double new[3];
//  new[0]=new[1]=new[2]=0;
//  //Test of newtons third law
//  for(i=0;i<NPart[GAS];i++)
//  {
//    new[0]+=PcleMass*G[i].ax;
//    new[1]+=PcleMass*G[i].ay;
//    new[2]+=PcleMass*G[i].az;
//  }
//  MPI_Allreduce(MPI_IN_PLACE,new,3,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
//  for(i=0;i<NPart[SINK];i++)
//  {
//    new[0]+=S[i].m * S[i].ax;
//    new[1]+=S[i].m * S[i].ay;
//    new[2]+=S[i].m * S[i].az;
//  }
//  if(Task==0)
//    printf("Total force after hydro is %g,%g,%g.\n",new[0],new[1],new[2]);
//#endif
//  free(WorkOrders);
//  free(IsWorking);
//  free(HasMe);
//  free(WorkSend);
//  free(RankLocal);
//  free(BlockStack);
//  free(ActiveBlock);
//  free(ExportSendRecvForeignTaskNo);
//  free(ExportSendRecvForeignDirection);
//  free(ExportSendRecvForeign);
//  free(ExportSendRecvTaskNo);
//  free(ExportSendRecvDirection);
//  free(ExportSendRecv);
//  free(TerminalAck);
//  Timings.hydro_misc += MPI_Wtime()-tstart;
//}
//
//void death_spiral()
//{
//  int finished;
//  int nalive;
//  int flag;
//  int done_idxs[NTask];
//  int i,j,k,l;
//  int tmp,shade;
//  int block_len;
//
//  //printf("[%d] Entered the death spiral.\n",Task);
//  finished=0;
//  nalive=NTask;
//  //I have no free blocks left, but I'm OK to do everything else
//  while(!finished || nalive || NexportSent || NexportReturn || NworkOrders)
//  {
//    //printf("[%d] finished=%d, nalive=%d, NexportSent=%d, NexportReturn=%d.\n",Task,finished,nalive,NexportSent,NexportReturn);
//    /*
//     * Test if any of the terminal ack packets have finished (i.e., have any of 
//     * the other processors finished everything they could possibly do)
//     */
//    MPI_Testsome(NTask,TerminalAck,&flag,done_idxs,MPI_STATUSES_IGNORE);
//    if(flag!=MPI_UNDEFINED)
//      nalive-=flag;
//    /*
//     * Have I finished everything I could possibly be exported to have finished 
//     * (and have I never done this before).  If so, receive the term signal from
//     * everyone else so they know I'm done.
//     */
//    if(!NexportSent && !NexportReturn && !finished)
//    {
//      //printf("[%d] Zombie time.\n",Task);
//      //I'm now a zombie of sorts, not really "alive"
//      nalive--;
//      finished=1;
//      //Receive the terminal ack signal from everyone so they know where I'm at
//      for(i=0;i<NTask;i++)
//      {
//        if(i!=Task)
//        {
//          MPI_Recv(0,0,MPI_INT,i,TAG_TERM,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
//        }
//      }
//    }
//    /*
//     * Processing of foreign (i.e, particles exported to me or those sent back after I've worked on them) particles
//     */
//    MPI_Testsome(NexportFeeders,ExportSendRecvForeign,&flag,done_idxs,MPI_STATUSES_IGNORE);
//    for(i=0;i<flag;i++)
//    {
//      j=done_idxs[i];
//      //printf("[%d] SendRecvForeign %d finished.\n",Task,ExportSendRecvForeignTaskNo[j]);
//      //Was it a send that finished?
//      if(ExportSendRecvForeignDirection[j]==1)
//      {
//        //printf("[%d] Sending data back to %d finish.\n",Task,ExportSendRecvForeignTaskNo[j]);
//        //Can now re-use this block, so re-set a recv on it
//        MPI_Irecv(HydroRecv+j*BlockSize,BlockSize*sizeof(union hydro_comm),MPI_BYTE,ExportSendRecvForeignTaskNo[j],TAG_EXPORT,MPI_COMM_WORLD,&ExportSendRecvForeign[j]);
//        NexportReturn--;
//      }
//      else
//      {
//        //printf("[%d] Receive from %d has finished.\n",Task,ExportSendRecvForeignTaskNo[j]);
//        //Now it was a receive that finished.
//        //How many did we get?
//        block_len = HydroRecv[j*BlockSize].in.from;
//        //printf("[%d] Block length inferred to be %d.\n",Task,block_len);
//        //Do the work
//        for(k=0;k<block_len;k++)
//        {
//          //Need to adjust the target to point at the right recv block
//          hydro_ngbs(k+j*BlockSize,1);
//        }
//        //Send it back.  Can't re-use the recv block until we have completed this
//        MPI_Issend(HydroRecv+j*BlockSize,block_len*sizeof(union hydro_comm),MPI_BYTE,ExportSendRecvForeignTaskNo[j],TAG_RETURN,MPI_COMM_WORLD,&ExportSendRecvForeign[j]);
//        //printf("[%d] Return block to rightful owner (%d).\n",Task,ExportSendRecvForeignTaskNo[j]);
//        NexportReturn++;
//      }
//      //printf("[%d] At the end, we have NexportRetun=%d.\n",Task,NexportReturn);
//      //Swap direction of the send/recv flag for this request
//      ExportSendRecvForeignDirection[j]*=-1;
//    }
//    /*
//     * Sending/Receiving of particles that I can't finish processing of locally.
//     */
//    if(NexportSent)
//    {
//      MPI_Testsome(NsendBlocks,ExportSendRecv,&flag,done_idxs,MPI_STATUSES_IGNORE);
//      for(i=0;i<flag;i++)
//      {
//        //printf("[%d] Have NexportSent=%d and flag=%d.\n",Task,NexportSent,flag);
//        j=done_idxs[i];
//        //printf("[%d] SendRecv %d finished.\n",Task,ExportSendRecvTaskNo[j]);
//        //Was it a send that finished?
//        if(ExportSendRecvDirection[j]==1)
//        {
//          //printf("[%d] Finished sending data to %d, creating a receive to expect the data back again.\n",Task,ExportSendRecvTaskNo[j]);
//          //If it was, then expect a receive back again
//          MPI_Irecv(HydroSend+BlockSize*j,BlockSize*sizeof(union hydro_comm),MPI_BYTE,ExportSendRecvTaskNo[j],TAG_RETURN,MPI_COMM_WORLD,&ExportSendRecv[j]);
//          //Mark it as inbound now
//          ExportSendRecvDirection[j]=-1;
//        }
//        else
//        {
//          //A receive finished, store it...
//          tmp = j*BlockSize;
//          block_len = tmp+HydroSend[tmp].in.from;
//          //printf("[%d] A receive from %d has finished, storing %d data stored in block number %d.\n",Task,ExportSendRecvTaskNo[j],block_len-tmp,j);
//          for(k=tmp;k<block_len;k++)
//          {
//            shade = HydroSend[k].out.what/MaxGas;
//            l = HydroSend[k].out.what%MaxGas;
//            Shadows[shade].G[l].ax += HydroSend[k].out.ax;
//            Shadows[shade].G[l].ay += HydroSend[k].out.ay;
//            Shadows[shade].G[l].az += HydroSend[k].out.az;
//            Shadows[shade].G[l].divv += HydroSend[k].out.divv;
//          }
//          //printf("[%d] Finished storing data.\n",Task);
//          //push this block back onto the stack
//          BlockStack[NfreeBlocks]=j;
//          NfreeBlocks++;
//          //This export is done now...
//          NexportSent--;
//        }
//      }
//    }
//    //No need to check WorkOrders since we can't be in local ourselves and all 
//    //feeders have also finished local at this point
//    if(NworkOrders)
//    {
//      MPI_Testsome(NTask,WorkOrders,&flag,done_idxs,MPI_STATUSES_IGNORE);
//      for(i=0;i<flag;i++)
//      {
//        NworkOrders--;
//        j=done_idxs[i];
//        //If this message finished, this processor must be done
//        if(IsWorking[j])
//        {
//          IsWorking[j]=0;
//          if(Shadows[j].G)
//            Nfeeders--;
//        }
//        //printf("[%d] Acknowledger or data sent to %d received nworking=%d.\n",Task,j,nworking_and_have_me);
//      }
//    }
//  }
//  //Cancel all the outstanding Irecvs for exported particles, no more
//  //can come in at this point
//  for(i=0;i<NexportFeeders;i++)
//  {
//    MPI_Cancel(ExportSendRecvForeign+i);
//  }
//  //Wait for them to actually finish
//  MPI_Waitall(NexportFeeders,ExportSendRecvForeign,MPI_STATUSES_IGNORE);
//}
//
//void holding_pattern()
//{
//  int i,j,k,l;
//  int done_idxs[NTask];
//  int flag;
//  int block_len;
//  int tmp;
//  int shade;
//  //I have no free blocks left, but I'm OK to do everything else
//  //printf("[%d] Entered holding pattern.\n",Task);
//  while(!NfreeBlocks)
//  {
//    /*
//     * Processing of foreign (i.e, particles exported to me or those sent back after I've worked on them) particles
//     */
//    MPI_Testsome(NexportFeeders,ExportSendRecvForeign,&flag,done_idxs,MPI_STATUSES_IGNORE);
//    for(i=0;i<flag;i++)
//    {
//      j=done_idxs[i];
//      //Was it a send that finished?
//      if(ExportSendRecvForeignDirection[j]==1)
//      {
//        //Can now re-use this block, so re-set a recv on it
//        MPI_Irecv(HydroRecv+j*BlockSize,BlockSize*sizeof(union hydro_comm),MPI_BYTE,ExportSendRecvForeignTaskNo[j],TAG_EXPORT,MPI_COMM_WORLD,&ExportSendRecvForeign[j]);
//        NexportReturn--;
//      }
//      else
//      {
//        //Now it was a receive that finished.
//        //How many did we get?
//        block_len = HydroRecv[j*BlockSize].in.from;
//        //Do the work
//        for(k=0;k<block_len;k++)
//        {
//          //Need to adjust the target to point at the right recv block
//          hydro_ngbs(k+j*BlockSize,1);
//        }
//        //Send it back.  Can't re-use the recv block until we have completed this
//        MPI_Issend(HydroRecv+j*BlockSize,block_len*sizeof(union hydro_comm),MPI_BYTE,ExportSendRecvForeignTaskNo[j],TAG_RETURN,MPI_COMM_WORLD,&ExportSendRecvForeign[j]);
//        NexportReturn++;
//      }
//      //Swap direction of the send/recv flag for this request
//      ExportSendRecvForeignDirection[j]*=-1;
//    }
//    /*
//     * Sending/Receiving of particles that I can't finish processing of locally.
//     */
//    if(NexportSent)
//    {
//      MPI_Testsome(NsendBlocks,ExportSendRecv,&flag,done_idxs,MPI_STATUSES_IGNORE);
//      for(i=0;i<flag;i++)
//      {
//        j=done_idxs[i];
//        //Was it a send that finished?
//        if(ExportSendRecvDirection[j]==1)
//        {
//          //If it was, then expect a receive back again
//          MPI_Irecv(HydroSend+BlockSize*j,BlockSize*sizeof(union hydro_comm),MPI_BYTE,ExportSendRecvTaskNo[j],TAG_RETURN,MPI_COMM_WORLD,&ExportSendRecv[j]);
//          //Mark it as inbound now
//          ExportSendRecvDirection[j]=-1;
//        }
//        else
//        {
//          //A receive finished, store it...
//          tmp = j*BlockSize;
//          block_len = tmp+HydroSend[tmp].in.from;
//          for(k=tmp;k<block_len;k++)
//          {
//            shade = HydroSend[k].out.what/MaxGas;
//            l = HydroSend[k].out.what%MaxGas;
//            Shadows[shade].G[l].ax += HydroSend[k].out.ax;
//            Shadows[shade].G[l].ay += HydroSend[k].out.ay;
//            Shadows[shade].G[l].az += HydroSend[k].out.az;
//            Shadows[shade].G[l].divv += HydroSend[k].out.divv;
//          }
//          //push this block back onto the stack
//          BlockStack[NfreeBlocks]=j;
//          NfreeBlocks++;
//          //This export is done now...
//          NexportSent--;
//        }
//      }
//    }
//    //Are we still doing local work or can we still get local work from others? 
//    if(Nfeeders || ChunkCounter<NChunks)
//    {
//      /*
//       * Check if any of the bits of the ack messages I sent have completed
//       * (i.e. have any other processors finished local processing too)
//       * or if a bit of local work I sent has completed.  Send any extra bits
//       * of local work off for processing if possible too.
//       */
//      MPI_Testsome(NTask,WorkOrders,&flag,done_idxs,MPI_STATUSES_IGNORE);
//      for(i=0;i<flag;i++)
//      {
//        NworkOrders--;
//        j=done_idxs[i];
//        //printf("[%d] Acknowledger or data sent to %d received.\n",Task,j);
//        //If we're sending it a new chunk of work, it must be finished.
//        if(IsWorking[j])
//        {
//          IsWorking[j]=0;
//          if(Shadows[j].G)
//            Nfeeders--;
//        }
//        //If it has a copy of me, send it some work
//        if(HasMe[j] && ChunkCounter<NChunks)
//        {
//          NworkOrders++;
//          //Send a new chunk of work
//          WorkSend[j*2]=((ChunkCounter*NPart[GAS])/NChunks)+Task*MaxGas;
//          WorkSend[j*2+1]=(((ChunkCounter+1)*NPart[GAS])/NChunks)+Task*MaxGas;
//          MPI_Issend(&WorkSend[j*2],2,MPI_INT,RankLocal[j],TAG_WORK,Shadows[Task].comm,&WorkOrders[j]);
//          //printf("[%d] Posting work to %d.\n",Task,j);
//          ChunkCounter++;
//          Ndrift+=WorkSend[j*2+1]-WorkSend[j*2];
//        }
//      }
//    }
//  }
//}
//
//void process_chunk(int start,int stop)
//{
//  int shade,i,j,k;
//  double tstart=MPI_Wtime();
//  double tcost;
//  shade=start/MaxGas;
//  start=start%MaxGas;
//  stop=stop%MaxGas;
//  //printf("[%d] Processing chunk using process %d from %d to %d.\n",Task,shade,start,stop);
//  for(i=start;i<stop;i++)
//  {
//    //Reset the export flag
//    for(j=0;j<NTask;j++)
//      Export[j]=0;
//
//
//
//    tcost=MPI_Wtime();
//    //Calculate the hydro force locally
//    hydro_ngbs(shade*MaxGas+i,0);
//    Shadows[shade].G[i].cost = MPI_Wtime()-tcost;
//    //Timings.hydro_idling_in_comm += Shadows[shade].G[i].cost;
//
//    //If the export flag is set, I have to export to this processor
//    //Rather than export the entire particle, just export what's 
//    //needed.
//    for(j=0;j<NTask;j++)
//    {
//      if(Export[j])
//      {
//        HydroSend[ActiveBlock[j]].in.x = Shadows[shade].G[i].x;
//        HydroSend[ActiveBlock[j]].in.y = Shadows[shade].G[i].y;
//        HydroSend[ActiveBlock[j]].in.z = Shadows[shade].G[i].z;
//        HydroSend[ActiveBlock[j]].in.vx = Shadows[shade].G[i].vpredx;
//        HydroSend[ActiveBlock[j]].in.vy = Shadows[shade].G[i].vpredy;
//        HydroSend[ActiveBlock[j]].in.vz = Shadows[shade].G[i].vpredz;
//        HydroSend[ActiveBlock[j]].in.drho_x = Shadows[shade].G[i].drho_x;
//        HydroSend[ActiveBlock[j]].in.drho_y = Shadows[shade].G[i].drho_y;
//        HydroSend[ActiveBlock[j]].in.drho_z = Shadows[shade].G[i].drho_z;
//        HydroSend[ActiveBlock[j]].in.hsml = Shadows[shade].G[i].hsml;
//        HydroSend[ActiveBlock[j]].in.density = Shadows[shade].G[i].density;
//        HydroSend[ActiveBlock[j]].in.dhsml = Shadows[shade].G[i].dhsml;
//        HydroSend[ActiveBlock[j]].in.pressure = Shadows[shade].G[i].pressure;
//        HydroSend[ActiveBlock[j]].in.zeta = Shadows[shade].G[i].zeta;
//        HydroSend[ActiveBlock[j]].in.what = shade*MaxGas+i;
//        HydroSend[ActiveBlock[j]].in.where = j;
//        HydroSend[ActiveBlock[j]].in.from = Task;
//        ActiveBlock[j]++;
//        if(ActiveBlock[j]%BlockSize==0)
//        {
//          //Block is finished, send it off
//          //What is the black number
//          k=(ActiveBlock[j]/BlockSize)-1;
//          //printf("[%d] Sending complete block from %d.\n",Task,j);
//          //Set first entry to encode block length
//          HydroSend[ActiveBlock[j]-BlockSize].in.from = BlockSize;
//          MPI_Issend(HydroSend+k*BlockSize,BlockSize*sizeof(union hydro_comm),MPI_BYTE,j,TAG_EXPORT,MPI_COMM_WORLD,&ExportSendRecv[k]);
//          ExportSendRecvTaskNo[k]=j;
//          ExportSendRecvDirection[k]=1;
//          NexportSent++;
//          //This block is finished, try and pop off a fresh one
//          if(!NfreeBlocks)
//          {
//            //No free blocks left, enter holding pattern until one is free
//            holding_pattern();
//          }
//          //Have a free block, pop it off and assign it
//          ActiveBlock[j]=BlockSize*BlockStack[NfreeBlocks-1];
//          NfreeBlocks--;
//        }
//      }
//    }
//  }
//  Timings.hydro_work += MPI_Wtime()-tstart;
//}
//
//void hydro_ngbs(int index,int mode)
//{
//  double h_a,rho_a,dhsml_a,pressure_a;
//  double pos_a[3];
//  double h2_a,fact_a,ax,ay,az;
//  double dw_ab;
//  double dx,dy,dz;
//  double sum,r,r2;
//  double vx_a,vy_a,vz_a,dvx,dvy,dvz;
//  double divv,divv_extra;
//  double q_ab;
//  double drho_x,drho_y,drho_z;
//  double h4_inv;
//#if GRAV>=2 && !defined CONSTANT_GAS_SOFT
//  double zeta_a;
//#endif
//  double tcost;
//  int current;
//  int ngbs,i,j,k;
//  int shade;
//  if(mode==0)
//  {
//    //Could be in any shadow domain
//    shade=index/MaxGas;
//    index=index%MaxGas;
//    pos_a[0] = Shadows[shade].G[index].x;
//    pos_a[1] = Shadows[shade].G[index].y;
//    pos_a[2] = Shadows[shade].G[index].z;
//    vx_a = Shadows[shade].G[index].vpredx;
//    vy_a = Shadows[shade].G[index].vpredy;
//    vz_a = Shadows[shade].G[index].vpredz;
//    drho_x = Shadows[shade].G[index].drho_x;
//    drho_y = Shadows[shade].G[index].drho_y;
//    drho_z = Shadows[shade].G[index].drho_z;
//    h_a = Shadows[shade].G[index].hsml;
//    rho_a = Shadows[shade].G[index].density;
//    dhsml_a = Shadows[shade].G[index].dhsml;
//    pressure_a = Shadows[shade].G[index].pressure;
//#if GRAV>=2 && !defined CONSTANT_GAS_SOFT
//    zeta_a = Shadows[shade].G[index].zeta;
//#endif
//  }
//  else
//  {
//    pos_a[0] = HydroRecv[index].in.x;
//    pos_a[1] = HydroRecv[index].in.y;
//    pos_a[2] = HydroRecv[index].in.z;
//    vx_a = HydroRecv[index].in.vx;
//    vy_a = HydroRecv[index].in.vy;
//    vz_a = HydroRecv[index].in.vz;
//    drho_x = HydroRecv[index].in.drho_x;
//    drho_y = HydroRecv[index].in.drho_y;
//    drho_z = HydroRecv[index].in.drho_z;
//    h_a = HydroRecv[index].in.hsml;
//    rho_a = HydroRecv[index].in.density;
//    dhsml_a = HydroRecv[index].in.dhsml;
//    pressure_a = HydroRecv[index].in.pressure;
//#if GRAV>=2 && !defined CONSTANT_GAS_SOFT
//    zeta_a = HydroRecv[index].in.zeta;
//#endif
//  }
//  h2_a = h_a*h_a*KER_SUPPORT*KER_SUPPORT;
//  h4_inv = 1.0/(h_a*h_a*h_a*h_a);
//  fact_a = PcleMass * h4_inv / (dhsml_a );
//  current = 0;
//  ax = ay = az=0;
//  divv = divv_extra =0;
//  do
//  {
//    tcost=MPI_Wtime();
//    ngbs = find_ngbs(pos_a,h_a*KER_SUPPORT,&current,!mode);
//    Timings.hydro_idling_in_comm += tcost-MPI_Wtime();
//    for(i=0;i<ngbs;i++)
//    {
//      j=Ngbs[i]%MaxGas;
//      k=Ngbs[i]/MaxGas;
//      dx = pos_a[0] - Shadows[k].G[j].x;
//      dy = pos_a[1] - Shadows[k].G[j].y;
//      dz = pos_a[2] - Shadows[k].G[j].z;
//      r2 = dx*dx + dy*dy + dz*dz;
//      if(r2<h2_a && r2>0)
//      {
//        r = sqrt(r2);
//        q_ab = r/h_a;
//        dw_ab = dkernel(q_ab);
//        dvx = vx_a - Shadows[k].G[j].vpredx;
//        dvy = vy_a - Shadows[k].G[j].vpredy;
//        dvz = vz_a - Shadows[k].G[j].vpredz;
//        //Add the normal h_a contribution to particle a
//        sum = fact_a * dw_ab / r;
//        //Add the correction due to adaptive gravitational softening
//#if GRAV>=2 && !defined CONSTANT_GAS_SOFT
//        sum *= (pressure_a/(rho_a*rho_a))+0.5*Params.G*zeta_a;
//#else
//        sum *= (pressure_a/(rho_a*rho_a));
//#endif
//        ax -= sum*dx;
//        ay -= sum*dy;
//        az -= sum*dz;
//        //The divv calculation...
//        divv -= dw_ab  * (dvx*dx+dvy*dy+dvz*dz)/ G[j].density;
//        divv_extra -= ( dvx*drho_x + dvy*drho_y * dvz*drho_z) * (kernel(q_ab) + (q_ab*dw_ab/3)) / Shadows[k].G[j].density;
//        //Rather than trying to gather all the additions to 
//        //particle a due to the h_b terms, we instead add 
//        //all the h_a terms to particle b now
//        //Essentially we're just explicitly satisfying newton's
//        //second law here
//        Shadows[k].G[j].ax += sum*dx;
//        Shadows[k].G[j].ay += sum*dy;
//        Shadows[k].G[j].az += sum*dz;
//      }
//    }
//  }while(current);
//
//  if(mode==0)
//  {
//    Shadows[shade].G[index].ax += ax;
//    Shadows[shade].G[index].ay += ay;
//    Shadows[shade].G[index].az += az;
//    Shadows[shade].G[index].divv += divv;
//    Shadows[shade].G[index].divv_extra += divv_extra;
//  }
//  else
//  {
//    mode = HydroRecv[index].in.where;
//    i = HydroRecv[index].in.from;
//    ngbs = HydroRecv[index].in.what;
//    HydroRecv[index].out.what = ngbs;
//    HydroRecv[index].out.where = mode;
//    HydroRecv[index].out.from = i;
//    HydroRecv[index].out.ax = ax;
//    HydroRecv[index].out.ay = ay;
//    HydroRecv[index].out.az = az;
//    HydroRecv[index].out.divv = divv;
//    HydroRecv[index].out.divv_extra = divv_extra;
//  }
//}
//
//
