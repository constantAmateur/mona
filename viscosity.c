/*
 * =====================================================================================
 *
 *       Filename:  viscosity.c
 *
 *    Description:  Functions needed to calculate the vicious (i.e., dissipative) forces
 *                  and changes in energy.
 *
 *        Version:  0.6.3
 *        Created:  20/08/13 17:34:53
 *
 *         Author:  Matthew D. Young, my304@ast.cam.ac.uk
 *        Company:  Institute of Astronomy, Cambridge, UK
 *        License:  GNU General Public License
 *      Copyright:  Copyright (c) 2014, Matthew D. Young
 *
 * =====================================================================================
 */

#include "variables.h"
#include "functions.h"

//int visc_comm_loop(int *nsend_local,int no_more_local)
//{
//  int offsets[NTask];
//  int send_idxs[NTask];
//  int keep_mem[NTask];
//  int nsend_hi,nsend_data,nsend_return;
//  int send_block,recv_block,finish;
//  int flag,nexport;
//  int nrecv,done;
//  int i,j,k;
//  int msrc,mtag;
//  int all_finish;
//  int tgt;
//  double tbalance;
//  tbalance=0;
//  //tstart=MPI_Wtime();
//
//  MPI_Status status;
//  MPI_Request send_requests[NTask];
//  MPI_Request data_send_requests[NTask];
//  MPI_Request data_return_requests[NTask];
//  MPI_Request complete_requests[NTask-1];
//
//
//
//  nexport=0;
//  for(i=0;i<NTask;i++)
//    nexport+=nsend_local[i];
//
//  //printf("[%d] Entered comm phase with %d particles to send.\n",Task,nexport);
//  //Sort it so things are back-to-back in buffer
//  qsort(ViscSend,nexport,sizeof(union visc_comm),visc_buffer_sort);
//  //What are the offsets 
//  offsets[0]=0;
//  for(i=1;i<NTask;i++)
//  {
//    offsets[i]=offsets[i-1]+nsend_local[i-1];
//  }
//  //Send greeting to dest-o-nation
//  nsend_hi=0;
//  nsend_data=0;
//  nsend_return=0;
//  for(i=0;i<NTask;i++)
//  {
//    keep_mem[i]=0;
//    data_send_requests[i] = MPI_REQUEST_NULL;
//    data_return_requests[i] = MPI_REQUEST_NULL;
//    if(nsend_local[i])
//    {
//      //printf("[%d] Sending greeting to %d.\n",Task,i);
//      MPI_Issend(0,0,MPI_INT,i,0,MPI_COMM_WORLD,&send_requests[i]);
//      nsend_hi++;
//    }
//    else
//    {
//      send_requests[i] = MPI_REQUEST_NULL;
//    }
//  }
//  //if(no_more_local)
//  //  printf("[%d] Entered the final comm phase.\n",Task);
//  //Now the main comm loop
//  recv_block=0;
//  send_block=0;
//  finish=0;
//  all_finish=0;
//  //Timings.bleh += MPI_Wtime()-tstart;
//  //tstart=MPI_Wtime();
//  while(1)
//  {
//    //tstart=MPI_Wtime();
//    //printf("[%d] This loop we have nterm=%d,nsend_data/hi/return = %d/%d/%d, recv_block=%d,send_block=%d.\n",Task,nterm,nsend_data,nsend_hi,nsend_return,recv_block,send_block);
//    //Are there any outstanding hi messages?
//    if(nsend_hi)
//    {
//      MPI_Testsome(NTask,send_requests,&done,send_idxs,MPI_STATUSES_IGNORE);
//      if(done)
//      {
//        //At least one hello message was received!
//        //For each of them, post the actual data
//        for(i=0;i<done;i++)
//        {
//          tgt = send_idxs[i];
//          //printf("[%d] Greetings posted to %d was received.\n",Task,tgt);
//          //printf("[%d] Send at offset %d of %d things going to %d and being stored in request slot %d.\n",Task,offsets[tgt],nsend_local[tgt],tgt,nsend_data);
//          MPI_Issend(ViscSend+offsets[tgt],nsend_local[tgt]*sizeof(union visc_comm),MPI_BYTE,tgt,1,MPI_COMM_WORLD,&data_send_requests[tgt]);
//          //printf("[%d] Send data to %d for processing.\n",Task,tgt);
//          nsend_data++;
//        }
//        nsend_hi -= done;
//        //printf("[%d] nsend_hi/data = %d,%d\n",Task,nsend_hi,nsend_data);
//        //for(i=0;i<NTask;i++)
//        //{
//        //  printf("[%d] Data/hi_pending[%d] = %d,%d.\n",Task,i,data_pending[i],hi_pending[i]);
//        //}
//      }
//    }
//    //Are there any outstanding requests to send data 
//    if(nsend_data)
//    {
//      //printf("[%d] Testing for completion of data requests nsend=%d.\n",Task,nsend_data);
//      MPI_Testsome(NTask,data_send_requests,&done,send_idxs,MPI_STATUSES_IGNORE);
//      //printf("[%d] Test completed done=%d.\n",Task,done);
//      if(done)
//      {
//        //At least one finished!
//        send_block += done;
//        //for(i=0;i<done;i++)
//        //{
//        //  printf("[%d] Data sent for processing to %d was received.\n",Task,send_idxs[i]);
//        //}
//        nsend_data -= done;
//        //printf("[%d] nsend_data = %d.\n",Task,nsend_data);
//        //for(i=0;i<NTask;i++)
//        //  printf("[%d] data_pending[%d] = %d.\n",Task,i,data_pending[i]);
//      }
//    }
//    //Are there any outstanding requests to return data
//    if(nsend_return)
//    {
//      //printf("[%d] Return sends test.\n",Task);
//      //Are there any outstanding requests to send data 
//      MPI_Testsome(NTask,data_return_requests,&done,send_idxs,MPI_STATUSES_IGNORE);
//      if(done)
//      {
//        //At least one finished!
//        //for(i=0;i<done;i++)
//        //{
//        //  printf("[%d] Processed data originally from %d and sent back to %d has been received.\n",Task,send_idxs[i],send_idxs[i]);
//        //}
//        nsend_return -= done;
//      }
//    }
//    //Finished everything I can locally?
//    if(no_more_local && all_finish==0 && nsend_data==0 && nsend_hi==0 && nsend_return==0 && recv_block==0 && send_block==0)
//    {
//      tbalance=MPI_Wtime();
//      //printf("[%d] I'm ready to terminate, sending term to all other processors.\n",Task);
//      //Only do this once, send out the "I've finished everything" message to everyone
//      j=0;
//      for(i=0;i<NTask;i++)
//      {
//        if(i==Task)
//        {
//          continue;
//        }
//        MPI_Issend(0,0,MPI_INT,i,3,MPI_COMM_WORLD,&complete_requests[j]);
//        j++;
//      }
//      //We're now ready to terminate
//      all_finish=1;
//      NTerm++;
//    }
//    //Have we received the termination message from every
//    //process?
//    if(NTerm==NTask)
//    {
//      //printf("[%d] All termination flags received.  Ending.\n",Task);
//      //All termination flags have been received, so end
//      MPI_Waitall(NTask-1,complete_requests,MPI_STATUSES_IGNORE);
//      mtag=0;
//      Timings.balance += MPI_Wtime()-tbalance;
//      break;
//    }
//    //Can we finish and return to work?
//    if(no_more_local==0 && finish && nsend_data==0 && nsend_return==0 && recv_block==0 && send_block==0)
//    {
//      //printf("[%d] Trying to return to local work.\n",Task);
//      for(i=0;i<NTask;i++)
//      {
//        if(send_requests[i]!=MPI_REQUEST_NULL)
//        {
//          printf("[%d] Cancelling greetings message to %d.\n",Task,i);
//          MPI_Cancel(&send_requests[i]);
//          MPI_Wait(&send_requests[i],&status);
//          MPI_Test_cancelled(&status,&flag);
//          if(!flag)
//          {
//            //printf("[%d] Cancel failed, so sending data to %d.\n",Task,i);
//            MPI_Issend(ViscSend+offsets[i],nsend_local[i]*sizeof(union visc_comm),MPI_BYTE,i,1,MPI_COMM_WORLD,&data_send_requests[i]);
//            //printf("[%d] Sent data to %d for processing.\n",Task,i);
//            nsend_data++;
//          }
//          else
//          {
//            //So we know this block is to be shifted around
//            keep_mem[i]=1;
//          }
//          nsend_hi--;
//        }
//      }
//      //Whatever happens, the hi array should be empty now
//      if(nsend_hi!=0)
//        kill(908);
//      //We still ok to finish?
//      if(nsend_data==0)
//      {
//        //printf("[%d] Clearing memory and finishing.\n",Task);
//        //Move the memory around
//        nexport=0;
//        for(i=0;i<NTask;i++)
//        {
//          if(keep_mem[i])
//          {
//            memmove(ViscSend+nexport,ViscSend+offsets[i],sizeof(union visc_comm)*nsend_local[i]);
//            nexport += nsend_local[i];
//          }
//          else
//          {
//            nsend_local[i]=0;
//          }
//        }
//        //Finish!
//        mtag=nexport;
//        break;
//      }
//    }
//    //Timings.bleh += MPI_Wtime()-tstart;
//
//    //CHANGED U>P TO HERE!!!
//
//
//    //Handle receiving data first
//    //Check them in special order so that we don't
//    //get stuck if we can't accept tag=1
//    //Would be nicer if we defined a tag-order array, but eh
//
//    //nanosleep(&Wait,NULL);
//
//    //tstart=MPI_Wtime();
//    mtag=0;
//    MPI_Iprobe(MPI_ANY_SOURCE,0,MPI_COMM_WORLD,&flag,&status);
//    if(!flag)
//    {
//      mtag=2;
//      MPI_Iprobe(MPI_ANY_SOURCE,2,MPI_COMM_WORLD,&flag,&status);
//      if(!flag)
//      {
//        mtag=3;
//        MPI_Iprobe(MPI_ANY_SOURCE,3,MPI_COMM_WORLD,&flag,&status);
//        if(!flag && nsend_return==0)
//        {
//          mtag=1;
//          MPI_Iprobe(MPI_ANY_SOURCE,1,MPI_COMM_WORLD,&flag,&status);
//        }
//      }
//    }
//    //Timings.bleh += MPI_Wtime()-tstart;
//    //MPI_Iprobe(MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&flag,&status);
//    if(flag)
//    {
//      //tstart=MPI_Wtime();
//      msrc = status.MPI_SOURCE;
//      //Was it a hello message
//      if(mtag==0)
//      {
//        //Complete the message receipt
//        MPI_Recv(0,0,MPI_INT,msrc,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
//        //printf("[%d] Received greetings from %d.\n",Task,msrc);
//        //Now we're expecting to be sent some data, so better wait for it
//        recv_block++;
//      }
//      //Was it a block of data to do work on?
//      //Can't receive while I have old data still
//      //to be sent out
//      else if(mtag==1)
//      {
//        if(nsend_return!=0)
//        {
//          kill(908);
//          //printf("[%d] Skipping receive of data from %d because buffer isn't ready.\n",Task,msrc);
//        }
//        //Get it
//        MPI_Get_count(&status,MPI_BYTE,&nrecv);
//        MPI_Recv(ViscRecv,nrecv,MPI_BYTE,msrc,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
//        //printf("[%d] Received data for processing from %d.\n",Task,msrc);
//        //One less thing to block for
//        recv_block--;
//        //Do stuff with it
//        for(j=0;j<nrecv/sizeof(union visc_comm);j++)
//        {
//          visc_ngbs(j,1);
//        }
//        //Send the results back
//        MPI_Issend(ViscRecv,nrecv,MPI_BYTE,msrc,2,MPI_COMM_WORLD,&data_return_requests[msrc]);
//        //printf("[%d] Sent processed data back to %d.\n",Task,msrc);
//        nsend_return++;
//      }
//      //Was it a block of data with work done on it to be stored?
//      else if(mtag==2)
//      {
//        //Get them back again
//        MPI_Get_count(&status,MPI_BYTE,&nrecv);
//        if(nrecv!=nsend_local[msrc]*sizeof(union visc_comm))
//        {
//          printf("[%d] I've made a huge mistake...Returning data \n",Task);
//          kill(908);
//        }
//        MPI_Recv(ViscSend+offsets[msrc],nrecv,MPI_BYTE,msrc,2,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
//        //printf("[%d] Received data with processing done on it back from %d.\n",Task,msrc);
//        //I've got something back!  Hooray!
//        finish=1;
//        send_block--;
//        //Store the results locally
//        for(j=offsets[msrc];j<offsets[msrc]+nrecv/sizeof(union visc_comm);j++)
//        {
//          k=ViscSend[j].out.what;
//          G[k].ax += ViscSend[j].out.ax;
//          G[k].ay += ViscSend[j].out.ay;
//          G[k].az += ViscSend[j].out.az;
//          G[k].dK += ViscSend[j].out.dK;
//          if(ViscSend[j].out.max_vsig > G[k].vsig)
//            G[k].vsig = ViscSend[j].out.max_vsig;
//        }
//      }
//      else if(mtag==3)
//      {
//        //Termination signal received.  Update global 
//        //marker
//        MPI_Recv(0,0,MPI_INT,msrc,3,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
//        //printf("[%d] Received termination flag from %d.\n",Task,msrc);
//        NTerm++;
//        //if(!no_more_local || !nterm)
//        //{
//        //  printf("[%d] Skipping receipt of termination flag from %d because we either still have local work to do or have not entered the termination phase ourselves.\n",Task,msrc);
//        //  continue;
//        //}
//        //Received a termination flag from someone.  Only
//        //really care about this if I'm it's possible for
//        //me to terminate too
//      }
//      else
//      {
//        printf("[%d] I've made a huge mistake...Unknown tag\n",Task);
//        kill(908);
//      }
//      //Timings.bleh += MPI_Wtime()-tstart;
//    }
//  }
//  //Timings.bleh += MPI_Wtime()-tstart;
//
//
//  return mtag;
//}



void viscosity(void)
{
  int i,j;
  //Initialise anything we're going to set
  //If we forget to do this these quantities
  //will accumulate over timesteps
  for(j=0;j<NTask;j++)
  {
    if(Shadows[j].G)
    {
      for(i=0;i<NGas[j];i++)
      {
        Shadows[j].G[i].dK=0;
      }
    }
  }
 
  ngb_search(ViscSend,ViscRecv,sizeof(union visc_comm),BufferSizeVisc,
      visc_ngbs,
      viscosity_export_return,
      visc_buffer_sort);

  //Finish off the change in energy
  for(i=0;i<NTask;i++)
  {
    if(Shadows[i].G)
    {
      for(j=0;j<NGas[i];j++)
      {
        Shadows[i].G[j].dK *= (Params.gamma-1.0)/pow(Shadows[i].G[j].density,Params.gamma-1.0);
      }
    }
  }
  //Cooling, if needed
#ifdef COOLING
  for(i=0;i<NPart[GAS];i++)
  {
    G[i].dK -= G[i].K * sqrt(Params.G*S[0].m/(G[i].R*G[i].R*G[i].R))/ Params.beta_cool;
    //Can't cool by more than half in a time step...
    //This check needs to be made after a cast of the shadows has been made
    //if(-Dt*G[i].dK > 0.5*G[i].K)
    //{
    //  G[i].dK = -0.5*G[i].K/Dt;
    //}
  }
#endif
}


void viscosity_export_return(void *data)
{
  union visc_comm *part = data;
  int k;
  k=part->out.what;
  G[k].ax += part->out.ax;
  G[k].ay += part->out.ay;
  G[k].az += part->out.az;
  G[k].dK += part->out.dK;
  G[k].nops += part->out.nops;
  if(part->out.max_vsig > G[k].vsig)
    G[k].vsig = part->out.max_vsig;
}

void visc_ngbs(void *index,void *export, int mode)
{
  double h_a,rho_a,pressure_a;
  double pos_a[3];
  double h2_a,ax,ay,az;
  double dx,dy,dz;
  double r;
  double vx_a,vy_a,vz_a,dvx,dvy,dvz;
  double dK;
  double mu_ab;
  double fact,vdotr,dw_ab,rho_ab;
  double visc;
  double vsig,max_vsig;
  double q_ab,pi_ab;
  double h;
  double cs_a,cs_b;
  PCLE_REF *ngbs;
  int owner,mob,count;
  int current;
  int reget;
  int nngbs,i,j,k;
  int export_offset[NTask];
  int owners;
  int nops=0;
  const struct node *cnode;
  //Cast the memory locations of the particle to search for (index/remote) and 
  //where to store any exported particles (storage)
  struct gas *local = NULL;
  union visc_comm *remote;
  union visc_comm *storage = NULL;

  if(mode)
  {
    //Cast pointer as needed
    remote = index;
    pos_a[0] = remote->in.x;
    pos_a[1] = remote->in.y;
    pos_a[2] = remote->in.z;
    vx_a = remote->in.vx;
    vy_a = remote->in.vy;
    vz_a = remote->in.vz;
    h_a = remote->in.hsml;
    rho_a = remote->in.density;
    pressure_a = remote->in.pressure;
    nngbs = remote->in.nnodes;
    ngbs = remote->in.nodes;
  }
  else
  {
    //Cast index pointer to appropriate type
    local = index;
    storage = export;
    pos_a[0] = local->x;
    pos_a[1] = local->y;
    pos_a[2] = local->z;
    vx_a = local->vpredx;
    vy_a = local->vpredy;
    vz_a = local->vpredz;
    h_a = local->hsml;
    rho_a = local->density;
    pressure_a = local->pressure;
    nngbs = local->nngbs;
    ngbs = local->ngbs;
    for(i=0;i<NTask;i++)
      export_offset[i]=-1;
  }
  cs_a = sqrt(Params.gamma * pressure_a / rho_a);
  h2_a = h_a*h_a*KER_SUPPORT*KER_SUPPORT;
  h = h_a;
  fact = 0.5*PcleMass / H_TO_THE_DPLUS1;
  current = 0;
  ax = ay = az=0;
  dK =0;
  max_vsig=0;
  //printf("[%d] Starting the game with %d ngbs.\n",Task,nngbs);
  //Outer loop in case we over-fill buffer
  do
  {
    //printf("[%d] Hey we're going around the loop with current=%d.\n",Task,current);
    //Do we already have a nice list of nodes/ngbs?
    //Second check is if we're looping over find_ngbs we 
    //don't accidentally end up skipping subsequent calls on overflow
    if(!nngbs || ngbs == Ngbs)
    {
      //printf("[%d] Mode=%d, Don't have ngbs, so calculating them...\n",Task,mode);
      //No we don't, so get one...
      nngbs = find_ngbs(pos_a,KER_SUPPORT*h_a,&current,mode);
      //Can we store it?
      //Not if on export, we don't have room, find_ngbs overflowed or 
      //we're not in the first run through the do loop
      if(!mode && nngbs<NGB_LIST_LEN && current==-1 && !reget)
      {
        //printf("[%d] Storing %d ngbs.\n",Task,nngbs);
        //So store it then
        memmove(local->ngbs,Ngbs,sizeof(int)*nngbs);
        local->nngbs=nngbs;
      }
      //Set pointer for work now
      ngbs = Ngbs;
      reget=1;
    }
    else
    {
      //So we exit the loop OK.
      current=-1;
    }
    ////printf("[%d] Now looping through %d neighbours.\n",Task,nngbs);
    //ngbs now has a list of nodes/neighbours, do stuff with it...
    for(i=0;i<nngbs;i++)
    {
      //OK.  The ngb buffer is assumed to have some special properties we're going to
      //exploit.  Firstly, if it's a particle, then we must have a copy of it 
      //(strongly local if on export), otherwise how did we find it?  If it's a node
      //and we're local, it must be exported.If it's a node and we're on export, it 
      //must be opened up (otherwise why was it sent to us)?  The debug section tests
      //these assumptions.
#if DEBUG>=1
      if(ngbs[i]<0)
      {
        cnode=Root-ngbs[i];
        if(mode)
        {
          //If it's on export, this must be strongly local
          if((cnode->owner>0 && cnode->owner!=Task) ||
             (cnode->owner<0 && !get_bit(Task,cnode->owners)))
          {
            pprintf("Neighbour list contains element %d = %d which is not local even though we're in export mode.\n",i,ngbs[i]);
            kill(ERROR_SANITY);
          }
        }
        else
        {
          //Can't possibly be entirely locally owned, otherwise we should have 
          //its contents rather than the node
          j=0;
          //Flag that we've found a foreigner
          k=0;
          do
          {
            owner = cnode->owner<0 ? (int) (cnode->extra[j].mob/MaxGas) : cnode->owner;
            //This is foreign, as at least one of them must be
            if(!Shadows[owner].G)
              k=1;
            j++;
          }while(j<(-cnode->owner) && !k);
          if(!k)
          {
            pprintf("Neighbour %d was %d, which is a node that we have entirely locally and so should have been opened up given we're in local mode.\n",i,ngbs[i]);
          kill(ERROR_SANITY);
          }
        }
      }
      else
      {
        //It's a particle, it had better be local 
        //It must be strongly local on export
        owner=ngbs[i]/MaxGas;
        if(owner!=Task && (mode || !Shadows[owner].G))
        {
          pprintf("With mode %d neighbour %d was particle %d, which is not local.\n",mode,i,ngbs[i]);
          kill(ERROR_SANITY);
        }
      }
#endif
      //Is it a particle or a node?
      cnode = ngbs[i]<0 ? Root-ngbs[i] : NULL;
      //This will ensure exit of the do-while loop for particles
      owners = ngbs[i]<0 ? cnode->owner : 1;
      j = 0;
      //Loop for multiple ownership
      do
      {
        if(!cnode)
        {
          owner = ngbs[i]/MaxGas;
          mob = ngbs[i]%MaxGas;
          count=1;
        }
        else if(owners<0)
        {
          owner = (int) (cnode->extra[j].mob/MaxGas);
          mob = (int) (cnode->extra[j].mob%MaxGas);
          count = cnode->extra[j].count;
        }
        else
        {
          owner = cnode->owner;
          mob = cnode->mob;
          count = cnode->count;
        }
        //Open up if it's a particle (always local) or if it's export mode and
        //this is the local part
        if(!cnode || (mode && owner==Task))
        {
          //printf("[%d] Cracking open on loop %d owner %d mob %d count %d and extracting pcles.\n",Task,j,owner,mob,count);
          //OK, open up the node and extract the goey particle centre
          for(k=mob;k<mob+count;k++)
          {
            nops++;
            dx = pos_a[0] - Shadows[owner].G[k].x;
            dy = pos_a[1] - Shadows[owner].G[k].y;
            dz = pos_a[2] - Shadows[owner].G[k].z;

            r = dx*dx + dy*dy + dz*dz;
            if(r<h2_a && r>0)
            {
              dvx = vx_a - Shadows[owner].G[k].vpredx ;
              dvy = vy_a - Shadows[owner].G[k].vpredy ;
              dvz = vz_a - Shadows[owner].G[k].vpredz ;
              vdotr = dvx*dx+dvy*dy+dvz*dz;
              cs_b = sqrt(Params.gamma * Shadows[owner].G[k].pressure / Shadows[owner].G[k].density);
              if(vdotr<0)
              {
                r = sqrt(r);
                q_ab = r/h_a;
                dw_ab = dkernel(q_ab);
                mu_ab = vdotr/r;
                vsig = cs_a + cs_b -3*mu_ab;
                rho_ab = 0.5*(rho_a+Shadows[owner].G[k].density);
                pi_ab = -0.5*Params.alpha_sph * vsig*mu_ab / rho_ab;
                visc = fact * pi_ab * dw_ab;
                //Viscosity limiter, prevent changes of greater than line of site
                //velocity difference.  Momentum still conserved if we're limited...
                if(0&&Dt && visc < (mu_ab/(8*Dt)))
                {
                  visc = mu_ab / (8*Dt*r);
                  //Not sure what to set the signal velocity to in this case...
                  //vsig = cs_a + cs_b;
                }
                else
                {
                  visc /= r;
                }
                ax -= visc*dx;
                ay -= visc*dy;
                az -= visc*dz;
                dK += 0.5 * visc * vdotr;
                //The foreign contribution, necessary for maintaining symmetry
                Shadows[owner].G[k].ax += visc*dx;
                Shadows[owner].G[k].ay += visc*dy;
                Shadows[owner].G[k].az += visc*dz;
                Shadows[owner].G[k].dK += 0.5*visc*vdotr;
              }
              else
              {
                vsig = cs_a+cs_b;
              }
              if(vsig>max_vsig)
                max_vsig=vsig;
            }
          }
        }
        //Only store the export if we're not on export, in which case
        //it must always be stored
        //The second condition is needed because the neighbour search
        //will return locally owned particles of a multiply owned node
        //and the node itself (if it is has foreign content) we obviously
        //don't want to try export the foreign content to ourselves or
        //our shadows hence the second condition.
        else if(!mode && !Shadows[owner].G)
        {
          //Establish export of this node to processor whatever
          if(export_offset[owner]<0)
          {
            export_offset[owner]=N_Export;
            storage[N_Export].in.x = local->x;
            storage[N_Export].in.y = local->y;
            storage[N_Export].in.z = local->z;
            storage[N_Export].in.vx = local->vpredx;
            storage[N_Export].in.vy = local->vpredy;
            storage[N_Export].in.vz = local->vpredz;
            storage[N_Export].in.hsml = local->hsml;
            storage[N_Export].in.density = local->density;
            storage[N_Export].in.pressure = local->pressure;
            storage[N_Export].in.what = (int) (local-G);
            storage[N_Export].in.where = owner;
            storage[N_Export].in.from = Task;
            storage[N_Export].in.nodes[0] = ngbs[i];
            storage[N_Export].in.nnodes = 1;
            N_Export++;
            N_Send_Local[owner]++;
          }
          else
          {
            if(storage[export_offset[owner]].in.nnodes == NGB_LIST_LEN)
            {
              //printf("[%d] Too many to export :(\n",Task);
              storage[export_offset[owner]].in.nnodes = 0;
            }
            //Don't add anything if we can't fit it all
            else if(storage[export_offset[owner]].in.nnodes)
            {
              //printf("[%d] Sup!  Adding in extra node to send, up to nnodes = %d.\n",Task,storage[export_offset[owner]].in.nnodes);
              //Add in the new node
              storage[export_offset[owner]].in.nodes[storage[export_offset[owner]].in.nnodes] = ngbs[i];
              storage[export_offset[owner]].in.nnodes++;
            }
          }
        }
        j++;
      }while(j<(-owners));
    }
  }while(current!=-1);

  if(mode)
  {
    mode = remote->in.where;
    i = remote->in.from;
    nngbs = remote->in.what;
    remote->out.what = nngbs;
    remote->out.where = mode;
    remote->out.from = i;
    remote->out.ax = ax;
    remote->out.ay = ay;
    remote->out.az = az;
    remote->out.dK = dK;
    remote->out.max_vsig = max_vsig;
    remote->out.nops=nops;
  }
  else
  {
    local->ax += ax;
    local->ay += ay;
    local->az += az;
    local->dK += dK;
    local->nops += nops;
    if(max_vsig > local->vsig)
      local->vsig = max_vsig;
  }
}


//void viscosity(void)
//{
//  int nexport,i,j;
//  int nsend_local[NTask];
//#ifdef COOLING
//  double omega;
//#endif
//#if DEBUG>=2
//  double new[3];
//#endif
//  //double tstart = MPI_Wtime();
//  //Initialise anything we're going to set
//  //If we forget to do this these quantities
//  //will accumulate over timesteps
//  for(j=0;j<NTask;j++)
//  {
//    if(Shadows[j].G)
//    {
//      for(i=0;i<NGas[j];i++)
//      {
//        Shadows[j].G[i].dK=0;
//      }
//    }
//  }
//  //for(i=0;i<NTask;i++)
//  //{
//  //  MPI_Barrier(MPI_COMM_WORLD);
//  //  if(Task==i)
//  //  {
//  //    if(Shadows[0].G)
//  //    {
//  //      printf("[%d] Shadows[0].G Time = %g.\n",Task,Time);
//  //      for(j=0;j<NGas[0];j++)
//  //      {
//  //        printf("[%d] Shadows[%d].G[%d].pressure/density,pos,vel = %g/%g,(%g,%g,%g),(%g,%g,%g)\n",Task,0,j,Shadows[0].G[j].pressure,Shadows[0].G[j].density,Shadows[0].G[j].x,Shadows[0].G[j].y,Shadows[0].G[j].z,Shadows[0].G[j].vpredx,Shadows[0].G[j].vpredy,Shadows[0].G[j].vpredz);
//  //      }
//  //    }
//  //  }
//  //  MPI_Barrier(MPI_COMM_WORLD);
//  //}
//
//
//  NTerm=0;
//  for(i=0;i<NTask;i++)
//  {
//    nsend_local[i]=0;
//  }
//  nexport=0;
//
//  for(i=0;i<NPart[GAS];i++)
//  {
//    //Reset the export flag
//    for(j=0;j<NTask;j++)
//      Export[j]=0;
//      //Calculate the viscosity force locally
//    visc_ngbs(i,0);
//
//    //If the export flag is set, I have to export to this processor
//    //Rather than export the entire particle, just export what's 
//    //needed.
//    for(j=0;j<NTask;j++)
//    {
//      if(Export[j])
//      {
//        ViscSend[nexport].in.x = G[i].x;
//        ViscSend[nexport].in.y = G[i].y;
//        ViscSend[nexport].in.z = G[i].z;
//        ViscSend[nexport].in.vx = G[i].vpredx;
//        ViscSend[nexport].in.vy = G[i].vpredy;
//        ViscSend[nexport].in.vz = G[i].vpredz;
//        ViscSend[nexport].in.hsml = G[i].hsml;
//        ViscSend[nexport].in.density = G[i].density;
//        ViscSend[nexport].in.pressure = G[i].pressure;
//        ViscSend[nexport].in.what = i;
//        ViscSend[nexport].in.where = j;
//        ViscSend[nexport].in.from = Task;
//        nexport++;
//        nsend_local[j]++;
//        if(nexport==BufferSizeVisc)
//        {
//          //Timings.av += MPI_Wtime()-tstart;
//          //tstart = MPI_Wtime();
//          nexport=visc_comm_loop(nsend_local,0);
//          //Timings.comm += MPI_Wtime() - tstart;
//          //tstart = MPI_Wtime();
//        }
//      }
//    }
//  }
//  //Timings.av += MPI_Wtime()-tstart;
//  //tstart = MPI_Wtime();
//  nexport=visc_comm_loop(nsend_local,1);
//  //Timings.comm += MPI_Wtime() - tstart;
// 
//  //Finish off the change in energy
//  for(i=0;i<NTask;i++)
//  {
//    if(Shadows[i].G)
//    {
//      for(j=0;j<NGas[i];j++)
//      {
//        Shadows[i].G[j].dK *= (Params.gamma-1.0)/pow(Shadows[i].G[j].density,Params.gamma-1.0);
//      }
//    }
//  }
//#ifdef COOLING
//  for(i=0;i<NPart[GAS];i++)
//  {
//    //Cooling
//    omega = sqrt(Params.G*S[0].m/(G[i].R*G[i].R*G[i].R));
//    G[i].dK -= G[i].K * omega / Params.beta_cool;
//    //Can't cool by more than half in a time step...
//    //This check needs to be made after a cast of the shadows has been made
//    //if(-Dt*G[i].dK > 0.5*G[i].K)
//    //{
//    //  G[i].dK = -0.5*G[i].K/Dt;
//    //}
//  }
//#endif
//#if DEBUG>=2
//  gather_acceleration_shadows();
//  new[0]=new[1]=new[2]=0;
//  //Test of newtons third law
//  for(i=0;i<NPart[GAS];i++)
//  {
//    new[0]+=PcleMass*G[i].ax;
//    new[1]+=PcleMass*G[i].ay;
//    new[2]+=PcleMass*G[i].az;
//  }
//  MPI_Allreduce(MPI_IN_PLACE,new,3,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
//  for(i=0;i<NPart[SINK];i++)
//  {
//    new[0]+=S[i].m * S[i].ax;
//    new[1]+=S[i].m * S[i].ay;
//    new[2]+=S[i].m * S[i].az;
//  }
//  if(Task==0)
//    printf("Total force after viscosity is %g,%g,%g.\n",new[0],new[1],new[2]);
//#endif
//}

//void viscosity(void)
//{
//  int i,j,k;
//  int n_export=0;
//  int n_done;
//  int n_grp;
//  int n_send_local[NTask];
//  int n_send_global[NTask*NTask];
//  int n_offsets[NTask];
//  int n_buffer[NTask];
//  int maxfill;
//  int remote_partner;
//  int level;
//
//  //Initialise things we're calculating
//  for(i=0;i<NTask;i++)
//  {
//    if(Shadows[i].G)
//    {
//      for(j=0;j<NGas[i];j++)
//      {
//        Shadows[i].G[j].dK=0;
//      }
//    }
//  }
//
//  i=0;
//  do
//  {
//
//    /*
//     *  The local work loop.
//     */
//
//    n_done=0;
//    for(j=0;j<NTask;j++)
//      n_send_local[j]=0;
//
//    //Do local particles until the export buffer is full up or we finish all locals
//    for(n_export=0;i<NPart[GAS] && BufferSizeVisc-n_done > NTask;i++)
//    {
//      //Reset export flag
//      for(j=0;j<NTask;j++)
//        Export[j]=0;
// 
//      //Do the calculation
//      visc_ngbs(i,0);
//
//      //Export anything that needs it
//      for(j=0;j<NTask;j++)
//      {
//        if(Export[j])
//        {
//          ViscSend[n_export].in.x = G[i].x;
//          ViscSend[n_export].in.y = G[i].y;
//          ViscSend[n_export].in.z = G[i].z;
//          ViscSend[n_export].in.vx = G[i].vpredx;
//          ViscSend[n_export].in.vy = G[i].vpredy;
//          ViscSend[n_export].in.vz = G[i].vpredz;
//          ViscSend[n_export].in.hsml = G[i].hsml;
//          ViscSend[n_export].in.density = G[i].density;
//          ViscSend[n_export].in.pressure = G[i].pressure;
//          ViscSend[n_export].in.what = i;
//          ViscSend[n_export].in.where = j;
//          ViscSend[n_export].in.from = Task;
//          n_export++;
//          n_send_local[j]++;
//        }
//      }
//      n_done++;
//    }
//    //printf("[%d] Finished local with i=%d,n_export=%d,n_done=%d,NPart[GAS]=%d.\n",Task,i,n_export,n_done,NPart[GAS]);
//
//    /*
//     *  Send off newly created exports
//     */
//
//    //Sort the send buffer
//    qsort(ViscSend,n_export,sizeof(union visc_comm),visc_buffer_sort);
//    //Find the offsets for each
//    for(j=1, n_offsets[0]=0;j<NTask;j++)
//    {
//      n_offsets[j]=n_offsets[j-1]+n_send_local[j-1];
//    }
//
//    //Gather the arrays of sends and measure how much time we wait for
//    MPI_Allgather(n_send_local,NTask,MPI_INT,n_send_global,NTask,MPI_INT,MPI_COMM_WORLD);
//
//
//    //Now the magical part, inspired by GADGET
//    //The first loop is to ensure that we clear the send buffer.  Each loop
//    //through level will do at least one send/recv for each processor.  This
//    //extra loop makes sure we get everything sent in the instance when 
//    //only the first send/recv executes each time
//    level=1;
//    do
//    {
//      //Initialise counter which tracks how full recv buffer gets
//      for(j=0;j<NTask;j++)
//      {
//        n_buffer[j]=0;
//      }
//      //Now the magic, each loop will ensure that a processor sends and 
//      //recieves from the same remote processor.  By looping over all 
//      //groups we guaruntee that all pairs are found
//      for(n_grp = level; n_grp < NTask; n_grp++)
//      {
//        //Do we have space to do all of the send/recieves in this round?
//        maxfill=0;
//        for(j=0;j<NTask;j++)
//        {
//          //This check only needed for non power of 2 tasks
//          if((j ^ n_grp) < NTask)
//          {
//            if( maxfill < n_buffer[j] + n_send_global[(j ^ n_grp) * NTask +j])
//            {
//              maxfill = n_buffer[j] + n_send_global[(j ^ n_grp) * NTask +j];
//            }
//          }
//        }
//        //Don't have enough space :(
//        if(maxfill >= BufferSizeVisc)
//          break;
//
//        //We do have enough space!
//        //This is the partner I'm paired up with through the exclusive or bit magic
//        remote_partner = Task ^ n_grp;
//
//        if(remote_partner < NTask)
//        {
//          //Do we have anything to send or receive with our remote partner?
//          if(n_send_local[remote_partner] || n_send_global[remote_partner * NTask + Task])
//          {
//            //It's business time!
//            MPI_Sendrecv(ViscSend+n_offsets[remote_partner],
//                n_send_local[remote_partner]*sizeof(union visc_comm),
//                MPI_BYTE,remote_partner,TAG_SEND,
//                ViscRecv+n_buffer[Task],
//                n_send_global[remote_partner*NTask+Task]*sizeof(union visc_comm),
//                MPI_BYTE,remote_partner,TAG_SEND,
//                MPI_COMM_WORLD, MPI_STATUS_IGNORE);
//          }
//        }
//
//        //Update the buffers and move on to next send/recv pair
//        for(j=0;j<NTask;j++)
//        {
//          if((j^n_grp) < NTask)
//          {
//            n_buffer[j] += n_send_global[(j ^ n_grp) * NTask +j];
//          }
//        }
//      }
//
//      //Do the processing of exported particles
//      for(j=0;j<n_buffer[Task];j++)
//        visc_ngbs(j,1);
//
//      //Keep track of wasted time by waiting for sync here
//      MPI_Barrier(MPI_COMM_WORLD);
//
//      //Now back to the magic send/recv loop
//      for(j=0;j<NTask;j++)
//      {
//        n_buffer[j]=0;
//      }
//      for(n_grp = level; n_grp < NTask; n_grp++)
//      {
//        //Do we have space to do all of the send/recieves in this round?
//        maxfill=0;
//        for(j=0;j<NTask;j++)
//        {
//          //This check only needed for non power of 2 tasks
//          if((j ^ n_grp) < NTask)
//          {
//            if( maxfill < n_buffer[j] + n_send_global[(j ^ n_grp) * NTask +j])
//            {
//              maxfill = n_buffer[j] + n_send_global[(j ^ n_grp) * NTask +j];
//            }
//          }
//        }
//        //Don't have enough space :(
//        if(maxfill >= BufferSizeVisc)
//          break;
//
//        //We do have enough space!
//        //This is the partner I'm paired up with through the exclusive or bit magic
//        remote_partner = Task ^ n_grp;
//
//        if(remote_partner < NTask)
//        {
//          //Do we have anything to return or recieve back from our remote partner?
//          if(n_send_local[remote_partner] || n_send_global[remote_partner * NTask + Task])
//          {
//            //It's business time!
//            MPI_Sendrecv(ViscRecv+n_buffer[Task],
//                n_send_global[remote_partner*NTask+Task]*sizeof(union visc_comm),
//                MPI_BYTE,remote_partner,TAG_RETURN,
//                ViscSend+n_offsets[remote_partner],
//                n_send_local[remote_partner]*sizeof(union visc_comm),
//                MPI_BYTE,remote_partner,TAG_RETURN,
//                MPI_COMM_WORLD, MPI_STATUS_IGNORE);
//          }
//        }
//
//        //Store the results
//        for(j=n_offsets[remote_partner];j<n_offsets[remote_partner]+n_send_local[remote_partner];j++)
//        {
//          k=ViscSend[j].out.what;
//          G[k].ax += ViscSend[j].out.ax;
//          G[k].ay += ViscSend[j].out.ay;
//          G[k].az += ViscSend[j].out.az;
//          G[k].dK += ViscSend[j].out.dK;
//          if(ViscSend[j].out.max_vsig > G[k].vsig)
//            G[k].vsig = ViscSend[j].out.max_vsig;
//        }
//
//        //Update the buffers and move on to next send/recv pair
//        for(j=0;j<NTask;j++)
//        {
//          if((j^n_grp) < NTask)
//          {
//            n_buffer[j] += n_send_global[(j ^ n_grp) * NTask +j];
//          }
//        }
//      }
//      //Done both sends and receives, repeat the loop if we couldn't
//      //do any of them because of the receive buffer over-filling.
//      level = n_grp;
//    }while(level<NTask);
//  //Finish overall loop
//  }while(i<NPart[GAS]);
//}

int visc_buffer_sort(const void* a,const void* b)
{
  if((*((union visc_comm*) a)).in.where < (*((union visc_comm*) b)).in.where) return -1;
  if((*((union visc_comm*) a)).in.where > (*((union visc_comm*) b)).in.where) return 1;
  return 0;
}
