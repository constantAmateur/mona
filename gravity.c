/*
 * =====================================================================================
 *
 *       Filename:  gravity.c
 *
 *    Description:  Routines concerned with calculating gravitation forces.
 *
 *        Version:  0.6.3
 *        Created:  16/08/13 11:34:07
 *
 *         Author:  Matthew D. Young, my304@ast.cam.ac.uk
 *        Company:  Institute of Astronomy, Cambridge, UK
 *        License:  GNU General Public License
 *      Copyright:  Copyright (c) 2014, Matthew D. Young
 *
 * =====================================================================================
 */

#include "variables.h"
#include "functions.h"

//struct node Body,NULL_Body,CommNode;
//
//int Nexport_sum,Nexport_coeff;
//int* Nsend_local_sum;
//int* Nsend_local_coeff;
//int Summed;
//int Touched;
//int Interacted;
//int Count;
//int Lock;
//int Flag;
//int CoeffSize = sizeof(struct grav_coeff_comm);
//int PartSize = sizeof(union grav_comm);
//int NBytesLeft ;
//int GravSendCoeffPos;
//int GravSendSumPos ;
//int NTerm;
//double SumDiffs;
//double SumSum[3];
//double sum;

void gravity(void)
{
  int i,j;
#if GRAV>=1 && DEBUG>=2
  double new[3]={0};
#endif
  //struct node* cell;
  //NBytesLeft = PartSize*(HalfBufferSizeBytes/PartSize);
  //GravSendSumPos = (HalfBufferSizeBytes/PartSize)-1;
  //GravSendCoeffPos = 0;
  //Summed=Touched=Interacted=Count=Lock=Flag=NTerm=0;
  //SumDiffs=SumSum[0]=SumSum[1]=SumSum[2]=0;

  //Calculate the sink gravitational accelerations
  //Calculate self gravity
  //Set all nodes coefficients to zero
  //print_tree(0);
  //for(i=0;i<NextFreeNode;i++)
  //{
  //  cell=Root+i;
  //  for(j=0;j<3;j++)
  //  {
  //    cell->coeff_1[j]=0;
  //    for(k=0;k<3;k++)
  //    {
  //      cell->coeff_2[j][k]=0;
  //      for(l=0;l<3;l++)
  //      {
  //        cell->coeff_3[j][k][l]=0;
  //      }
  //    }
  //  }
  //}
  
  for(j=0;j<NTask;j++)
  {
    if(Shadows[j].G)
    {
      for(i=0;i<NGas[j];i++)
      {
        Shadows[j].G[i].pot=0;
        Shadows[j].G[i].ax=0;
        Shadows[j].G[i].ay=0;
        Shadows[j].G[i].az=0;
      }
    }
  }
  for(i=0;i<NPart[SINK];i++)
    S[i].pot=0;
  //printf("[%d] Begining self-gravity.\n",Task);
  //printf("[%d] Test point is at (%g,%g,%g).\n",Task,G[0].x,G[0].y,G[0].z);
  //SumSum[0]=SumSum[1]=SumSum[2]=0;
#if GRAV>=2
  //Calculate the tree properties needed by self-gravity
  calc_grav_props();
  //Test that COM is correct
  //double com[3];
  //double diff,max_diff=0;
  //struct node *cnode;
  //max_diff=0;
  //for(i=0;i<NextFreeNode;i++)
  //{
  //  cnode=Root+i;
  //  direct_com(cnode,com);
  //  diff=sqrt((com[0]-cnode->com[0])*(com[0]-cnode->com[0])+
  //            (com[1]-cnode->com[1])*(com[1]-cnode->com[1])+
  //            (com[2]-cnode->com[2])*(com[2]-cnode->com[2]));
  //  if(diff>max_diff)
  //    max_diff=diff;
  //  printf("[%d] Node at lvl=%d,lid=%d,owner=%d,count=%d has COM_tree=(%g,%g,%g) and COM_exact=(%g,%g,%g)(diff %g).  Also has hmax=%g, rmax=%g.\n",Task,(int) (cnode->lvl_lid/MaxLID),(int) (cnode->lvl_lid%MaxLID),cnode->owner,cnode->count,cnode->com[0],cnode->com[1],cnode->com[2],com[0],com[1],com[2],diff,cnode->hmax,cnode->rmax);
  //}
  //printf("[%d] Maximum divergence = %g.\n",Task,max_diff);
  //kill(333);

  //pprintf("Starting self gravity calc.\n");
  self_gravity();
  //pprintf("Finished self gravity calc.\n");
  //This is needed in order for the storing of the gravitational accelerations
  //to work correctly.
  gather_acceleration_shadows();
#if DEBUG>=2
  //Calculate the potential
  exact_gas();
  //Have to gather all the accelerations locally for this...
  new[0]=new[1]=new[2]=0;
  //Test of newtons third law
  for(j=0;j<NTask;j++)
  {
    if(Shadows[j].G)
    {
      for(i=0;i<NGas[j];i++)
      {
        new[0]+=PcleMass*Shadows[j].G[i].ax;
        new[1]+=PcleMass*Shadows[j].G[i].ay;
        new[2]+=PcleMass*Shadows[j].G[i].az;
      }
    }
  }
  MPI_Allreduce(MPI_IN_PLACE,new,3,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  gpprintf("Total force after self-gravity is %g,%g,%g.\n",new[0],new[1],new[2]);
  //Test of the accuracy of the tree-force (Averaged square error)
  new[0]=0;
  new[1]=0;
  for(i=0;i<NPart[GAS];i++)
  {
    new[0] += (G[i].ax-G[i].grav_ax)*(G[i].ax-G[i].grav_ax)+(G[i].ay-G[i].grav_ay)*(G[i].ay-G[i].grav_ay)+(G[i].az-G[i].grav_az)*(G[i].az-G[i].grav_az);
    new[2] = G[i].grav_ax*G[i].grav_ax+G[i].grav_ay*G[i].grav_ay+G[i].grav_az*G[i].grav_az;
    if(new[2]>new[1])
      new[1]=new[2];
  }
  //Amalgamate all the sums on process 0
  MPI_Reduce(&new[0],&new[2],1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
  //new[2] now has the sum on process 0, so we don't care about new[0] any more
  MPI_Reduce(&new[1],&new[0],1,MPI_DOUBLE,MPI_MAX,0,MPI_COMM_WORLD);
  //Now finish off the calculation
  gpprintf("Averaged Square Error in force calculation is %g\n",new[2]/(new[0]*NPartWorld[GAS]));
#endif
#endif
#if GRAV>=1
  sink_gravity();

#if DEBUG>=2
  new[0]=new[1]=new[2]=0;
  //Test of newtons third law
  for(i=0;i<NPart[GAS];i++)
  {
    new[0]+=PcleMass*G[i].ax;
    new[1]+=PcleMass*G[i].ay;
    new[2]+=PcleMass*G[i].az;
  }
  MPI_Allreduce(MPI_IN_PLACE,new,3,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  for(i=0;i<NPart[SINK];i++)
  {
    new[0]+=S[i].m * S[i].ax;
    new[1]+=S[i].m * S[i].ay;
    new[2]+=S[i].m * S[i].az;
  }
  gpprintf("Total force after sink-gravity is %g,%g,%g.\n",new[0],new[1],new[2]);
#endif
#endif

  //Store the grav acceleration so we don't have to re-calculate it when
  //only the hydro forces should change
  for(i=0;i<NPart[GAS];i++)
  {
    G[i].grav_ax = G[i].ax;
    G[i].grav_ay = G[i].ay;
    G[i].grav_az = G[i].az;
  }
}

//The Barnes & Hutt version of the self-gravity calculation
void self_gravity(void)
{
  ngb_search(GravSend,GravRecv,sizeof(union grav_comm),BufferSizeGrav,
      gravity_eval,
      gravity_export_return,
      grav_buffer_sort);
}

void gravity_export_return(void *data)
{
  union grav_comm *part = data;
  int k;
  k=part->out.what;
  G[k].ax += part->out.ax;
  G[k].ay += part->out.ay;
  G[k].az += part->out.az;
  G[k].nops += part->out.nops;
#if GRAV>=3
  G[k].pot += part->out.pot;
#endif
}


/* 
 * ===  FUNCTION  ======================================================================
 *  Description:  The core of the gravitational force calculation.  Uses the Barnes &
 *                Hutt opening criteria and processes each particle independently.  This
 *                allows for better scaling, but lower accuracy and absolute speed than
 *                the fast multipole method that I was using before.  This method can
 *                still start to scale poorly when many nodes need exporting (more than
 *                NGB_LIST_LEN), which forces the tree walk to be re-performed for the
 *                exported particle.  It may be possible to optimise this slightly by
 *                exporting the current position through the tree walk at the point when
 *                the node buffer becomes too full.
 *                Uses the same calling convention as the density/hydro/viscosity loops
 *                but cannot efficiently be written as a single nested loop, which is 
 *                why the actual evaluation routine appears in multiple places.
 * =====================================================================================
 */
void gravity_eval(void *index,void *export, int mode)
{
  double dist,fact,r2;
  double pos[3];
  double ax,ay,az;
  double h;
  double dx,dy,dz;
  int next;
  int nngbs;
  int *ngbs;
  int i,j,k;
  int export_offset[NTask];
  int nops=0;
  const struct node *cnode;
  int owner,mob,count;
#if GRAV>=3
  double pot=0;
#endif
  struct gas *local = NULL;
  union grav_comm *remote = NULL;
  union grav_comm *storage = NULL;
  if(mode)
  {
    remote = index;
    pos[0]=remote->in.x;
    pos[1]=remote->in.y;
    pos[2]=remote->in.z;
    nngbs = remote->in.nnodes;
    ngbs = remote->in.nodes;
    h = remote->in.h;
  }
  else
  {
    local = index;
    storage = export;
    pos[0]=local->x;
    pos[1]=local->y;
    pos[2]=local->z;
    h=local->hsml;
    nngbs=0;
    ngbs=NULL;
    for(i=0;i<NTask;i++)
      export_offset[i]=-1;
  }
  ax=ay=az=0;
  fact=Params.G*PcleMass;

  //If we've been exported a set of nodes, we know that's all we need
  //to look at (in N^2 fashion), so just do that...
  if(mode && nngbs)
  {
    //Check that all the nodes have been obtained...
#if DEBUG>=2
    next=0;
    while(next!=-1)
    {
      cnode=Root+next;
      //Is it non-local when we're on export? If it is we can skip it entirely
      owner=cnode->owner;
      if(owner!=Task && (owner>=0 || !get_bit(Task,cnode->owners)))
      {
        next=cnode->sister;
        continue;
      }
      //Distance from particle to com
      dx = pos[0] - cnode->com[0];
      dy = pos[1] - cnode->com[1];
      dz = pos[2] - cnode->com[2];

      r2 = dx*dx+dy*dy+dz*dz;
      //Is it well separated
      if(cnode->rmax < r2*Params.grav_opening)
      {
        //It is, we don't need to export it...
        next=cnode->sister;
        continue;
      }
      if(cnode->n_daughters)
      {
        next=cnode->daughter;
      }
      else
      {
        //It's a leaf that is local, check that it's in the list we already have
        i=0;
        while(i<nngbs && ngbs[i]!= Root-cnode)
          i++;
        if(i==nngbs)
        {
          pprintf("Local node %d was not in the export list.\n",(int) (cnode-Root));
          kill(ERROR_SANITY);
        }
        else
        {
          ngbs[i] *= -1;
        }
        next=cnode->sister;
      }
    }
    for(i=0;i<nngbs;i++)
    {
      if(ngbs[i]<0)
      {
        pprintf("Node %d was not found in local search.\n",ngbs[i]);
        kill(ERROR_SANITY);
      }
      else
      {
        ngbs[i] *= -1;
      }
    }
#endif
 
     
    //printf("[%d] Export sent us the nodes to look at.\n",Task);
    for(i=0;i<nngbs;i++)
    {
      cnode=Root-ngbs[i];
      k=0;
      do
      {
        if(cnode->owner<0)
        {
          owner=(int) (cnode->extra[k].mob/MaxGas);
          mob=(int) (cnode->extra[k].mob%MaxGas);
          count = cnode->extra[k].count;
        }
        else
        {
          owner = cnode->owner;
          mob = cnode->mob;
          count = cnode->count;
        }
        if(owner==Task)
        {
          for(j=mob;j<mob+count;j++)
          {
            nops++;
            dx = pos[0] - G[j].x;
            dy = pos[1] - G[j].y;
            dz = pos[2] - G[j].z;
            r2 = sqrt(dx*dx+dy*dy+dz*dz);
#if HYDRO<=1 || defined CONSTANT_GAS_SOFT
            dist = fact * softened_grav(r2,Params.gas_soft);
#if GRAV>=3
            pot += fact * softened_pot(r2,Params.gas_soft);
#endif
#else
            dist = fact * 0.5 * (softened_grav(r2,G[j].hsml)+softened_grav(r2,h));
#if GRAV>=3
            pot += fact * 0.5 * (softened_pot(r2,G[j].hsml)+softened_pot(r2,h));
#endif
#endif
            ax -= dist*dx;
            ay -= dist*dy;
            az -= dist*dz;
          }
        }
        k++;
      }while(k<(-1*cnode->owner));
    }
  }
  else
  {
    //Hope we don't have to walk the tree on export, as that will be costly...
    //Search through the tree, open up as needed and evaluate as needed
    next=0;
    while(next!=-1)
    {
      cnode=Root+next;
      //Is it non-local when we're on export? If it is we can skip it entirely
      owner=cnode->owner;
      if(mode && (owner!=Task && (owner>=0 || !get_bit(Task,cnode->owners))))
      {
        next=cnode->sister;
        continue;
      }
      //Distance from particle to com
      dx = pos[0] - cnode->com[0];
      dy = pos[1] - cnode->com[1];
      dz = pos[2] - cnode->com[2];

      r2 = dx*dx+dy*dy+dz*dz;
      //Is it well separated
      if(cnode->rmax < r2*Params.grav_opening)
      {
        //If we're here on export, only care about leaves
        if(!mode)
        {
          nops++;
          r2 = sqrt(r2);
#if HYDRO<=1 || defined CONSTANT_GAS_SOFT
          dist = fact * softened_grav(r2,Params.gas_soft);
#if GRAV>=3
          pot += fact * softened_pot(r2,Params.gas_soft);
#endif
#else
          dist = fact *cnode->count* 0.5 * (softened_grav(r2,cnode->hmax)+softened_grav(r2,h));
#if GRAV>=3
          pot += fact * 0.5 * (softened_pot(r2,cnode->hmax)+softened_pot(r2,h));
#endif
#endif
          ax -= dist*dx;
          ay -= dist*dy;
          az -= dist*dz;

        }
        next=cnode->sister;
      }
      //Not yet, open it up
      else
      {
        if(cnode->n_daughters)
        {
          next=cnode->daughter;
        }
        //This is a leaf...
        else
        {
          //Loop over owners of this leaf.
          i=0;
          do
          {
            if(cnode->owner<0)
            {
              owner=(int) (cnode->extra[i].mob/MaxGas);
              mob=(int) (cnode->extra[i].mob%MaxGas);
              count = cnode->extra[i].count;
            }
            else
            {
              owner = cnode->owner;
              mob = cnode->mob;
              count = cnode->count;
            }
            //Is it local?  The meaning of this depend on mode
            if((mode && owner==Task) || (!mode && Shadows[owner].G))
            {
              //It is, do N^2 on children
              for(j=mob;j<mob+count;j++)
              {
                nops++;
                dx = pos[0] - Shadows[owner].G[j].x;
                dy = pos[1] - Shadows[owner].G[j].y;
                dz = pos[2] - Shadows[owner].G[j].z;
                r2 = sqrt(dx*dx+dy*dy+dz*dz);
#if HYDRO<=1 || defined CONSTANT_GAS_SOFT
                dist = fact * softened_grav(r2,Params.gas_soft);
#if GRAV>=3
                pot += fact * softened_pot(r2,Params.gas_soft);
#endif
#else
                dist = fact * 0.5 * (softened_grav(r2,Shadows[owner].G[j].hsml)+softened_grav(r2,h));
#if GRAV>=3
                pot += fact * 0.5 * (softened_pot(r2,Shadows[owner].G[j].hsml)+softened_pot(r2,h));
#endif
#endif
                ax -= dist*dx;
                ay -= dist*dy;
                az -= dist*dz;
              }
            }
            //Export it, if not already on export
            else if(!mode)
            {
              //Establish export of this node to processor whatever
              if(export_offset[owner]<0)
              {
                export_offset[owner]=N_Export;
                storage[N_Export].in.x = pos[0];
                storage[N_Export].in.y = pos[1];
                storage[N_Export].in.z = pos[2];
                storage[N_Export].in.h = h;
                storage[N_Export].in.what = (int) (local-G);
                storage[N_Export].in.where = owner;
                storage[N_Export].in.from = Task;
                //Keep the convention of nodes being negative
                storage[N_Export].in.nodes[0] = -1*((int) (cnode-Root));
                storage[N_Export].in.nnodes = 1;
                N_Export++;
                N_Send_Local[owner]++;
              }
              else
              {
                if(storage[export_offset[owner]].in.nnodes == NGB_LIST_LEN)
                {
                  storage[export_offset[owner]].in.nnodes = 0;
                }
                //Don't add anything if we can't fit it all
                else if(storage[export_offset[owner]].in.nnodes)
                {
                  //Add in the new node
                  storage[export_offset[owner]].in.nodes[storage[export_offset[owner]].in.nnodes] = -1*((int) (cnode-Root));
                  storage[export_offset[owner]].in.nnodes++;
                }
              }
            }
            i++;
          }while(i<(-1*cnode->owner));
          //Move on to sister
          next=cnode->sister;
        }
      }
    }
  }

  //Finish...
  if(mode)
  {
    mode = remote->in.where;
    i = remote->in.from;
    nngbs = remote->in.what;
    remote->out.what = nngbs;
    remote->out.where = mode;
    remote->out.from = i;
    remote->out.ax = ax;
    remote->out.ay = ay;
    remote->out.az = az;
    remote->out.nops = nops;
#if GRAV>=3
    remote->out.pot = pot;
#endif
  }
  else
  {
    local->ax += ax;
    local->ay += ay;
    local->az += az;
    local->nops += nops;
#if GRAV>=3
    local->pot += pot;
#endif
  }
}

void calc_grav_props(void)
{
  int i,j,ii;
  int cnt,scnt;
  int start;
  int end_start;
  int mob,count;
  double r2;
  struct node * cnode;
  //pprintf("Calculating com and other things.\n");

  //Calculate centre of mass and hmax at leaves
  i=0;
  cnt=0;
  end_start=BufferSizeFloats-1;
  //Outer loop is in case comm buffer gets full
  do
  {
    //printf("[%d] Doing a COM loop starting from %d.\n",Task,i);
    start=i;
    //This is the actual loop over the nodes
    while(i<NextFreeNode && (cnt+1)*4<BufferSizeFloats)
    {
      cnode=Root+i;
      //Is it a leaf?
      if(!cnode->n_daughters)
      {
        //Zero out bits of memory we're about to use
        FBuffer[3*cnt]=FBuffer[3*cnt+1]=FBuffer[3*cnt+2]=0;
        FBuffer[end_start-cnt]=0;
        //Get local particle ids or move on if there are none
        if(cnode->owner>=0)
        {
          if(cnode->owner!=Task)
          {
            cnt++;
            i++;
            continue;
          }
          mob=cnode->mob;
          count=cnode->count;
        }
        else
        {
          if(!get_bit(Task,cnode->owners))
          {
            cnt++;
            i++;
            continue;
          }
          j=0;
          while(cnode->extra[j].mob/MaxGas!=Task)
            j++;
          mob=cnode->extra[j].mob%MaxGas;
          count=cnode->extra[j].count;
        }
        //Calculate com sum and hmax for leaf
        for(j=mob;j<mob+count;j++)
        {
          FBuffer[3*cnt+0] += G[j].x;
          FBuffer[3*cnt+1] += G[j].y;
          FBuffer[3*cnt+2] += G[j].z;
          if(G[j].hsml>FBuffer[end_start-cnt])
            FBuffer[end_start-cnt]=G[j].hsml;
        }
        cnt++;
      }
      i++;
    }
    //printf("[%d] About to reduce on %d leaves.\n",Task,cnt);
    //Either ran out of storage space or ran out of leaves, either way reduce and save
    MPI_Allreduce(MPI_IN_PLACE,FBuffer,cnt*3,MFLOAT,MPI_SUM,MPI_COMM_WORLD);
    MPI_Allreduce(MPI_IN_PLACE,FBuffer+(end_start-cnt),cnt,MFLOAT,MPI_MAX,MPI_COMM_WORLD);
    //printf("[%d] Reduction completed, storing answers.\n",Task);
    //Go through the same nodes and store the answer
    scnt=0;
    ii=start;
    while(scnt<cnt)
    {
      cnode=Root+ii;
      //Is it a leaf?
      if(!cnode->n_daughters)
      {
        //Don't care if it's local or not, store the answer
        cnode->com[0] = FBuffer[scnt*3+0];
        cnode->com[1] = FBuffer[scnt*3+1];
        cnode->com[2] = FBuffer[scnt*3+2];
        cnode->hmax = FBuffer[end_start-scnt];
        scnt++;
      }
      ii++;
    }
    //Cool, now we're either finished, or we have space in the buffer to process more
    //leaves
  }while(i<NextFreeNode);
  //printf("[%d] Answers all stored, finalising values.\n",Task);
  //Now we have the centre of mass and hmax set accurately on all leaves, walk the tree once to set non-leaf nodes iteratively
  set_com(Root);
  //Finally, divide by total counts to finalise centre of mass
  for(i=0;i<NextFreeNode;i++)
  {
    Root[i].com[0]/=Root[i].count;
    Root[i].com[1]/=Root[i].count;
    Root[i].com[2]/=Root[i].count;
  }
  //OK, COM is established as is hmax.  Now we can calculate com dependent quantities
  //Like the COM loop, except we're calculating minimum covering distance at leaves
  //now...
  i=0;
  cnt=0;
  //Outer loop is in case comm buffer gets full
  do
  {
    start=i;
    //This is the actual loop over the nodes
    while(i<NextFreeNode && (cnt+1)<BufferSizeFloats)
    {
      cnode=Root+i;
      //Is it a leaf?
      if(!cnode->n_daughters)
      {
        //Zero out bits of memory we're about to use
        FBuffer[cnt]=0;
        //Get local particle ids or move on if there are none
        if(cnode->owner>=0)
        {
          if(cnode->owner!=Task)
          {
            cnt++;
            i++;
            continue;
          }
          mob=cnode->mob;
          count=cnode->count;
        }
        else
        {
          if(!get_bit(Task,cnode->owners))
          {
            cnt++;
            i++;
            continue;
          }
          j=0;
          while(cnode->extra[j].mob/MaxGas!=Task)
            j++;
          mob=cnode->extra[j].mob%MaxGas;
          count=cnode->extra[j].count;
        }
        //Calculate the square of the maximum separation from COM within the cell
        for(j=mob;j<mob+count;j++)
        {
          r2=(cnode->com[0]-G[j].x)*(cnode->com[0]-G[j].x)+
             (cnode->com[1]-G[j].y)*(cnode->com[1]-G[j].y)+
             (cnode->com[2]-G[j].z)*(cnode->com[2]-G[j].z);
          if(r2>FBuffer[cnt])
            FBuffer[cnt]=r2;
        }
        cnt++;
      }
      i++;
    }
    //Either ran out of storage space or ran out of leaves, either way reduce and save
    MPI_Allreduce(MPI_IN_PLACE,FBuffer,cnt,MFLOAT,MPI_MAX,MPI_COMM_WORLD);
    //Go through the same nodes and store the answer
    scnt=0;
    ii=start;
    while(scnt<cnt)
    {
      cnode=Root+ii;
      //Is it a leaf?
      if(!cnode->n_daughters)
      {
        //Don't care if it's local or not, store the answer
        cnode->rmax = sqrt(FBuffer[scnt]);
        scnt++;
      }
      ii++;
    }
    //Cool, now we're either finished, or we have space in the buffer to process more
    //leaves
  }while(i<NextFreeNode);
  //We have rmax on all leaves, use this to calculate rmax on all non-leaf nodes
  //Don't care about the return value so throw it away
  FBuffer[0]=set_rmax(Root);
  //Square all rmaxes as that is what is used in comparison
  for(i=0;i<NextFreeNode;i++)
    Root[i].rmax *= Root[i].rmax;
}

double set_rmax(struct node* cnode)
{
  struct node *kid;
  double r,altr,tmp;
  int i;
  r=0;
  //Rmax not set if I have daughters
  if(cnode->n_daughters)
  {
    //Set node size to max distance to corner
    r=get_rmax(cnode);
    //Now get max(r_max + |z_i-z|)
    kid=Root+cnode->daughter;
    altr=0;
    for(i=0;i<cnode->n_daughters;i++)
    {
      //Distance between COMs
      tmp=sqrt((cnode->com[0]-kid->com[0])*(cnode->com[0]-kid->com[0])+
             (cnode->com[1]-kid->com[1])*(cnode->com[1]-kid->com[1])+
             (cnode->com[2]-kid->com[2])*(cnode->com[2]-kid->com[2]));
      //Add on the rmax of the child
      tmp+=set_rmax(kid);
      if(tmp>altr)
        altr=tmp;
      kid=Root+kid->sister;
    }
    if(altr<r)
      r=altr;
    cnode->rmax=r;
  }
  return cnode->rmax;
}

void set_com(struct node* cnode)
{
  struct node *kid;
  int i;
  //Open up children
  if(cnode->n_daughters)
  {
    kid=Root+cnode->daughter;
    for(i=0;i<cnode->n_daughters;i++)
    {
      set_com(kid);
      kid=Root+kid->sister;
    }
  }
  if(cnode->mother!=-1)
  {
    //Opened all children (or don't have any), move contribution up tree
    //kid is really mother, just saving on memory
    kid=Root+cnode->mother;
    kid->com[0] += cnode->com[0];
    kid->com[1] += cnode->com[1];
    kid->com[2] += cnode->com[2];
    if(cnode->hmax>kid->hmax)
      kid->hmax=cnode->hmax;
  }
}

//void self_gravity(void)
//{
//  int i,j,k;
//  double tstart;
//  //double new[3];
//  //for(i=0;i<30;i++)
//  //  count_at_level(i);
//  //First initialise things that need initialising
//  //Tstart = MPI_Wtime();
//  Nsend_local_sum = malloc(NTask*sizeof(int));
//  Nsend_local_coeff = malloc(NTask*sizeof(int));
//  for(i=0;i<NTask;i++)
//  {
//    Nsend_local_sum[i] = 0;
//    Nsend_local_coeff[i] = 0;
//  }
//  Nexport_sum=0;
//  Nexport_coeff=0;
//  //Setup the default Body settings
//  NULL_Body.count=1;
//  NULL_Body.mob=-10;
//  NULL_Body.rmax=0;
//  NULL_Body.hmax=0;
//  NULL_Body.mother=0;
//  NULL_Body.sister=0;
//  NULL_Body.coeff_0=0;
//  CommNode.mob=-100;
//  CommNode.flag = NODE_LOCAL+Task;
//  for(i=0;i<3;i++)
//  {
//    NULL_Body.coeff_1[i]=0;
//    NULL_Body.lows[i]=0;
//    NULL_Body.highs[i]=0;
//    NULL_Body.com[i]=0;
//    for(j=0;j<3;j++)
//    {
//      NULL_Body.coeff_2[i][j]=0;
//      NULL_Body.quad[i][j]=0;
//      for(k=0;k<3;k++)
//      {
//        NULL_Body.coeff_3[i][j][k]=0;
//      }
//    }
//  }
//  Body=NULL_Body;
//  //The main event.  Calculate the Gas particles effect on themselves
//  //using the tree.  
//  //printf("[%d] Collecting taylor series co-efficients.\n",Task);
//  NTerm=0;
//
//  tstart=MPI_Wtime();
//  interact_nodes(Nodes,Nodes);
//  //Enter the final comm phase
//  //Timings.gravity += MPI_Wtime()-Tstart;
//  //Tstart = MPI_Wtime();
//  grav_comm_loop(1);
//  Timings.grav_coeffs += MPI_Wtime()-tstart;
//  //Timings.comm += MPI_Wtime()-Tstart;
//  //kill(343);
//  //printf("[%d] Had to touch %d nodes, do %d intercations and %d sums.\n",Task,Touched,Interacted,Summed);
//  //printf("[%d] Contribution from %d nodes.\n",Task,Count);
//  //if(Nexport_sum || Nexport_coeff)
//  //  kill(3454);
//  //Enter comm loop until everyone is finished
//  //while(!done_global)
//  //{
//  //  done_global = grav_comm(1);
//  //}
//  //How did we do?
//  //for(i=0;i<NPart[GAS];i++)
//  //{
//  //  if(G[i].mark!=NPart[GAS]-1)
//  //  {
//  //    printf("[%d] Number of sources felt by %d was %d.\n",Task,i,G[i].mark);
//  //    kill(908);
//  //  }
//  //  G[i].imark=0;
//  //}
//  //Now we have all the co-efficients, move on down!
//  //printf("[%d] Beginning evaluation phase.  Should be easy right?\n",Task);
//  //kill(11111);
//  tstart = MPI_Wtime();
//  evaluate_nodes();
//  Timings.grav_eval += MPI_Wtime()-tstart;
//  //printf("[%d] It finished?!?  Really?!?  Wow, okay, party time then.\n",Task);
//  //Freeeeeeeedom!
//  free(Nsend_local_sum);
//  free(Nsend_local_coeff);
//}

//void grav_comm_loop(int no_more_local)
//{
//  int ncoeff[NTask];
//  int offsets[NTask];
//  int sum_offsets[NTask];
//  int coeff_offsets[NTask];
//  int send_idxs[NTask];
//  int keep_mem[NTask];
//  int nsend_hi,nsend_data,nsend_return;
//  int send_block,recv_block;
//  int finish,all_finish;
//  int flag;
//  int nrecv,done;
//  int i,j,k,l,ii;
//  int tgt,npart;
//  int nexport_sum,nexport_coeff;
//  int msrc,mtag;
//  void *void_gravsumsend=GravSend;
//  void *void_gravsumrecv=GravRecv;
//  union grav_comm *pa;
//  struct node* node;
//  double tstart,tbalance;
//  tbalance=0;
//
//  MPI_Status status;
//  MPI_Request hi_send_requests[NTask];
//  MPI_Request data_send_requests[NTask];
//  MPI_Request data_return_requests[NTask];
//  MPI_Request complete_requests[NTask-1];
//
//  tstart = MPI_Wtime();
//  //Timings.gravity += MPI_Wtime()-Tstart;
//
//  //        struct grav_coeff_comm *co;
//
//
//  nexport_sum=0;
//  nexport_coeff=0;
//  for(i=0;i<NTask;i++)
//  {
//    nexport_sum+=Nsend_local_sum[i];
//    nexport_coeff+=Nsend_local_coeff[i];
//  }
//#if DEBUG>=3
//  printf("[%d] Entered comm phase with %d particles and %d coeffs to send NTerm=%d.\n",Task,nexport_sum,nexport_coeff,NTerm);
//#endif
//  //if(Dt)
//  //  for(i=0;i<nexport_coeff;i++)
//  //    printf("[%d] UnSorted Coeff[%d] w/w/f=%d/%d/%d.\n",Task,i,GravCoeffSend[i].what,GravCoeffSend[i].where,GravCoeffSend[i].from);
//
//  //for(i=0;i<NTask;i++)
//  //{
//  //  printf("[%d] About to send %d coeff and %d part to %d.\n",Task,Nsend_local_coeff[i],Nsend_local_sum[i],i);
//  //}
//  //Now have to sort things so we have contiguous blocks in mem
//  //First sort coefficients and particles on their own
//  qsort(&GravSend[GravSendSumPos+1],nexport_sum,PartSize,grav_buffer_sort);
//  qsort(GravCoeffSend,nexport_coeff,CoeffSize,grav_coeff_buffer_sort);
//  //printf("[%d] Sorted.\n",Task);
//  //Move them all to the copy buffer
//  memcpy(GravRecv,GravSend,HalfBufferSizeBytes);
//
//  //co = GravCoeffRecv;
//  //pa = &GravRecv[GravSendSumPos+1];
// 
//  //if(Dt)
//  //for(i=0;i<nexport_coeff;i++)
//  //{
//  //  printf("[%d] Sorted Coeff[%d] w/w/f=%d/%d/%d.\n",Task,i,co[i].what,co[i].where,co[i].from);
//  //}
//  //for(i=0;i<nexport_sum;i++)
//  //{
//  //  printf("[%d] Part[%d] = %g,%g,%g.\n",Task,i,pa[i].in.x,pa[i].in.y,pa[i].in.z);
//  //}
//  //OK to here
//  //Now they're sorted, we have to do the interleaving
//  //Now move them back one at a time so they're nice and interleaved
//  //printf("[%d] Moved mem.\n",Task);
//  //kill(10);
//  //Calculate the offsets
//  sum_offsets[0]=0;
//  coeff_offsets[0]=0;
//  offsets[0]=0;
//  for(i=1;i<NTask;i++)
//  {
//    sum_offsets[i]=sum_offsets[i-1]+Nsend_local_sum[i-1];
//    coeff_offsets[i]=coeff_offsets[i-1]+Nsend_local_coeff[i-1];
//    offsets[i] = offsets[i-1] + Nsend_local_sum[i-1]*PartSize + Nsend_local_coeff[i-1]*CoeffSize;
//    //printf("[%d] Offsets[%d] = %d,%d,%d.\n",Task,i,offsets[i],sum_offsets[i],coeff_offsets[i]);
//  }
// //Move the data around
//  for(i=0;i<NTask;i++)
//  {
//    //Copy the coefficients
//    //printf("[%d] About to copy %d bytes from %p to %p.\n",Task,Nsend_local_coeff[i]*CoeffSize,GravCoeffRecv+coeff_offsets[i],dst+offsets[i]);
//    //co=GravCoeffRecv+coeff_offsets[i];
//    //for(j=0;j<Nsend_local_coeff[i];j++)
//    //  printf("[%d] Coeff[%d].co,c1=%g,%g,%g,%g.\n",Task,j,co[j].c0,co[j].c1[0],co[j].c1[1],co[j].c1[2]);
//    memcpy(void_gravsumsend+offsets[i],GravCoeffRecv+coeff_offsets[i],Nsend_local_coeff[i]*CoeffSize);
//    //co=dst+offsets[i];
//    //for(j=0;j<Nsend_local_coeff[i];j++)
//    //{
//    //  printf("[%d] Coeff[%d].co,c1=%g,%g,%g,%g.\n",Task,j,co[j].c0,co[j].c1[0],co[j].c1[1],co[j].c1[2]);
//    //}
//    //Copy the particles
//    //pa=&GravRecv[GravSendSumPos+1+sum_offsets[i]];
//    //for(j=0;j<Nsend_local_sum[i];j++)
//    //{
//    //  printf("[%d] Part[%d] = %g,%g,%g.\n",Task,j,pa[j].in.x,pa[j].in.y,pa[j].in.z);
//    //}
//    memcpy(void_gravsumsend+offsets[i]+Nsend_local_coeff[i]*CoeffSize,&GravRecv[GravSendSumPos+1+sum_offsets[i]],Nsend_local_sum[i]*PartSize);
//    //pa=dst+offsets[i]+Nsend_local_coeff[i]*CoeffSize;
//    //for(j=0;j<Nsend_local_sum[i];j++)
//    //{
//    //  printf("[%d] Part[%d] = %g,%g,%g.\n",Task,j,pa[j].in.x,pa[j].in.y,pa[j].in.z);
//    //}
//
//  }
//  //kill(340);
//  //Print things out.  See if they're as we expect
//  //for(i=0;i<NTask;i++)
//  //{
//  //  printf("[%d] To send to particle %d.\n",Task,i);
//  //  GravCoeffSend = dst+offsets[i];
//  //  for(j=0;j<Nsend_local_coeff[i];j++)
//  //  {
//  //    printf("[%d] Coeff[%d].co,c1=%g,%g,%g,%g.\n",Task,j,GravCoeffSend[j].c0,GravCoeffSend[j].c1[0],GravCoeffSend[j].c1[1],GravCoeffSend[j].c1[2]);
//  //  }
//  //  GravSend = dst + offsets[i]+Nsend_local_coeff[i]*CoeffSize;
//  //  for(j=0;j<Nsend_local_coeff[i];j++)
//  //  {
//  //    printf("[%d] Part[%d] = %g,%g,%g.\n",Task,j,GravSend[j].in.x,GravSend[j].in.y,GravSend[j].in.z);
//  //  }
//  //}
//  //kill(3);
//  //Now ready to be sent across...
//
//  //Send greeting to dest-o-nation.  Unlike other comm loops
//  //the greeting contains the number of coefficients to store
//  nsend_hi=0;
//  nsend_data=0;
//  nsend_return=0;
//  for(i=0;i<NTask;i++)
//  {
//    keep_mem[i]=0;
//    ncoeff[i]=-1;
//    data_send_requests[i]=MPI_REQUEST_NULL;
//    data_return_requests[i]=MPI_REQUEST_NULL;
//    if(Nsend_local_sum[i] || Nsend_local_coeff[i])
//    {
//#if DEBUG>=3
//      printf("[%d] Sending greeting with %d to %d.\n",Task,Nsend_local_coeff[i],i);
//#endif
//      MPI_Issend(&Nsend_local_coeff[i],1,MPI_INT,i,0,MPI_COMM_WORLD,&hi_send_requests[i]);
//      nsend_hi++;
//    }
//    else
//    {
//      hi_send_requests[i]=MPI_REQUEST_NULL;
//    }
//  }
//#if DEBUG>=3
//  if(no_more_local)
//    printf("[%d] Entered the final comm phase.\n",Task);
//#endif
//  //Now the main comm loop
//  recv_block=0;
//  send_block=0;
//  finish=0;
//  all_finish=0;
//  while(1)
//  {
//    //printf("[%d] This loop we have nterm=%d,nsend_data/hi/return = %d/%d/%d, recv_block=%d,send_block=%d.\n",Task,nterm,nsend_data,nsend_hi,nsend_return,recv_block,send_block);
//    //Are there any outstanding hi messages?
//    if(nsend_hi)
//    {
//      MPI_Testsome(NTask,hi_send_requests,&done,send_idxs,MPI_STATUSES_IGNORE);
//      if(done==MPI_UNDEFINED)
//        kill(908);
//      if(done)
//      {
//        //At least one hello message was received!
//        //For each of them, post the actual data
//        for(i=0;i<done;i++)
//        {
//          tgt = send_idxs[i];
//#if DEBUG>=3
//          printf("[%d] Greetings posted to %d was received.\n",Task,tgt);
//          printf("[%d] Sent at offset %d, sending %d bytes.\n",Task,offsets[tgt],Nsend_local_coeff[tgt]*CoeffSize+Nsend_local_sum[tgt]*PartSize);
//#endif
//          MPI_Issend(void_gravsumsend+offsets[tgt],Nsend_local_coeff[tgt]*CoeffSize+Nsend_local_sum[tgt]*PartSize,MPI_BYTE,tgt,1,MPI_COMM_WORLD,&data_send_requests[tgt]);
//          nsend_data++;
//#if DEBUG>=3
//          printf("[%d] Sent %d coeffs and %d patricles of data to %d for processing.\n",Task,Nsend_local_coeff[tgt],Nsend_local_sum[tgt],tgt);
//#endif
//          //printf("[%d] The %d coeffs we just sent were...\n",Task,Nsend_local_coeff[tgt]);
//          //co = void_gravsumsend+offsets[tgt];
//          //for(j=0;j<Nsend_local_coeff[tgt];j++)
//          //  printf("[%d] Coeff %d (what/where/from) %d,%d,%d.\n",Task,j,co[j].what,co[j].where,co[j].from);
//          //printf("[%d] The %d particles we just sent were...\n",Task,Nsend_local_sum[tgt]);
//          //pa = void_gravsumsend+offsets[tgt]+CoeffSize*Nsend_local_coeff[tgt];
//          //for(j=0;j<Nsend_local_sum[tgt];j++)
//          //  printf("[%d] Particle %d = %g,%g,%g.\n",Task,j,pa[j].in.x,pa[j].in.y,pa[j].in.z);
//          //data_send_where[nsend_data]=tgt;
//        }
//        nsend_hi -= done;
//        //printf("[%d] nsend_hi/data = %d,%d\n",Task,nsend_hi,nsend_data);
//        //for(i=0;i<NTask;i++)
//        //{
//        //  printf("[%d] Data/hi_pending[%d] = %d,%d.\n",Task,i,data_pending[i],hi_pending[i]);
//        //}
//      }
//    }
//    //Are there any outstanding requests to send data 
//    if(nsend_data)
//    {
//      //printf("[%d] Testing for completion of data requests nsend=%d.\n",Task,nsend_data);
//      MPI_Testsome(NTask,data_send_requests,&done,send_idxs,MPI_STATUSES_IGNORE);
//      if(done==MPI_UNDEFINED)
//        kill(908);
//      //printf("[%d] Test completed done=%d.\n",Task,done);
//      if(done)
//      {
//        //At least one finished!
//        for(i=0;i<done;i++)
//        {
//          //tgt= data_send_where[send_idxs[i]];
//          tgt = send_idxs[i];
//#if DEBUG>=3
//          printf("[%d] Data sent for processing to %d was received.\n",Task,tgt);
//#endif
//          //data_pending[tgt]=0;
//          //We still need to get the processed data back
//          if(Nsend_local_sum[tgt])
//          {
//            send_block++;
//          }
//          else
//          {
//            //Cool, we're ready to finish then
//            finish=1;
//          }
//        }
//        nsend_data -= done;
//        //printf("[%d] nsend_data = %d.\n",Task,nsend_data);
//        //for(i=0;i<NTask;i++)
//        //  printf("[%d] data_pending[%d] = %d.\n",Task,i,data_pending[i]);
//      }
//    }
//    //Are there any outstanding requests to return data
//    if(nsend_return)
//    {
//      //printf("[%d] Return sends test.\n",Task);
//      //Are there any outstanding requests to send data 
//      MPI_Testsome(NTask,data_return_requests,&done,send_idxs,MPI_STATUSES_IGNORE);
//      if(done)
//      {
//        //At least one finished!
//#if DEBUG>=3
//        for(i=0;i<done;i++)
//          printf("[%d] Processed data originally from %d and sent back to %d has been received.\n",Task,send_idxs[i],send_idxs[i]);
//#endif
//        //Fix up the array of requests
//        nsend_return -= done;
//      }
//    }
//    //printf("[%d] finish = %d, no_more_local = %d all_finish= %d nsend_data = %d, nsend_hi = %d, nsend_return = %d, recv_block = %d, send_block = %d nterm = %d.\n",Task,finish,no_more_local,all_finish,nsend_data,nsend_hi,nsend_return,recv_block,send_block,NTerm);
//    //Finished everything I can locally?
//    if(no_more_local && all_finish==0 && nsend_data==0 && nsend_hi==0 && nsend_return==0 && recv_block==0 && send_block==0)
//    {
//      tbalance=MPI_Wtime();
//#if DEBUG>=3
//      printf("[%d] I'm ready to terminate, sending term to all other processors.\n",Task);
//#endif
//      //Only do this once, send out the "I've finished everything" message to everyone
//      j=0;
//      for(i=0;i<NTask;i++)
//      {
//        if(i==Task)
//          continue;
//        MPI_Issend(0,0,MPI_INT,i,3,MPI_COMM_WORLD,&complete_requests[j]);
//        j++;
//      }
//      //Don't tell everyone about us finishing more than once
//      all_finish=1;
//      NTerm++;
//    }
//    //Have we received the termination message from every
//    //process?
//    if(NTerm==NTask)
//    {
//#if DEBUG>=3
//      printf("[%d] All termination flags received.  Ending.\n",Task);
//#endif
//      //All termination flags have been received, so end
//      MPI_Waitall(NTask-1,complete_requests,MPI_STATUSES_IGNORE);
//      Timings.balance += MPI_Wtime()-tbalance;
//      break;
//    }
//    //Can we finish and return to work?
//    if(no_more_local==0 && finish && nsend_data==0 && nsend_return==0 && recv_block==0 && send_block==0)
//    {
//      //printf("[%d] Trying to return to local work.\n",Task);
//      for(i=0;i<NTask;i++)
//      {
//        if(hi_send_requests[i]!=MPI_REQUEST_NULL)
//        {
//#if DEBUG>=3
//          printf("[%d] Cancelling greetings message to %d.\n",Task,i);
//#endif
//          MPI_Cancel(&hi_send_requests[i]);
//          MPI_Wait(&hi_send_requests[i],&status);
//          MPI_Test_cancelled(&status,&flag);
//          if(!flag)
//          {
//#if DEBUG>=3
//            printf("[%d] Cancel failed, so sending data to %d.\n",Task,i);
//#endif
//            MPI_Issend(void_gravsumsend+offsets[i],Nsend_local_coeff[i]*CoeffSize+Nsend_local_sum[i]*PartSize,MPI_BYTE,i,1,MPI_COMM_WORLD,&data_send_requests[i]);
//#if DEBUG>=3
//            printf("[%d] Sent data to %d for processing.\n",Task,i);
//#endif
//            //data_send_where[nsend_data]=tgt;
//            nsend_data++;
//          }
//          else
//          {
//#if DEBUG>=3
//            printf("[%d] Cancel completed to %d.\n",Task,i);
//#endif
//            //So we know this block is to be shifted around
//            keep_mem[i]=1;
//          }
//          nsend_hi--;
//        }
//      }
//      //Whatever happens, the hi array should be empty now
//      if(nsend_hi!=0)
//        kill(908);
//      //We still ok to finish?
//      if(nsend_data==0)
//      {
//#if DEBUG>=3
//        printf("[%d] Clearing memory and finishing.\n",Task);
//#endif
//        //Move the memory around
//        NBytesLeft = PartSize*(HalfBufferSizeBytes/PartSize);
//        GravSendSumPos = (HalfBufferSizeBytes/PartSize)-1;
//        GravSendCoeffPos = 0;
//        //Do another copy to recv and shuffle back
//        memcpy(GravRecv,GravSend,HalfBufferSizeBytes);
//        for(i=0;i<NTask;i++)
//        {
//          if(keep_mem[i])
//          {
//            //Move the coefficients back
//            memcpy(&GravCoeffSend[GravSendCoeffPos],void_gravsumrecv+offsets[i],Nsend_local_coeff[i]*CoeffSize);
//            NBytesLeft -= CoeffSize*Nsend_local_coeff[i];
//            GravSendCoeffPos += Nsend_local_coeff[i];
//            //Move the particles back
//            memcpy(&GravSend[GravSendSumPos-Nsend_local_sum[i]+1],void_gravsumrecv+offsets[i]+Nsend_local_coeff[i]*CoeffSize,Nsend_local_sum[i]*PartSize);
//            NBytesLeft -= PartSize*Nsend_local_sum[i];
//            GravSendSumPos -= Nsend_local_sum[i];
//          }
//          else
//          {
//            Nsend_local_sum[i]=0;
//            Nsend_local_coeff[i]=0;
//          }
//        }
//        //Finish!
//        break;
//      }
//    }
//
//
//
//    //Handle receiving data first
//    //Check them in special order so that we don't
//    //get stuck if we can't accept tag=1
//    //Would be nicer if we defined a tag-order array, but eh
//    //nanosleep(&Wait,NULL);
//    //tstart=MPI_Wtime();
//    mtag=0;
//    MPI_Iprobe(MPI_ANY_SOURCE,0,MPI_COMM_WORLD,&flag,&status);
//    if(!flag)
//    {
//      mtag=2;
//      MPI_Iprobe(MPI_ANY_SOURCE,2,MPI_COMM_WORLD,&flag,&status);
//      if(!flag)
//      {
//        mtag=3;
//        MPI_Iprobe(MPI_ANY_SOURCE,3,MPI_COMM_WORLD,&flag,&status);
//        if(!flag && nsend_return==0)
//        {
//          mtag=1;
//          MPI_Iprobe(MPI_ANY_SOURCE,1,MPI_COMM_WORLD,&flag,&status);
//        }
//      }
//    }
//    //Timings.bleh += MPI_Wtime()-tstart;
//    //printf("[%d] Probed tag = %d.\n",Task,msrc);
//    //MPI_Iprobe(MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&flag,&status);
//    if(flag)
//    {
//      //Store where the message originated
//      msrc=status.MPI_SOURCE;
//      //Was it a hello message
//      if(mtag==0)
//      {
//        //Complete the message receipt
//        MPI_Recv(&ncoeff[msrc],1,MPI_INT,msrc,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
//#if DEBUG>=3
//        printf("[%d] Received greetings from %d expecting %d coefficients.\n",Task,msrc,ncoeff[msrc]);
//#endif
//        //Now we're expecting to be sent some data, so better wait for it
//        recv_block++;
//      }
//      //Was it a block of data to do work on?
//      //Can't receive while I have old data still
//      //to be sent out
//      else if(mtag==1)
//      {
//        if(ncoeff[msrc]==-1)
//        {
//          for(i=0;i<NTask;i++)
//            printf("[%d] ncoeff[%d]=%d.\n",Task,i,ncoeff[i]);
//          printf("[%d] Yet to get hello message from %d.  How did we end up with data?  Seems to work ok with a continue, but I don't understand why it happens at all which makes me nervous.\n",Task,msrc);
//          kill(908);
//        }
//        if(nsend_return!=0)
//        {
//          printf("[%d] Skipping receive of data from %d because buffer isn't ready.\n",Task,msrc);
//          kill(908);
//        }
//        //Get it
//        MPI_Get_count(&status,MPI_BYTE,&nrecv);
//        MPI_Recv(GravRecv,nrecv,MPI_BYTE,msrc,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
//#if DEBUG>=3
//        printf("[%d] Received data for processing from %d, %d coeffs worth.\n",Task,msrc,nrecv/CoeffSize);
//#endif
//        if(ncoeff[msrc]>nrecv/CoeffSize)
//        {
//          printf("[%d] We got a big problem here, with ncoeff=%d nrecv=%d.\n",Task,ncoeff[msrc],nrecv);
//          for(i=0;i<NTask;i++)
//          {
//            printf("[%d] nrecv[%d] = %d.\n",Task,i,ncoeff[i]);
//          }
//          kill(908);
//        }
//        //printf("[%d] The %d coefficients we just received are.\n",Task,ncoeff[msrc]);
//        //for(i=0;i<ncoeff[msrc];i++)
//        //  printf("[%d] Received coeff %d (w/w/f) %d/%d/%d.\n",Task,i,GravCoeffRecv[i].what,GravCoeffRecv[i].where,GravCoeffRecv[i].from);
//        //One less thing to block for
//        recv_block--;
//        //First store any coefficients
//#if DEBUG>=3
//        printf("[%d] Storing %d coefficients.\n",Task,ncoeff[msrc]);
//#endif
//        for(ii=0;ii<ncoeff[msrc];ii++)
//        {
//          node = Nodes + GravCoeffRecv[ii].what;
//          node->coeff_0 += GravCoeffRecv[ii].c0;
//          for(i=0;i<3;i++)
//          {
//            node->coeff_1[i] += GravCoeffRecv[ii].c1[i];
//            for(j=0;j<3;j++)
//            {
//              node->coeff_2[i][j] += GravCoeffRecv[ii].c2[i][j];
//              for(k=0;k<3;k++)
//              {
//                node->coeff_3[i][j][k] += GravCoeffRecv[ii].c3[i][j][k];
//              }
//            }
//          }
//        }
//        //Now handle any particles we may have
//        npart = (nrecv-CoeffSize*ncoeff[msrc])/PartSize;
//#if DEBUG>=3
//        printf("[%d] Stored %d local coefficients about to process %d particles.\n",Task,ncoeff[msrc],npart);
//#endif
//        if(npart)
//        {
//          CommNode.count = npart;
//          CommNode.daughter = CoeffSize*ncoeff[msrc];
//          //for(i=0;i<npart;i++)
//          //{
//          //  printf("[%d] About to process foreign particle %d at x/y/z %g,%g,%g.\n",Task,i,pa[i].in.x,pa[i].in.y,pa[i].in.z);
//          //}
//          direct_sum(&CommNode,&CommNode);
//          //Send the results back if necessary
//          MPI_Issend(void_gravsumrecv+CommNode.daughter,npart*PartSize,MPI_BYTE,msrc,2,MPI_COMM_WORLD,&data_return_requests[msrc]);
//          //GravRecv = void_gravsumrecv;
//#if DEBUG>=3
//          printf("[%d] Sent processed data back to %d.\n",Task,msrc);
//#endif
//          //return_send_where[nsend_return]=msrc;
//          //return_pending[msrc]=1;
//          nsend_return++;
//        }
//        ncoeff[msrc]=-1;
//      }
//      //Was it a block of data with work done on it to be stored?
//      else if(mtag==2)
//      {
//        //Get them back again
//        MPI_Get_count(&status,MPI_BYTE,&nrecv);
//        pa = void_gravsumsend+offsets[msrc]+Nsend_local_coeff[msrc]*CoeffSize;
//        MPI_Recv(pa,nrecv,MPI_BYTE,msrc,2,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
//#if DEBUG>=3
//        printf("[%d] Received data with processing done on it back from %d.\n",Task,msrc);
//#endif
//        //I've got something back!  Hooray!
//        finish=1;
//        send_block--;
//        //Store the results locally
//        for(j=0;j<Nsend_local_sum[msrc];j++)
//        {
//          l = pa[j].out.store;
//          G[l].ax += pa[j].out.ax;
//          G[l].ay += pa[j].out.ay;
//          G[l].az += pa[j].out.az;
//        }
//        //printf("[%d] Stored.\n",Task);
//      }
//      else if(mtag==3)
//      {
//        msrc = status.MPI_SOURCE;
//        //Termination signal received.  Update global 
//        //marker
//        MPI_Recv(0,0,MPI_INT,msrc,3,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
//        //printf("[%d] Received termination flag from %d.\n",Task,msrc);
//        NTerm++;
//      }
//      else
//      {
//        printf("[%d] I've made a huge mistake...Unknown tag\n",Task);
//        kill(908);
//      }
//    }
//  }
//
//  Timings.grav_com += MPI_Wtime()-tstart;
//  //Tstart = MPI_Wtime();
//
//}

//void evaluate_nodes(void)
//{
//  struct node* cell=Nodes;
//  while(1)
//  {
//    //If it's meta, open it up
//    if(cell->flag & NODE_META)
//    {
//      cell = Nodes+cell->daughter;
//    }
//    //If it's not locally owned, skip it (Change this to local if you're
//    //storing non-locally owned co-efficients)
//    else if((cell->flag & NODE_OWNER)!=Task)
//    {
//      if(cell->sister==-1)
//        break;
//      cell = Nodes+cell->sister;
//    }
//    //It's local, add the answer in
//    else
//    {
//      //Do it!
//      evaluate_node(cell);
//      //Have to move on. Open it up unless it's a leaf (they're already done)
//      if(cell->flag & NODE_LEAF)
//      {
//        if(cell->sister==-1)
//          break;
//        cell = Nodes + cell->sister;
//      }
//      else
//      {
//        cell = Nodes + cell->daughter;
//      }
//    }
//  }
//}

//void evaluate_node(struct node* cell)
//{
//  int i,j,k,ii,start;
//  double x[3];
//  //Need to set the starting point ok
//  start = cell->mob==-10 ? cell->daughter : cell->mob;
//  for(ii=start;ii<start+cell->count;ii++)
//  {
//    //if(ii==0 && cell->coeff_1[0]!=0)
//    //{
//    //   printf("[%d] Adding a contribution from node at  ([%g,%g],[%g,%g],[%g,%g]) offset=%d,count=%d,mother=%d,sister=%d,daughter=%d,trunkity=%d,locality=%d,leafity=%d,meta=%d,owner=%d.\n",Task,cell->lows[0],cell->highs[0],cell->lows[1],cell->highs[1],cell->lows[2],cell->highs[2],(int) (cell-Nodes),cell->count,cell->mother,cell->sister,cell->daughter,cell->flag & NODE_TRUNK,cell->flag & NODE_LOCAL,cell->flag & NODE_LEAF,cell->flag & NODE_META,(cell->flag) & NODE_OWNER);
//    //}
//    x[0]=G[ii].x-cell->com[0];
//    x[1]=G[ii].y-cell->com[1];
//    x[2]=G[ii].z-cell->com[2];
//    //G[ii].pot -= cell->coeff_0;
//    //G[ii].imark++;
//    for(i=0;i<3;i++)
//    {
//      //G[ii].pot -= x[i]*cell->coeff_1[i];
//      if(i==0)
//        G[ii].ax += cell->coeff_1[i];
//      else if(i==1)
//        G[ii].ay += cell->coeff_1[i];
//      else
//        G[ii].az += cell->coeff_1[i];
//      for(j=0;j<3;j++)
//      {
//        //G[ii].pot -= 0.5*x[i]*x[j]*cell->coeff_2[i][j];
//        if(i==0)
//          G[ii].ax +=  cell->coeff_2[i][j]*x[j];
//        else if(i==1)
//          G[ii].ay +=  cell->coeff_2[i][j]*x[j];
//        else
//          G[ii].az +=  cell->coeff_2[i][j]*x[j];
//        for(k=0;k<3;k++)
//        {
//          //G[ii].pot -= (1.0/6.0) * x[i]*x[j]*x[k]*cell->coeff_3[i][j][k];
//          if(i==0)
//            G[ii].ax += 0.5 * cell->coeff_3[i][j][k]*x[j]*x[k];
//          else if(i==1)
//            G[ii].ay += 0.5 * cell->coeff_3[i][j][k]*x[j]*x[k];
//          else
//            G[ii].az += 0.5 * cell->coeff_3[i][j][k]*x[j]*x[k];
//        }
//      }
//    }
//  }
//}

//void interact_nodes(struct node* nodeA,struct node* nodeB)
//{
//  int i;
//  struct node* child;
//  struct node* child2;
//  //if(Flag || ((nodeA->flag & NODE_OWNER)!=(nodeB->flag & NODE_OWNER)))
//  //{
//  //printf("\n[%d] NodeA is from ([%g,%g],[%g,%g],[%g,%g]) offset=%d,count=%d,mother=%d,sister=%d,daughter=%d,trunkity=%d,locality=%d,leafity=%d,meta=%d,owner=%d,mob=%d,rmax=%g,com=%g,%g,%g.\n",Task,nodeA->lows[0],nodeA->highs[0],nodeA->lows[1],nodeA->highs[1],nodeA->lows[2],nodeA->highs[2],(int) (nodeA-Nodes),nodeA->count,nodeA->mother,nodeA->sister,nodeA->daughter,nodeA->flag & NODE_TRUNK,nodeA->flag & NODE_LOCAL,nodeA->flag & NODE_LEAF,nodeA->flag & NODE_META,(nodeA->flag) & NODE_OWNER,nodeA->mob,nodeA->rmax,nodeA->com[0],nodeA->com[1],nodeA->com[2]);
//  //printf("[%d] NodeB is from ([%g,%g],[%g,%g],[%g,%g]) offset=%d,count=%d,mother=%d,sister=%d,daughter=%d,trunkity=%d,locality=%d,leafity=%d,meta=%d,owner=%d,mob=%d,rmax=%g,com=%g,%g,%g.\n\n",Task,nodeB->lows[0],nodeB->highs[0],nodeB->lows[1],nodeB->highs[1],nodeB->lows[2],nodeB->highs[2],(int) (nodeB-Nodes),nodeB->count,nodeB->mother,nodeB->sister,nodeB->daughter,nodeB->flag & NODE_TRUNK,nodeB->flag & NODE_LOCAL,nodeB->flag & NODE_LEAF,nodeB->flag & NODE_META,(nodeB->flag) & NODE_OWNER,nodeB->mob,nodeB->rmax,nodeB->com[0],nodeB->com[1],nodeB->com[2]);
//  //  Flag=1;
//  //  //kill(112);
//  //}
//  //if(!(nodeB->flag & NODE_LOCAL) && !(nodeB->flag & NODE_META))
//  //{
//  //  kill(343);
//  //}
//  //If neither have any (possibly) owned children, this is a job for someone else
//  if(!(nodeA->flag & NODE_META) && ((nodeA->flag & NODE_OWNER)!=Task) &&
//     !(nodeB->flag & NODE_META) && ((nodeB->flag & NODE_OWNER)!=Task))
//  {
//    //printf("[%d] Skipping.\n",Task);
//    //kill(1212);
//    return;
//  }
//  Touched++;
//  //if(Dt)
//  //printf("[%d] Touched %d times.\n",Task,Touched);
//  //For now just open up anything that is ambiguously assigned
//  //Might be a tiny bit slower, but simplifies other things a lot
//  //OK, we've got at least one locally owned node, try and process it
//  //Do we need to open it up?
//  if((nodeA==nodeB) || (nodeA->flag & NODE_META) || (nodeB->flag & NODE_META) || !process_interaction(nodeA,nodeB))
//  {
//    //if(Flag)
//    //  printf("[%d] Opening node.\n",Task);
//    //Self interaction?
//    if(nodeA==nodeB)
//    {
//      //if(Flag)
//      //  printf("[%d] Nodes the same.\n",Task);
//      //Each mutual interaction should appear but once
//      //loop over all children interactions to ensure this
//      //need to handle leaf self-interaction specially
//      if(nodeA->flag & NODE_LEAF)
//      {
//        direct_sum(nodeA,nodeA);
//      }
//      else
//      {
//        //Ensure all processors do self-interaction first
//        child = Nodes+nodeA->daughter;
//        while(1)
//        {
//          interact_nodes(child,child);
//          if(child->sister==nodeA->sister)
//            break;
//          child = Nodes+child->sister;
//        }
//        //Then do all the other interactions
//        child = Nodes+nodeA->daughter;
//        while(child->sister!=nodeA->sister)
//        {
//          child2 = Nodes + child->sister;
//          while(1)
//          {
//            interact_nodes(child,child2);
//            if(child2->sister==nodeA->sister)
//              break;
//            child2 = Nodes+child2->sister;
//          }
//          if(child->sister==nodeA->sister)
//            break;
//          child = Nodes + child->sister;
//        }
//      }
//    }
//    //Bad, no good split whichever node we need to split
//    else if((nodeA->flag & NODE_META) || (!(nodeB->flag & NODE_META) && (nodeA->rmax>nodeB->rmax)))
//    {
//      //if(Flag)
//      //  printf("[%d] NodeA needs opening.\n",Task);
//      if(nodeB->mob==-10)
//      {
//        printf("[%d] Convention is for nodeA to always be the body node when needed...\n",Task);
//        kill(403);
//      }
//      if(nodeA->flag & NODE_LEAF)
//      {
//        //If it's not local we'll receive the 
//        //relevant co-efficients at the export phase
//        //Do all the body children
//        if(nodeA->flag & NODE_LOCAL)
//        {
//          for(i=0;i<nodeA->count;i++)
//          {
//            if(Lock)
//            {
//              printf("[%d] Body current has (%g,%g,%g).\n",Task,Body.com[0],Body.com[1],Body.com[2]);
//              printf("[%d] But nodeA from ([%g,%g],[%g,%g],[%g,%g]) offset=%d,count=%d,mother=%d,sister=%d,daughter=%d,trunkity=%d,locality=%d,leafity=%d,meta=%d,owner=%d,rmax=%g.\n",Task,nodeA->lows[0],nodeA->highs[0],nodeA->lows[1],nodeA->highs[1],nodeA->lows[2],nodeA->highs[2],(int) (nodeA-Nodes),nodeA->count,nodeA->mother,nodeA->sister,nodeA->daughter,nodeA->flag & NODE_TRUNK,nodeA->flag & NODE_LOCAL,nodeA->flag & NODE_LEAF,nodeA->flag & NODE_META,(nodeA->flag) & NODE_OWNER,nodeA->rmax);
//              kill(908);
//            }
//            Body=NULL_Body;
//            Body.com[0] = Shadows[nodeA->flag & NODE_OWNER].G[nodeA->daughter+i].x;
//            Body.com[1] = Shadows[nodeA->flag & NODE_OWNER].G[nodeA->daughter+i].y;
//            Body.com[2] = Shadows[nodeA->flag & NODE_OWNER].G[nodeA->daughter+i].z;
//            Body.hmax = Shadows[nodeA->flag & NODE_OWNER].G[nodeA->daughter+i].hsml;
//            Body.daughter = nodeA->daughter+i;
//            Body.flag = nodeA->flag;
//            Lock=1;
//            interact_nodes(&Body,nodeB);
//            evaluate_node(&Body);
//            Lock=0;
//          }
//        }
//      }
//      else
//      {
//        child = Nodes + nodeA->daughter;
//        while(1)
//        {
//          interact_nodes(nodeB,child);
//          if(child->sister==nodeA->sister)
//            break;
//          child = Nodes + child->sister;
//        }
//      }
//    }
//    else
//    {
//      //if(Flag)
//      //  printf("[%d] NodeB needs opening.\n",Task);
//      if(nodeB->flag & NODE_LEAF)
//      {
//        //If nodeB is already a body, then we're now on
//        //to the painful direct sum case
//        if(nodeA->mob==-10)
//        {
//          direct_sum(nodeA,nodeB);
//        }
//        //If it's not local we'll receive the 
//        //relevant co-efficients at the export phase
//        //Do all the body children
//        else if(nodeB->flag & NODE_LOCAL)
//        {
//          for(i=0;i<nodeB->count;i++)
//          {
//            if(Lock)
//            {
//              printf("[%d] Body current has (%g,%g,%g).\n",Task,Body.com[0],Body.com[1],Body.com[2]);
//              printf("[%d] But nodeB from ([%g,%g],[%g,%g],[%g,%g]) offset=%d,count=%d,mother=%d,sister=%d,daughter=%d,trunkity=%d,locality=%d,leafity=%d,meta=%d,owner=%d,rmax=%g.\n",Task,nodeB->lows[0],nodeB->highs[0],nodeB->lows[1],nodeB->highs[1],nodeB->lows[2],nodeB->highs[2],(int) (nodeB-Nodes),nodeB->count,nodeB->mother,nodeB->sister,nodeB->daughter,nodeB->flag & NODE_TRUNK,nodeB->flag & NODE_LOCAL,nodeB->flag & NODE_LEAF,nodeB->flag & NODE_META,(nodeB->flag) & NODE_OWNER,nodeB->rmax);
// 
//              kill(908);
//            }
//            Body=NULL_Body;
//            Body.com[0] = Shadows[nodeB->flag & NODE_OWNER].G[nodeB->daughter+i].x;
//            Body.com[1] = Shadows[nodeB->flag & NODE_OWNER].G[nodeB->daughter+i].y;
//            Body.com[2] = Shadows[nodeB->flag & NODE_OWNER].G[nodeB->daughter+i].z;
//            Body.hmax = Shadows[nodeB->flag & NODE_OWNER].G[nodeB->daughter+i].hsml;
//            Body.daughter = nodeB->daughter+i;
//            Body.flag = nodeB->flag;
//            Lock=1;
//            interact_nodes(&Body,nodeA);
//            evaluate_node(&Body);
//            Lock=0;
//          }
//        }
//      }
//      else
//      {
//        child = Nodes + nodeB->daughter;
//        while(1)
//        {
//          interact_nodes(nodeA,child);
//          if(child->sister==nodeB->sister)
//            break;
//          child = Nodes + child->sister;
//        }
//      }
//    }
//  }
//}

//When doing a direct sum we always have to store
//the results IN BOTH DIRECTIONS!  That is F_AB and F_BA, even
//if A and B are on different processors.
//It should be OK to do any direct sums at any point in the coefficient
//construction as long as it is done in such a way that one processor
//will "own" it.  That is, only one processor should call the direct
//sum function, the other should just skip over that interaction and 
//let it be taken care of later in the comm phase
//void direct_sum(struct node* nodeA,struct node* nodeB)
//{
//  int localA,localB;
//  int ownedA,ownedB;
//  int i,j,store;
//  double dx,dy,dz,fac,r;
//  double x,y,z;
//#ifndef CONSTANT_GAS_SOFT
//  double h;
//#endif
//  void *void_gravsumrecv = GravRecv;
//  union grav_comm *pa;
//  Summed++;
//  //One of these will be local, the other not necessarily so.
//  //The local one is the only one that will ever know this interaction
//  //is happening though
//  ownedA = (nodeA->flag & NODE_OWNER)==Task;
//  ownedB = (nodeB->flag & NODE_OWNER)==Task;
//  localA = nodeA->flag & NODE_LOCAL;
//  localB = nodeB->flag & NODE_LOCAL;
//  //The mob==-100 tells us that although nodeA appears local,
//  //it is actually "on loan" from another process.  We should calculate
//  //the forces on daughters of nodeB and store the forces on nodeA's daughters
//  //in the slots is GravRecv that used to hold positions
//  if(nodeA->mob==-100)
//  {
//    pa = void_gravsumrecv+nodeA->daughter;
//    for(i=0;i<nodeA->count;i++)
//    {
//      x = pa[i].in.x;
//      y = pa[i].in.y;
//      z = pa[i].in.z;
//      //printf("[%d] Foreign x/y/z/i = %g,%g,%g,%d.\n",Task,x,y,z,i);
//      //printf("[%d] Foreign x/y/z/i+1 = %g,%g,%g,%d.\n",Task,GravRecv[i+1].in.x,GravRecv[i+1].in.y,GravRecv[i+1].in.z,i+1);
//#ifndef CONSTANT_GAS_SOFT
//      h = pa[i].in.h;
//#endif
//      localA = pa[i].in.what;
//      localB = pa[i].in.where;
//      j = pa[i].in.from;
//      store = pa[i].in.store;
//      //Set the target node for this particle
//      nodeB = Nodes+pa[i].in.what;
//      //Initialise the acceleration
//      pa[i].out.ax = 0;
//      pa[i].out.ay = 0;
//      pa[i].out.az = 0;
//      pa[i].out.what = localA;
//      pa[i].out.where = localB;
//      pa[i].out.from = j;
//      pa[i].out.store = store;
//      for(j=0;j<nodeB->count;j++)
//      {
//        dx = x-G[nodeB->daughter+j].x;
//        dy = y-G[nodeB->daughter+j].y;
//        dz = z-G[nodeB->daughter+j].z;
//        r = sqrt(dx*dx+dy*dy+dz*dz);
//#if HYDRO<=1 || defined CONSTANT_GAS_SOFT
//        fac = Params.G*PcleMass*softened_grav(r,Params.gas_soft);
//#else
//        fac = Params.G*PcleMass*.5*(softened_grav(r,h)+softened_grav(r,G[nodeB->daughter+j].hsml));
//#endif
//        pa[i].out.ax -= fac*dx;
//        pa[i].out.ay -= fac*dy;
//        pa[i].out.az -= fac*dz;
//        G[nodeB->daughter+j].ax += fac*dx;
//        G[nodeB->daughter+j].ay += fac*dy;
//        G[nodeB->daughter+j].az += fac*dz;
//      }
//    }
//  }
//  //The easy case, is when we're both owned by this processor
//  else if(ownedA && ownedB)
//  {
//    for(i=0;i<nodeA->count;i++)
//    {
//      //Be aware of self interaction and prevent double counting
//      j = nodeA==nodeB ? i+1 : 0;
//      for(;j<nodeB->count;j++)
//      {
//        dx = G[nodeA->daughter+i].x-G[nodeB->daughter+j].x;
//        dy = G[nodeA->daughter+i].y-G[nodeB->daughter+j].y;
//        dz = G[nodeA->daughter+i].z-G[nodeB->daughter+j].z;
//        r = sqrt(dx*dx+dy*dy+dz*dz);
//#if HYDRO<=1 || defined CONSTANT_GAS_SOFT
//        fac=Params.G*PcleMass*softened_grav(r,Params.gas_soft);
//#else
//        fac=Params.G*PcleMass*.5*(softened_grav(r,G[nodeA->daughter+i].hsml)+softened_grav(r,G[nodeB->daughter+j].hsml));
//#endif
//        G[nodeA->daughter+i].ax -= fac*dx;
//        G[nodeA->daughter+i].ay -= fac*dy;
//        G[nodeA->daughter+i].az -= fac*dz;
//        G[nodeB->daughter+j].ax += fac*dx;
//        G[nodeB->daughter+j].ay += fac*dy;
//        G[nodeB->daughter+j].az += fac*dz;
//
//      }
//    }
//  }
//  else
//  {
//    //One of the nodes is locally owned, the other one could be either in a shadow domain,
//    //so still local, or it could be foreign.  If it's foreign, we need to run the export
//    //routine if the foreign processor doesn't have a copy of our domain (if it does
//    //we can ignore it).
//    if(ownedA && !localB)
//    {
//      //This will be processed the other way around, so ignore it for now
//      if(LShadows[nodeB->flag & NODE_OWNER])
//        return;
//      for(i=0;i<nodeA->count;i++)
//      {
//        //Will storing this overwrite something or hit the wall?
//        if(NBytesLeft<PartSize)
//        {
//          grav_comm_loop(0);
//        }
//        //Cool, store it
//        GravSend[GravSendSumPos].in.x = G[nodeA->daughter+i].x;
//        GravSend[GravSendSumPos].in.y = G[nodeA->daughter+i].y;
//        GravSend[GravSendSumPos].in.z = G[nodeA->daughter+i].z;
//        //GravSend[GravSendSumPos].in.id = G[nodeA->daughter+i].id;
//        //if(GravSend[GravSendSumPos+1].in.id==G[nodeA->daughter+i].id)
//        //{
//        //  printf("[%d] nodeA = %d, daughter = %d, pid-1 = %d, pid=%d i=%d.\n",Task,nodeA,nodeA->daughter,G[nodeA->daughter+i-1].id,G[nodeA->daughter+i].id);
//        //  kill(3);
//        //}
//        GravSend[GravSendSumPos].in.h = G[nodeA->daughter+i].hsml;
//        GravSend[GravSendSumPos].in.from = Task;
//        GravSend[GravSendSumPos].in.where = nodeB->flag & NODE_OWNER;
//        GravSend[GravSendSumPos].in.what = nodeB-Nodes;
//        GravSend[GravSendSumPos].in.store = nodeA->daughter+i;
//
//        //Update the pointer and counters
//        GravSendSumPos--;
//        NBytesLeft -= PartSize;
//        Nsend_local_sum[nodeB->flag & NODE_OWNER]++;
//      }
//    }
//    else if(ownedB && !localA)
//    {
//      if(LShadows[nodeA->flag & NODE_OWNER])
//        return;
//      for(i=0;i<nodeB->count;i++)
//      {
//        //Will storing this overwrite something or hit the wall?
//        if(NBytesLeft<PartSize)
//        {
//          grav_comm_loop(0);
//        }
//        //Cool, store it
//        GravSend[GravSendSumPos].in.x = G[nodeB->daughter+i].x;
//        GravSend[GravSendSumPos].in.y = G[nodeB->daughter+i].y;
//        GravSend[GravSendSumPos].in.z = G[nodeB->daughter+i].z;
//        //GravSend[GravSendSumPos].in.id = G[nodeA->daughter+i].id;
//        GravSend[GravSendSumPos].in.h = G[nodeB->daughter+i].hsml;
//        GravSend[GravSendSumPos].in.from = Task;
//        GravSend[GravSendSumPos].in.where = nodeA->flag & NODE_OWNER;
//        GravSend[GravSendSumPos].in.what = nodeA-Nodes;
//        GravSend[GravSendSumPos].in.store = nodeB->daughter+i;
//
//        //Update the pointer and counters
//        GravSendSumPos--;
//        NBytesLeft -= PartSize;
//        Nsend_local_sum[nodeA->flag & NODE_OWNER]++;
//      }
//    }
//    //OK, no exporting is needed, so do the local calculations, storing this
//    //as necessary
//    else
//    {
//      //These should be seen as storeA and storeB from here on out
//      //Store results for B on this process if process B isn't going to see this interaction
//      ownedA = ownedA ? 1 : !LShadows[nodeA->flag & NODE_OWNER];
//      ownedB = ownedB ? 1 : !LShadows[nodeB->flag & NODE_OWNER];
//      for(i=0;i<nodeA->count;i++)
//      {
//        for(j=0;j<nodeB->count;j++)
//        {
//          dx = Shadows[nodeA->flag & NODE_OWNER].G[nodeA->daughter+i].x-Shadows[nodeB->flag & NODE_OWNER].G[nodeB->daughter+j].x;
//          dy = Shadows[nodeA->flag & NODE_OWNER].G[nodeA->daughter+i].y-Shadows[nodeB->flag & NODE_OWNER].G[nodeB->daughter+j].y;
//          dz = Shadows[nodeA->flag & NODE_OWNER].G[nodeA->daughter+i].z-Shadows[nodeB->flag & NODE_OWNER].G[nodeB->daughter+j].z;
//          r = sqrt(dx*dx+dy*dy+dz*dz);
//  #if HYDRO<=1 || defined CONSTANT_GAS_SOFT
//          fac=Params.G*PcleMass*softened_grav(r,Params.gas_soft);
//  #else
//          fac=Params.G*PcleMass*.5*(softened_grav(r,Shadows[nodeA->flag & NODE_OWNER].G[nodeA->daughter+i].hsml)+softened_grav(r,Shadows[nodeB->flag & NODE_OWNER].G[nodeB->daughter+j].hsml));
//  #endif
//          //Work out what needs storing
//          if(ownedA)
//          {
//            Shadows[nodeA->flag & NODE_OWNER].G[nodeA->daughter+i].ax -= fac*dx;
//            Shadows[nodeA->flag & NODE_OWNER].G[nodeA->daughter+i].ay -= fac*dy;
//            Shadows[nodeA->flag & NODE_OWNER].G[nodeA->daughter+i].az -= fac*dz;
//          }
//          if(ownedB)
//          {
//            Shadows[nodeB->flag & NODE_OWNER].G[nodeB->daughter+j].ax += fac*dx;
//            Shadows[nodeB->flag & NODE_OWNER].G[nodeB->daughter+j].ay += fac*dy;
//            Shadows[nodeB->flag & NODE_OWNER].G[nodeB->daughter+j].az += fac*dz;
//          }
//        }
//      }
//    }
//  }
//}




//Returns 0 if A and B aren't well separated
//Assumes at least one of A and B are local
//Can't both by body nodes, direct_sum will be called in that
//case.  By convention nodeA is the body node if there is one
//int process_interaction(struct node* nodeA,struct node* nodeB)
//{
//  double com[3],com_mag;
//  double dg0,dg1,dg2,dg3;
//  double massA,massB;
//  double coeff_BtoA_0=0;
//  double coeff_AtoB_0=0;
//  double coeff_BtoA_1[3]={0};
//  double coeff_AtoB_1[3]={0};
//  double coeff_BtoA_2[3][3]={{0}};
//  double coeff_BtoA_3[3][3][3]={{{0}}};
//  int storeA,storeB;
//  int i,j,k;
//  //Test if these two nodes are well separated
//  //Assume that nodeA is the sink and B the source
//  com[0] = nodeA->com[0]-nodeB->com[0];
//  com[1] = nodeA->com[1]-nodeB->com[1];
//  com[2] = nodeA->com[2]-nodeB->com[2];
//  com_mag = sqrt(com[0]*com[0]+com[1]*com[1]+com[2]*com[2]);
//  //Add a little bit extra to account for the smoothing length.
//  //The point of this is to ensure that if the two nodes contain
//  //any particles that should receive some softening as part of
//  //their interaction, we open it up
//#if HYDRO<=1 || defined CONSTANT_GAS_SOFT
//  dg0= KER_SUPPORT*Params.gas_soft;
//#else
//  dg0= nodeA->hmax>nodeB->hmax ? KER_SUPPORT*nodeA->hmax : KER_SUPPORT*nodeB->hmax;
//#endif
//  if((com_mag*Params.grav_opening)<=(nodeA->rmax+nodeB->rmax+dg0))
//  {
//    //Not well separated so stop
//    return 0;
//  }
//  //They are well separated so do the actual processing
//  //Calculate the derivatives frist
//  dg0 = deriv_grav_greens_0(com_mag);
//  dg1 = deriv_grav_greens_1(com_mag);
//  dg2 = deriv_grav_greens_2(com_mag);
//  dg3 = deriv_grav_greens_3(com_mag);
//  //Is this a body node interaction?
//  if(nodeB->mob==-10)
//  {
//    printf("[%d] I've made a huge mistake...\n",Task);
//    kill(908);
//  }
//  //Calculate the co-efficients.  Can be made more efficient by only calculating
//  //the coefficients we need, but eh.
//  coeff_BtoA_0 = dg0;
//  coeff_AtoB_0 = dg0;
//  for(i=0;i<3;i++)
//  {
//    coeff_BtoA_0 += .5*dg1*nodeB->quad[i][i];
//    coeff_BtoA_1[i] += com[i]*dg1;
//    coeff_AtoB_0 += .5*dg1*nodeA->quad[i][i];
//    //Because R_i -> -R_i when we swap A and B
//    coeff_AtoB_1[i] -= com[i]*dg1;
//    for(j=0;j<3;j++)
//    {
//      coeff_BtoA_0 += .5*com[i]*com[j]*nodeB->quad[i][j]*dg2;
//      coeff_BtoA_1[i] += .5*com[i]*dg2*nodeB->quad[j][j];
//      coeff_BtoA_1[i] += com[j]*nodeB->quad[i][j]*dg2;
//      coeff_AtoB_0 += .5*com[i]*com[j]*nodeA->quad[i][j]*dg2;
//      coeff_AtoB_1[i] -= .5*com[i]*dg2*nodeA->quad[j][j];
//      coeff_AtoB_1[i] -= com[j]*nodeA->quad[i][j]*dg2;
//      if(i==j)
//        coeff_BtoA_2[i][j] += dg1;
//      coeff_BtoA_2[i][j] += com[i]*com[j]*dg2;
//      for(k=0;k<3;k++)
//      {
//        coeff_BtoA_1[i] += .5*com[i]*com[j]*com[k]*dg3*nodeB->quad[j][k];
//        coeff_AtoB_1[i] -= .5*com[i]*com[j]*com[k]*dg3*nodeA->quad[j][k];
//        if(i==j)
//          coeff_BtoA_3[i][j][k] += com[k] * dg2;
//        if(j==k)
//          coeff_BtoA_3[i][j][k] += com[i] * dg2;
//        if(i==k)
//          coeff_BtoA_3[i][j][k] += com[j] * dg2;
//        coeff_BtoA_3[i][j][k] += com[i]*com[j]*com[k] * dg3;
//      }
//    }
//  }
//  //Now store them appropriately
//  massA = PcleMass*nodeA->count;
//  massB = PcleMass*nodeB->count;
//  //Store the locally owned coefficients
//  storeA = ((nodeA->flag & NODE_OWNER)==Task);
//  storeB = ((nodeB->flag & NODE_OWNER)==Task);
//  if(storeA)
//    nodeA->coeff_0 += massB*coeff_BtoA_0;
//  if(storeB)
//    nodeB->coeff_0 += massA*coeff_AtoB_0;
//  for(i=0;i<3;i++)
//  {
//    if(storeA)
//      nodeA->coeff_1[i] += massB * coeff_BtoA_1[i];
//    if(storeB)
//      nodeB->coeff_1[i] += massA * coeff_AtoB_1[i];
//    for(j=0;j<3;j++)
//    {
//      if(storeA)
//        nodeA->coeff_2[i][j] += massB * coeff_BtoA_2[i][j];
//      //Because AtoB_2 = BtoA_2
//      if(storeB)
//        nodeB->coeff_2[i][j] += massA * coeff_BtoA_2[i][j];
//      for(k=0;k<3;k++)
//      {
//        if(storeA)
//          nodeA->coeff_3[i][j][k] += massB * coeff_BtoA_3[i][j][k];
//        //Because AtoB_3 = -BtoA_3
//        if(storeB)
//          nodeB->coeff_3[i][j][k] -= massA * coeff_BtoA_3[i][j][k];
//      }
//    }
//  }
//
//  //Do we need to export the coefficients on B to B?
//  if(nodeA->mob==-10 && storeA && !storeB && !LShadows[nodeB->flag & NODE_OWNER])
//  {
//    //Is there space to export it?
//    if(NBytesLeft<CoeffSize)
//    {
//      grav_comm_loop(0);
//    }
//    GravCoeffSend[GravSendCoeffPos].c0 = massA*coeff_AtoB_0;
//    GravCoeffSend[GravSendCoeffPos].where = nodeB->flag & NODE_OWNER;
//    GravCoeffSend[GravSendCoeffPos].what = nodeB - Nodes;
//    GravCoeffSend[GravSendCoeffPos].from = Task;
//    for(i=0;i<3;i++)
//    {
//      GravCoeffSend[GravSendCoeffPos].c1[i] = massA*coeff_AtoB_1[i];
//      for(j=0;j<3;j++)
//      {
//        GravCoeffSend[GravSendCoeffPos].c2[i][j] = massA*coeff_BtoA_2[i][j];
//        for(k=0;k<3;k++)
//        {
//          GravCoeffSend[GravSendCoeffPos].c3[i][j][k] = -1*massA*coeff_BtoA_3[i][j][k];
//        }
//      }
//    }
//    //Update pointers and such
//    NBytesLeft -= CoeffSize;
//    GravSendCoeffPos++;
//    Nsend_local_coeff[nodeB->flag & NODE_OWNER]++;
//  }
//  return 1;
//}

//Has to be FLOAT not double because it takes argument straight
//from data store
double get_rmax(struct node* cell)
{
  double d2=0;
  double phi,tmp;
  //Add the z contribution.  Easy
  d2 += fmax((cell->highs[2]-cell->com[2])*(cell->highs[2]-cell->com[2]),(cell->lows[2]-cell->com[2])*(cell->lows[2]-cell->com[2]));
  //Get the ideal phi in the range [0,2pi)
  phi = atan2(cell->com[1],cell->com[0]);
  if(phi<0)
    phi+=PI2;
  //Is it within our cell?
  //This is always added
  d2+=cell->com[0]*cell->com[0]+cell->com[1]*cell->com[1];
  //This assumes no "boundary crossing" i.e. phi_low<phi_high
  if(phi>=cell->lows[1] && phi<=cell->highs[1])
  {
    //It's in the range, need only check two points
    phi = -2*(cell->com[0]*cos(phi)+cell->com[1]*sin(phi));
    d2+=fmax(cell->lows[0]*(cell->lows[0]+phi),cell->highs[0]*(cell->highs[0]+phi));
  }
  else
  {
    //Bugger.  Guess we have to check all four corners then...
    phi = -2*(cell->com[0]*cos(cell->lows[1])+cell->com[1]*sin(cell->lows[1]));
    phi = fmax(cell->lows[0]*(cell->lows[0]+phi),cell->highs[0]*(cell->highs[0]+phi));
    tmp = -2*(cell->com[0]*cos(cell->highs[1])+cell->com[1]*sin(cell->highs[1]));
    tmp = fmax(cell->lows[0]*(cell->lows[0]+tmp),cell->highs[0]*(cell->highs[0]+tmp));
    d2 += fmax(phi,tmp);
  }
  return d2;
}

//This should be run BEFORE tree_join().  There is no attempt
//to move locally calculated multipoles onto other processors
//if it's called after that, any node owned by another processor
//will not have the right multipoles
//void multipole_calc(void)
//{
//  int i,j,k;
//  double (*com_quad)[12];
//
//  com_quad = malloc(NNodesTrunk*sizeof(double[12]));
//
//  //Should do everything local I can do (i.e., branch and bellow finalised)
//  calc_branch_poles(Nodes,Nodes->sister);
//  //Put all the things we've got so far in an array for transmission
//  for(i=0;i<NNodesTrunk;i++)
//  {
//    //Most of these contributions will actually be 0, but that's OK
//    for(j=0;j<3;j++)
//    {
//      com_quad[i][j] = Nodes[i].com[j];
//      for(k=0;k<3;k++)
//      {
//        com_quad[i][3+3*j+k] = Nodes[i].quad[j][k];
//      }
//    }
//    //printf("[%d] Trunk[%d] with count=%d,meta=%d has COM=(%g,%g,%g).\n",Task,i,Nodes[i].count,Nodes[i].flag & NODE_META,Nodes[i].com[0],Nodes[i].com[1],Nodes[i].com[2]);
//  }
//  //Sum up across all processors and put everything back
//  MPI_Allreduce(MPI_IN_PLACE,com_quad,NNodesTrunk*12,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
//  for(i=0;i<NNodesTrunk;i++)
//  {
//    for(j=0;j<3;j++)
//    {
//      for(k=0;k<3;k++)
//      {
//        Nodes[i].quad[j][k] = com_quad[i][3+3*j+k];
//      }
//      Nodes[i].com[j] =com_quad[i][j];
//    }
//  }
//  //Go through the trunk finishing anything that relied on more than one processor
//  //we got the trunk nodes remotely owned in the allreduce operation above, they
//  //were already finished by their host processors
//  for(i=0;i<NNodesTrunk;i++)
//  {
//    if(Nodes[i].flag & NODE_META)
//    {
//      //Finalise COM
//      for(j=0;j<3;j++)
//        Nodes[i].com[j] /= Nodes[i].count;
//      //Add the last quadrupole correction factor in
//      for(j=0;j<3;j++)
//      {
//        for(k=0;k<3;k++)
//        {
//          Nodes[i].quad[j][k] -= Nodes[i].count * Nodes[i].com[j] * Nodes[i].com[k];
//        }
//      }
//      //Divide by count to get the right answer here too
//      for(j=0;j<3;j++)
//      {
//        for(k=0;k<3;k++)
//        {
//          Nodes[i].quad[j][k] /= Nodes[i].count;
//        }
//      }
//    }
//    //At this point local still means locally owned.  So this is OK.
//    if((Nodes[i].flag & NODE_META)  || !(Nodes[i].flag & NODE_LOCAL))
//    {
//      //Too complex to get the full rmax for the meta nodes, so just set it to 
//      //the distance to corner bound.  This will almost always be the lower
//      //bound for these guys anyway.
//      Nodes[i].rmax = sqrt(get_rmax(Nodes+i));
//    }
//  }
//  //Freedom!
//  free(com_quad);
//}

void direct_com(struct node* cell,double com[3])
{
  int i;
  int mob,count;
  com[0]=com[1]=com[2]=0;
  if(cell->owner==Task)
  {
    mob=cell->mob;
    count=cell->count;
  }
  else if(cell->owner<0 && get_bit(Task,cell->owners))
  {
    i=0;
    while(cell->extra[i].mob/MaxGas!=Task)
      i++;
    mob=cell->extra[i].mob%MaxGas;
    count=cell->extra[i].count;
  }
  else
  {
    mob=0;
    count=0;
  }
  for(i=mob;i<mob+count;i++)
  {
    com[0] += G[i].x;
    com[1] += G[i].y;
    com[2] += G[i].z;
  }
  MPI_Allreduce(MPI_IN_PLACE,com,3,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  com[0] /= cell->count;
  com[1] /= cell->count;
  com[2] /= cell->count;
}

//void direct_quad(struct node* cell,double quad[3][3])
//{
//  double com[3]={0};
//  int i,j;
//  direct_com(cell,com);
//  for(i=0;i<3;i++)
//    for(j=0;j<3;j++)
//      quad[i][j]=0;
//  for(i=cell->mob;i<cell->mob+cell->count;i++)
//  {
//    quad[0][0] += (com[0]-G[i].x)*(com[0]-G[i].x);
//    quad[0][1] += (com[0]-G[i].x)*(com[1]-G[i].y);
//    quad[0][2] += (com[0]-G[i].x)*(com[2]-G[i].z);
//    quad[1][0] += (com[1]-G[i].y)*(com[0]-G[i].x);
//    quad[1][1] += (com[1]-G[i].y)*(com[1]-G[i].y);
//    quad[1][2] += (com[1]-G[i].y)*(com[2]-G[i].z);
//    quad[2][0] += (com[2]-G[i].z)*(com[0]-G[i].x);
//    quad[2][1] += (com[2]-G[i].z)*(com[1]-G[i].y);
//    quad[2][2] += (com[2]-G[i].z)*(com[2]-G[i].z);
//  }
//  for(i=0;i<3;i++)
//    for(j=0;j<3;j++)
//      quad[i][j]/=cell->count;
//}

//void calc_branch_poles(struct node* cell,int aunty)
//{
//  struct node* mummy = Nodes;
//  struct node* child;
//  double test_com[3]={0};
//  double test_quad[3][3];
//  double rmax;
//  int i,j,nothing;
//  //printf("[%d] Calculating branch poles at ([%g,%g],[%g,%g],[%g,%g]) offset=%d,count=%d,mother=%d,sister=%d,daughter=%d,trunkity=%d,locality=%d,leafity=%d,meta=%d,owner=%d.\n",Task,cell->lows[0],cell->highs[0],cell->lows[1],cell->highs[1],cell->lows[2],cell->highs[2],(int) (cell-Nodes),cell->count,cell->mother,cell->sister,cell->daughter,cell->flag & NODE_TRUNK,cell->flag & NODE_LOCAL,cell->flag & NODE_LEAF,cell->flag & NODE_META,(cell->flag) & NODE_OWNER);
//  nothing=0;
//  if(cell->flag & NODE_LEAF)
//  {
//    //printf("[%d] Opening leaf.\n",Task);
//    //Do the actual multipole calculation on real particles
//    //dipole (i.e. Centre of mass)
//    for(i=0;i<cell->count;i++)
//    {
//      cell->com[0] += G[cell->daughter+i].x;
//      cell->com[1] += G[cell->daughter+i].y;
//      cell->com[2] += G[cell->daughter+i].z;
//    }
//    cell->com[0] /= cell->count;
//    cell->com[1] /= cell->count;
//    cell->com[2] /= cell->count;
//    //Specific quadrupole (i.e. quadrupole per unit mass)
//    for(i=0;i<cell->count;i++)
//    {
//      //Perhaps there are some benefits to not having x,y,z
//      //components after all...
//      cell->quad[0][0] += (cell->com[0]-G[cell->daughter+i].x) * (cell->com[0]-G[cell->daughter+i].x);
//      cell->quad[0][1] += (cell->com[0]-G[cell->daughter+i].x) * (cell->com[1]-G[cell->daughter+i].y);
//      cell->quad[0][2] += (cell->com[0]-G[cell->daughter+i].x) * (cell->com[2]-G[cell->daughter+i].z);
//      cell->quad[1][0] += (cell->com[1]-G[cell->daughter+i].y) * (cell->com[0]-G[cell->daughter+i].x);
//      cell->quad[1][1] += (cell->com[1]-G[cell->daughter+i].y) * (cell->com[1]-G[cell->daughter+i].y);
//      cell->quad[1][2] += (cell->com[1]-G[cell->daughter+i].y) * (cell->com[2]-G[cell->daughter+i].z);
//      cell->quad[2][0] += (cell->com[2]-G[cell->daughter+i].z) * (cell->com[0]-G[cell->daughter+i].x);
//      cell->quad[2][1] += (cell->com[2]-G[cell->daughter+i].z) * (cell->com[1]-G[cell->daughter+i].y);
//      cell->quad[2][2] += (cell->com[2]-G[cell->daughter+i].z) * (cell->com[2]-G[cell->daughter+i].z);
//      //Calculate rmax at the same time...
//      rmax = sqrt((cell->com[0]-G[cell->daughter+i].x)*(cell->com[0]-G[cell->daughter+i].x)+(cell->com[1]-G[cell->daughter+i].y)*(cell->com[1]-G[cell->daughter+i].y)+(cell->com[2]-G[cell->daughter+i].z)*(cell->com[2]-G[cell->daughter+i].z));
//      if(rmax>cell->rmax)
//      {
//        cell->rmax = rmax;
//      }
//    }
//    for(i=0;i<3;i++)
//    {
//      for(j=0;j<3;j++)
//      {
//        cell->quad[i][j] /= cell->count;
//      }
//    }
//    //printf("[%d] calculated quadrupole moment.\n",Task);
//  }
//  //It's not a leaf, so do the children (unless we're in the trunk, then we might
//  //not be local...)
//  //This only looks like this because we're doing this BEFORE tree_join
//  else if(!(cell->flag & NODE_TRUNK) || (cell->flag & NODE_LOCAL) || (cell->flag & NODE_META))
//  {
//    //printf("[%d] Opening new daughter.\n",Task);
//    calc_branch_poles(Nodes + cell->daughter,cell->sister);
//  }
//  else
//  {
//    nothing=1;
//  }
//  //Show mummy what we've done (assuming it's not nothing)
//  if(cell->mother!=-1 && !nothing)
//  {
//    //printf("[%d] Now adding to mother with local at ([%g,%g],[%g,%g],[%g,%g]) offset=%d,count=%d,mother=%d,sister=%d,daughter=%d,trunkity=%d,locality=%d,leafity=%d,meta=%d,owner=%d.\n",Task,cell->lows[0],cell->highs[0],cell->lows[1],cell->highs[1],cell->lows[2],cell->highs[2],(int) (cell-Nodes),cell->count,cell->mother,cell->sister,cell->daughter,cell->flag & NODE_TRUNK,cell->flag & NODE_LOCAL,cell->flag & NODE_LEAF,cell->flag & NODE_META,(cell->flag) & NODE_OWNER);
//    mummy = Nodes + cell->mother;
//    for(i=0;i<3;i++)
//    {
//      for(j=0;j<3;j++)
//      {
//        mummy->quad[i][j] += cell->count * (cell->quad[i][j]+cell->com[i]*cell->com[j]);
//      }
//      mummy->com[i] += cell->count * cell->com[i];
//    }
//  }
//  //Do we have to do another cell?
//  if(cell->sister!=aunty)
//  {
//    //printf("[%d] Moving on to sister.\n",Task);
//    if(cell->sister==-1)
//    {
//      //Should never happen
//      printf("[%d] I've made a huge mistake...\n",Task);
//      kill(908);
//    }
//    calc_branch_poles(Nodes + cell->sister,aunty);
//  }
//  //Horray we've finished this level, get mummy to finish up
//  //unless mummy needs information from other processors before
//  //it finishes
//  else if(!(mummy->flag & NODE_META) && ((mummy->flag & NODE_OWNER)==Task))
//  {
//    //printf("[%d] Finishing.\n",Task);
//    //Finalise COM
//    for(i=0;i<3;i++)
//      mummy->com[i] /= mummy->count;
//    //Verified that this calculates com to machine precision.  It does it fast
//    //and it does it right.  If there's an error it's not here.
//    direct_com(mummy,test_com);
//    for(i=0;i<3;i++)
//      mummy->com[i] = test_com[i];
//    //printf("[%d] gold %g,%g,%g calc %g,%g,%g.\n",Task,test_com[0],test_com[1],test_com[2],mummy->com[0],mummy->com[1],mummy->com[2]);
//    //printf("[%d] Ratios %g,%g,%g on node with count %d.\n",Task,fabs(mummy->com[0]-test_com[0])/test_com[0],fabs(mummy->com[1]-test_com[1])/test_com[1],fabs(mummy->com[2]-test_com[2])/test_com[2],mummy->count);
//    //kill(111);
//
//    //Add the last quadrupole correction factor in
//    for(i=0;i<3;i++)
//    {
//      for(j=0;j<3;j++)
//      {
//        mummy->quad[i][j] -= mummy->count * mummy->com[i] * mummy->com[j];
//      }
//    }
//    //Divide by count to get the right answer here too
//    for(i=0;i<3;i++)
//    {
//      for(j=0;j<3;j++)
//      {
//        mummy->quad[i][j] /= mummy->count;
//      }
//    }
//    //This verifies that we indeed calculate quad to machine precision as well
//    //when passing stuff up the tree.  That's good to know.
//    direct_quad(mummy,test_quad);
//    for(i=0;i<3;i++)
//      for(j=0;j<3;j++)
//        mummy->quad[i][j]=test_quad[i][j];
//    //printf("[%d] Ratios %g,%g,%g,%g,%g,%g,%g,%g,%g count %d.\n",Task,
//    //    fabs(test_quad[0][0]-mummy->quad[0][0])/test_quad[0][0],
//    //    fabs(test_quad[0][1]-mummy->quad[0][1])/test_quad[0][1],
//    //    fabs(test_quad[0][2]-mummy->quad[0][2])/test_quad[0][2],
//    //    fabs(test_quad[1][0]-mummy->quad[1][0])/test_quad[1][0],
//    //    fabs(test_quad[1][1]-mummy->quad[1][1])/test_quad[1][1],
//    //    fabs(test_quad[1][2]-mummy->quad[1][2])/test_quad[1][2],
//    //    fabs(test_quad[2][0]-mummy->quad[2][0])/test_quad[2][0],
//    //    fabs(test_quad[2][1]-mummy->quad[2][1])/test_quad[2][1],
//    //    fabs(test_quad[2][2]-mummy->quad[2][2])/test_quad[2][2],
//    //    mummy->count);
//
//    //To get rmax, we have to loop over mummy's immediate children (ugh)
//    //because mummy->com - child->com couldn't be calculated until mummy->com
//    //was finalised
//    child = Nodes + mummy->daughter;
//    while(1)
//    {
//      //Update mummy's rmax for this child
//      rmax = child->rmax + sqrt((mummy->com[0]-child->com[0])*(mummy->com[0]-child->com[0])+(mummy->com[1]-child->com[1])*(mummy->com[1]-child->com[1])+(mummy->com[2]-child->com[2])*(mummy->com[2]-child->com[2]));
//      if(rmax > mummy->rmax)
//      {
//        mummy->rmax = rmax;
//      }
//      //Move onto sibling, until we run out of them
//      if(child->sister==mummy->sister)
//        break;
//      child = Nodes + child->sister;
//    }
//    //Finally we have to see if we'd have been better off with distance to the furthest
//    //corner
//    rmax = sqrt(get_rmax(mummy));
//    if(rmax < mummy->rmax)
//    {
//      mummy->rmax = rmax;
//    }
//  }
//  //printf("[%d] Returning.\n",Task);
//}

//Calculates the self-gravity using the N^2 method for
//the local points in *points
//DEPRECATED IN FAVOUR OF JUST OPENING UP THE TREE.  DON'T USE THIS!
//void self_gravity_test(int *points,int npoints)
//{
//  double acc_x,acc_y,acc_z,acc_pot;
//  double cacc_x,cacc_y,cacc_z;
//  double dx,dy,dz,r,fac;
//  double gf,cf;
//  int i,j,k,cnt;
//  int done;
//  int nsend[NTask];
//  int nleft[NTask];
//  MPI_Status status;
//  MPI_Request send_request,recv_request;
//  struct slow_grav_comm* slow_grav_comm_tmp;
// 
//  printf("[%d] Hello there.\n",Task);
//  //Send the particles around in a loop until they're done
//  MPI_Allgather(&npoints,1,MPI_INT,nleft,1,MPI_INT,MPI_COMM_WORLD);
//  for(j=0;j<NTask;j++)
//  {
//    printf("[%d] nleft[%d] = %d.\n",Task,j,nleft[j]);
//  }
//  i=0;
//  acc_x=acc_y=acc_z=0;
//  cacc_x=cacc_y=cacc_z=0;
//  do
//  {
//    //Work out how much data to send this time from each processor
//    for(j=0;j<NTask;j++)
//    {
//      nsend[j] = imin(nleft[j],BufferSizeGravSlow);
//    }
//    //Prepare the data to be sent
//    for(j=0;j<nsend[Task];j++)
//    {
//      GravSlowSend[j].x = G[points[npoints-nleft[Task]+j]].x;
//      GravSlowSend[j].y = G[points[npoints-nleft[Task]+j]].y;
//      GravSlowSend[j].z = G[points[npoints-nleft[Task]+j]].z;
//      GravSlowSend[j].pot = 0;
//      GravSlowSend[j].ax = 0;
//      GravSlowSend[j].ay = 0;
//      GravSlowSend[j].az = 0;
//    }
//    //Move the data round in a circle
//    for(cnt=0;cnt<NTask;cnt++)
//    {
//      printf("[%d] Move number %d of %d.\n",Task,cnt+1,NTask);
//      printf("[%d] Going to send %d particles and recieve %d.\n",Task,nsend[(NTask+Task-cnt)%NTask],nsend[(NTask+Task-1-cnt)%NTask]);
//      //Initiate the sending/receiving
//      MPI_Issend(GravSlowSend,nsend[(NTask+Task-cnt)%NTask]*sizeof(struct slow_grav_comm),MPI_BYTE,(Task+1)%NTask,i,MPI_COMM_WORLD,&send_request);
//      MPI_Irecv(GravSlowRecv,nsend[(NTask+Task-1-cnt)%NTask]*sizeof(struct slow_grav_comm),MPI_BYTE,(NTask+Task-1)%NTask,i,MPI_COMM_WORLD,&recv_request);
//      //Wait till it completes
//      MPI_Wait(&send_request,&status);
//      MPI_Wait(&recv_request,&status);
//      //Now we've got some fresh data, calculate the local gravity due to it
//      for(j=0;j<nsend[(NTask+Task-1-cnt)%NTask];j++)
//      {
//        for(k=0;k<NPart[GAS];k++)
//        {
//          dx = G[k].x - GravSlowRecv[j].x;
//          dy = G[k].y - GravSlowRecv[j].y;
//          dz = G[k].z - GravSlowRecv[j].z;
//          r = sqrt(dx*dx+dy*dy+dz*dz);
//          if(r==0)
//            continue;
//          //Will be 1/r^3 unless softened
//          fac = Params.G *PcleMass* softened_grav(r,Params.gas_soft);
//          //Add the acceleration on j due to i
//          GravSlowRecv[j].ax += fac*dx;
//          GravSlowRecv[j].ay += fac*dy;
//          GravSlowRecv[j].az += fac*dz;
//          GravSlowRecv[j].pot += Params.G*PcleMass*softened_pot(r,Params.gas_soft);
//          //if(points[npoints-nleft[Task]+j]==0)
//          //  if(Params.G*PcleMass*softened_pot(r,Params.gas_soft)!=-G[k].test_pot)
//          //  {
//          //    //printf("[%d] Test potential was %g and got %g  diff %g b/w %d and %d.\n",Task,G[k].test_pot,-1*Params.G*PcleMass*softened_pot(r,Params.gas_soft), G[k].test_pot+Params.G*PcleMass*softened_pot(r,Params.gas_soft),0,k);
//          //    SumDiffs += G[k].test_pot+Params.G*PcleMass*softened_pot(r,Params.gas_soft);
//          //  }
//        }
//      }
//      //Now we've got things to send in receive, so swap them
//      slow_grav_comm_tmp=GravSlowRecv;
//      GravSlowRecv=GravSlowSend;
//      GravSlowSend=slow_grav_comm_tmp;
//    }
//    printf("\n\nSumDiffs = %g\n",SumDiffs);
//    //How do these accelerations compare with the tree ones?
//    for(j=0;j<nsend[Task];j++)
//    {
//      acc_x += GravSlowSend[j].ax;
//      acc_y += GravSlowSend[j].ay;
//      acc_z += GravSlowSend[j].az;
//      cacc_x += G[points[npoints-nleft[Task]+j]].ax;
//      cacc_y += G[points[npoints-nleft[Task]+j]].ay;
//      cacc_z += G[points[npoints-nleft[Task]+j]].az;
//      //acc_x = fabs(GravSlowSend[j].ax - G[points[npoints-nleft[Task]+j]].ax)/(GravSlowSend[j].ax);
//      //acc_y = fabs(GravSlowSend[j].ay - G[points[npoints-nleft[Task]+j]].ay)/(GravSlowSend[j].ay);
//      //acc_z = fabs(GravSlowSend[j].az - G[points[npoints-nleft[Task]+j]].az)/(GravSlowSend[j].az);
//      acc_pot = fabs(GravSlowSend[j].pot - G[points[npoints-nleft[Task]+j]].pot)/(GravSlowSend[j].pot);
//      gf=sqrt(GravSlowSend[j].ax*GravSlowSend[j].ax+GravSlowSend[j].ay*GravSlowSend[j].ay+GravSlowSend[j].az*GravSlowSend[j].az);
//      cf=sqrt(G[points[npoints-nleft[Task]+j]].ax*G[points[npoints-nleft[Task]+j]].ax+G[points[npoints-nleft[Task]+j]].ay*G[points[npoints-nleft[Task]+j]].ay+G[points[npoints-nleft[Task]+j]].az*G[points[npoints-nleft[Task]+j]].az);
//
//      printf("[%d] Force/pot gold/tree (%g,%g) gold (%g,%g) tree (%g,%g).\n",Task,fabs(gf-cf)/gf,acc_pot,gf,GravSlowSend[j].pot,cf,G[points[npoints-nleft[Task]+j]].pot);
//      //printf("[%d] Test particle has gold/tree (%g,%g,%g,%g) on gold (%g,%g,%g,%g) and coal (%g,%g,%g,%g).\n",Task,acc_x,acc_y,acc_z,acc_pot,GravSlowSend[j].ax,GravSlowSend[j].ay,GravSlowSend[j].az,GravSlowSend[j].pot,G[points[npoints-nleft[Task]+j]].ax,G[points[npoints-nleft[Task]+j]].ay,G[points[npoints-nleft[Task]+j]].az,G[points[npoints-nleft[Task]+j]].pot);
//    }
//    //Update the counters and decide if we're done
//    done=1;
//    for(j=0;j<NTask;j++)
//    {
//      nleft[j] -= nsend[j];
//      if(nleft[j])
//        done=0;
//    }
//  }while(!done);
//  printf("[%d] Total force gold (%g,%g,%g) tree (%g,%g,%g).\n",Task,acc_x,acc_y,acc_z,cacc_x,cacc_y,cacc_z);
//}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  exact_gas
 *  Description:  Calculates exactly the potential and the force due to the gas.
 * =====================================================================================
 */
void exact_gas(void)
{
  int i,j,nsend,ndone;
  int nleft[NTask];
  int src;
  double dx,dy,dz,r;
  double fac;
  //Initialise to zero
  for(i=0;i<NPart[GAS];i++)
  {
    G[i].pot=0;
    G[i].grav_ax=0;
    G[i].grav_ay=0;
    G[i].grav_az=0;
  }
  //Work out how many to send on each processor
  MPI_Allgather(&NPart[GAS],1,MPI_INT,NGas,1,MPI_INT,MPI_COMM_WORLD);
  //Move the particles around in a loop until they're done
  for(i=0;i<NTask;i++)
  {
    nleft[i] = NGas[i];
    //printf("[%d] nleft[%d] = %d.\n",Task,i,nleft[i]);
  }
  ndone=0;
  while(ndone!=NTask)
  {
    //pprintf("Starting loop with ndone=%d.\n",ndone);
    //Bcast everything that will fit
    for(src=0;src<NTask;src++)
    {
      //How many can I fit in the buffer?
      nsend = imin(nleft[src],BufferSizeCoord);
      //pprintf("Sending %d from src=%d with %d.\n",nsend,src,nleft[src]);
      //Skip if nothing to be done
      if(nsend==0)
        continue;
      //Prepare them to be sent
      if(Task==src)
      {
        for(i=0;i<nsend;i++)
        {
          Coords[i].x = G[NPart[GAS]-nleft[Task]+i].x;
          Coords[i].y = G[NPart[GAS]-nleft[Task]+i].y;
          Coords[i].z = G[NPart[GAS]-nleft[Task]+i].z;
          Coords[i].h = G[NPart[GAS]-nleft[Task]+i].hsml;
        }
      }
      //Broadcast them
      MPI_Bcast(Buffer,nsend*sizeof(struct coords),MPI_BYTE,src,MPI_COMM_WORLD);
      //pprintf("BCast done.\n");
      //Calculate contribution due to these
      //This will take care of self-interaction too
      for(i=0;i<NPart[GAS];i++)
      {
        for(j=0;j<nsend;j++)
        {
          //Include the self-contribution in the potential
          //If we do not include it, we should not include the self-potential
          //in the calculation of zeta either...
          dx = G[i].x - Coords[j].x;
          dy = G[i].y - Coords[j].y;
          dz = G[i].z - Coords[j].z;
          r = sqrt(dx*dx+dy*dy+dz*dz);
          //Don't add self-gravity, but do add self-potential
          if(Task==src && i==NPart[GAS]-nleft[Task]+j)
            dx=dy=dz=0;
#if HYDRO<=1 || defined CONSTANT_GAS_SOFT
          G[i].pot += Params.G*PcleMass*softened_pot(r,Params.gas_soft);
          fac = Params.G*PcleMass*softened_grav(r,Params.gas_soft);
          G[i].grav_ax -= fac * dx; 
          G[i].grav_ay -= fac * dy; 
          G[i].grav_az -= fac * dz; 
#else
          G[i].pot += Params.G*PcleMass*0.5*(softened_pot(r,G[i].hsml)+softened_pot(r,Coords[j].h));
          fac = Params.G*PcleMass*0.5*(softened_grav(r,G[i].hsml)+softened_grav(r,Coords[j].h));
          G[i].grav_ax -= fac * dx; 
          G[i].grav_ay -= fac * dy; 
          G[i].grav_az -= fac * dz; 
#endif
        }
      }
      //pprintf("Calculation done.\n");
      //Update counters
      nleft[src] -= nsend;
      if(nleft[src]==0)
        ndone++;
      //pprintf("Calculation done, have %d left and ndone=%d.\n",nleft[src],ndone);
    }
  }
}

//Calculates the self-gravity (i.e., the gas's gravitational pull on itself)
//using the full N^2 force calculation.  Obviously this is VERY slow.
//It really only exists for testing purposes
void self_gravity_slow(void)
{
  int i,j,k,cnt;
  int nsend[NTask];
  int nleft[NTask];
  double dx,dy,dz,r,fac;
  double test_ax[NPart[GAS]];
  double test_ay[NPart[GAS]];
  double test_az[NPart[GAS]];
  double test_pot[NPart[GAS]];
  MPI_Status status;
  MPI_Request send_request,recv_request;
  int done;
  struct slow_grav_comm* slow_grav_comm_tmp;
  

  pprintf("Calculating local self-gravitational interactions.\n");
  //Initialise the test fields
  for(i=0;i<NPart[GAS];i++)
  {
    test_ax[i] = 0;
    test_ay[i] = 0;
    test_az[i] = 0;
    test_pot[i] = 0;
  }
  //First do all the local interactions
  for(i=0;i<NPart[GAS];i++)
  {
    pprintf("Starting from %d.\n",i);
    for(j=i+1;j<NPart[GAS];j++)
    {
      dx = G[j].x-G[i].x;
      dy = G[j].y-G[i].y;
      dz = G[j].z-G[i].z;
      r = sqrt(dx*dx+dy*dy+dz*dz);
      //Will be 1/r^3 unless softened
      test_pot[i] += Params.G*PcleMass*0.5*(softened_pot(r,G[i].hsml)+softened_pot(r,G[j].hsml));
      fac = Params.G *PcleMass* 0.5*(softened_grav(r,G[i].hsml)+softened_grav(r,G[j].hsml));
      //Add the acceleration on j due to i
      test_ax[j] -= fac*dx;
      test_ay[j] -= fac*dy;
      test_az[j] -= fac*dz;
      //Equal and opposite
      test_ax[i] += fac*dx;
      test_ay[i] += fac*dy;
      test_az[i] += fac*dz;
    }
  }

  //Move the particles around in a loop until they're done
  for(j=0;j<NTask;j++)
  {
    nleft[j] = NGas[j];
    pprintf("nleft[%d] = %d.\n",j,nleft[j]);
  }
  i=0;
  do
  {
    pprintf("Beginning gravity data loop %d.\n",i);
    //Work out how much data to send this time from each processor
    for(j=0;j<NTask;j++)
    {
      nsend[j] = imin(nleft[j],BufferSizeGravSlow);
    }
    //Prepare the data to be sent
    for(j=0;j<nsend[Task];j++)
    {
      GravSlowSend[j].x = G[NGas[Task]-nleft[Task]+j].x;
      GravSlowSend[j].y = G[NGas[Task]-nleft[Task]+j].y;
      GravSlowSend[j].z = G[NGas[Task]-nleft[Task]+j].z;
      GravSlowSend[j].h = G[NGas[Task]-nleft[Task]+j].hsml;
    }
    //Move the data round in a circle
    for(cnt=0;cnt<NTask-1;cnt++)
    {
      pprintf("Move number %d of %d.\n",cnt+1,NTask-1);
      pprintf("Going to send %d particles and recieve %d.\n",nsend[(NTask+Task-cnt)%NTask],nsend[(NTask+Task-1-cnt)%NTask]);
      //Initiate the sending/receiving
      MPI_Issend(GravSlowSend,nsend[(NTask+Task-cnt)%NTask]*sizeof(struct slow_grav_comm),MPI_BYTE,(Task+1)%NTask,i,MPI_COMM_WORLD,&send_request);
      MPI_Irecv(GravSlowRecv,nsend[(NTask+Task-1-cnt)%NTask]*sizeof(struct slow_grav_comm),MPI_BYTE,(NTask+Task-1)%NTask,i,MPI_COMM_WORLD,&recv_request);
      //Wait till it completes
      MPI_Wait(&send_request,&status);
      MPI_Wait(&recv_request,&status);
      //Now we've got some fresh data, calculate the local gravity due to it
      for(j=0;j<nsend[(NTask+Task-1-cnt)%NTask];j++)
      {
        for(k=0;k<NPart[GAS];k++)
        {
          dx = GravSlowRecv[j].x - G[k].x ;
          dy = GravSlowRecv[j].y - G[k].y ;
          dz = GravSlowRecv[j].z - G[k].z ;
          r = sqrt(dx*dx+dy*dy+dz*dz);
          //Will be 1/r^3 unless softened
          test_pot[k] += Params.G*PcleMass*0.5*(softened_pot(r,G[k].hsml)+softened_pot(r,G[j].hsml));
          fac = Params.G *PcleMass* 0.5*(softened_grav(r,G[k].hsml)+softened_grav(r,GravSlowRecv[j].h));
          //Add the acceleration on j due to i
          test_ax[k] += fac*dx;
          test_ay[k] += fac*dy;
          test_az[k] += fac*dz;
        }
      }
      //Now we've got things to send in receive, so swap them
      slow_grav_comm_tmp=GravSlowRecv;
      GravSlowRecv=GravSlowSend;
      GravSlowSend=slow_grav_comm_tmp;
    }
    //Update the counters and decide if we're done
    done=1;
    for(j=0;j<NTask;j++)
    {
      nleft[j] -= nsend[j];
      if(nleft[j])
        done=0;
    }
  }while(!done);
  //Finished  now do comparison
  for(i=0;0&&i<NPart[GAS];i++)
  {
    fac = sqrt(test_ax[i]*test_ax[i]+test_ay[i]*test_ay[i]+test_az[i]*test_az[i]);
    dx = sqrt((test_ax[i]-G[i].ax)*(test_ax[i]-G[i].ax)+(test_ay[i]-G[i].ay)*(test_ay[i]-G[i].ay)+(test_az[i]-G[i].az)*(test_az[i]-G[i].az));
    dy = sqrt(G[i].ax*G[i].ax+G[i].ay*G[i].ay+G[i].az*G[i].az);
    pprintf("Particle[%d] at %g,%g,%g has true = %g,%g,%g approx = %g,%g,%g force error |est-true|/true = %g |est| = %g |true| = %g.\n",i,G[i].R,G[i].phi,G[i].z,test_ax[i],test_ay[i],test_az[i],G[i].ax,G[i].ay,G[i].az,dx/fac,dy,fac);
    //printf("[MOO] %g\t%g\n",dx/fac,G[i].R);
  }
  for(i=0;i<NPart[GAS];i++)
  {
    G[i].pot = test_pot[i];
  }
}

//Calculates the gravitational force due to the sink particles.
//Always done in full N^2 glory 
void sink_gravity(void)
{
  int i,j;
  double dx,dy,dz,r,fac;
  double sink_acc_x[NPart[SINK]];
  double sink_acc_y[NPart[SINK]];
  double sink_acc_z[NPart[SINK]];
  double sink_pot[NPart[SINK]];
  //Tstart = MPI_Wtime();


  //All sinks are on all processors, so this should be self contained
  //and require no communication
  for(i=0;i<NPart[SINK];i++)
  {
    sink_acc_x[i]=0;
    sink_acc_y[i]=0;
    sink_acc_z[i]=0;
    sink_pot[i]=0;
    //Interaction within the sinks
    for(j=i+1;j<NPart[SINK];j++)
    {
      dx = S[j].x-S[i].x;
      dy = S[j].y-S[i].y;
      dz = S[j].z-S[i].z;
      r = sqrt(dx*dx+dy*dy+dz*dz);
      //Will be 1/r^3 unless softened
      fac = Params.G * softened_pot(r,Params.sink_soft);
      S[j].pot += S[i].m *fac;
      S[i].pot += S[j].m *fac;
      fac = Params.G *  softened_grav(r,Params.sink_soft);
      //Add the acceleration on j due to i
      S[j].ax -= S[i].m * fac*dx;
      S[j].ay -= S[i].m * fac*dy;
      S[j].az -= S[i].m * fac*dz;
      //Equal and opposite reaction...
      S[i].ax += S[j].m * fac*dx;
      S[i].ay += S[j].m * fac*dy;
      S[i].az += S[j].m * fac*dz;
    }
    //Sink to gas interaction
    for(j=0;j<NPart[GAS];j++)
    {
      dx = G[j].x - S[i].x;
      dy = G[j].y - S[i].y;
      dz = G[j].z - S[i].z;
      r = sqrt(dx*dx+dy*dy+dz*dz);
      //Will be 1/r^3 unless softened
      fac = Params.G * softened_pot(r,Params.sink_soft);
      G[j].pot += S[i].m * fac;
      sink_pot[i] += PcleMass * fac;
      fac = Params.G *  softened_grav(r,Params.sink_soft);
      //How does this gas move in response to the sink?
      G[j].ax -= S[i].m * fac * dx;
      G[j].ay -= S[i].m * fac * dy;
      G[j].az -= S[i].m * fac * dz;
      //Equal and opposite reaction.  need to store temporarily
      //until we can sum across all processors
      sink_acc_x[i] += PcleMass * fac*dx;
      sink_acc_y[i] += PcleMass * fac*dy;
      sink_acc_z[i] += PcleMass * fac*dz;
    }
  }
  //Timings.gravity += MPI_Wtime()-Tstart;
  //Tstart = MPI_Wtime();
  //Now add the gas acceleration onto the sinks
  MPI_Allreduce(MPI_IN_PLACE,sink_acc_x,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE,sink_acc_y,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE,sink_acc_z,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE,sink_pot,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  //Timings.comm += MPI_Wtime()-Tstart;
  for(i=0;i<NPart[SINK];i++)
  {
    S[i].ax += sink_acc_x[i];
    S[i].ay += sink_acc_y[i];
    S[i].az += sink_acc_z[i];
    S[i].pot += sink_pot[i];
  }
  //Print sink accel
  //for(i=0;i<NPart[SINK];i++)
  //{
  //  printf("[%d] Sink acceleration is (%g,%g,%g).\n",Task,S[i].ax,S[i].ay,S[i].az);
  //}
  //Freedom!
}

//Plumber sphere
//double softened_grav(double r, double h)
//{
//  if(r >= h)
//  {
//    return 1.0 / (r*r*r);
//  }
//  return 1.0 / pow(r*r+h*h,1.5);
//}

//Gravity is always 3D, even when we're modelling it with reduced dimensions...
//Derived from cubic spline gives normalised force (including extra factor of 1/r)
double softened_grav(double r, double h)
{
  double q;
#if NDIM<=2
  //In reduced dimensions we might put the source a characteristic distance
  //away in order to mimic the effect of integrating over the unmodelled
  //dimension(s)
  r=sqrt(r*r+Params.dim_grav_soft);
#endif
  q=r/h;
  if(q>=2)
    return 1.0/ (r*r*r);
  if(q>=1)
    return ((8.0/3.0)-3*q+(6.0/5.0)*q*q-(1.0/6.0)*q*q*q - (1.0/(15.0*q*q*q)))/(h*h*h);
  return ((4.0/3.0)-(6.0/5.0)*q*q+.5*q*q*q)/(h*h*h);
}

double softened_pot(double r, double h)
{
  double q;
#if NDIM<=2
  //In reduced dimensions we might put the source a characteristic distance
  //away in order to mimic the effect of integrating over the unmodelled
  //dimension(s)
  r=sqrt(r*r+Params.dim_grav_soft);
#endif
  if(h==0 || r>2*h)
    return -1.0/r;
  q=r/h;
  if(q>=1)
  {
    return ((4.0/3.0)*q*q-q*q*q+.3*q*q*q*q-(1.0/30.0)*q*q*q*q*q-1.6+(1.0/(15.0*q)))/h;
  }
  return ((2.0/3.0)*q*q-.3*q*q*q*q+.1*q*q*q*q*q-1.4)/h;
}

//Correction term for gravity
double dphi_dh(double q)
{
  if(q>=2)
    return 0;
  if(q>=1)
    return (-4*q*q+4*q*q*q-1.5*q*q*q*q+.2*q*q*q*q*q+1.6);
  return (-2*q*q+1.5*q*q*q*q-.6*q*q*q*q*q+1.4);
}
double deriv_grav_greens_0(double R)
{
  return Params.G/R;
}

double deriv_grav_greens_1(double R)
{
  return -Params.G/(R*R*R);
}

double deriv_grav_greens_2(double R)
{
  return 3*Params.G/(R*R*R*R*R);
}

double deriv_grav_greens_3(double R)
{
  return -15*Params.G/(R*R*R*R*R*R*R);
}

int grav_buffer_sort(const void* a,const void* b)
{
  if((*((union grav_comm*) a)).in.where < (*((union grav_comm*) b)).in.where) return -1;
  if((*((union grav_comm*) a)).in.where > (*((union grav_comm*) b)).in.where) return 1;
  return 0;
}

//int grav_coeff_buffer_sort(const void* a,const void* b)
//{
//  if((*((struct grav_coeff_comm*) a)).where < (*((struct grav_coeff_comm*) b)).where) return -1;
//  if((*((struct grav_coeff_comm*) a)).where > (*((struct grav_coeff_comm*) b)).where) return 1;
//  return 0;
//}
