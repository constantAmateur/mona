/*
 * =====================================================================================
 *
 *       Filename:  density.c
 *
 *    Description:  Calculates the SPH density and the smoothing length recursively.
 *
 *        Version:  0.6.3
 *        Created:  05/08/13 10:05:37
 *
 *         Author:  Matthew D. Young, my304@ast.cam.ac.uk
 *        Company:  Institute of Astronomy, Cambridge, UK
 *        License:  GNU General Public License
 *      Copyright:  Copyright (c) 2014, Matthew D. Young
 *
 * =====================================================================================
 */

#include "variables.h"
#include "functions.h"

/* 
 * ===  FUNCTION  ======================================================================
 *  Description:  The central density routine.  The solving of the iterative equation 
 *                linking density and smoothing length is solved here.  That is the 
 *                setting of the next value of h based on the current guess is determined
 *                here and the decision as to when h and rho have converged is also made.
 *                The finalisation of all quantities calculated in the density loop is 
 *                also done.  For example, rho needs to be multiplied by the m/h^NDIM 
 *                because this factor is left out of the density loop for speed.
 * =====================================================================================
 */
void density(void)
{
  double mvol,new_h_mult,tmp,eta;
  //double new_ngb_num,old_ngb_num;
  double lowh[NPart[GAS]];
  double highh[NPart[GAS]];
  double h0[NPart[GAS]];
  double h;
  int nleft,nlefttot,i;
  int ctr;

  //Determine the upper bound for bisection 
  highh[0]=0;
  for(i=0;i<=NTask;i++)
  {
    if(10.*DomainWalls[i]>highh[0])
      highh[0]=10.*DomainWalls[i];
  }
  //highh[0]=100;
  //Set the bisection limits and the initial h
  for(i=0;i<NPart[GAS];i++)
  {
    //Reset neighbour list
    G[i].nngbs=0;
    lowh[i]=0;
    highh[i]=highh[0];
    h0[i] = G[i].hsml;
    if(h0[i]==0)
      kill(519);
  }

  //Some frequently used values
  mvol = pow(Params.eta,NDIM);
  //Counts
  nlefttot = NPartWorld[GAS];
  ctr=0;
  //Completed particles are marked by a negative smoothing length
  do
  {
    gpprintf("Loop %d for %d pcles.\n",ctr,nlefttot);
    //The bulk of the work is done here.  That is the actual sum over
    //neighbours including any inter-processsor communication
    density_eval();
    //Check each particle and see if it's done
    nleft=0;
    for(i=0;i<NPart[GAS];i++)
    {
      //This particle isn't done yet, see if it is now
      if(G[i].hsml>0)
      {
        //Update the limits for bisection, regardless of how we're taking this step
        //Remember, density and dhsml *don't* include the factor m
        //Is it too high or too low?
        if(G[i].density>mvol)
        {
          if(G[i].hsml<highh[i])
            highh[i]=G[i].hsml;
        }
        else
        {
          if(G[i].hsml>lowh[i])
            lowh[i]=G[i].hsml;
        }
        //Remember density here is actually just sum w_ab
        eta = pow(G[i].density,1.0/NDIM);


        //First we guess at what the new value should be iteratively
        //The linear Newton-Raphson method
        //new_h_mult = (G[i].density+G[i].dhsml-mvol)/G[i].dhsml;
        //The suggested Cullen-Dehnen modification to linear Newton-Raphson
        //if(G[i].density<mvol)
        //{
        //  new_h_mult = (G[i].density+G[i].dhsml - mvol -dkernel(0))/(G[i].dhsml-dkernel(0));
        //}
        //else
        //{
        //  new_h_mult = (G[i].density+G[i].dhsml-mvol)/G[i].dhsml;
        //}
        //The log Newton-Raphson method
        //Fall back on safe method if we hit an edge case
        if(G[i].dhsml < Params.toll)
        {
          //Consider linear NR safe
          //new_h_mult = Params.eta / eta;
          new_h_mult = (G[i].density+G[i].dhsml-mvol)/G[i].dhsml;
        }
        else if(G[i].density>mvol)
        {
          new_h_mult = pow((G[i].density-kernel(0))/(mvol-kernel(0)),(G[i].density-kernel(0))/(G[i].dhsml-dkernel(0)));
        }
        else
        {
          new_h_mult = pow(G[i].density/mvol,G[i].density/G[i].dhsml);
        }
        //Prevent it going too far (because this will probably be wrong)
        //old_ngb_num = 4*PI*G[i].density*KER_SUPPORT*KER_SUPPORT*KER_SUPPORT/NDIM;
        //new_ngb_num = old_ngb_num*new_h_mult*new_h_mult*new_h_mult;
        //Don't want to jump to an h that's going to require us to find
        //an obscene number of neighbours
        //if(new_ngb_num>100 || new_ngb_num <5)
        //{
        //  //Fall back on safe way
        //  new_h_mult = Params.eta/eta;
        //}
        //The dumb dumb method for the first time
        //It converges slower but doesn't slow us
        //to a crawl by predicting any massively large smoothing lengths
        if(!Time)
          new_h_mult = Params.eta / eta;

        //Would we be better off doing the bisection method?

        tmp = (.5*(highh[i]+lowh[i]))/G[i].hsml;
        //We would be if the bisection method gives a smaller step
        //Always use bisection after 20 iterations. Or if it would
        //result in us taking a smaller step than we're currently
        //set up to take
        if(ctr>40 || (tmp!=1 && fabs(1-tmp)<fabs(1-new_h_mult)))
        {
          new_h_mult = tmp;
        }
        //printf("[%d] pcle=%d,Sum0=%g,sum1=%g,high=%g,low=%g.\n",Task,i,G[i].density,G[i].dhsml,highh[i],lowh[i]);
        //printf("[%d] hold = %g hnew = %g, fact = %g eta = %g.\n",Task,G[i].hsml,G[i].hsml*new_h_mult,new_h_mult,pow(G[i].density,1.0/NDIM));
        //This should never happen
        if(highh[i]<lowh[i])
        {
          pprintf("DANGER! hold = %g hnew = %g, fact = %g eta = %g high = %g low = %g.\n",G[i].hsml,G[i].hsml*new_h_mult,new_h_mult,pow(G[i].density,1.0/NDIM),highh[i],lowh[i]);
          //Fall back on bisection method
          highh[i]=20;
          lowh[i]=0;
          tmp = (.5*(highh[i]+lowh[i]))/G[i].hsml;
          //kill(908);
        }
        //Now take the step
        //G[i].hsml *= new_h_mult;
        //if(G[i].hsml==0)
        //{
        //  pprintf("G[%d].h = %g and new_h_mult = %g, Sum_0 = %g, Nngb = %g, eta =%g, high=%g, low = %g.\n",i,G[i].hsml,new_h_mult,G[i].density,0.0,eta,highh[i],lowh[i]);
        //  kill(ERROR_SANITY);
        //}
        //Is it extreme?
        //if(G[i].hsml>1.0 || G[i].hsml<1e-4 || ctr>100)
        //  printf("[%d] G[%d].h = %g and new_h_mult = %g, Sum_0 = %g, Nngb = %g, eta =%g, high=%g, low = %g.\n",Task,i,G[i].hsml,new_h_mult,G[i].density,new_ngb_num,eta,highh[i],lowh[i]);
        //Is it converged?
        //Convergence criteria as suggested by Price grav softening paper
        //if(fabs(eta-Params.eta)<Params.toll)
        //Am I close enough to finish?
        if(fabs(1-new_h_mult)*(G[i].hsml/h0[i])<Params.toll)
        {
          if(G[i].density==0)
          {
            pprintf("G[%d].hsml,dhsml,rho = %g,%g,%g\n",i,G[i].hsml,G[i].dhsml,G[i].density);
            kill(ERROR_SANITY);
          }
          //Variable names shouldn't be taken too seriously in what follows
          //We'll just re-use things we don't need to use any more
          //Calculate the normalisation factor (which should be close to zero)
          //This is assuming we can replace m/h/rho^DIMS with 1/eta^dims (which is 1/mvol)
          new_h_mult = 1.0 - G[i].density/mvol;
          //Finish density calculation
          h=G[i].hsml;
          G[i].density=(PcleMass*G[i].density)/H_TO_THE_D;
          //Finish smoothing length gradient calculations
          //This assumes dh_a/drho_a = -h_a/(NDIM*rho_a(h_a)), i.e., rho_a determined by
          //h_a = eta*(m/rho_a)^(1/NDIM) not the sum.
          //G[i].dhsml = new_h_mult-PcleMass*G[i].dhsml/(NDIM*G[i].density*G[i].hsml*G[i].hsml*G[i].hsml);
          G[i].dhsml = new_h_mult - G[i].dhsml /(NDIM*mvol);
          //Finish Density gradient calculation
          //This is my personal obsession and is probably of no consequence
          //new_h_mult = PcleMass / (G[i].dhsml * G[i].hsml * G[i].dhsml * G[i].hsml * G[i].hsml);
          //G[i].drho_x *= new_h_mult;
          //G[i].drho_y *= new_h_mult;
          //G[i].drho_z *= new_h_mult;
          //Finish price correction term calculation we left the 1/h^2 out of dphi/dh
          //so make sure to include it back here.  As above dh_a/drho_a is defined in 
          //terms of rho_a(h_a) not rho_a,sum
          G[i].zeta *= -H_TO_THE_DMINUS1/(NDIM*mvol);
          //if(G[i].hsml>1.0 || G[i].hsml<1e-4 || ctr>100)
          //  printf("[%d] G[%d].h = %g eta =%g, high=%g, low = %g.\n",Task,i,G[i].hsml,eta,highh[i],lowh[i]);
          //Turn particle off
          G[i].hsml *= -1;
          //G[i].nngbs =0;
        }
        else
        {
          //Set to new value and loop again
          G[i].hsml *= new_h_mult;
          //Particle not converged, do another loop
          nleft++;
        }
      }
    }
    //If any processors have things left to do, have to continue on all processors
    //printf("[%d] %d left.\n",Task,nleft);
    //Update the global counter
    MPI_Allreduce(&nleft,&nlefttot,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
    ctr++;
  }
  while(nlefttot);
  //Smoothing lengths aren't really negative you silly human
  for(i=0;i<NPart[GAS];i++)
  {
    if(G[i].hsml<0)
    {
      G[i].hsml *= -1;
    }
    else if(G[i].density==0)
    {
      pprintf("G[%d].hsml,dhsml = %g,%g\n",i,G[i].hsml,G[i].dhsml);
      kill(ERROR_SANITY);
    }
  }
}

/* 
 * ===  FUNCTION  ======================================================================
 * DEPRECATED IN FAVOUR OF ABSTRACTED COMMUNICATION ROUTINES
 *  Description:  The inter-processor communication part of the density calculation.
 *                When a processor either runs out of local calculations to do, or it
 *                runs out of space to store the particles to export, it ends up in this
 *                routine.  When it enters the routine, it announces which processors it
 *                needs to send data to.  As soon as one of those processors also enters
 *                this routine and responds, the data is sent and the answer returned.
 *                Once one send/return of data has successfully been done, that memory
 *                is freed for storing more exports and we return to local calculations
 *                again.  Unless of course there are no more local particles to process
 *                (signalled by no_more_local=1) in which case we wait for everyone to
 *                finish.  nsend_local is the array of how many particles to send
 *                to each processor, which is updated before we exit the comm loop.  
 *                This function returns the number of particles still to be exported 
 *                from this processor, i.e., nexport.
 * =====================================================================================
 */
//int density_comm_loop(int *nsend_local,int no_more_local)
//{
//  int offsets[NTask];
//  int send_idxs[NTask];
//  int keep_mem[NTask];
//  int nsend_hi,nsend_data,nsend_return;
//  int send_block,recv_block,finish;
//  int flag,nexport;
//  int nrecv,done;
//  int i,j,k;
//  int tgt;
//  int msrc,mtag;
//  int all_finish;
//  double tstart,tbalance;
//  tbalance = 0;
//  tstart = MPI_Wtime();
//
//  MPI_Status status;
//  MPI_Request send_requests[NTask];
//  MPI_Request data_send_requests[NTask];
//  MPI_Request data_return_requests[NTask];
//  MPI_Request complete_requests[NTask-1];
//
//
//
//  nexport=0;
//  for(i=0;i<NTask;i++)
//    nexport+=nsend_local[i];
//
//  //printf("[%d] Entered comm phase with %d particles to send.\n",Task,nexport);
//  //Sort it so things are back-to-back in buffer
//  qsort(DenSend,nexport,sizeof(union density_comm),den_buffer_sort);
//  //What are the offsets 
//  offsets[0]=0;
//  for(i=1;i<NTask;i++)
//  {
//    offsets[i]=offsets[i-1]+nsend_local[i-1];
//  }
//  //Send greeting to dest-o-nation
//  nsend_hi=0;
//  nsend_data=0;
//  nsend_return=0;
//  for(i=0;i<NTask;i++)
//  {
//    keep_mem[i]=0;
//    data_send_requests[i] = MPI_REQUEST_NULL;
//    data_return_requests[i] = MPI_REQUEST_NULL;
//    if(nsend_local[i])
//    {
//      //printf("[%d] Sending greeting to %d.\n",Task,i);
//      MPI_Issend(0,0,MPI_INT,i,0,MPI_COMM_WORLD,&send_requests[i]);
//      nsend_hi++;
//    }
//    else
//    {
//      send_requests[i] = MPI_REQUEST_NULL;
//    }
//  }
//  //if(no_more_local)
//  //  printf("[%d] Entered the final comm phase.\n",Task);
//  //Now the main comm loop
//  recv_block=0;
//  send_block=0;
//  finish=0;
//  all_finish=0;
//  Timings.init += MPI_Wtime()-tstart;
//  tstart = MPI_Wtime();
//  while(1)
//  {
//    //printf("[%d] This loop we have nterm=%d,nsend_data/hi/return = %d/%d/%d, recv_block=%d,send_block=%d.\n",Task,nterm,nsend_data,nsend_hi,nsend_return,recv_block,send_block);
//    //Are there any outstanding hi messages?
//    if(nsend_hi)
//    {
//      MPI_Testsome(NTask,send_requests,&done,send_idxs,MPI_STATUSES_IGNORE);
//      if(done)
//      {
//        Timings.wait += MPI_Wtime()-tstart;
//        tstart=MPI_Wtime();
//        //At least one hello message was received!
//        //For each of them, post the actual data
//        for(i=0;i<done;i++)
//        {
//          tgt = send_idxs[i];
//          //printf("[%d] Greetings posted to %d was received.\n",Task,tgt);
//          //printf("[%d] Send at offset %d of %d things going to %d and being stored in request slot %d.\n",Task,offsets[tgt],nsend_local[tgt],tgt,nsend_data);
//          MPI_Issend(DenSend+offsets[tgt],nsend_local[tgt]*sizeof(union density_comm),MPI_BYTE,tgt,1,MPI_COMM_WORLD,&data_send_requests[tgt]);
//          //printf("[%d] Send data to %d for processing.\n",Task,tgt);
//          nsend_data++;
//        }
//        nsend_hi -= done;
//        //printf("[%d] nsend_hi/data = %d,%d\n",Task,nsend_hi,nsend_data);
//        //for(i=0;i<NTask;i++)
//        //{
//        //  printf("[%d] Data/hi_pending[%d] = %d,%d.\n",Task,i,data_pending[i],hi_pending[i]);
//        //}
//        Timings.send+=MPI_Wtime()-tstart;
//        tstart=MPI_Wtime();
//      }
//    }
//    //Are there any outstanding requests to send data 
//    if(nsend_data)
//    {
//      //printf("[%d] Testing for completion of data requests nsend=%d.\n",Task,nsend_data);
//      MPI_Testsome(NTask,data_send_requests,&done,send_idxs,MPI_STATUSES_IGNORE);
//      //printf("[%d] Test completed done=%d.\n",Task,done);
//      if(done)
//      {
//        Timings.wait += MPI_Wtime()-tstart;
//        //At least one finished!
//        //We still need to get the processed data back
//        send_block += done;
//        //for(i=0;i<done;i++)
//        //{
//        //  printf("[%d] Data sent for processing to %d was received.\n",Task,send_idxs[i]);
//        //}
//        nsend_data -= done;
//        //printf("[%d] nsend_data = %d.\n",Task,nsend_data);
//        //for(i=0;i<NTask;i++)
//        //  printf("[%d] data_pending[%d] = %d.\n",Task,i,data_pending[i]);
//        tstart=MPI_Wtime();
//      }
//    }
//    //Are there any outstanding requests to return data
//    if(nsend_return)
//    {
//      //Are there any outstanding requests to send data 
//      MPI_Testsome(NTask,data_return_requests,&done,send_idxs,MPI_STATUSES_IGNORE);
//      if(done)
//      {
//        Timings.wait += MPI_Wtime()-tstart;
//        //At least one finished!
//        //for(i=0;i<done;i++)
//        //{
//        //  printf("[%d] Processed data originally from %d and sent back to %d has been received.\n",Task,send_idxs[i],send_idxs[i]);
//        //}
//        nsend_return -= done;
//        tstart=MPI_Wtime();
//      }
//    }
//    //Finished everything I can locally?
//    if(no_more_local && all_finish==0 && nsend_data==0 && nsend_hi==0 && nsend_return==0 && recv_block==0 && send_block==0)
//    {
//      Timings.wait += MPI_Wtime()-tstart;
//      tstart=MPI_Wtime();
//      //printf("[%d] I'm ready to terminate, sending term to all other processors.\n",Task);
//      //Only do this once, send out the "I've finished everything" message to everyone
//      j=0;
//      for(i=0;i<NTask;i++)
//      {
//        if(i==Task)
//        {
//          continue;
//        }
//        MPI_Issend(0,0,MPI_INT,i,3,MPI_COMM_WORLD,&complete_requests[j]);
//        j++;
//      }
//      tbalance=MPI_Wtime();
//      //Don't want to shout out the terminate message more than once
//      all_finish=1;
//      NTerm++;
//      Timings.send += MPI_Wtime()-tstart;
//      tstart=MPI_Wtime();
//    }
//    //Have we received the termination message from every
//    //process?
//    if(NTerm==NTask)
//    {
//      Timings.wait += MPI_Wtime()-tstart;
//      tstart=MPI_Wtime();
//      //printf("[%d] All termination flags received.  Ending.\n",Task);
//      //All termination flags have been received, so end
//      MPI_Waitall(NTask-1,complete_requests,MPI_STATUSES_IGNORE);
//      mtag=0;
//      Timings.send += MPI_Wtime()-tstart;
//      Timings.balance += MPI_Wtime()-tbalance;
//      break;
//    }
//    //Can we finish and return to work?
//    if(no_more_local==0 && finish && nsend_data==0 && nsend_return==0 && recv_block==0 && send_block==0)
//    {
//      Timings.wait += MPI_Wtime()-tstart;
//      tstart=MPI_Wtime();
//      //printf("[%d] Trying to return to local work.\n",Task);
//      for(i=0;i<NTask;i++)
//      {
//        if(send_requests[i]!=MPI_REQUEST_NULL)
//        {
//          //printf("[%d] Cancelling greetings message to %d.\n",Task,i);
//          MPI_Cancel(&send_requests[i]);
//          MPI_Wait(&send_requests[i],&status);
//          MPI_Test_cancelled(&status,&flag);
//          if(!flag)
//          {
//            //printf("[%d] Cancel failed, so sending data to %d.\n",Task,i);
//            MPI_Issend(DenSend+offsets[i],nsend_local[i]*sizeof(union density_comm),MPI_BYTE,i,1,MPI_COMM_WORLD,&data_send_requests[i]);
//            //printf("[%d] Sent data to %d for processing.\n",Task,i);
//            nsend_data++;
//          }
//          else
//          {
//            //So we know this block is to be shifted around
//            keep_mem[i]=1;
//          }
//          nsend_hi--;
//        }
//      }
//      //Whatever happens, the hi array should be empty now
//      if(nsend_hi!=0)
//        kill(908);
//      //We still ok to finish?
//      if(nsend_data==0)
//      {
//        //printf("[%d] Clearing memory and finishing.\n",Task);
//        //Move the memory around
//        nexport=0;
//        for(i=0;i<NTask;i++)
//        {
//          if(keep_mem[i])
//          {
//            memmove(DenSend+nexport,DenSend+offsets[i],sizeof(union density_comm)*nsend_local[i]);
//            nexport += nsend_local[i];
//          }
//          else
//          {
//            nsend_local[i]=0;
//          }
//        }
//        //Finish!
//        mtag=nexport;
//        Timings.send += MPI_Wtime()-tstart;
//        break;
//      }
//      Timings.send += MPI_Wtime()-tstart;
//      tstart=MPI_Wtime();
//    }
//
//
//
//    //Handle receiving data first
//    //Check them in special order so that we don't
//    //get stuck if we can't accept tag=1
//    //Would be nicer if we defined a tag-order array, but eh
//    //nanosleep(&Wait,NULL);
//    mtag=0;
//    MPI_Iprobe(MPI_ANY_SOURCE,0,MPI_COMM_WORLD,&flag,&status);
//    if(!flag)
//    {
//      mtag=2;
//      MPI_Iprobe(MPI_ANY_SOURCE,2,MPI_COMM_WORLD,&flag,&status);
//      if(!flag)
//      {
//        mtag=3;
//        MPI_Iprobe(MPI_ANY_SOURCE,3,MPI_COMM_WORLD,&flag,&status);
//        if(!flag && nsend_return==0)
//        {
//          mtag=1;
//          MPI_Iprobe(MPI_ANY_SOURCE,1,MPI_COMM_WORLD,&flag,&status);
//        }
//      }
//    }
//    //MPI_Iprobe(MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&flag,&status);
//    if(flag)
//    {
//      Timings.wait += MPI_Wtime()-tstart;
//      tstart=MPI_Wtime();
//      msrc=status.MPI_SOURCE;
//      //Was it a hello message
//      if(mtag==0)
//      {
//        //Complete the message receipt
//        MPI_Recv(0,0,MPI_INT,msrc,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
//        //printf("[%d] Received greetings from %d.\n",Task,msrc);
//        //Now we're expecting to be sent some data, so better wait for it
//        recv_block++;
//      }
//      //Was it a block of data to do work on?
//      //Can't receive while I have old data still
//      //to be sent out
//      else if(status.MPI_TAG==1)
//      {
//        if(nsend_return!=0)
//        {
//          kill(908);
//          //printf("[%d] Skipping receive of data from %d because buffer isn't ready.\n",Task,msrc);
//        }
//        //Get it
//        MPI_Get_count(&status,MPI_BYTE,&nrecv);
//        MPI_Recv(DenRecv,nrecv,MPI_BYTE,msrc,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
//        //printf("[%d] Received data for processing from %d.\n",Task,msrc);
//        //One less thing to block for
//        recv_block--;
//        //Do stuff with it
//        for(j=0;j<nrecv/sizeof(union density_comm);j++)
//        {
//          density_ngbs(j,1);
//        }
//        //Send the results back
//        MPI_Issend(DenRecv,nrecv,MPI_BYTE,msrc,2,MPI_COMM_WORLD,&data_return_requests[msrc]);
//        //printf("[%d] Sent processed data back to %d.\n",Task,msrc);
//        nsend_return++;
//      }
//      //Was it a block of data with work done on it to be stored?
//      else if(status.MPI_TAG==2)
//      {
//        //Get them back again
//        MPI_Get_count(&status,MPI_BYTE,&nrecv);
//        if(nrecv!=nsend_local[msrc]*sizeof(union density_comm))
//        {
//          printf("[%d] I've made a huge mistake...Returning data \n",Task);
//          kill(908);
//        }
//        MPI_Recv(DenSend+offsets[msrc],nrecv,MPI_BYTE,msrc,2,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
//        //printf("[%d] Received data with processing done on it back from %d.\n",Task,msrc);
//        //I've got something back!  Hooray!
//        finish=1;
//        send_block--;
//        //Store the results locally
//        for(j=offsets[msrc];j<offsets[msrc]+nrecv/sizeof(union density_comm);j++)
//        {
//          k=DenSend[j].out.what;
//          G[k].density += DenSend[j].out.rho;
//          G[k].dhsml += DenSend[j].out.dhsml;
//          G[k].zeta += DenSend[j].out.zeta;
//          G[k].drho_x += DenSend[j].out.drho_x;
//          G[k].drho_y += DenSend[j].out.drho_y;
//          G[k].drho_z += DenSend[j].out.drho_z;
//        }
//      }
//      else if(status.MPI_TAG==3)
//      {
//        //Termination signal received.  Update global 
//        //marker
//        MPI_Recv(0,0,MPI_INT,msrc,3,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
//        //printf("[%d] Received termination flag from %d.\n",Task,msrc);
//        NTerm++;
//        //if(!no_more_local || !nterm)
//        //{
//        //  printf("[%d] Skipping receipt of termination flag from %d because we either still have local work to do or have not entered the termination phase ourselves.\n",Task,msrc);
//        //  continue;
//        //}
//        //Received a termination flag from someone.  Only
//        //really care about this if I'm it's possible for
//        //me to terminate too
//      }
//      else
//      {
//        printf("[%d] I've made a huge mistake...Unknown tag\n",Task);
//        kill(908);
//      }
//      Timings.process += MPI_Wtime()-tstart;
//      tstart = MPI_Wtime();
//    }
//  }
//
//  return mtag;
//}

/* 
 * ===  FUNCTION  ======================================================================
 *  Description:  Launches the density neighbour search.
 * =====================================================================================
 */
void density_eval(void)
{
  ngb_search(DenSend,DenRecv,sizeof(union density_comm),BufferSizeDen,
      density_ngbs,
      density_export_return,
      den_buffer_sort);
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Description:  data points to some memory containing a particle that has had work 
 *                done on it and needs to be stored in the local array.  It is necessary
 *                to use uncast pointers as this function is to be called by a generic
 *                communication function.
 * =====================================================================================
 */
void density_export_return(void *data)
{
  union density_comm *part = data;
  int k;
  k=part->out.what;
  G[k].density += part->out.rho;
  G[k].dhsml += part->out.dhsml;
  G[k].zeta += part->out.zeta;
  G[k].nops += part->out.nops;
}

/* 
 * ===  FUNCTION  ======================================================================
 * DEPRECATED IN FAVOUR OF ABSTRACTED COMM ROUTINES
 *  Description:  The core loop of the density evaluation.  Loops over all particles
 *                calling the inner SPH loop (the one over neighbours) on each particle.
 *                Also responsible for keeping track of those particles that need to be
 *                exported to other processors and calling the comm loop as needed.
 * =====================================================================================
 */
//void density_eval(void)
//{
//  int nexport,i,j;
//  int nsend_local[NTask];
//  double tstart;
//
//  NTerm=0;
//  for(i=0;i<NTask;i++)
//  {
//    nsend_local[i]=0;
//  }
//  nexport=0;
//
//
//  for(i=0;i<NPart[GAS];i++)
//  {
//    //Only evaluate the ones that aren't already done
//    if(G[i].hsml>0)
//    {
//      //Reset the export flag
//      for(j=0;j<NTask;j++)
//        Export[j]=0;
//
//      //Calculate the density locally
//      density_ngbs(i,0);
//      if(G[i].density==0)
//      {
//        printf("[%d] G[%d].density = %g,h=%g.\n",Task,i,G[i].density,G[i].hsml);
//        density_ngbs(i,2);
//        printf("[%d] After re-run G[%d].density = %g,h=%g.\n",Task,i,G[i].density,G[i].hsml);
//        kill(999);
//      }
//
//      //If the export flag is set, I have to export to this processor
//      //Rather than export the entire particle, just export what's 
//      //needed.
//      for(j=0;j<NTask;j++)
//      {
//        if(Export[j])
//        {
//          ////printf("[%d] Particle index %d, location %g,%g,%g hsml = %g.\n",Task,i,G[i].x,G[i].y,G[i].z,G[i].hsml);
//          DenSend[nexport].in.x = G[i].x;
//          DenSend[nexport].in.y = G[i].y;
//          DenSend[nexport].in.z = G[i].z;
//          DenSend[nexport].in.hsml = G[i].hsml;
//          DenSend[nexport].in.what = i;
//          DenSend[nexport].in.where = j;
//          DenSend[nexport].in.from = Task;
//          nexport++;
//          nsend_local[j]++;
//          if(nexport==BufferSizeDen)
//          {
//            tstart=MPI_Wtime();
//            nexport=density_comm_loop(nsend_local,0);
//            Timings.dcomm += MPI_Wtime()-tstart;
//          }
//        }
//      }
//    }
//  }
//  //Do the final communication and wait for other processors to finish
//  tstart=MPI_Wtime();
//  nexport=density_comm_loop(nsend_local,1);
//  Timings.dcomm += MPI_Wtime()-tstart;
//}

/* 
 * ===  FUNCTION  ======================================================================
 *  Description:  The heart of the density calculation.  This evaluates the loop over 
 *                all SPH neighbours and calculates whatever quantities can be calculated
 *                in the first loop over neighbours.  The obvious one being density of
 *                course.  Because this loop is evaluated a lot, to make it more efficient
 *                only the bits of the loop that change every time are calculated here
 *                and other factors (e.g. m/h^3 in the density loop) are multiplied only
 *                once the calculation is finish and h and rho have converged.
 *                This function is intended to be passed as an argument to an appropriate
 *                communication routine.  This is why the location of the particle to 
 *                work on (index) and the location of the block of memory to store 
 *                exports in (export) are given as void pointers and cast as needed to
 *                the relevant density type.
 *                The mode flag specifies if the particle to work in is local (0) or
 *                exported to us (1).
 * =====================================================================================
 */

void density_ngbs(void *index,void *export, int mode)
{
  double pos[3];
  double h;
  double dker;
  double hinv,h2,q,rho;
  double dx,dy,dz,r2,r,dhsml;
  double zeta;
  int nngbs,i,j,k;
  PCLE_REF *ngbs;
  int export_offset[NTask];
  int owner,mob,count;
  int owners;
  int current=0;
  int reget=0;
  int nops=0;
  const struct node *cnode;
  //Cast the memory locations of the particle to search for (index/remote) and 
  //where to store any exported particles (storage)
  struct gas *local = NULL;
  union density_comm *remote;
  union density_comm *storage = NULL;

  if(mode)
  {
    //Cast pointer as needed
    remote = index;
    pos[0] = remote->in.x;
    pos[1] = remote->in.y;
    pos[2] = remote->in.z;
    h = remote->in.hsml;
    nngbs = remote->in.nnodes;
    ngbs = remote->in.nodes;
  }
  else
  {
    //Cast index pointer to appropriate type
    local = index;
    storage = export;
    pos[0] = local->x;
    pos[1] = local->y;
    pos[2] = local->z;
    h = local->hsml;
    local->nngbs = 0;
    nngbs = 0;
    //This is a special property just for the density loop to enforce finding
    //neighbours freshly every time
    ngbs = local->ngbs;
    //set up the export array
    for(i=0;i<NTask;i++)
      export_offset[i]=-1;
  }

  hinv = 1.0/h;
  h2 = h*h*KER_SUPPORT*KER_SUPPORT;
  rho=0;
  dhsml = 0;
  zeta = 0;
  //printf("[%d] Starting the game with %d ngbs.\n",Task,nngbs);
  //Outer loop in case we over-fill buffer
  do
  {
    //printf("[%d] Hey we're going around the loop with current=%d.\n",Task,current);
    //Do we already have a nice list of nodes/ngbs?
    //Second check is if we're looping over find_ngbs we 
    //don't accidentally end up skipping subsequent calls on overflow
    if(!nngbs || ngbs == Ngbs)
    {
      //printf("[%d] Mode=%d, Don't have ngbs, so calculating them...\n",Task,mode);
      //No we don't, so get one...
      nngbs = find_ngbs(pos,KER_SUPPORT*h,&current,mode);
      //Can we store it?
      //Not if on export, we don't have room, find_ngbs overflowed or 
      //we're not in the first run through the do loop
      if(!mode && nngbs<NGB_LIST_LEN && current==-1 && !reget)
      {
        //printf("[%d] Storing %d ngbs.\n",Task,nngbs);
        //So store it then
        memmove(local->ngbs,Ngbs,sizeof(int)*nngbs);
        local->nngbs=nngbs;
      }
      //Set pointer for work now
      ngbs = Ngbs;
      reget=1;
    }
    else
    {
      //So we exit the loop OK.
      current=-1;
    }
    ////printf("[%d] Now looping through %d neighbours.\n",Task,nngbs);
    //ngbs now has a list of nodes/neighbours, do stuff with it...
    for(i=0;i<nngbs;i++)
    {
      //OK.  The ngb buffer is assumed to have some special properties we're going to
      //exploit.  Firstly, if it's a particle, then we must have a copy of it 
      //(strongly local if on export), otherwise how did we find it?  If it's a node
      //and we're local, it must be exported.If it's a node and we're on export, it 
      //must be opened up (otherwise why was it sent to us)?  The debug section tests
      //these assumptions.
#if DEBUG>=1
      if(ngbs[i]<0)
      {
        cnode=Root-ngbs[i];
        if(mode)
        {
          //If it's on export, this must be strongly local
          if((cnode->owner>0 && cnode->owner!=Task) ||
             (cnode->owner<0 && !get_bit(Task,cnode->owners)))
          {
            pprintf("Neighbour list contains element %d = %d which is not local even though we're in export mode.\n",i,ngbs[i]);
            kill(ERROR_SANITY);
          }
        }
        else
        {
          //Can't possibly be entirely locally owned, otherwise we should have 
          //its contents rather than the node
          j=0;
          //Flag that we've found a foreigner
          k=0;
          do
          {
            owner = cnode->owner<0 ? (int) (cnode->extra[j].mob/MaxGas) : cnode->owner;
            //This is foreign, as at least one of them must be
            if(!Shadows[owner].G)
              k=1;
            j++;
          }while(j<(-cnode->owner) && !k);
          if(!k)
          {
            pprintf("Neighbour %d was %d, which is a node that we have entirely locally and so should have been opened up given we're in local mode.\n",i,ngbs[i]);
          kill(ERROR_SANITY);
          }
        }
      }
      else
      {
        //It's a particle, it had better be local 
        //It must be strongly local on export
        owner=ngbs[i]/MaxGas;
        if(owner!=Task && (mode || !Shadows[owner].G))
        {
          pprintf("With mode %d neighbour %d was particle %d, which is not local.\n",mode,i,ngbs[i]);
          kill(ERROR_SANITY);
        }
      }
#endif
      //Is it a particle or a node?
      cnode = ngbs[i]<0 ? Root-ngbs[i] : NULL;
      //This will ensure exit of the do-while loop for particles
      owners = ngbs[i]<0 ? cnode->owner : 1;
      j = 0;
      //Loop for multiple ownership
      do
      {
        if(!cnode)
        {
          owner = ngbs[i]/MaxGas;
          mob = ngbs[i]%MaxGas;
          count=1;
        }
        else if(owners<0)
        {
          owner = (int) (cnode->extra[j].mob/MaxGas);
          mob = (int) (cnode->extra[j].mob%MaxGas);
          count = cnode->extra[j].count;
        }
        else
        {
          owner = cnode->owner;
          mob = cnode->mob;
          count = cnode->count;
        }
        //Open up if it's a particle (always local) or if it's export mode and
        //this is the local part
        if(!cnode || (mode && owner==Task))
        {
          //printf("[%d] Cracking open on loop %d owner %d mob %d count %d and extracting pcles.\n",Task,j,owner,mob,count);
          //OK, open up the node and extract the goey particle centre
          for(k=mob;k<mob+count;k++)
          {
            nops++;
            dx = pos[0] - Shadows[owner].G[k].x;
            dy = pos[1] - Shadows[owner].G[k].y;
            dz = pos[2] - Shadows[owner].G[k].z;
  
            r2 = dx*dx + dy*dy + dz*dz;
            if(r2<h2)
            {
              //It's a real neighbour, do stuff with it...
              r=sqrt(r2);
              q = r *hinv;
              //Actually just sum w_ab, include m and h^-3 later
              rho += kernel(q);
              //printf("[%d] Within the inner zone r=%g rho=%g\n",Task,r,rho);
              //Similarly just sum w'_ab * q_ab
              //The price correction terms for self-gravity doesn't include the factor 
              //of 1/h^2. Should include self-interaction as per correspondence
#if NDIM==3
              zeta += dphi_dh(q);
#else
              //In reduced dimensions we might put the source a characteristic distance
              //away in order to mimic the effect of integrating over the unmodelled
              //dimension
              zeta += dphi_dh(hinv*sqrt(r2+Params.dim_grav_soft));
#endif
              if(q)
              {
                dker = dkernel(q);
                dhsml += q*dker;
              }
            }
          }
        }
        //Only store the export if we're not on export, in which case
        //it must always be stored
        //The second condition is needed because the neighbour search
        //will return locally owned particles of a multiply owned node
        //and the node itself (if it is has foreign content) we obviously
        //don't want to try export the foreign content to ourselves or
        //our shadows hence the second condition.
        else if(!mode && !Shadows[owner].G)
        {
          //Establish export of this node to processor whatever
          if(export_offset[owner]<0)
          {
            export_offset[owner]=N_Export;
            storage[N_Export].in.x = local->x;
            storage[N_Export].in.y = local->y;
            storage[N_Export].in.z = local->z;
            storage[N_Export].in.hsml = local->hsml;
            storage[N_Export].in.what = (int) (local-G);
            storage[N_Export].in.where = owner;
            storage[N_Export].in.from = Task;
            storage[N_Export].in.nodes[0] = ngbs[i];
            storage[N_Export].in.nnodes = 1;
            N_Export++;
            N_Send_Local[owner]++;
          }
          else
          {
            if(storage[export_offset[owner]].in.nnodes == NGB_LIST_LEN)
            {
              //printf("[%d] Too many to export :(\n",Task);
              storage[export_offset[owner]].in.nnodes = 0;
            }
            //Don't add anything if we can't fit it all
            else if(storage[export_offset[owner]].in.nnodes)
            {
              //printf("[%d] Sup!  Adding in extra node to send, up to nnodes = %d.\n",Task,storage[export_offset[owner]].in.nnodes);
              //Add in the new node
              storage[export_offset[owner]].in.nodes[storage[export_offset[owner]].in.nnodes] = ngbs[i];
              storage[export_offset[owner]].in.nnodes++;
            }
          }
        }
        j++;
      }while(j<(-owners));
    }
  }while(current!=-1);

  if(mode)
  {
    mode = remote->in.where;
    i = remote->in.from;
    nngbs = remote->in.what;
    remote->out.what = nngbs;
    remote->out.where = mode;
    remote->out.from = i;
    remote->out.rho = rho;
    remote->out.dhsml = dhsml;
    remote->out.zeta = zeta;
    remote->out.zeta = nops;
  }
  else
  {
    local->density = rho;
    local->dhsml = dhsml;
    local->zeta = zeta;
    local->nops += nops;
  }
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Description:  Comparison function to arrange the communication buffer so that 
 *                particles to be sent to the same processor are adjacent.
 * =====================================================================================
 */
int den_buffer_sort(const void* a,const void* b)
{
  if((*((union density_comm*) a)).in.where < (*((union density_comm*) b)).in.where) return -1;
  if((*((union density_comm*) a)).in.where > (*((union density_comm*) b)).in.where) return 1;
  return 0;
}
