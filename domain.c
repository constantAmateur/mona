/*
 * =====================================================================================
 *
 *       Filename:  domain.c
 *
 *    Description:  Domain decomposition, particle accretion/destruction and handling 
 *                  of shadow routines.
 *
 *        Version:  0.6.3
 *        Created:  20/07/13 11:34:51
 *
 *         Author:  Matthew D. Young, my304@ast.cam.ac.uk
 *        Company:  Institute of Astronomy, Cambridge, UK
 *        License:  GNU General Public License
 *      Copyright:  Copyright (c) 2014, Matthew D. Young
 *
 * =====================================================================================
 */

#include "variables.h"
#include "functions.h"

struct balancer {
  int nops;
  FLOAT cylR;
};
//This function takes all the particles and splits them into even radial rings 
//in terms of number of particles.  That is, after this function is run, each
//processor will have a roughly even number of particles, which all lie within
//the one radial bin, taking the centre of mass as the origin.
//The number of particles are only roughly split due to some funny issues
//with rounding at domain boundaries which can cause the routine to think
//that a particle which is in the right domain should be moved.

//  struct temp_struct {
//    double R;
//    double cpp; //Cost per particle
//  };
//
//
//int temp_sort(const void* a,const void* b)
//{
//  if( (*((struct temp_struct*) a)).R < (*((struct temp_struct*) b)).R) return -1;
//  if( (*((struct temp_struct*) a)).R > (*((struct temp_struct*) b)).R) return 1;
//  return 0;
//}

//The alternative where we split into NTrunk equal pieces
//where it is assumed that NTrunk and NTask are powers of 2
//void domain_decomp(void)
//{
//  int i,cut;
//  int ppn;
//  int count;
//  int nparts[NTask];
//  int offsets[NTask];
//  double R[NPart[GAS]];
//  double *allR;
//  for(i=0;i<NPart[GAS];i++)
//  {
//    R[i]=G[i].R;
//  }
//  //How many Trunk cells per node?
//  ppn = Params.ntrunk / NTask;
//  if(!Task)
//  {
//    //Allocate memory for allR
//    allR = malloc(NPartWorld[GAS]*sizeof(double));
//    MPI_Gather(&NPart[GAS],1,MPI_INT,
//        nparts,1,MPI_INT,
//        0,MPI_COMM_WORLD);
//    //Where to store things in the allR array?
//    for(i=1,offsets[0]=0;i<NTask;i++)
//      offsets[i]=offsets[i-1]+nparts[i-1];
//    //Gather R
//    MPI_Gatherv(&R[0],NPart[GAS],MPI_DOUBLE,
//        allR,nparts,offsets,MPI_DOUBLE,
//        0,MPI_COMM_WORLD);
//    //Sort it
//    qsort(allR,NPartWorld[GAS],sizeof(double),dsort);
//    //How many particles per node
//    //Find out where we should split it
//    TrunkBoundaries[0]=allR[0];
//    for(i=0,cut=0;i<Params.ntrunk;i++)
//    {
//      count=NPartWorld[GAS]/Params.ntrunk;
//      if(i<NPartWorld[GAS]%Params.ntrunk)
//        count++;
//      cut += count;
//      TrunkBoundaries[i+1]=allR[cut-1];
//    }
//    MPI_Bcast(TrunkBoundaries,Params.ntrunk+1,MPI_DOUBLE,0,MPI_COMM_WORLD);
//    free(allR);
//  }
//  else
//  {
//    MPI_Gather(&NPart[GAS],1,MPI_INT,
//        NULL,0,MPI_INT,
//        0,MPI_COMM_WORLD);
//    MPI_Gatherv(&R[0],NPart[GAS],MPI_DOUBLE,
//        NULL,NULL,NULL,MPI_DOUBLE,
//        0,MPI_COMM_WORLD);
//    //Receive the boundaries
//    printf("[%d] NTrunk=%d.\n",Task,Params.ntrunk);
//    MPI_Bcast(TrunkBoundaries,Params.ntrunk+1,MPI_DOUBLE,0,MPI_COMM_WORLD);
//  }
//  //Add a little bit extra (as upper boundaries are not inclusive)
//  TrunkBoundaries[Params.ntrunk] *= 1.01;
//  DomainWalls[0]=TrunkBoundaries[0];
//  DomainWalls[NTask]=TrunkBoundaries[Params.ntrunk];
//  for(i=1;i<NTask;i++)
//  {
//    DomainWalls[i]=TrunkBoundaries[i*ppn];
//  }
//  if(!Task)
//  {
//    for(i=0;i<NTask+1;i++)
//    {
//      printf("DomainWall[%d]=%g.\n",i,DomainWalls[i]);
//    }
//    for(i=0;i<Params.ntrunk+1;i++)
//    {
//      printf("TrunkBoundaries[%d]=%g.\n",i,TrunkBoundaries[i]);
//    }
//  }
//}



//This really needs to be replaced with a parallel sort algorithm
//Find out where to draw the domain walls (and no more)
void domain_decomp(void)
{
  int i,cut;
  int *nparts;
  int *offsets;
  int ntarget[NTask];
  double R[NPart[GAS]];
  double *allR;
  //Calculate the radius of each particle
  for(i=0;i<NPart[GAS];i++)
  {
    R[i]=G[i].R;
  }
  //Work out how many particles on each node
  if(Task==0)
  {
    nparts = malloc(NTask * sizeof(int));
    offsets = malloc(NTask * sizeof(int));
    MPI_Gather(&NPart[GAS],1,MPI_INT,
        nparts,1,MPI_INT,
        0,MPI_COMM_WORLD);
    //Allocate memory for sorting
    allR = malloc(NPartWorld[GAS]*sizeof(double));
    //Where to store things in the allR array?
    for(i=1,offsets[0]=0;i<NTask;i++)
      offsets[i]=offsets[i-1]+nparts[i-1];
    //Gather R
    MPI_Gatherv(&R[0],NPart[GAS],MPI_DOUBLE,
        allR,nparts,offsets,MPI_DOUBLE,
        0,MPI_COMM_WORLD);
    qsort(allR,NPartWorld[GAS],sizeof(double),dsort);
    //Find ideal split points
    DomainWalls[0]=allR[0];
    cut=0;
    for(i=0;i<NTask;i++)
    {
      ntarget[i]=NPartWorld[GAS]/NTask;
      if(i<NPartWorld[GAS] % NTask)
        ntarget[i]++;
      cut+=ntarget[i];
      DomainWalls[i+1]=allR[cut-1];
    }
    //Send them back to everyone
    MPI_Bcast(DomainWalls,NTask+1,MPI_DOUBLE,0,MPI_COMM_WORLD);
    //Free memory
    free(nparts);
    free(allR);
    free(offsets);
  }
  else
  {
    MPI_Gather(&NPart[GAS],1,MPI_INT,
        NULL,0,MPI_INT,
        0,MPI_COMM_WORLD);
    MPI_Gatherv(&R[0],NPart[GAS],MPI_DOUBLE,
        NULL,NULL,NULL,MPI_DOUBLE,
        0,MPI_COMM_WORLD);
    //Receive the boundaries
    MPI_Bcast(DomainWalls,NTask+1,MPI_DOUBLE,0,MPI_COMM_WORLD);
  }
  //printf("[%d] HELLO JOE!.\n",Task);
  //Add a little extra to the outer domain (as upper boundaries are not inclusive)
  DomainWalls[NTask] *= 1.01;
}


//int cost_sort(const void *a,const void *b)
//{
//  if( (*(struct balancer *) a).R < (*(struct balancer *) b).R) return -1;
//  if( (*(struct balancer *) a).R == (*(struct balancer *) b).R) return 0;
//  return 1;
//}

int sort_balancer(const void *a,const void *b)
{
  double diff= ((struct balancer *) a)->cylR - ((struct balancer *) b)->cylR;
  if(diff<0)
    return -1;
  else if(diff>0)
    return 1;
  else
    return 0;
}

//The inner/outer boundaries should be open and should move each timestep
void drift_domain_walls(void)
{
  //double rmin,rmax;
  //int i;
  //rmin=DBL_MAX;
  //rmax=0;
  //for(i=0;i<NPart[GAS];i++)
  //{
  //  if(G[i].R<rmin)
  //    rmin=G[i].R;
  //  //Make the outer boundary a little larger as the edge is excluded
  //  if(G[i].R>rmax)
  //    rmax=G[i].R*1.01;
  //}
  //MPI_Allreduce(&rmin,&DomainWalls[0],1,MPI_DOUBLE,MPI_MIN,MPI_COMM_WORLD);
  //MPI_Allreduce(&rmax,&DomainWalls[NTask],1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
  //Balance domains based on work
  struct balancer pcles[NPart[GAS]];
  struct balancer *allPart;
  int nparts[NTask];
  int offsets[NTask];
  int i,j,k;
  int bonus;
  int spare;
  double load;
  double tgt_ops;
  for(i=0;i<NPart[GAS];i++)
  {
    pcles[i].nops = G[i].nops;
    pcles[i].cylR = G[i].R;
  }
  MPI_Gather(&NPart[GAS],1,MPI_INT,nparts,1,MPI_INT,0,MPI_COMM_WORLD);
  if(Task)
  {
    MPI_Gatherv(pcles,NPart[GAS]*sizeof(struct balancer),MPI_BYTE,
        NULL,NULL,NULL,MPI_BYTE,0,MPI_COMM_WORLD);
    //Receive the boundaries
    MPI_Bcast(DomainWalls,NTask+1,MPI_DOUBLE,0,MPI_COMM_WORLD);
  }
  else
  {
    allPart = malloc(NPartWorld[GAS] * sizeof(struct balancer));
    //Where to store things in this array?
    nparts[0] *= sizeof(struct balancer);
    for(i=1,offsets[0]=0;i<NTask;i++)
    {
      offsets[i]=offsets[i-1]+nparts[i-1];
      nparts[i] *= sizeof(struct balancer);
    }
    //Gather them
    MPI_Gatherv(pcles,nparts[0],MPI_BYTE,
        allPart,nparts,offsets,MPI_BYTE,
        0,MPI_COMM_WORLD);
    //Sort them by R
    qsort(allPart,NPartWorld[GAS],sizeof(struct balancer),sort_balancer);
    //How many ops should we have per processor?
    tgt_ops=0;
    for(i=0;i<NPartWorld[GAS];i++)
    {
      //gpprintf("G[%d].R/nops = %g/%d\n",i,allPart[i].cylR,allPart[i].nops);
      tgt_ops += allPart[i].nops;
    }
    tgt_ops /= NTask;
    //Now add particles to new domains one at a time until we get to the 
    //boundaries
    //nparts is now the number of particles we want on processor i
    load=0;
    j=0;
    k=0;
    for(i=0;i<NPartWorld[GAS];i++)
    {
      load += allPart[i].nops;
      if(load > tgt_ops)
      {
        //How many particles do we need on this node?
        nparts[j]=i-k;
        k=i;
        j++;
        load -= tgt_ops;
      }
    }
    if(j!=NTask)
    {
      nparts[NTask-1]=i-k;
    }
    //Are any of them over the limit?  If so then assign them the max and 
    //share their particles between others equally.
    k=0;
    //j is the number of saturated nodes
    j=0;
    for(i=0;i<NTask;i++)
    {
      if(nparts[i]>MaxGas)
      {
        k+=(nparts[i]-MaxGas);
        nparts[i]=MaxGas;
        j++;
      }
      gpprintf("nparts[%d]=%d.\n",i,nparts[i]);
    }
    //Rebalance if we're memory limited
    while(k)
    {
      //If we re-distribute them evenly, how many should each 
      //non-saturated node get?
      bonus=k/(NTask-j);
      spare = k - bonus*(NTask-j);
      gpprintf("%d extra particles and %d saturated domains.  Adding %d to each un-saturated domain with %d spare.  Consider increasing ParticleExcessFraction for better work balance.\n",k,j,bonus,spare);
      k=0;
      for(i=0;i<NTask;i++)
      {
        //Skip already saturated nodes
        if(nparts[i]==MaxGas)
          continue;
        nparts[i] += bonus;
        if(spare)
        {
          nparts[i]++;
          spare--;
        }
        if(nparts[i]>MaxGas)
        {
          //How many extra left?
          k += (nparts[i]-MaxGas);
          nparts[i]=MaxGas;
          j++;
        }
      }
    }
    //Finally, work out what the corresponding domain walls are
    k=0;
    for(i=0;i<NTask;i++)
    {
      DomainWalls[i]=allPart[k].cylR;
      k+=nparts[i];
    }
    DomainWalls[NTask]=allPart[NPartWorld[GAS]-1].cylR*1.01;
    //Send them back
    MPI_Bcast(DomainWalls,NTask+1,MPI_DOUBLE,0,MPI_COMM_WORLD);
    //Free memory
    free(allPart);
  }
  //kill(3333);
  //Recalculate domain boundaries based on work
  //double w_tot,w;
  //double *ws;
  //double R[NPart[GAS]];
  //double *allR;
  //int *nparts;
  //int *offsets;
  //int ntarget[NTask];
  //int cut;
  //int n_tot;
  //for(i=0;i<NPart[GAS];i++)
  //{
  //  R[i]=G[i].R;
  //}
  ////This weighting is chosen so that the total number of particles is
  ////the same before and after we re-adjust, and so that if a processor
  ////is taking longer than another, it gets fewer particles than before
  //w = (Timings.work_local+Timings.work_remote)/(Timings.work_local+Timings.work_remote+Timings.wait_local+Timings.wait_remote);
  //w = (NPart[GAS])/w;
  ////w = 1.0;
  //if(Task)
  //{
  //  MPI_Gather(&w,1,MPI_DOUBLE,NULL,0,MPI_DOUBLE,0,MPI_COMM_WORLD);
  //  MPI_Gather(&NPart[GAS],1,MPI_INT,
  //      NULL,0,MPI_INT,
  //      0,MPI_COMM_WORLD);
  //  MPI_Gatherv(&R[0],NPart[GAS],MPI_DOUBLE,
  //      NULL,NULL,NULL,MPI_DOUBLE,
  //      0,MPI_COMM_WORLD);
  //  //Receive the boundaries
  //  MPI_Bcast(DomainWalls,NTask+1,MPI_DOUBLE,0,MPI_COMM_WORLD);
  //}
  //else
  //{
  //  ws = malloc(NTask * sizeof(double));
  //  nparts = malloc(NTask*sizeof(int));
  //  offsets = malloc(NTask*sizeof(int));
  //  MPI_Gather(&w,1,MPI_DOUBLE,ws,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
  //  MPI_Gather(&NPart[GAS],1,MPI_INT,nparts,1,MPI_INT,0,MPI_COMM_WORLD);
  //  w_tot=0;
  //  for(i=0;i<NTask;i++)
  //    w_tot += ws[i];
  //  //Calculate new Ns
  //  n_tot=0;
  //  for(i=0;i<NTask;i++)
  //  {
  //    ntarget[i] = (int) (NPartWorld[GAS]*(ws[i]/w_tot));
  //    gpprintf("Weighting on %d from %g is %g yielding %d from %d.\n",i,(nparts[i])/ws[i],ws[i]/w_tot,ntarget[i],nparts[i]);
  //    n_tot += ntarget[i];
  //  }
  //  //How many remainders...
  //  n_tot = n_tot - NPartWorld[GAS];
  //  for(i=0;i<NTask;i++)
  //  {
  //    if(i<n_tot)
  //      ntarget[i]++;
  //  }
  //  //What are the new ones...
  //  for(i=0;i<NTask;i++)
  //  {
  //    gpprintf("Changed number of particles on %d from %d to %d.\n",i,nparts[i],ntarget[i]);
  //  }
  //  allR = malloc(NPartWorld[GAS]*sizeof(double));
  //  //Where to store things in the allR array?
  //  for(i=1,offsets[0]=0;i<NTask;i++)
  //    offsets[i]=offsets[i-1]+nparts[i-1];
  //  //Gather R
  //  MPI_Gatherv(&R[0],NPart[GAS],MPI_DOUBLE,
  //      allR,nparts,offsets,MPI_DOUBLE,
  //      0,MPI_COMM_WORLD);
  //  qsort(allR,NPartWorld[GAS],sizeof(double),dsort);
  //  //Find ideal split points
  //  DomainWalls[0]=allR[0];
  //  cut=0;
  //  for(i=0;i<NTask;i++)
  //  {
  //    cut += ntarget[i];
  //    DomainWalls[i+1] = allR[cut-1];
  //  }
  //  //Send them back
  //  MPI_Bcast(DomainWalls,NTask+1,MPI_DOUBLE,0,MPI_COMM_WORLD);
  //  //Free memory
  //  free(nparts);
  //  free(allR);
  //  free(ws);
  //  free(offsets);
  //}
  //DomainWalls[NTask] *= 1.01;

  //
  //int i,cut;
  //int *nparts;
  //int *offsets;
  //int *ntarget;
  //double *bounds;
  //double R[NPart[GAS]];
  //double glob_costs[NPart[GAS]];
  //double *allR;
  //struct balancer *all;
  //double costs[NTask];
  //double sum;
  //double tot_cost;
  //gather_costs();
  //sum=0;
  ////double low,high,rsplit;
  ////For the boundaries
  //bounds = malloc((NTask+1)*sizeof(double));
  //ntarget = malloc(NTask*sizeof(int));
  ////Calculate the radius of each particle
  //for(i=0;i<NPart[GAS];i++)
  //{
  //  R[i]=G[i].R;
  //  glob_costs[i]=G[i].cost;
  //}
  ////Work out how many each node should have now...
  ////CostPerPcle currently has the time-per-pcle
  ////Convert to 1/t_i
  //CostPerPcle = 1.0/CostPerPcle;
  //MPI_Gather(&CostPerPcle,1,MPI_DOUBLE,costs,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
  ////Work out how many particles on each node
  //if(Task==0)
  //{
  //  sum=0;
  //  for(i=0;i<NTask;i++)
  //  {
  //    sum+=costs[i];
  //  }
  //  for(i=0;i<NTask;i++)
  //  {
  //    //The new number of particles on node i
  //    costs[i] = (costs[i]*NPartWorld[GAS])/sum;
  //  }
  //  nparts = malloc(NTask * sizeof(int));
  //  offsets = malloc(NTask * sizeof(int));
  //  MPI_Gather(&NPart[GAS],1,MPI_INT,
  //      nparts,1,MPI_INT,
  //      0,MPI_COMM_WORLD);
  //  //Allocate memory for sorting
  //  allR = malloc(NPartWorld[GAS]*sizeof(double));
  //  all = malloc(NPartWorld[GAS]*sizeof(struct balancer));
  //  //Where to store things in the allR array?
  //  for(i=1,offsets[0]=0;i<NTask;i++)
  //    offsets[i]=offsets[i-1]+nparts[i-1];
  //  //Gather R
  //  MPI_Gatherv(&R[0],NPart[GAS],MPI_DOUBLE,
  //      allR,nparts,offsets,MPI_DOUBLE,
  //      0,MPI_COMM_WORLD);
  //  //Stick it in array
  //  for(i=0;i<NPartWorld[GAS];i++)
  //  {
  //    all[i].R = allR[i];
  //  }
  //  //Gather costs
  //  MPI_Gatherv(&glob_costs[0],NPart[GAS],MPI_DOUBLE,
  //      allR,nparts,offsets,MPI_DOUBLE,
  //      0,MPI_COMM_WORLD);
  //  //Stick them in array
  //  for(i=0;i<NPartWorld[GAS];i++)
  //  {
  //    all[i].cost = allR[i];
  //  }
  //  //Sort the array
  //  qsort(all,NPartWorld[GAS],sizeof(struct balancer),cost_sort);
  //  //Find where to split each processor
  //  tot_cost=0;
  //  for(i=0;i<NPartWorld[GAS];i++)
  //  {
  //    tot_cost += all[i].cost;
  //  }
  //  tot_cost /= NTask;
  //  bounds[0]=all[0].R;
  //  cut=1;
  //  sum=0;
  //  for(i=0;i<NPartWorld[GAS];i++)
  //  {
  //    sum += all[i].cost;
  //    if(sum > tot_cost)
  //    {
  //      bounds[cut]=all[i].R;
  //      printf("Domain wall %d is %g.\n",cut,all[i].R);
  //      cut++;
  //      sum=0;
  //    }
  //  }
  //  bounds[NTask]=all[NPartWorld[GAS]-1].R;
  //  //qsort(allR,NPartWorld[GAS],sizeof(double),dsort);
  //  //Find ideal split points
  //  //bounds[0]=allR[0];
  //  //cut=0;
  //  //for(i=0;i<NTask;i++)
  //  //{
  //  //  //ntarget[i]=NPartWorld[GAS]/NTask;
  //  //  //if(i<NPartWorld[GAS] % NTask)
  //  //  //  ntarget[i]++;
  //  //  ntarget[i]=(int) costs[i];
  //  //  printf("Processor %d Will have %d pcles.\n",i,ntarget[i]);
  //  //  cut+=ntarget[i];
  //  //  bounds[i+1]=allR[cut-1];
  //  //}
  //  //bounds[NTask]=allR[NPartWorld[GAS]-1];
  //  //for(i=0;i<=NTask;i++)
  //  //  printf("Ideal boundaries are %g.\n",bounds[i]);
  //  //Translate them into tree wall splits
  //  //for(i=0;i<NTask;i++)
  //  //{
  //  //  low=allR[0];
  //  //  high=allR[NPartWorld[GAS]-1];
  //  //  rsplit=high;
  //  //  //Are we already close enough?
  //  //  while((fabs(rsplit-bounds[i+1])/bounds[i+1])>Params.domain_delta)
  //  //  {
  //  //    //Split the domain
  //  //    rsplit=sqrt(.5*(low*low+high*high));
  //  //    //Are we too high?
  //  //    if(rsplit>bounds[i+1])
  //  //    {
  //  //      //Open lower half
  //  //      high=rsplit;
  //  //    }
  //  //    else
  //  //    {
  //  //      //open upper half
  //  //      low=rsplit;
  //  //    }
  //  //  }
  //  //  bounds[i+1]=rsplit;
  //  //}
  //  //Send them back to everyone
  //  MPI_Bcast(bounds,NTask+1,MPI_DOUBLE,0,MPI_COMM_WORLD);
  //  //Free memory
  //  free(nparts);
  //  free(allR);
  //  free(offsets);
  //}
  //else
  //{
  //  MPI_Gather(&NPart[GAS],1,MPI_INT,
  //      NULL,0,MPI_INT,
  //      0,MPI_COMM_WORLD);
  //  MPI_Gatherv(&R[0],NPart[GAS],MPI_DOUBLE,
  //      NULL,NULL,NULL,MPI_DOUBLE,
  //      0,MPI_COMM_WORLD);
  //  MPI_Gatherv(&glob_costs[0],NPart[GAS],MPI_DOUBLE,
  //      NULL,NULL,NULL,MPI_DOUBLE,
  //      0,MPI_COMM_WORLD);
  //  //Receive the boundaries
  //  MPI_Bcast(bounds,NTask+1,MPI_DOUBLE,0,MPI_COMM_WORLD);
  //}
  ////Store the domain boundaries
  //for(i=0;i<=NTask;i++)
  //{
  //  DomainWalls[i]=bounds[i];
  //}
  ////Add a little extra to the outer domain (as upper boundaries are not inclusive)
  //DomainWalls[NTask] *= 1.01;
  //free(bounds);
  //free(ntarget);
  //CostPerPcle=0;
}

//Move particles around so they respect domainwalls
void exchange_particles(void)
{
  int i,j,k,nsend_tot,free_loc;
  int senders_start,nleft,max;
  int send,recv,avail_cnt;
  int direct,fit;
  int nsend[NTask];
  int nsend_glob[NTask];
  int nsend_all[NTask*NTask];
  int senders_offsets[NTask];
  int free_mem[NTask];
  int free_buff[NTask];
  int avail[NTask];
  //Allocate memory for all the different things we need
  pprintf("Currently have %d particles locally.  My domain walls are %g and %g.\n",NPart[GAS],DomainWalls[Task],DomainWalls[Task+1]);
  //Calculate how many we need to send...
  //Sort them by R
  //NOTE THIS DESTROYS THE TREE!!!
  qsort(G,NPart[GAS],sizeof(struct gas),gsort_radius);
  for(i=0;i<NTask;i++)
    nsend[i]=0;
  j=0;
  for(i=0;i<NPart[GAS];i++)
  {
    //If we breach the outer domain wall, still want to send it to 
    //the last processor
    while(j<(NTask-1) && G[i].R >= DomainWalls[j+1])
    {
      j++;
      //Print those around the send boundary
      //printf("[%d] Around domain boundary %d G[%d].R=%g.\n",Task,j,imax(i-2,0),G[imax(i-2,0)].R);
      //printf("[%d] Around domain boundary %d G[%d].R=%g.\n",Task,j,imax(i-1,0),G[imax(i-1,0)].R);
      //printf("[%d] Around domain boundary %d G[%d].R=%g.\n",Task,j,imax(i-0,0),G[imax(i-0,0)].R);
      //printf("[%d] Around domain boundary %d G[%d].R=%g.\n",Task,j,imin(i+1,NPart[GAS]-1),G[imin(i+1,NPart[GAS]-1)].R);
      //printf("[%d] Around domain boundary %d G[%d].R=%g.\n",Task,j,imin(i+2,NPart[GAS]-1),G[imax(i+2,NPart[GAS]-1)].R);
    }
    nsend[j]++;
  }
  //Check if this is going to be possible...
  MPI_Allreduce(nsend,nsend_glob,NTask,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
  //printf("[%d] We'll have %d particles here at the end.\n",Task,nsend_glob[Task]);
  //If they were evenly distributed, how many would we have?
  j=0;
  for(i=0;i<NTask;i++)
    j+=nsend_glob[i];
  j=j/NTask;
  for(i=0;i<NTask;i++)
  {
    //If we're out of space or if we would have an extreme outlier
    if(nsend_glob[i]>MaxGas || nsend_glob[i]<(j/2) || nsend_glob[i]>(j*2))
    {
      pprintf("Have space for %d pcles on %d, average pcles per process %d, but trying to send %d to it! Launching full decomposition.\n",MaxGas,i,j,nsend_glob[i]);
      domain_decomp();
      exchange_particles();
      return;
    }
  }
  //Obviously no need to send to ourself...
  nsend[Task]=0;
  //What do we need to send where?
  //for(i=0;i<NTask;i++)
  //  printf("[%d] Need to send %d particles to %d.\n",Task,nsend[i],i);
  //Arrange particles so that local ones are first, then sorted
  //by destination in ascending order
  nsend_tot=0;
  for(i=0;i<NTask;i++)
    nsend_tot += nsend[i];
  free_loc = MaxGas - NPart[GAS];
  //Given they're already sorted by R, this could be done more efficiently...
  qsort(G,NPart[GAS],sizeof(struct gas),arrange_for_comm);
  //for(i=0;i<NPart[GAS]-nsend_tot;i++)
  //{
  //  if(G[i].R>=DomainWalls[Task+1] || G[i].R<DomainWalls[Task])
  //  {
  //    printf("[%d] Particle G[%d].R = %g.\n",Task,imax(0,i-2),G[imax(0,i-2)].R);
  //    printf("[%d] Particle G[%d].R = %g.\n",Task,imax(0,i-1),G[imax(0,i-1)].R);
  //    printf("[%d] Particle G[%d].R = %g is out of range [%g,%g) nsend_tot=%d.\n",Task,i,G[i].R,DomainWalls[Task],DomainWalls[Task+1],nsend_tot);
  //    printf("[%d] Particle G[%d].R = %g.\n",Task,imin(NPart[GAS]-nsend_tot-1,i+1),G[imin(NPart[GAS]-nsend_tot-1,i+1)].R);
  //    printf("[%d] Particle G[%d].R = %g.\n",Task,imin(NPart[GAS]-nsend_tot-1,i+2),G[imin(NPart[GAS]-nsend_tot-1,i+2)].R);
  //    kill(43);
  //  }
  //}
  //if(Task==0)
  //{
  //  for(i=0;i<nsend_tot;i++)
  //    printf("[%d] G[%d].R=%g.\n",Task,NPart[GAS]-nsend_tot+i,G[NPart[GAS]-nsend_tot+i].R);
  //}
  //printf("[%d] Moving mem from %d to %d size %d.\n",Task,NPart[GAS]-nsend_tot,NPart[GAS]-nsend_tot+free_loc,nsend_tot);
  //Move any free space to immediately after the end of the local block
  memmove(&G[NPart[GAS]-nsend_tot+free_loc],&G[NPart[GAS]-nsend_tot],nsend_tot*sizeof(struct gas));
  //Where do the particles to send start
  senders_start = NPart[GAS]-nsend_tot+free_loc;
  //What offset do we need to reach them?
  senders_offsets[0]=0;
  for(i=1;i<NTask;i++)
  {
    senders_offsets[i] = senders_offsets[i-1]+nsend[i-1];
  }
  //And how much free space we have everywhere
  MPI_Allgather(&free_loc,1,MPI_INT,free_mem,1,MPI_INT,MPI_COMM_WORLD);
  //Get the big array of things to send locally
  MPI_Allgather(nsend,NTask,MPI_INT,nsend_all,NTask,MPI_INT,MPI_COMM_WORLD);
  //How many sends to we have left
  nleft=0;
  for(i=0;i<NTask*NTask;i++)
  {
    if(nsend_all[i])
      nleft++;
  }
  //This is needed for communication because of the way I've written it
  NPart[GAS] -= nsend_tot;
  //printf("[%d] We're claiming %d truely local.\n",Task,NPart[GAS]);
  //Will this work at all?
  //for(i=0;i<NTask;i++)
  //{
  //  max=0;
  //  for(j=0;j<NTask;j++)
  //  {
  //    //How many will process j try and send to i?
  //    max+=nsend_all[j*NTask+i];
  //  }
  //  //This decomposition will try and send more than are available!
  //  if(max>MaxGas-NPart[GAS])
  //  {
  //    printf("[%d] Have space for %d pcles, but trying to send %d to it!.\n",Task,MaxGas,NPart[GAS]+max);
  //    kill(1221);
  //  }
  //}
  //All buffer space initially available on all processors
  for(i=0;i<NTask;i++)
    free_buff[i]=BufferSizeGas;
  //Test if what we think is local, is local
  //for(i=0;i<NPart[GAS];i++)
  //{
  //  if(G[i].R>=DomainWalls[Task+1] || G[i].R<DomainWalls[Task])
  //  {
  //    printf("[%d] Particle G[%d].R = %g.\n",Task,imax(0,i-2),G[imax(0,i-2)].R);
  //    printf("[%d] Particle G[%d].R = %g.\n",Task,imax(0,i-1),G[imax(0,i-1)].R);
  //    printf("[%d] Particle G[%d].R = %g is out of range [%g,%g).\n",Task,i,G[i].R,DomainWalls[Task],DomainWalls[Task+1]);
  //    printf("[%d] Particle G[%d].R = %g.\n",Task,imin(NPart[GAS]-1,i+1),G[imin(NPart[GAS]-1,i+1)].R);
  //    printf("[%d] Particle G[%d].R = %g.\n",Task,imin(NPart[GAS]-1,i+2),G[imin(NPart[GAS]-1,i+2)].R);
  //    kill(343);
  //  }
  //}
  //Test the memory to send to remote is also setup as it should be
  //toff = senders_start;
  //recv=0;
  //while(nsend[recv]==0 && recv<NTask)
  //  recv++;
  //printf("[%d] Claiming we need to send %d particles.\n",Task,nsend_tot);
  //for(i=0;i<nsend_tot;i++)
  //{
  //  j=toff+i;
  //  while(i==senders_offsets[recv]+nsend[recv])
  //    recv++;
  //  if(G[j].R<DomainWalls[recv] || G[j].R>DomainWalls[recv+1])
  //  {
  //    printf("[%d] Particle G[%d].R = %g.\n",Task,imax(j-2,0),G[imax(j-2,0)].R);
  //    printf("[%d] Particle G[%d].R = %g.\n",Task,imax(j-1,0),G[imax(j-1,0)].R);
  //    printf("[%d] Particle G[%d].R = %g is out of range [%g,%g) (offset = %d, block size = %d).\n",Task,j,G[j].R,DomainWalls[recv],DomainWalls[recv+1],i,nsend[recv]);
  //    printf("[%d] Particle G[%d].R = %g.\n",Task,imin(j+1,toff+nsend_tot-1),G[imin(j+1,toff+nsend_tot-1)].R);
  //    printf("[%d] Particle G[%d].R = %g.\n",Task,imin(j+2,toff+nsend_tot-1),G[imin(j+2,toff+nsend_tot-1)].R);
  //    kill(13431);
  //  }
  //}

  //Do the sends we have to do...
  //printf("[%d] Decomposition possible, beginning exchange.\n",Task);
  //printf("[%d] Comm buffer has space for %d pcles.\n",Task,BufferSizeGas);
  send=0;
  recv=0;
  //All processors are available to begin with
  avail_cnt=NTask;
  for(i=0;i<NTask;i++)
    avail[i]=1;
  while(nleft)
  {
    //Do the biggest send that isn't yet done and can still be done
    //try and avoid having the same processor involved in a point-to-point
    //communication twice before all others have had a go (this should try
    //and ensure less wasted time where processors idle waiting for others
    //to finish sending/receiving)
    max=0;
    //Lets us relax the availability requirement if necessary
    do
    {
      direct=1;
      //Loop over the sender
      for(i=0;i<NTask;i++)
      {
        if(!avail[i])
          continue;
        //Loop over receiver
        for(j=0;j<NTask;j++)
        {
          if(!avail[j])
            continue;
          k=nsend_all[i*NTask+j];
          //Only allowed if there's space to do it on receiver (j)
          if(k>max && k<=free_mem[j])
          {
            send = i;
            recv = j;
            max = k;
          }
        }
      }
      //No solution yet, try and do indirect send via comm buffer
      if(max==0)
      {
        //Flag to say we're sending a possibly incomplete payload to the comm buffer
        direct=0;
        //Can't just send directly, need to split up payload into chunks
        //It's better to do this than have the processors sit idle while
        //others are busy sending
        //Loop over sender again
        for(i=0;i<NTask;i++)
        {
          if(!avail[i])
            continue;
          for(j=0;j<NTask;j++)
          {
            if(!avail[j])
              continue;
            k=nsend_all[i*NTask+j];
            fit =imin(k,free_buff[j]);
            if(fit>max)
            {
              send=i;
              recv=j;
              max=fit;
            }
          }
        }
      }
      //At this point, we've either got a valid point-to-point
      //communication or need to relax the restriction on availability
      //to achieve one
      if(max==0)
      {
        avail_cnt=NTask;
        for(i=0;i<NTask;i++)
          avail[i]=1;
      }
    }while(max==0);
    //Finally have a valid comm solution, mark these processors as 
    //unavailable and proceed
    //No more point-to-point solutions possible with new availability
    //so reset.  <4 rather than 2 because we haven't done -2 for this
    //round yet
    if(avail_cnt<4)
    {
      avail_cnt=NTask;
      for(i=0;i<NTask;i++)
        avail[i]=1;
    }
    //Mark current ones unavailable and update counter
    avail[send]=0;
    avail[recv]=0;
    avail_cnt-=2;
    //if(direct)
    //{
    //  printf("[%d] Have to send %d particles from %d to comm buffer on %d.\n",Task,max,send,recv);
    //}
    //else
    //{
    //  printf("[%d] Easy send possible, sending %d particles from %d to %d.\n",Task,max,send,recv);
    //}
    //Do the actual point to point communication
    //The send operation is the same regardless
    if(Task==send)
    {
      //Are we really sending the right thing?
      //toff = senders_start+senders_offsets[recv];
      //for(i=0;i<max;i++)
      //{
      //  j=toff+i;
      //  if(G[j].R<DomainWalls[recv] || G[j].R>DomainWalls[recv+1])
      //  {
      //    printf("[%d] Particle G[%d].R = %g.\n",Task,imax(j-2,0),G[imax(j-2,0)].R);
      //    printf("[%d] Particle G[%d].R = %g.\n",Task,imax(j-1,0),G[imax(j-1,0)].R);
      //    printf("[%d] Particle G[%d].R = %g is out of range [%g,%g) off=%d,max=%d.\n",Task,j,G[j].R,DomainWalls[recv],DomainWalls[recv+1],i,max);
      //    printf("[%d] Particle G[%d].R = %g.\n",Task,imin(j+1,toff+max-1),G[imin(j+1,toff+max-1)].R);
      //    printf("[%d] Particle G[%d].R = %g.\n",Task,imin(j+2,toff+max-1),G[imin(j+2,toff+max-1)].R);
      //    kill(1343);
      //  }
      //}
      //Send to receiver
      MPI_Send(&G[senders_start+senders_offsets[recv]],max*sizeof(struct gas),MPI_BYTE,recv,nleft,MPI_COMM_WORLD);
      //Move memory up so that the free memory is one continuous block
      //Only have to do it if we didn't move from the start of the senders block
      if(senders_offsets[recv])
      {
        memmove(&G[senders_start+max],&G[senders_start],senders_offsets[recv]*sizeof(struct gas));
      }
      //Update where the free memory is
      senders_start += max;
      //And any offsets that need correcting
      for(i=recv+1;i<NTask;i++)
        senders_offsets[i] -= max;
    }
    //The receive operation has to be slightly different obviously...
    if(direct)
    {
      if(Task==recv)
      {
        MPI_Recv(&G[NPart[GAS]],max*sizeof(struct gas),MPI_BYTE,send,nleft,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
        NPart[GAS] += max;
      }
      free_mem[recv] -= max;
    }
    else
    {
      if(Task==recv)
      {
        MPI_Recv(&GBuffer[BufferSizeGas-free_buff[recv]],max*sizeof(struct gas),MPI_BYTE,send,nleft,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
      }
      free_buff[recv] -= max;
    }
    //These things are still common though
    free_mem[send] += max;
    nsend_all[send*NTask+recv] -= max;
    //If we finished it this time...
    if(nsend_all[send*NTask+recv]==0)
      nleft--;
    //If there's anything kicking about in the comm buffer,
    //make sure you transfer as much as possible to the free space available
    for(i=0;i<NTask;i++)
    {
      if(free_buff[i]!=BufferSizeGas)
      {
        //How much can I move?
        max=imin(BufferSizeGas-free_buff[i],free_mem[i]);
        //Do the actual moving
        if(Task==i)
        {
          //printf("[%d] Shifting %d particles from comm buffer to store.\n",Task,max);
          memmove(&G[NPart[GAS]],&GBuffer[BufferSizeGas-free_buff[i]-max],max*sizeof(struct gas));
          NPart[GAS] += max;
        }
        //Update the counters (done globally)
        free_mem[i] -= max;
        free_buff[i] += max;
      }
    }
    //printf("\n\n[%d] At end of round %d we have:\n",Task,cnt);
    //for(i=0;i<NTask*NTask;i++)
    //{
    //  printf("[%d] send_all[%d] = %d.\n",Task,i,nsend_all[i]);
    //}
    //for(i=0;i<NTask;i++)
    //{
    //  printf("[%d] free_mem[%d] = %d.\n",Task,i,free_mem[i]);
    //  printf("[%d] free_buff[%d] = %d.\n",Task,i,free_buff[i]);
    //}
    //printf("[%d] Nlocal = %d.\n",Task,NPart[GAS]);
    //Test if what we think is local, is local
    //for(i=0;i<NPart[GAS];i++)
    //{
    //  if(G[i].R>=DomainWalls[Task+1] || G[i].R<DomainWalls[Task])
    //  {
    //    printf("[%d] Particle G[%d].R = %g.\n",Task,i-2,G[i-2].R);
    //    printf("[%d] Particle G[%d].R = %g.\n",Task,i-1,G[i-1].R);
    //    printf("[%d] Particle G[%d].R = %g is out of range [%g,%g).\n",Task,i,G[i].R,DomainWalls[Task],DomainWalls[Task+1]);
    //    printf("[%d] Particle G[%d].R = %g.\n",Task,i+1,G[i+1].R);
    //    printf("[%d] Particle G[%d].R = %g.\n",Task,i+2,G[i+2].R);
    //    kill(343);
    //  }
    //}
    //Test the memory to send to remote is also setup as it should be
    //toff = senders_start;
    //recv=0;
    //while(nsend_all[NTask*Task+recv]==0 && recv<NTask)
    //  recv++;
    //nsend_tot=0;
    //for(i=0;i<NTask;i++)
    //  nsend_tot+=nsend_all[NTask*Task+i];
    //for(i=0;i<nsend_tot;i++)
    //{
    //  j=toff+i;
    //  while(i==senders_offsets[recv]+nsend_all[NTask*Task+recv])
    //    recv++;
    //  if(G[j].R<DomainWalls[recv] || G[j].R>DomainWalls[recv+1])
    //  {
    //    printf("[%d] Particle G[%d].R = %g.\n",Task,imax(j-2,0),G[imax(j-2,0)].R);
    //    printf("[%d] Particle G[%d].R = %g.\n",Task,imax(j-1,0),G[imax(j-1,0)].R);
    //    printf("[%d] Particle G[%d].R = %g is out of range [%g,%g) (offset = %d, block size = %d).\n",Task,j,G[j].R,DomainWalls[recv],DomainWalls[recv+1],i,nsend_all[NTask*Task+recv]);
    //    printf("[%d] Particle G[%d].R = %g.\n",Task,imin(j+1,toff+nsend_tot-1),G[imin(j+1,toff+nsend_tot-1)].R);
    //    printf("[%d] Particle G[%d].R = %g.\n",Task,imin(j+2,toff+nsend_tot-1),G[imin(j+2,toff+nsend_tot-1)].R);
    //    kill(111);
    //  }
    //}
    //cnt++;
  }
  //Make sure all the particle counters are correctly set (as far
  //as we can given the information available to us here)
  update_counters();
  //printf("[%d] I have %d particles here now.\n",Task,NPart[GAS]);
  //MPI_Allgather(&NPart[GAS],1,MPI_INT,NGas,1,MPI_INT,MPI_COMM_WORLD);
  //freedom!
}

void cast_shadows(void)
{
  int i;
  for(i=0;i<NTask;i++)
  {
    if(Shadows[i].G)
    {
      MPI_Bcast(Shadows[i].G,NGas[i]*sizeof(struct gas),MPI_BYTE,Shadows[i].owner_rank_in_comm,Shadows[i].comm);
    }
  }
}

//Prototype for broadcasting using the communication buffer
void cast_pressure_shadows(void)
{
  int i,j;
  int left,offset,nsend;
  int floats_per_entry=1;
  for(i=0;i<NTask;i++)
  {
    if(Shadows[i].G)
    {
      left=NGas[i];
      offset=0;
      //Loop until all of the particles have been sent
      while(left)
      {
        nsend=imin(left,BufferSizeFloats/floats_per_entry);
        if(i==Task)
        {
          for(j=0;j<nsend;j++)
          {
            FBuffer[j*floats_per_entry+0]=G[offset+j].pressure;
          }
        }
        //Send them out
        MPI_Bcast(FBuffer,nsend*floats_per_entry,MFLOAT,Shadows[i].owner_rank_in_comm,Shadows[i].comm);
        //Store them appropriately
        if(i!=Task)
        {
          for(j=0;j<nsend;j++)
          {
            Shadows[i].G[offset+j].pressure = FBuffer[j*floats_per_entry+0];
          }
        }
        offset += nsend;
        left -= nsend;
      }
    }
  }
}
 
void cast_density_shadows(void)
{
  int i,j;
  int left,offset,nsend;
  int floats_per_entry=4;
  for(i=0;i<NTask;i++)
  {
    if(Shadows[i].G)
    {
      left=NGas[i];
      offset=0;
      //Loop until all of the particles have been sent
      while(left)
      {
        nsend=imin(left,BufferSizeFloats/floats_per_entry);
        if(i==Task)
        {
          for(j=0;j<nsend;j++)
          {
            FBuffer[j*floats_per_entry+0]=G[offset+j].density;
            FBuffer[j*floats_per_entry+1]=G[offset+j].dhsml;
            FBuffer[j*floats_per_entry+2]=G[offset+j].hsml;
            FBuffer[j*floats_per_entry+3]=G[offset+j].zeta;
          }
        }
        //Send them out
        MPI_Bcast(FBuffer,nsend*floats_per_entry,MFLOAT,Shadows[i].owner_rank_in_comm,Shadows[i].comm);
        //Store them appropriately
        if(i!=Task)
        {
          for(j=0;j<nsend;j++)
          {
            Shadows[i].G[offset+j].density = FBuffer[j*floats_per_entry+0];
            Shadows[i].G[offset+j].dhsml = FBuffer[j*floats_per_entry+1];
            Shadows[i].G[offset+j].hsml = FBuffer[j*floats_per_entry+2];
            Shadows[i].G[offset+j].zeta = FBuffer[j*floats_per_entry+3];
          }
        }
        offset += nsend;
        left -= nsend;
      }
    }
  }
}

void cast_hydro_shadows(void)
{
  int i,j;
  int left,offset,nsend;
  int floats_per_entry=1;
  for(i=0;i<NTask;i++)
  {
    if(Shadows[i].G)
    {
      left=NGas[i];
      offset=0;
      //Loop until all of the particles have been sent
      while(left)
      {
        nsend=imin(left,BufferSizeFloats/floats_per_entry);
        if(i==Task)
        {
          for(j=0;j<nsend;j++)
          {
            FBuffer[j*floats_per_entry+0]=G[offset+j].divv;
          }
        }
        //Send them out
        MPI_Bcast(FBuffer,nsend*floats_per_entry,MFLOAT,Shadows[i].owner_rank_in_comm,Shadows[i].comm);
        //Store them appropriately
        if(i!=Task)
        {
          for(j=0;j<nsend;j++)
          {
            Shadows[i].G[offset+j].divv = FBuffer[j*floats_per_entry+0];
          }
        }
        offset += nsend;
        left -= nsend;
      }
    }
  }
}

void cast_kicked_shadows(void)
{
  int i,j;
  int left,offset,nsend;
  int floats_per_entry=4;
  for(i=0;i<NTask;i++)
  {
    if(Shadows[i].G)
    {
      left=NGas[i];
      offset=0;
      //Loop until all of the particles have been sent
      while(left)
      {
        nsend=imin(left,BufferSizeFloats/floats_per_entry);
        if(i==Task)
        {
          for(j=0;j<nsend;j++)
          {
            FBuffer[j*floats_per_entry+0]=G[offset+j].vx;
            FBuffer[j*floats_per_entry+1]=G[offset+j].vy;
            FBuffer[j*floats_per_entry+2]=G[offset+j].vz;
            FBuffer[j*floats_per_entry+3]=G[offset+j].K;
          }
        }
        //Send them out
        MPI_Bcast(FBuffer,nsend*floats_per_entry,MFLOAT,Shadows[i].owner_rank_in_comm,Shadows[i].comm);
        //Store them appropriately
        if(i!=Task)
        {
          for(j=0;j<nsend;j++)
          {
            Shadows[i].G[offset+j].vx = FBuffer[j*floats_per_entry+0];
            Shadows[i].G[offset+j].vy = FBuffer[j*floats_per_entry+1];
            Shadows[i].G[offset+j].vz = FBuffer[j*floats_per_entry+2];
            Shadows[i].G[offset+j].K = FBuffer[j*floats_per_entry+3];
          }
        }
        offset += nsend;
        left -= nsend;
      }
    }
  }

  //Now set the predicted quantities to the same
  for(i=0;i<NTask;i++)
  {
    if(Shadows[i].G)
    {
      for(j=0;j<NGas[i];j++)
      {
        Shadows[i].G[j].vpredx = Shadows[i].G[j].vx;
        Shadows[i].G[j].vpredy = Shadows[i].G[j].vy;
        Shadows[i].G[j].vpredz = Shadows[i].G[j].vz;
        Shadows[i].G[j].Kpred = Shadows[i].G[j].K;
      }
    }
  }
}


void gather_costs(void)
{
  int i,j;
  int buffSize;
  int nsend,left,offset;
  int floats_per_entry=1;
  FLOAT *halfBuff;
  //How much space for sending
  buffSize=BufferSizeFloats/2;
  //Where to start receiving
  halfBuff = FBuffer+buffSize;
  for(i=0;i<NTask;i++)
  {
    if(Shadows[i].G)
    {
      left=NGas[i];
      offset=0;
      while(left)
      {
        nsend=imin(left,buffSize/floats_per_entry);
        for(j=0;j<nsend;j++)
        {
          FBuffer[j*floats_per_entry+0] = Shadows[i].G[offset+j].cost;
        }
        MPI_Reduce(FBuffer,halfBuff,nsend*floats_per_entry,MFLOAT,MPI_SUM,Shadows[i].owner_rank_in_comm,Shadows[i].comm);
        if(i==Task)
        {
          for(j=0;j<nsend;j++)
          {
            G[offset+j].cost = halfBuff[j*floats_per_entry+0];
          }
        }
        offset += nsend;
        left -= nsend;
      }
    }
  }
  //Set all the shadow domain accelerations to zero
  for(i=0;i<NTask;i++)
  {
    if(Shadows[i].G && i!=Task)
    {
      for(j=0;j<NGas[i];j++)
      {
        Shadows[i].G[j].cost=0;
      }
    }
  }

}

//Prototype for a reduce operation using no extra memory
void gather_acceleration_shadows(void)
{
  int i,j;
  int buffSize;
  int nsend,left,offset;
  int floats_per_entry=3;
  FLOAT *halfBuff;
  //How much space for sending
  buffSize=BufferSizeFloats/2;
  //Where to start receiving
  halfBuff = FBuffer+buffSize;
  for(i=0;i<NTask;i++)
  {
    if(Shadows[i].G)
    {
      left=NGas[i];
      offset=0;
      while(left)
      {
        nsend=imin(left,buffSize/floats_per_entry);
        for(j=0;j<nsend;j++)
        {
          FBuffer[j*floats_per_entry+0] = Shadows[i].G[offset+j].ax;
          FBuffer[j*floats_per_entry+1] = Shadows[i].G[offset+j].ay;
          FBuffer[j*floats_per_entry+2] = Shadows[i].G[offset+j].az;
        }
        MPI_Reduce(FBuffer,halfBuff,nsend*floats_per_entry,MFLOAT,MPI_SUM,Shadows[i].owner_rank_in_comm,Shadows[i].comm);
        if(i==Task)
        {
          for(j=0;j<nsend;j++)
          {
            G[offset+j].ax = halfBuff[j*floats_per_entry+0];
            G[offset+j].ay = halfBuff[j*floats_per_entry+1];
            G[offset+j].az = halfBuff[j*floats_per_entry+2];
          }
        }
        offset += nsend;
        left -= nsend;
      }
    }
  }
  //Set all the shadow domain accelerations to zero
  for(i=0;i<NTask;i++)
  {
    if(Shadows[i].G && i!=Task)
    {
      for(j=0;j<NGas[i];j++)
      {
        Shadows[i].G[j].ax=0;
        Shadows[i].G[j].ay=0;
        Shadows[i].G[j].az=0;
      }
    }
  }
}

void gather_shadows_for_kick(void)
{
  int i,j;
  int buffSize;
  int nsend,left,offset;
  int floats_per_entry=4;
  FLOAT *halfBuff;
  //How much space for sending
  buffSize=BufferSizeFloats/2;
  //Where to start receiving
  halfBuff = FBuffer+buffSize;
  for(i=0;i<NTask;i++)
  {
    if(Shadows[i].G)
    {
      left=NGas[i];
      offset=0;
      while(left)
      {
        nsend=imin(left,buffSize/floats_per_entry);
        for(j=0;j<nsend;j++)
        {
          FBuffer[j*floats_per_entry+0] = Shadows[i].G[offset+j].ax;
          FBuffer[j*floats_per_entry+1] = Shadows[i].G[offset+j].ay;
          FBuffer[j*floats_per_entry+2] = Shadows[i].G[offset+j].az;
          FBuffer[j*floats_per_entry+3] = Shadows[i].G[offset+j].dK;
        }
        MPI_Reduce(FBuffer,halfBuff,nsend*floats_per_entry,MFLOAT,MPI_SUM,Shadows[i].owner_rank_in_comm,Shadows[i].comm);
        if(i==Task)
        {
          for(j=0;j<nsend;j++)
          {
            G[offset+j].ax = halfBuff[j*floats_per_entry+0];
            G[offset+j].ay = halfBuff[j*floats_per_entry+1];
            G[offset+j].az = halfBuff[j*floats_per_entry+2];
            G[offset+j].dK = halfBuff[j*floats_per_entry+3];
          }
        }
        offset += nsend;
        left -= nsend;
      }
    }
  }
  //Set all the shadow domain accelerations to zero
  for(i=0;i<NTask;i++)
  {
    if(Shadows[i].G && i!=Task)
    {
      for(j=0;j<NGas[i];j++)
      {
        Shadows[i].G[j].ax=0;
        Shadows[i].G[j].ay=0;
        Shadows[i].G[j].az=0;
        Shadows[i].G[j].dK=0;
      }
    }
  }
}

void update_counters(void)
{
  //Assume that NPart is correctly set at the start
  //Update NGas
  MPI_Allgather(&NPart[GAS],1,MPI_INT,NGas,1,MPI_INT,MPI_COMM_WORLD);
  //Update npart 
  MPI_Allreduce(&NPart[GAS],&NPartWorld[GAS],1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
  //These ones are global, so npart ==local npart
  NPartWorld[SINK]=NPart[SINK];
}

void add_remove_particles(void)
{
  int i;
  int n_tot=0;
  double r;
  double c2;
#ifdef ACCRETE
  int j;
  double en;
  int n_accrete[NPart[SINK]];
  double new_com[NPart[SINK]][3];
  double new_vel[NPart[SINK]][3];
  //Set everything to zero
  for(i=0;i<NPart[SINK];i++)
  {
    n_accrete[i]=0;
    new_com[i][0]=new_com[i][1]=new_com[i][2]=0;
    new_vel[i][0]=new_vel[i][1]=new_vel[i][2]=0;
  }
  c2=Params.accretion_radius * Params.accretion_radius;
  for(i=0;i<NPart[GAS];i++)
  {
    //Check if any of them are too close to a sink
    for(j=0;j<NPart[SINK];j++)
    {
      //Distance from sink
      r=(G[i].x-S[j].x)*(G[i].x-S[j].x) + 
        (G[i].y-S[j].y)*(G[i].y-S[j].y) +
        (G[i].z-S[j].z)*(G[i].z-S[j].z);
      //Are we too close?
      if(r<=c2)
      {
        //Are we gravitationally bound in the frame of the sink?
        //Potential energy (assume accretion_radius>softening radius)
        r=sqrt(r);
        en = (-Params.G * S[j].m) / r;
        //kinetic energy
        en += .5*((G[i].vx-S[j].vx)*(G[i].vx-S[j].vx) + 
                  (G[i].vy-S[j].vy)*(G[i].vy-S[j].vy) +
                  (G[i].vz-S[j].vz)*(G[i].vz-S[j].vz));
        if(en<0)
        {
          pprintf("Particle %d is %g from sink %d.  Setting accretion.\n",i,r,j);
          //Set the id to negative to signal this is to be accreted
          G[i].id *= -1;
          //OK, now we can accrete it unless you want any other checks first
          n_tot++;
          //New mass for sink
          n_accrete[j]++;
          //New position for sink to keep COM unchanged
          new_com[j][0] += G[i].x;
          new_com[j][1] += G[i].y;
          new_com[j][2] += G[i].z;
          //New velocity to keep momentum unchanged
          new_vel[j][0] += G[i].vx;
          new_vel[j][1] += G[i].vy;
          new_vel[j][2] += G[i].vz;
          //Calculate the current angular momentum of involved particles if you want
          //to know how much was lost...
        }
      }
    }
  }
  pprintf("Found %d particles needing accretion.\n",n_tot);
  //Get the total accretion across all processors
  MPI_Allreduce(MPI_IN_PLACE,new_com,NPart[SINK]*3,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE,new_vel,NPart[SINK]*3,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE,n_accrete,NPart[SINK],MPI_INT,MPI_SUM,MPI_COMM_WORLD);
  //Now update the position, mass and velocity of sinks appropriately
  for(i=0;i<NPart[SINK];i++)
  {
    //Store the new mass temporarily
    r = S[i].m + n_accrete[i]*PcleMass;
    //Update the positions
    S[i].x = (S[i].m*S[i].x+PcleMass*new_com[i][0])/r;
    S[i].y = (S[i].m*S[i].y+PcleMass*new_com[i][1])/r;
    S[i].z = (S[i].m*S[i].z+PcleMass*new_com[i][2])/r;
    //Update the velocities
    S[i].vx = (S[i].m*S[i].vx+PcleMass*new_vel[i][0])/r;
    S[i].vy = (S[i].m*S[i].vy+PcleMass*new_vel[i][1])/r;
    S[i].vz = (S[i].m*S[i].vz+PcleMass*new_vel[i][2])/r;
    //Update the mass
    S[i].m = r;
  }
#endif
#ifdef ESCAPE
  c2=Params.escape_radius * Params.escape_radius;
  //We've just calculated the new Rs, which are the cylindrical distances from the COM
  //use it to check we're not too far out
  for(i=0;i<NPart[GAS];i++)
  {
    r=G[i].R*G[i].R+(G[i].z-COM[2])*(G[i].z-COM[2]);
    if(r>c2)
    {
      pprintf("Particle %d is distance %g from COM, removing.\n",i,sqrt(r));
      //Any extra checks should go here, otherwise remove it...
      G[i].id *= -1;
      n_tot++;
    }
  }
#endif
  //If there's anything to be removed, we will have the number (locally) and 
  //they will be marked by having negative ids
  if(n_tot)
  {
    //Sort them by ID, then destroy the negative ones
    qsort(G,NPart[GAS],sizeof(struct gas),sort_by_id);
    memmove(G,G+n_tot,(NPart[GAS]-n_tot)*sizeof(struct gas));
  }
  //Update all the counters
  NPart[GAS] -= n_tot;
  update_counters();
  //MPI_Allreduce(&NPart[GAS],&NPartWorld[GAS],1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
  //MPI_Allgather(&NPart[GAS],1,MPI_INT,NGas,1,MPI_INT,MPI_COMM_WORLD);
  //for(i=0;i<NPart[GAS];i++)
  //{
  //  printf("[%d] After accretion G[%d].R = %g.\n",Task,i,G[i].R);
  //}
  //printf("[%d] After accretion we have %d locally.\n",Task,NPart[GAS]);
  //for(i=0;i<NTask;i++)
  //{
  //  printf("[%d] NGas[%d]=%d.\n",Task,i,NGas[i]);
  //}
  //Removal of particles finished, how about adding them?
}

int arrange_for_comm(const void* a,const void* b)
{
  double Ra,Rb;
  Ra = (*((struct gas*) a)).R;
  Rb = (*((struct gas*) b)).R;
  //Always go to front if it's within local domain
  if( Ra < DomainWalls[Task+1] && Ra >= DomainWalls[Task]) return -1;
  if( Rb < DomainWalls[Task+1] && Rb >= DomainWalls[Task]) return 1;
  //If neither in the local domain, just sort by R;
  return Ra-Rb;
}

int sort_by_id(const void* a,const void* b)
{
  return ((struct gas*) a)->id - ((struct gas*) b)->id;
}
