/*
 * =====================================================================================
 *
 *       Filename:  tree.c
 *
 *    Description:  Functions to do with constructing and walking of the tree.
 *
 *        Version:  0.6.3
 *        Created:  25/07/13 10:24:40
 *
 *         Author:  Matthew D. Young, my304@ast.cam.ac.uk
 *        Company:  Institute of Astronomy, Cambridge, UK
 *        License:  GNU General Public License
 *      Copyright:  Copyright (c) 2014, Matthew D. Young
 *
 * =====================================================================================
 */

#include "variables.h"
#include "functions.h"

//To avoid memory leaks, the tree has to be broken down carefully before
//a new one is built.
void destroy_tree(void)
{
  int i;
  for(i=0;i<NextFreeNode;i++)
  {
    //Deallocate any memory that may have been allocated
    if(Root[i].owners)
      free(Root[i].owners);
    if(Root[i].extra)
      free(Root[i].extra);
  }
}

//Calculate a bounding box that will cover simulation domain
void set_bounding_box(void)
{
  int i;
  double dmax[3],dmin[3];
  //Determine the bounding box
  dmax[0]=dmax[1]=0;
  dmin[0]=dmin[1]=DBL_MAX;
  for(i=0;i<NPart[GAS];i++)
  {
    if(G[i].R>dmax[0])
      dmax[0]=G[i].R;
    if(G[i].R<dmin[0])
      dmin[0]=G[i].R;
    if(G[i].z>dmax[1])
      dmax[1]=G[i].z;
    if(G[i].z<dmin[1])
      dmin[1]=G[i].z;
  }
  MPI_Allreduce(MPI_IN_PLACE,dmax,2,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
  MPI_Allreduce(MPI_IN_PLACE,dmin,2,MPI_DOUBLE,MPI_MIN,MPI_COMM_WORLD);
  //The expansion at outer edges is because cells are not inclusive of upper boundaries
  SimBox[0][0]=dmin[0];
  SimBox[0][1]=dmax[0]*1.01;
  SimBox[1][0]=0;
  SimBox[1][1]=PI2;
  SimBox[2][0]=dmin[1];
  SimBox[2][1]=dmax[1]*1.01;
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  build_tree
 *  Description:  Creates the tree for neighbour finding.  Should be run after domain
 *                decomposition.  Currently done in a way which is independent of the
 *                number of tasks.  That is, the structure of the tree will look the
 *                same regardless of the number of tasks, just the data it points to
 *                will differ.  Tree construction is a cylindrical oct-tree.
 * =====================================================================================
 */
void build_tree(void)
{
  int leaves[NTask];
  int displ[NTask];
  int nleaves=0;
  int i,j,k;
  int lvl,cnt;
  //Destroy any existing tree
  destroy_tree();
  //Set new boundary box
  set_bounding_box();

  //Construct the root node, everything will be build by splitting from here
  Root->lows[0] = SimBox[0][0];
  Root->lows[1] = SimBox[1][0];
  Root->lows[2] = SimBox[2][0];
  Root->highs[0] = SimBox[0][1];
  Root->highs[1] = SimBox[1][1];
  Root->highs[2] = SimBox[2][1];
  Root->com[0]=Root->com[1]=Root->com[2]=0;
  Root->hmax=Root->rmax=0;
  Root->count = NPart[GAS];
  Root->lvl_lid = 0;
  Root->mob = 0;
  Root->n_daughters = 0;
  Root->mother = -1;
  Root->daughter = -1;
  Root->sister = -1;
  Root->owner = Task;
  Root->owners = NULL;
  Root->extra = NULL;
  NextFreeNode = 1;
  LeafCtr=0;

  /* Initial local build */

  //pprintf("Done initial setup.\n");
  //Build the tree using only local particles
  build_base(Root);
  //pprintf("Built tree locally.  Created %d nodes.\n",NextFreeNode);
  //LeafCtr=0;
  //print_tree(Root);
  //pprintf("Walked %d nodes out of %d.\n",LeafCtr,NextFreeNode);
  //kill(9394);


  /* Expand local nodes for consistency with remotes */

  //How many non-empty leaves on other processors?
  MPI_Allgather(&LeafCtr,1,MPI_INT,leaves,1,MPI_INT,MPI_COMM_WORLD);
  //Work out displacements to merge the table
  nleaves = leaves[0];
  leaves[0] *= sizeof(TREE_REF);
  for(i=1,displ[0]=0;i<NTask;i++)
  {
    displ[i]=displ[i-1]+leaves[i-1];
    nleaves += leaves[i];
    leaves[i] *= sizeof(TREE_REF);
  }
  //Move the local memory to the end so MPI doesn't cry
  if((nleaves+LeafCtr)>=BufferSizeTRs)
  {
    pprintf("Insufficient memory to build leaf table in buffer.\n");
    kill(ERROR_MEM);
  }
  //Create a copy of the local part of the table for transport
  memmove(TRBuffer+nleaves,TRBuffer,leaves[Task]);
  //Do the transport
  MPI_Allgatherv(TRBuffer+nleaves,leaves[Task],MPI_BYTE,TRBuffer,leaves,displ,MPI_BYTE,MPI_COMM_WORLD);
  //Sort the whole thing 
  qsort(TRBuffer, nleaves, sizeof(TREE_REF), sortLeafTable);
  //What is the deepest lvl?
  TreeDepth=TRBuffer[nleaves-1] / MaxLID;
  //pprintf("[%d] Max level is %d.\n",TreeDepth);
  //Create array of offsets
  LvlOffset = malloc((TreeDepth+2)*sizeof(int));
  //Populate them
  lvl=0;
  cnt=0;
  LvlOffset[0]=0;
  for(i=0;i<nleaves;i++)
  {
    k=(int) (TRBuffer[i] / MaxLID);
    if(lvl!=k)
    {
      //Set all the offsets
      for(j=lvl+1;j<=k;j++)
      {
        LvlOffset[j] = cnt;
      }
      lvl = k;
    }
    cnt++;
  }
  LvlOffset[TreeDepth+1] = cnt;
  //Point to the new end of the table
  LeafCtr=nleaves;
  //Walk the tree opening up any node that needs it...
  //printf("[%d] Starting tree refinement.\n",Task);
  refineTree(Root);
  //printf("[%d] After tree refinement I have %d nodes.\n",Task,NextFreeNode);
  //LeafCtr=0;
  //print_tree(Root);
  //printf("[%d] Walked %d nodes out of %d.\n",Task,LeafCtr,NextFreeNode);
  //kill(9394);

  /* Merge trees */

  //Now we have all the nodes needed for local particles, except anything
  //that might be pushed over the splitting boundary in a merge
  //Transport them all locally...
  MPI_Allgather(&NextFreeNode,1,MPI_INT,leaves,1,MPI_INT,MPI_COMM_WORLD);
  //Work out displacements to merge the table
  nleaves = leaves[0];
  leaves[0] *= sizeof(struct node);
  for(i=1,displ[0]=0;i<NTask;i++)
  {
    displ[i]=displ[i-1]+leaves[i-1];
    nleaves += leaves[i];
    leaves[i] *= sizeof(struct node);
  }
  //Create a copy of the local part of the table for transport
  if(NextFreeNode*sizeof(struct node) > BufferSizeBytes)
  {
    pprintf("Insufficient memory in buffer to transport tree nodes.\n");
    kill(ERROR_MEM);
  }
  memmove(Buffer,Root,sizeof(struct node)*NextFreeNode);
  //Do the transport
  MPI_Allgatherv(Buffer,leaves[Task],MPI_BYTE,
      Root,leaves,displ,MPI_BYTE,MPI_COMM_WORLD);
  //pprintf("Gathered together %d nodes.\n",nleaves);
  //And merge them all together
  NextFreeNode=nleaves;
  mergeTree();
  //printf("[%d] Merged all local trees for a total of %d nodes.\n",Task,NextFreeNode);
  //LeafCtr=0;
  //print_tree(Root);
  //printf("[%d] Walked %d nodes out of %d.\n",Task,LeafCtr,NextFreeNode);
  //kill(9394);


  /* Final refinement */

  //Tree is done except for those nodes pushed over the edge in merge, 
  //which need to be opened up recursively until we're under the threshold
  while(1)
  {
    cnt=NextFreeNode;
    //Open up anything that needs it...
    for(i=0;i<cnt;i++)
    {
      //Last condition is OK, because we'll only pass the first two if we're multiply owned
      if(!Root[i].n_daughters && Root[i].count > Params.pcles_per_node && get_bit(Task,Root[i].owners))
      {
        //printf("[%d] Opening up node with lvl = %d LID = %d, owner = %d count=%d.\n",Task,Root[i].lvl_lid/MaxLID,Root[i].lvl_lid%MaxLID,Root[i].owner,Root[i].count);
        //So we don't over-run buffer creating a leaf table we won't use...
        LeafCtr=0;
        build_base(Root+i);
      }
    }
    //How many new nodes did we make?
    i=NextFreeNode-cnt;
    //Bring over any newly created nodes
    MPI_Allgather(&i,1,MPI_INT,leaves,1,MPI_INT,MPI_COMM_WORLD);
    //Work out displacements to merge the table
    nleaves = leaves[0];
    leaves[0] *= sizeof(struct node);
    for(i=1,displ[0]=0;i<NTask;i++)
    {
      displ[i]=displ[i-1]+leaves[i-1];
      nleaves += leaves[i];
      leaves[i] *= sizeof(struct node);
    }
    //printf("[%d] Refinement loop created %d new nodes for a total of %d.\n",Task,nleaves,cnt+nleaves);
    //Did we make any new ones?  If not we're finished!
    if(!nleaves)
      break;

    //Create a copy of the local part of the table for transport
    if(leaves[Task] > BufferSizeBytes)
    {
      pprintf("Insufficient memory in buffer to transport tree nodes.\n");
      kill(ERROR_MEM);
    }
    memmove(Buffer,Root+cnt,leaves[Task]);
    //Do the transport
    MPI_Allgatherv(Buffer,leaves[Task],MPI_BYTE,
        Root+cnt,leaves,displ,MPI_BYTE,MPI_COMM_WORLD);
    NextFreeNode=cnt+nleaves;
    //Merge them into the tree and repeat...
    mergeTree();
    //printf("[%d] After refinement loop we have %d total nodes.\n",Task,NextFreeNode);
    //LeafCtr=0;
    //print_tree(Root);
    //printf("[%d] Walked %d nodes out of %d.\n",Task,LeafCtr,NextFreeNode);
  }
  //LeafCtr=0;
  //print_tree(Root);
  //printf("[%d] Walked %d nodes out of %d.\n",Task,LeafCtr,NextFreeNode);
  //kill(9394);
  //Done!
  free(LvlOffset);
}

void print_tree(struct node* cnode)
{
  //Start from root, print counts, lvl/lid and owner
  int i;
  int nkids;
  LeafCtr++;
  pprintf("Node at lvl %d, with LID %d, count %d, mob %d, owner %d and mother/sister/daughter/i %d/%d/%d/%d and %d daughters. ",(int) (cnode->lvl_lid/MaxLID),(int) (cnode->lvl_lid % MaxLID),cnode->count,cnode->mob,cnode->owner,cnode->mother,cnode->sister,cnode->daughter,(int) (cnode-Root),cnode->n_daughters);
  if(cnode->owner<0)
  {
    for(i=0;i<(-cnode->owner);i++)
    {
      printf("%d ",(int) (cnode->extra[i].mob/MaxGas));
    }
    printf("\n");
    for(i=0;i<(-cnode->owner);i++)
    {
      pprintf("Owner %d is %d, mob is %d, count is %d.\n",i,(int) (cnode->extra[i].mob/MaxGas),(int) (cnode->extra[i].mob%MaxGas),cnode->extra[i].count);
    }
  }
  else
  {
    printf("\n");
  }
  if(cnode->daughter!=-1)
  {
    nkids=cnode->n_daughters;
    cnode = Root + cnode->daughter;
    for(i=0;i<nkids;i++)
    {
      print_tree(cnode);
      cnode = Root + cnode->sister;
    }
  }
}

//Merges all duplicate nodes and corrects all references from scratch
void mergeTree()
{
  int i,j,k;
  int big_len=NextFreeNode;
  int mummy_offset=0;
  TREE_REF mummy=0,my_mummy;
  int n_own;
  int cnt;
  struct node *cnode;
  //printf("[%d] Sorting %d nodes.\n",Task,NextFreeNode);
  qsort(Root,NextFreeNode,sizeof(struct node),lvl_lid_sort);

  //Basically the same operation as standardise tree, but need to merge identical nodes
  NextFreeNode=0;
  i=0;
  while(i<big_len)
  {
    //printf("[%d] Processing node %d. NFN=%d\n",Task,i,NextFreeNode);
    //Move next node to next free slot
    Root[NextFreeNode] = Root[i];
    cnode = Root+NextFreeNode;
    //Do a look ahead and count how many are the same
    j=i+1;
    while(j<big_len && cnode->lvl_lid == Root[j].lvl_lid)
      j++;
    n_own=j-i;
    //Allocate space if multiply owned
    if(n_own>1)
    {
      //printf("[%d] Multiply owned, allocating memory and merging...\n",Task);
      //Allocate memory for bit mask
      cnode->owners = malloc(MaskSize);
      memmove(cnode->owners,NullMask,MaskSize);
      //Allocate memory for owner/count/mob
      cnode->extra = malloc(n_own*sizeof(struct mnode));
      //Now loop through and set everything that needs setting
      cnt=0;
      for(k=0;k<n_own;k++)
      {
        //Set mob to local mob
        if(Root[i+k].owner==Task)
        {
          cnode->mob = Root[i+k].mob;
        }
        //printf("[%d] Merging node with count=%d,n_daughters=%d and owner=%d into joint owned node with lvl=%d lid=%d\n",Task,Root[i+k].count,Root[i+k].n_daughters,Root[i+k].owner,cnode->lvl_lid/MaxLID,cnode->lvl_lid%MaxLID);
        set_bit(Root[i+k].owner,cnode->owners);
        cnode->extra[k].mob = Root[i+k].mob+MaxGas*Root[i+k].owner;
        cnode->extra[k].count = Root[i+k].count;
        cnt+=cnode->extra[k].count;
        //cnode->extra[k*3+0] = Root[i+k].owner;
        //cnode->extra[k*3+1] = Root[i+k].count;
        //cnode->extra[k*3+2] = Root[i+k].mob;
        //Don't double count the founder node
      }
      //Finalise variables
      cnode->count = cnt;
      cnode->owner = -n_own;
      //printf("[%d] Merged into %d owners\n",Task,n_own);
    }
    //printf("[%d] Moved node, fixing references.\n",Task);

    //Fix up the references
    cnode->daughter=-1;
    cnode->n_daughters = 0;
    //Root needs to be treated specially
    if(NextFreeNode==0)
    {
      cnode->sister=-1;
      cnode->mother=-1;
      mummy=0;
    }
    else
    {
      //printf("[%d] Correcting node %d NFN %d lvl=%d,lid=%d,m_lvl/lid=%d/%d.\n",Task,i,NextFreeNode,cnode->lvl,cnode->lid,mummy_lvl,mummy_lid);
      //What is the identifier for my mother?
      my_mummy = ((cnode->lvl_lid % MaxLID)/8) + ((cnode->lvl_lid /MaxLID)-1)*MaxLID;
      //Is it different from my current mother?
      if(my_mummy!=mummy)
      {
        //Update pointer of current mother node
        mummy=my_mummy;
        //This is needed to skip over leaf nodes
        while(Root[mummy_offset].lvl_lid != my_mummy)
          mummy_offset++;
        //Set the sister differently for the first daughter
        cnode->sister = Root[mummy_offset].sister;
      }
      else
      {
        cnode->sister = NextFreeNode==1 ? -1 : NextFreeNode-1;
      }
      //This ensure that the daughter is set to the last child
      Root[mummy_offset].daughter = NextFreeNode;
      //Update the pointer to mother
      cnode->mother = mummy_offset;
      //Update the number of daughters
      Root[mummy_offset].n_daughters++;
    }
    //printf("[%d] Moving on to node %d\n",Task,j);
    //Move onto next unique node
    i=j;
    NextFreeNode++;
  }
  //printf("[%d] Finished merge.\n",Task);
}



int lvl_lid_sort(const void *a,const void *b)
{
  const TREE_REF aa = ((const struct node*)a)->lvl_lid;
  const TREE_REF bb = ((const struct node*)b)->lvl_lid;
  if(aa<bb)
  {
    return -1;
  }
  else if(aa>bb)
  {
    return 1;
  }
  return 0;
}


void refineTree(struct node* parent)
{
  struct node* cnode;
  int i,j;
  int lvl,open;
  int plvl;
  TREE_REF fact,high;
  //Does it already have children?
  if(parent->n_daughters)
  {
    //printf("[%d] Opening up daughters of node at %d/%d.\n",Task,parent->lvl,parent->lid);
    //Open up the children then...
    cnode = Root+parent->daughter;
    for(i=0;i<parent->n_daughters;i++)
    {
      refineTree(cnode);
      cnode = Root + cnode->sister;
    }
  }
  else
  {
    //printf("[%d] Node at %d/%d has no children.\n",Task,parent->lvl,parent->lid);
    //No children, check if we should have some...
    //If we're at the lowest level then no
    plvl = (int) (parent->lvl_lid / MaxLID);
    if(plvl==TreeDepth)
    {
      //printf("[%d] Node at %d/%d is at lowest level, returning...\n",Task,parent->lvl,parent->lid);
      return;
    }
    //Start at the level below parent level
    lvl = plvl+1;
    open=0;
    while(!open && lvl<=TreeDepth)
    {
      //What is the range in the table where the level is lvl?
      i=LvlOffset[lvl+1]-1;
      j=LvlOffset[lvl];
      //To be below current one, lid must be in the range 2**(lvl-lvl_parent) * [lid,lid+1)
      fact = 1;
      fact <<= 3*(lvl-plvl);
      high = fact*((parent->lvl_lid % MaxLID)+1);
      //Step backwards from top of range
      while((TRBuffer[i] % MaxLID)>=high && i>=j)
        i--;
      //Is this one within the range required to be a child of parent?  If it's not
      //neither is anyone else on this level
      if(i>=j && (TRBuffer[i] % MaxLID)>=high-fact)
        open=1;
      lvl++;
    }
    //We need to open it up.  This will NOT recurse.
    if(open)
    {
      //printf("[%d] Node at %lu/%lu Needs opening...\n",Task,parent->lvl_lid/MaxLID,parent->lvl_lid%MaxLID);
      //This is the highest sub-level where we found a leaf
      lvl--;
      //Split this cell
      build_base(parent);
      //Check if the new daughters need splitting too
      cnode = Root + parent->daughter;
      for(i=0;i<parent->n_daughters;i++)
      {
        refineTree(cnode);
        cnode = Root + cnode->sister;
      }
    }
  }
}

int sortLeafTable(const void* a, const void* b) 
{
  const TREE_REF aa = *((const TREE_REF*)a);
  const TREE_REF bb = *((const TREE_REF*)b);
  if(aa<bb)
  {
    return -1;
  }
  else if(aa>bb)
  {
    return 1;
  }
  return 0;
  //return (*(const TREE_REF*) arr1)-(*(const TREE_REF*) arr2);
}


void splitter(int offset,int count,int axis,double split,int *counts)
{
  //Sort by the appropriate thing
  switch(axis)
  {
    case 0:
      qsort(&G[offset],count,sizeof(struct gas),gsort_radius);
      //Count the number either side.  Could probably
      //be mode more efficient by using a binary search
      counts[0]=0;
      while(G[offset+counts[0]].R<split && counts[0]<count)
        counts[0]++;
      counts[1] = count - counts[0];
      break;
    case 1:
      qsort(&G[offset],count,sizeof(struct gas),gsort_phi);
      counts[0]=0;
      while(G[offset+counts[0]].phi<split && counts[0]<count)
        counts[0]++;
      counts[1] = count - counts[0];
      break;
    case 2:
      qsort(&G[offset],count,sizeof(struct gas),gsort_z);
      counts[0]=0;
      while(G[offset+counts[0]].z<split && counts[0]<count)
        counts[0]++;
      counts[1] = count - counts[0];
      break;
    default:
      kill(ERROR_SANITY);
      break;
  }
}

void build_base(struct node *parent)
{
  struct node *cnode;
  double mids[3];
  int counts[8];
  int Rl,Ru,zll,zlu,zul,zuu;
  int offset,count;
  TREE_REF lvl_lid;
  int i,last;
  FLOAT *lows,*highs;
  //Where does this node start in memory and how big is it?
  //This should only be called on local nodes, so exist if not-local
  if(parent->owner<0)
  {
    i=0;
    while(i<(-parent->owner) && (parent->extra[i].mob/MaxGas)!=Task)
      i++;
    if(i==-parent->owner)
    {
      //Don't open non-local
      pprintf("build_base called with non-local node.\n");
      kill(ERROR_SANITY);
      return;
    }
    else
    {
      offset=parent->extra[i].mob % MaxGas;
      count=parent->extra[i].count;
      //offset=parent->extra[i*3+2];
      //count=parent->extra[i*3+1];
    }
  }
  else
  {
    //Don't open non-local
    if(parent->owner!=Task)
    {
      pprintf("build_base called with non-local node.\n");
      kill(ERROR_SANITY);
      return;
    }
    offset=parent->mob;
    count=parent->count;
  }
  //Where to split?
  lows = parent->lows;
  highs = parent->highs;
  mids[0] = sqrt(0.5 * (lows[0]*lows[0] + highs[0]*highs[0]));
  mids[1] = 0.5 * (lows[1] + highs[1]);
  mids[2] = 0.5 * (lows[2] + highs[2]);
  //printf("[%d] R = (%g,%g,%g), phi = %g,%g,%g, z = %g,%g,%g \n",Task,lows[0],mids[0],highs[0],lows[1],mids[1],highs[1],lows[2],mids[2],highs[2]);
  //Sort the memory appropriately and count the number of particles per node
  splitter(offset,count,0,mids[0],counts);
  Rl=counts[0];
  Ru=counts[1];
  splitter(offset,Rl,2,mids[2],counts);
  zll=counts[0];
  zlu=counts[1];
  splitter(offset+Rl,Ru,2,mids[2],counts);
  zul=counts[0];
  zuu=counts[1];
  splitter(offset,zll,1,mids[1],counts);
  splitter(offset+zll,zlu,1,mids[1],&counts[2]);
  splitter(offset+Rl,zul,1,mids[1],&counts[4]);
  splitter(offset+Rl+zul,zuu,1,mids[1],&counts[6]);
  lvl_lid = (((parent->lvl_lid / MaxLID)+1)*MaxLID)+(parent->lvl_lid % MaxLID)*8;
  last = 0;
  Rl=NextFreeNode;
  //Now make the daughter nodes
  for(i=0;i<8;i++)
  {
    //Create a new node if it's not empty
    if(counts[i])
    {
      cnode = Root+NextFreeNode;
      //Allocate its boundaries
      cnode->lows[0] = i<4 ? lows[0] : mids[0];
      cnode->highs[0] = i<4 ? mids[0] : highs[0];
      cnode->lows[1] = i%2 ? mids[1] : lows[1];
      cnode->highs[1] = i%2 ? highs[1] : mids[1];
      cnode->lows[2] = (i%4)<2 ? lows[2] : mids[2];
      cnode->highs[2] = (i%4)<2 ? mids[2] : highs[2];
      cnode->com[0]=cnode->com[1]=cnode->com[2]=0;
      cnode->rmax=cnode->hmax=0;
      cnode->count = counts[i];
      cnode->mob = offset;
      cnode->lvl_lid = lvl_lid +i;
      cnode->mother = (int) (parent-Root);
      cnode->owner = Task;
      cnode->owners = NULL;
      cnode->daughter = -1;
      cnode->n_daughters = 0;
      cnode->sister = i ? last : parent->sister;
      cnode->extra = NULL;
      //Set the daughter to the last node
      parent->daughter = NextFreeNode;
      //Set the number of children on parent
      parent->n_daughters++;
      offset += counts[i];
      last = NextFreeNode;
      NextFreeNode++;
    }
  }
  //Split them as needed
  for(i=0;i<8;i++)
  {
    if(counts[i])
    {
      cnode = Root+Rl;
      //Split this node if we need to (if this is called by refineTree we will never need to)
      if(cnode->count > Params.pcles_per_node)
      {
        //printf("[%d] New node has %d.  Sub-dividing...\n",Task,counts[i]);
        build_base(cnode);
      }
      //If we have reached a data containing leaf node
      else
      {
        TRBuffer[LeafCtr] = cnode->lvl_lid;
        LeafCtr++;
        if(LeafCtr>=BufferSizeTRs)
        {
          pprintf("Insufficient memory to build leaf table in buffer.\n");
          kill(ERROR_MEM);
        }
      }
      //Point at the next non-zero node
      Rl++;
    }
  }
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Description:  Function which finds all neighbours of point within distance d. 
 *                Storing the resulting particles in the global neighbour buffer Ngbs.
 *                If any particles can be identified it is put in the buffer (except in
 *                the case of strong_locality, see below) and if a node which is not
 *                local potentially has neighbours, then the nodes index in the Root 
 *                array is stored with the minus bit set in Ngbs.  Note that in the 
 *                case of multiply owned nodes, this means that we store both any local
 *                particles that are neighbours in that node as well as a reference
 *                to the node itself.
 *                *startnode - 0 - Points to the node at which to begin searching.  This 
 *                     should be set to zero, except when the neighbour buffer filled 
 *                     up during a previous call to find_ngbs and we now want to finish
 *                     the search.
 *                strong_locality - 0 - If enabled this forces the neighbour search to 
 *                     ignore anything which isn't strongly local (i.e, owned by this
 *                     processor, not its shadow domains).  Useful for doing work on
 *                     exported particles.
 * =====================================================================================
 */
int find_ngbs(double point[3],double d,int *startnode,int strong_locality)
{
  double bbox[3][2],R;
  double dphi,phi;
  int i,j,nngbs;
  int stored;
  int owner,mob,count;
  struct node *cnode;
  //Calculate the bounding box
  R=sqrt((point[0]-COM[0])*(point[0]-COM[0])+(point[1]-COM[1])*(point[1]-COM[1]));
  //Note boxmin/max is slightly different for the angle coordinate
  //min stores the central point of the range on [0,2pi) and max
  //stores the distance from the central point to the edge
  //Bounding box is a minimum bounding angular segment necessary to 
  //contain the circle size d, centred at point
  if(d>=R)
  {
    //Shouldn't happen often really...
    bbox[0][0]=0;
    bbox[1][0]=PI;
    bbox[1][1]=PI;
  }
  else
  {
    bbox[0][0]=R-d;
    bbox[1][0]=point[1]<0 ? atan2(point[1]-COM[1],point[0]-COM[0])+PI2: atan2(point[1]-COM[1],point[0]-COM[0]);
    bbox[1][1]=acos(sqrt(1-(d/R)*(d/R)));
    //bbox[1][1]=acos(R/sqrt(R*R-d*d));
  }
  bbox[0][1]=R+d;
  bbox[2][0]=point[2]-d;
  bbox[2][1]=point[2]+d;
  //Comparison made to square, so square d
  d*=d;
  //Box constructed, search the tree
  nngbs=0;
  while(*startnode!=-1)
  {
    cnode = Root + *startnode;
    //Skip anything that isn't on this processor when that's all we care about
    if(strong_locality && !((cnode->owner<0 && get_bit(Task,cnode->owners)) || cnode->owner==Task))
    {
      *startnode = cnode->sister;
      continue;
    }
    //printf("[%d] Looking at node %d, with lid/lvl/count = %d/%d/%d\n",Task,*startnode,cnode->lid,cnode->lvl,cnode->count);
    //Check if it overlaps with our box
    dphi=0.5*(cnode->highs[1]-cnode->lows[1]);
    phi=cnode->highs[1]<dphi ? cnode->highs[1]-dphi+PI2 : cnode->highs[1]-dphi;
    //Calculate the minimum distance between midpoints
    phi=fabs(bbox[1][0]-phi);
    phi=fmin(phi,PI2-phi);
    //Does it overlap
    if(cnode->highs[0] >= bbox[0][0] && cnode->lows[0] <= bbox[0][1] && 
          (phi<=(bbox[1][1]+dphi)) &&
          cnode->highs[2] >= bbox[2][0] && cnode->lows[2] <= bbox[2][1])
    {
      //printf("[%d] Which overlaps our target area.\n",Task);
      //Is it a leaf?  If not, open it up
      if(cnode->n_daughters)
      {
        //printf("[%d] But isn't a leaf.\n",Task);
        *startnode=cnode->daughter;
        continue;
      }
      //Do we have the potential to overload the ngb buffer with this node?
      if(nngbs+cnode->count > NgbBufferSizeInts)
      {
        pprintf("Completely filled the neighbour buffer (%d) looking for neighbours of (%g,%g,%g) within %g!\n",NgbBufferSizeInts,point[0],point[1],point[2],sqrt(d));
        break;
      }
      //The fun starts here...
      i=0;
      //Never want to store anything if we're just looking for strongly local neighbours
      stored=strong_locality;
      do
      {
        //Make a pointer to the task count and offset
        if(cnode->owner<0)
        {
          owner =(int) (cnode->extra[i].mob / MaxGas);
          mob = (int) (cnode->extra[i].mob % MaxGas);
          count = cnode->extra[i].count;
        }
        else
        {
          owner = cnode->owner;
          mob = cnode->mob;
          count = cnode->count;
        }
        //printf("[%d] Has a part owner/count = %d/%d and owners %d.\n",Task,owner,count,cnode->owner);
        //Is this local?
        //Only permit shadow domains as local when not searching for strong locality.
        if(owner==Task || (!strong_locality && Shadows[owner].G))
        {
          //printf("[%d] Which is local.\n",Task);
          //Loop over the particles
          for(j=mob;j<mob+count;j++)
          {
            //How far are we from our target?
            R= (point[0]-Shadows[owner].G[j].x)*(point[0]-Shadows[owner].G[j].x)+
                (point[1]-Shadows[owner].G[j].y)*(point[1]-Shadows[owner].G[j].y)+
                (point[2]-Shadows[owner].G[j].z)*(point[2]-Shadows[owner].G[j].z);
            if(R < d)
            {
              //printf("[%d] And in range, so storing j=%d,owner=%d.\n",Task,j,owner);
              //Close enough, include it!
              Ngbs[nngbs]=j+MaxGas*owner;
              nngbs++;
            }
          }
        }
        //Not local, store the node...
        //Prevent multiply owned nodes from being stored multiple times...
        else if(!stored)
        {
          //printf("[%d] Which isn't local.\n",Task);
          Ngbs[nngbs]=-1*((int) (cnode-Root));
          stored=1;
          nngbs++;
        }
        //Move onto the next owner.  If it's singly owned then cnode->owner>=0 and 
        //so -cnode->owner <=0 and the loop will terminate first time around
        i++;
      }while(i<(-1*cnode->owner));
    }
    
    //printf("[%d] Now moving onto my sister at %d.\n",Task,cnode->sister);
    //No overlap, or we've added it, move onto sister
    *startnode=cnode->sister;
  }
  return nngbs;
  //The core of the search is done in the following recursive function
  //return find_ngb_nodes(point,bbox);
  //return find_ngbs_local(point,d*d,bbox,startnode);
}

//int find_ngbs_local(double point[3], double d2, double bbox[3][2],int *startnode)
//{
//  int tco[3]; //Task,Count,Offset
//  int *dat; //The matrix to use
//  struct node *cnode;
//  nngbs=0;
//  while(*startnode!=-1)
//  {
//    cnode = Root + *startnode;
//    //Skip empty nodes
//    if(cnode->count)
//    {
//      //Check if it overlaps with our box
//      dphi=0.5*(cnode->highs[1]-cnode->lows[1]);
//      phi=cnode->highs[1]<dphi ? cnode->highs[1]-dphi+PI2 : cnode->highs[1]-dphi;
//      //Calculate the minimum distance between midpoints
//      phi=fabs(bbox[1][0]-phi);
//      phi=fmin(phi,PI2-phi);
//      //printf("[%d] Node with bounds R=%g to %g, phi= %g to %g and z= %g to %g counts %d owned by %d sister %d  and daughter %d\n",Task,cnode->lows[0],cnode->highs[0],cnode->lows[1],cnode->highs[1],cnode->lows[2],cnode->highs[2],cnode->count,cnode->owner,cnode->sister,cnode->daughter);
//      //Does it overlap
//      if(cnode->highs[0] >= bbox[0][0] && cnode->lows[0] <= bbox[0][1] && 
//            (phi<=(bbox[1][1]+dphi)) &&
//            cnode->highs[2] >= bbox[2][0] && cnode->lows[2] <= bbox[2][1])
//      {
//        //Is it a leaf?  If not, open it up
//        if(cnode->daughter!=-1)
//        {
//          *startnode=cnode->daughter
//          continue;
//        }
//        //Do we have the potential to overload the ngb buffer with this node?
//        if(nngbs+cnode->count > NgbBufferSizeInts)
//        {
//          printf("[%d] Completely filled the neighbour buffer (%d) looking for neighbours of (%g,%g,%g) within %g!\n",Task,NgbBufferSizeInts,point[0],point[1],point[2],sqrt(d2));
//          break;
//        }
//        //The fun starts here...
//        i=0;
//        do
//        {
//          //Make a pointer to the task count and offset
//          if(cnode->owner<0)
//          {
//            dat = cnode->extra[i*3];
//          }
//          else
//          {
//            tco[0] = cnode->owner;
//            tco[1] = cnode->count;
//            tco[2] = cnode->mob;
//            dat = tco;
//          }
//          //Is this local?
//          if(Shadows[dat[0]].G)
//          {
//            //Loop over the particles
//            for(j=dat[2];j<dat[2]+dat[1];j++)
//            {
//              //How far are we from our target?
//              r2= (point[0]-Shadows[dat[0]].G[j].x)*(point[0]-Shadows[dat[0]].G[j].x)+
//                  (point[1]-Shadows[dat[0]].G[j].y)*(point[1]-Shadows[dat[0]].G[j].y)+
//                  (point[2]-Shadows[dat[0]].G[j].z)*(point[2]-Shadows[dat[0]].G[j].z);
//              if(r2 < d2)
//              {
//                //Close enough, include it!
//                Ngbs[nngbs]=j+MaxGas*dat[0];
//                nngbs++;
//              }
//            }
//          }
//          //Not local, store the node...
//          else
//          {
//            Ngbs[nngbs]=(int) (Root-cnode);
//            nngbs++;
//          }
//          //Move onto the next owner.  If it's singly owned then cnode->owner>=0 and 
//          //so -cnode->owner <=0 and the loop will terminate first time around
//          i++;
//        }while(i<(-1*cnode->owner));
//      }
//    }
//    //No overlap, or we've added it, move onto sister
//    *startnode=cnode->sister;
//  }
//  return nngbs;
//}

//Starting from startnode, fill up the neighbour buffer until we can't any more or 
//until we're done.  
//int find_ngbs_local(double point[3], double d2, double bbox[3][2],int* startnode)
//{
//  double dphi,phi;
//  int i,j,nngbs;
//  int local;
//  int offset;
//  struct node* cnode=Root+*startnode;
//  nngbs=0;
//  while(1)
//  {
//    //Skip empty nodes
//    if(cnode->count)
//    {
//      //Check if it overlaps with our box
//      dphi=0.5*(cnode->highs[1]-cnode->lows[1]);
//      phi=cnode->highs[1]<dphi ? cnode->highs[1]-dphi+PI2 : cnode->highs[1]-dphi;
//      //Calculate the minimum distance between midpoints
//      phi=fabs(bbox[1][0]-phi);
//      phi=fmin(phi,PI2-phi);
//      //printf("[%d] Node with bounds R=%g to %g, phi= %g to %g and z= %g to %g counts %d owned by %d sister %d  and daughter %d\n",Task,cnode->lows[0],cnode->highs[0],cnode->lows[1],cnode->highs[1],cnode->lows[2],cnode->highs[2],cnode->count,cnode->owner,cnode->sister,cnode->daughter);
//      //Does it overlap
//      if(cnode->highs[0] >= bbox[0][0] && cnode->lows[0] <= bbox[0][1] && 
//            (phi<=(bbox[1][1]+dphi)) &&
//            cnode->highs[2] >= bbox[2][0] && cnode->lows[2] <= bbox[2][1])
//      {
//        //printf("[%d] Overlaps.\n",Task);
//        //Is it local?
//        if(cnode->owner<0)
//        {
//          local=0;
//          for(i=0;i<MaskSize && !local;i++)
//            local = ShadowMask[i] & cnode->owners[i];
//        }
//        else
//        {
//          if(Shadows[cnode->owner].G)
//          {
//            local=1;
//          }
//          else
//          {
//            local=0;
//          }
//        }
//        //If it's not local, move on...
//        if(local)
//        {
//          //printf("[%d] Declared local.\n",Task);
//          //If it's not a leaf, open it up...
//          if(cnode->daughter!=-1)
//          {
//            //printf("[%d] Not a leaf, opening up.\n",Task);
//            //Open it up...
//            cnode=Root+cnode->daughter;
//            continue;
//          }
//          //Cool, we've found something!
//          //printf("[%d] Found something!\n",Task);
//          //Will we overflow the neighbour buffer by adding these in?
//          if(nngbs+cnode->count > NgbBufferSizeInts)
//          {
//            printf("[%d] Completely filled the neighbour buffer (%d) looking for neighbours of (%g,%g,%g) within %g!\n",Task,NgbBufferSizeInts,point[0],point[1],point[2],sqrt(d2));
//            break;
//          }
//          //We're good.  Is this a multiply owned fucker?
//          if(cnode->owner<0)
//          {
//            //It is, OK, store the local ones and set the export flag as needed.
//            for(i=0;i<(-1*cnode->owner);i++)
//            {
//              //Are these particles contained locally?
//              //if(Shadows[cnode->extra[i*3]].G)
//              if(Shadows[cnode->extra[i].mob/MaxGas].G)
//              {
//                //This allows us to work out which processor to look in
//                //offset=cnode->extra[i*3+2]+MaxGas*cnode->extra[i*3];
//                offset=cnode->extra[i].mob;
//                for(j=0;j<cnode->extra[i].count;j++)
//                {
//                  //printf("[%d] Saving particle with index %d.\n",Task,offset+j);
//                  Ngbs[nngbs]=offset+j;
//                  nngbs++;
//                }
//              }
//              //It's not local, set export flag 
//              else
//              {
//                Export[cnode->extra[i].mob/MaxGas]=1;
//              }
//            }
//          }
//          //Singly owned, couldn't be simpler...
//          else
//          {
//            offset=cnode->mob+MaxGas*cnode->owner;
//            for(i=0;i<cnode->count;i++)
//            {
//              ////Check that the ngb really is where it says it is
//              //if(Shadows[cnode->owner].G[cnode->mob+i].R > cnode->highs[0] ||
//              //   Shadows[cnode->owner].G[cnode->mob+i].R < cnode->lows[0] ||
//              //   Shadows[cnode->owner].G[cnode->mob+i].phi > cnode->highs[1] ||
//              //   Shadows[cnode->owner].G[cnode->mob+i].phi < cnode->lows[1] ||
//              //   Shadows[cnode->owner].G[cnode->mob+i].z > cnode->highs[2] ||
//              //   Shadows[cnode->owner].G[cnode->mob+i].z < cnode->lows[2])
//              //{
//              //  printf("[%d] Particle not where it should be!\n",Task);
//              //  kill(634);
//              //}
//              //printf("[%d] Saving particle with index %d.\n",Task,offset+i);
//              Ngbs[nngbs]=offset+i;
//              nngbs++;
//            }
//          }
//        }
//        //But before we move on, set export flags
//        else
//        {
//          //printf("[%d] Declaring non-local for particle with owner %d.",Task,cnode->owner);
//          if(cnode->owner<0)
//          {
//            for(i=0;i<NTask;i++)
//            {
//              //If it overlaps with the bbox, set the export flag
//              if(get_bit(i,cnode->owners))
//              {
//                //printf(" %d ",i);
//                Export[i]=1;
//              }
//            }
//          }
//          else
//          {
//            Export[cnode->owner]=1;
//          }
//          //printf("\n");
//        }
//      }
//    }
//    //OK, if we're here it means it's time to move onto the next one
//    if(cnode->sister==-1)
//    {
//      //printf("[%d] Reached the end of the road, exiting.\n",Task);
//      //If we're finishing, set node to root so we'll know
//      cnode=Root;
//      break;
//    }
//    //printf("[%d] Moving onto sister.\n",Task);
//    cnode = Root+cnode->sister;
//  }
//  //Set the current node in case we need to resume
//  *startnode = cnode-Root;
//  return nngbs;
//}

//Point is given in cartesian coordinates
//d is distance to find points within
//startnode is the starting node for the local neighbour search
//the trunk is also walked from the root in determining which processors
//we need to export to.  It is up to the user to initialise Export to 
//something sensible before calling this and to do something sensible
//with it after it has been called.
//mode = 1 -> check which processors to export to
//mode = 0 -> just run local ngb search starting from startnode
//int Extra;
//int find_ngbs(double point[3],double d,int* startnode,int mode)
//{
//  double bbox[3][2],R,phi,dphi;
//  double tcost=MPI_Wtime();
//  struct node *trunk;
//  Extra=0;
//  if(mode==10)
//  {
//    mode=0;
//    Extra=1;
//  }
//  Extra=1;
//  //Convert to relative to COM
//  //point[0]=point[0]-COM[0];
//  //point[1]=point[1]-COM[1];
//  //point[2]=point[2]-COM[2];
//  //Calculate the bounding box
//  R=sqrt((point[0]-COM[0])*(point[0]-COM[0])+(point[1]-COM[1])*(point[1]-COM[1]));
//  //Note boxmin/max is slightly different for the angle coordinate
//  //min stores the central point of the range on [0,2pi) and max
//  //stores the distance from the central point to the edge
//  //Bounding box is a minimum bounding angular segment necessary to 
//  //contain the circle size d, centred at point
//  if(d>=R)
//  {
//    //Shouldn't happen often really...
//    bbox[0][0]=0;
//    bbox[1][0]=PI;
//    bbox[1][1]=PI;
//  }
//  else
//  {
//    bbox[0][0]=R-d;
//    bbox[1][0]=point[1]<0 ? atan2(point[1]-COM[1],point[0]-COM[0])+PI2: atan2(point[1]-COM[1],point[0]-COM[0]);
//    bbox[1][1]=acos(sqrt(1-(d/R)*(d/R)));
//    //bbox[1][1]=acos(R/sqrt(R*R-d*d));
//  }
//  bbox[0][1]=R+d;
//  bbox[2][0]=point[2]-d;
//  bbox[2][1]=point[2]+d;
//  if(mode)
//  {
//    //Check if we need to do anything non-local, all export flags set after this
//    trunk=Nodes;
//    while(1)
//    {
//      //If it's meta (multiply owned) or if it's not local and not already set for export, check it out
//      //printf("[%d] Investigating node %d with owner = %d, locality=%d and meta =%d.\n",Task,(int) (trunk-Nodes),trunk->flag & NODE_OWNER,trunk->flag & NODE_LOCAL,trunk->flag & NODE_META);
//      if((trunk->flag & NODE_META) || (!(trunk->flag & NODE_LOCAL) && !Export[trunk->flag & NODE_OWNER]))
//      {
//        //Check if it overlaps with our box
//        dphi=0.5*(trunk->highs[1]-trunk->lows[1]);
//        phi=trunk->highs[1]<dphi ? trunk->highs[1]-dphi+PI2 : trunk->highs[1]-dphi;
//        //Calculate the minimum distance between midpoints
//        phi=fabs(bbox[1][0]-phi);
//        phi=fmin(phi,PI2-phi);
//        //Does it overlap
//        if(trunk->highs[0] >= bbox[0][0] && trunk->lows[0] <= bbox[0][1] && 
//            (phi<=(bbox[1][1]+dphi)) &&
//            trunk->highs[2] >= bbox[2][0] && trunk->lows[2] <= bbox[2][1])
//        {
//          //If it's multiply owned, investigate further, otherwise set flag
//          if(trunk->flag & NODE_META)
//          {
//            trunk=Nodes+trunk->daughter;
//            continue;
//          }
//          Export[trunk->flag & NODE_OWNER]=1;
//        }
//      }
//      //Move onto the next one
//      if(trunk->sister==-1)
//        break;
//      trunk = Nodes + trunk->sister;
//    }
//  }
//  Timings.ngb_time += MPI_Wtime() - tcost;
//  //Now calculate the local neighbours
//  return find_ngbs_local(point,d*d,bbox,startnode);
//}
//
////Starting from startnode, fill up the neighbour buffer until we can't any more or 
////until we're done
//int find_ngbs_local(double point[3], double d2, double bbox[3][2],int* startnode)
//{
//  double dphi,phi;
//  int i,nngbs,offset;
//  nngbs=0;
//  struct node* node=Nodes+*startnode;
//  if(Extra)
//    printf("[%d] Bounding box is ([%g,%g],[%g,%g],[%g,%g]).\n",Task,bbox[0][0],bbox[0][1],bbox[1][0],bbox[1][1],bbox[2][0],bbox[2][1]);
//  while(1)
//  {
//    if(Extra)
//      printf("[%d] Looking at node with %d pcles from ([%g,%g],[%g,%g],[%g,%g]).\n",Task,node->count,node->lows[0],node->highs[0],node->lows[1],node->highs[1],node->lows[2],node->highs[2]);
//    //If it's meta or local, investigate it
//    if((node->flag & NODE_META) || (node->flag & NODE_LOCAL))
//    {
//      if(Extra)
//        printf("[%d] It's local, investigating.\n",Task);
//      //Check if it overlaps with our box
//      dphi=0.5*(node->highs[1]-node->lows[1]);
//      phi=node->highs[1]<dphi ? node->highs[1]-dphi+PI2 : node->highs[1]-dphi;
//      //Calculate the minimum distance between midpoints
//      phi=fabs(bbox[1][0]-phi);
//      phi=fmin(phi,PI2-phi);
//      //Does it overlap
//      if(node->highs[0] >= bbox[0][0] && node->lows[0] <= bbox[0][1] && 
//            (phi<=(bbox[1][1]+dphi)) &&
//            node->highs[2] >= bbox[2][0] && node->lows[2] <= bbox[2][1])
//      {
//        if(Extra)
//        printf("[%d] It's overlaping, investigating.\n",Task);
//        //Is it a normal node?
//        if(!(node->flag & NODE_LEAF))
//        {
//          node=Nodes+node->daughter;
//          continue;
//        }
//        //Must be a leaf, do leafy things
//        if(nngbs+node->count > NgbBufferSizeInts)
//        {
//          printf("[%d] Completely filled the neighbour buffer (%d) looking for neighbours of (%g,%g,%g) within %g!\n",Task,NgbBufferSizeInts,point[0],point[1],point[2],sqrt(d2));
//          break;
//        }
//        if(Extra)
//        printf("[%d] It's found, store stuff %d of them.\n",Task,node->count);
//        offset=node->daughter+MaxGas*(node->flag & NODE_OWNER);
//        for(i=0;i<node->count;i++)
//        {
//          if(Extra)
//          {
//            printf("[%d] Adding particle %d at %g,%g,%g.\n",Task,node->daughter+i,G[node->daughter+i].x,G[node->daughter+i].y,G[node->daughter+i].z);
//            printf("[%d] Next Adding particle %d at %g,%g,%g.\n",Task,node->daughter+i+1,G[node->daughter+i+1].x,G[node->daughter+i+1].y,G[node->daughter+i+1].z);
//          }
//          //Check each particle for distance to query point (might be quicker to do something
//          //else first.
//          //if((G[p].x-point[0])*(G[p].x-point[0])+(G[p].y-point[1])*(G[p].y-point[1])+(G[p].z-point[2])*(G[p].z-point[2])<=d2)
//          //{
//            Ngbs[nngbs]=offset+i;
//            nngbs++;
//          //}
//        }
//      }
//    }
//    //Time to move onto next node (or finish)
//    if(node->sister==-1)
//    {
//      //If we're finishing, set node to root so we'll know
//      node=Nodes;
//      break;
//    }
//    node = Nodes+node->sister;
//  }
//  *startnode = node-Nodes;
//  //if(nngbs==0)
//  //{
//  //  printf("[%d] No neighbours of %g,%g,%g found within %g.\n",Task,point[0],point[1],point[2],sqrt(d2));
//  //  kill(1);
//  //}
//  return nngbs;
//}

//Point is given in cartesian coordinates
//d is distance to find points within
//startnode is the starting node for the local neighbour search
//the trunk is also walked from the root in determining which processors
//we need to export to.  It is up to the user to initialise Export to 
//something sensible before calling this and to do something sensible
//with it after it has been called.
//mode = 1 -> check which processors to export to
//mode = 0 -> just run local ngb search starting from startnode
//int find_ngbs_2D(double point[2],double d,struct node *startnode,int mode)
//{
//  double bbox[2][2],R,phi,dphi;
//  struct node* trunk;
//  //Calculate the bounding box
//  R=sqrt(point[0]*point[0]+point[1]*point[1]);
//  //Note boxmin/max is slightly different for the angle coordinate
//  //min stores the central point of the range on [0,2pi) and max
//  //stores the distance from the central point to the edge
//  //Bounding box is a minimum bounding angular segment necessary to 
//  //contain the circle size d, centred at point
//  if(d>=R)
//  {
//    //Shouldn't happen often really...
//    bbox[0][0]=0;
//    bbox[1][0]=PI;
//    bbox[1][1]=PI;
//  }
//  else
//  {
//    bbox[0][0]=R-d;
//    bbox[1][0]=point[1]<0 ? atan2(point[1],point[0])+PI2: atan2(point[1],point[0]);
//    bbox[1][1]=acos(sqrt(1-(d/R)*(d/R)));
//  }
//  bbox[0][1]=R+d;
//  if(mode)
//  {
//    //Check if we need to do anything non-local, all export flags set after this
//    trunk=Nodes;
//    while(1)
//    {
//      //If it's meta (multiply owned) or if it's not local and not already set for export, check it out
//      //printf("[%d] Investigating node %d with owner = %d, locality=%d and meta =%d.\n",Task,(int) (trunk-Nodes),trunk->flag & NODE_OWNER,trunk->flag & NODE_LOCAL,trunk->flag & NODE_META);
//      if((trunk->flag & NODE_META) || (!(trunk->flag & NODE_LOCAL) && !Export[trunk->flag & NODE_OWNER]))
//      {
//        //Check if it overlaps with our box
//        dphi=0.5*(trunk->highs[1]-trunk->lows[1]);
//        phi=trunk->highs[1]<dphi ? trunk->highs[1]-dphi+PI2 : trunk->highs[1]-dphi;
//        //Calculate the minimum distance between midpoints
//        phi=fabs(bbox[1][0]-phi);
//        phi=fmin(phi,PI2-phi);
//        //Does it overlap
//        if(trunk->highs[0] >= bbox[0][0] && trunk->lows[0] <= bbox[0][1] && 
//            (phi<=(bbox[1][1]+dphi)))
//        {
//          //If it's multiply owned, investigate further, otherwise set flag
//          if(trunk->flag & NODE_META)
//          {
//            trunk=Nodes+trunk->daughter;
//            continue;
//          }
//          Export[trunk->flag & NODE_OWNER]=1;
//        }
//      }
//      //Move onto the next one
//      if(trunk->sister==-1)
//        break;
//      trunk = Nodes + trunk->sister;
//    }
//  }
//  //Now calculate the local neighbours
//  return find_ngbs_local_2D(point,d*d,bbox,startnode);
//}
//
////Starting from startnode, fill up the neighbour buffer until we can't any more or 
////until we're done
//int find_ngbs_local_2D(double point[2], double d2, double bbox[2][2],struct node *startnode)
//{
//  double dphi,phi;
//  int i,p,nngbs;
//  nngbs=0;
//  while(1)
//  {
//    //If it's meta or local, investigate it
//    if((startnode->flag & NODE_META) || (startnode->flag & NODE_LOCAL))
//    {
//      //Check if it overlaps with our box
//      dphi=0.5*(startnode->highs[1]-startnode->lows[1]);
//      phi=startnode->highs[1]<dphi ? startnode->highs[1]-dphi+PI2 : startnode->highs[1]-dphi;
//      //Calculate the minimum distance between midpoints
//      phi=fabs(bbox[1][0]-phi);
//      phi=fmin(phi,PI2-phi);
//      //Does it overlap
//      if(startnode->highs[0] >= bbox[0][0] && startnode->lows[0] <= bbox[0][1] && 
//            (phi<=(bbox[1][1]+dphi)))
//      {
//        //Is it a normal node?
//        if(!(startnode->flag & NODE_LEAF))
//        {
//          startnode=Nodes+startnode->daughter;
//          continue;
//        }
//        //Must be a leaf, do leafy things
//        if(nngbs+startnode->count > BufferSizeInts)
//        {
//          printf("[%d] Completely filled the neighbour buffer (%d)!\n",Task,BufferSizeInts);
//          break;
//        }
//        for(i=0;i<startnode->count;i++)
//        {
//          //Check each particle for distance to query point (might be quicker to do something
//          //else first.
//          p=startnode->daughter+i;
//          if((G[p].x-point[0])*(G[p].x-point[0])+(G[p].y-point[1])*(G[p].y-point[1])<=d2)
//          {
//            Ngbs[nngbs]=p;
//            nngbs++;
//          }
//        }
//      }
//    }
//    //Time to move onto next node (or finish)
//    if(startnode->sister==-1)
//      break;
//    startnode = Nodes+startnode->sister;
//  }
//  return nngbs;
//}
//
////Loop through the nodes, starting at the bottom, set hmax (the maximum smoothing length in the node
//void set_hmax(void)
//{
//  int left,offset,i,nsend;
//  //Set hmax for local nodes
//  walk_hmax(Nodes,Nodes->sister);
//  //Now sync across all processors so everyone has the right hmax
//  //Because nodes are continuous in memory and the right answer is
//  //just the max across all processors, just do an allreduce
//  //on hmaxes
//  left=NNodes;
//  offset=0;
//  while(left)
//  {
//    nsend=imin(left,BufferSizeFloats);
//    for(i=0;i<nsend;i++)
//    {
//      FBuffer[i]=Nodes[offset+i].hmax;
//    }
//    //Get the answer
//    MPI_Allreduce(MPI_IN_PLACE,FBuffer,nsend,MFLOAT,MPI_MAX,MPI_COMM_WORLD);
//    //Set them
//    for(i=0;i<nsend;i++)
//    {
//      Nodes[offset+i].hmax = FBuffer[i];
//    }
//    offset+=nsend;
//    left -= nsend;
//  }
//}
//
//void walk_hmax(struct node* cell,int aunty)
//{
//  struct node* mummy;
//  int i;
//  int nothing=0;
//  if(cell->flag & NODE_LEAF)
//  {
//    for(i=0;i<cell->count;i++)
//    {
//      if(G[cell->daughter+i].hsml>cell->hmax)
//      {
//        cell->hmax = G[cell->daughter+i].hsml;
//      }
//    }
//  }
//  else if(((cell->flag & NODE_OWNER)==Task) || (cell->flag & NODE_META))
//  {
//    walk_hmax(Nodes + cell->daughter,cell->sister);
//  }
//  else
//  {
//    nothing=1;
//  }
//  //Show the result to mother
//  if(cell->mother!=-1 && !nothing)
//  {
//    mummy = Nodes + cell->mother;
//    if(cell->hmax > mummy->hmax)
//      mummy->hmax=cell->hmax;
//  }
//  //Do we have to do another cell?
//  if(cell->sister!=aunty)
//  {
//    if(cell->sister==Nodes->sister)
//    {
//      //Should never happen
//      printf("[%d] I've made a huge mistake...\n",Task);
//      kill(908);
//    }
//    walk_hmax(Nodes+cell->sister,aunty);
//  }
//  //Hooray, finished this level!
//}
