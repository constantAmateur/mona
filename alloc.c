/*
 * =====================================================================================
 *
 *       Filename:  alloc.c
 *
 *    Description:  Contains routines for creating and freeing global blocks of memory
 *                  used elsewhere in the simulation. Also assigns various constants
 *                  that specify the size of the buffer to different types.
 *
 *        Version:  0.6.3
 *        Created:  25/07/13 10:39:40
 *
 *         Author:  Matthew D. Young, my304@ast.cam.ac.uk
 *        Company:  Institute of Astronomy, Cambridge, UK
 *        License:  GNU General Public License
 *      Copyright:  Copyright (c) 2014, Matthew D. Young
 *
 * =====================================================================================
 */

#include "variables.h"
#include "functions.h"

/* 
 * ===  FUNCTION  ======================================================================
 *  Description:  Frees all allocated memory.  Called before shut down.
 * =====================================================================================
 */
void freedom(void)
{
  int i;
  for(i=0;i<MAX_NO_PART_TYPES;i++)
    dealloc_particles(i);
  dealloc_tree();
  dealloc_buffers();
  dealloc_misc();
}


/* 
 * ===  FUNCTION  ======================================================================
 *  Description:  Allocate memory for anything not else not assigned elsewhere.
 * =====================================================================================
 */
void alloc_misc(void)
{
  Export = malloc(NTask*sizeof(int));
  N_Send_Local = malloc(NTask*sizeof(int));
  DomainWalls = malloc(sizeof(double)*(NTask+1));
  NullMask = malloc(MaskSize);
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Description:  Dual function to alloc_misc.  Frees any memory allocated by it.
 * =====================================================================================
 */
void dealloc_misc(void)
{
  free(Export);
  free(N_Send_Local);
  free(DomainWalls);
  free(NullMask);
}


/* 
 * ===  FUNCTION  ======================================================================
 *  Description:  Allocate memory for the communication/construction/IO buffer and 
 *                the neighbour buffer.  Additionally set all pointers of different
 *                types to the newly allocated block of memory.  Calculate globals
 *                that tell us how many elements of various types fit in the buffer.
 * =====================================================================================
 */
void alloc_buffers(void)
{
  size_t total;
  size_t bytes;
  //void* tmp;
  if(!(Buffer = malloc(bytes = Params.buffer_size*1024*1024)))
  {
    pprintf("Failed to allocate %g MiB of memory for the buffer.\n",bytes/(1024.0*1024.0));
    kill(ERROR_MEM);
  }
  //Set information about the buffer and point pointers of various types
  //at our newly allocated memory
  BufferSizeBytes=bytes;
  BufferSizeGas=bytes/sizeof(struct gas);
  BufferSizeInts = bytes/sizeof(int);
  BufferSizeTRs = bytes/sizeof(unsigned long long);
  BufferSizeFloats = bytes/sizeof(FLOAT);
  //Setup the three communication buffers for SPH loops
  BufferSizeDen = bytes/(2*sizeof(union density_comm));
  DenSend = (union density_comm *) Buffer;
  DenRecv = (union density_comm *) Buffer;
  DenRecv += BufferSizeDen;
  BufferSizeHydro = bytes/(2*sizeof(union hydro_comm));
  HydroSend = (union hydro_comm *) Buffer;
  HydroRecv = (union hydro_comm *) Buffer;
  HydroBuffer = (union hydro_comm *) Buffer;
  HydroRecv += BufferSizeHydro;
  HydroBuffer += 2*BufferSizeHydro;
  BufferSizeVisc = bytes/(2*sizeof(union visc_comm));
  ViscSend = (union visc_comm *) Buffer;
  ViscRecv = (union visc_comm *) Buffer;
  ViscRecv += BufferSizeVisc;
  //For sending just coordinates
  Coords = (struct coords *) Buffer;
  BufferSizeCoord = bytes / sizeof(struct coords);
  //Lots of grav communication constructs
  //This one is just for N^2 tests
  BufferSizeGravSlow = bytes / (2*sizeof(struct slow_grav_comm));
  GravSlowSend = (struct slow_grav_comm *) Buffer;
  GravSlowRecv = (struct slow_grav_comm *) Buffer;
  GravSlowRecv += BufferSizeGravSlow;
  //For gravity we're going to be dealing with things directly in bytes
  //for the most part
  HalfBufferSizeBytes = bytes/2;
  BufferSizeGrav = bytes/ (2*sizeof(union grav_comm));
  GravSend = (union grav_comm *) Buffer;
  GravRecv = (union grav_comm *) Buffer;
  GravRecv += BufferSizeGrav;
  //GravCoeffSend = (struct grav_coeff_comm *) Buffer;
  //tmp = Buffer+HalfBufferSizeBytes;
  //GravRecv = (union grav_comm *) tmp;
  //GravCoeffRecv = (struct grav_coeff_comm *) tmp;
  //Some general buffers we use in places
  GBuffer = Buffer;
  IBuffer = Buffer;
  TRBuffer = Buffer;
  FBuffer = Buffer;
  total=bytes;
  //Allocate a buffer for neighbour searches
  if(!(Ngbs = malloc(bytes = Params.ngb_buffer_size*1024*1024)))
  {
    pprintf("Failed to allocate %g MiB of memory for the neighbour search buffer.\n",bytes/(1024.0*1024.0));
    kill(ERROR_MEM);
  }
  total += bytes;
  NgbBufferSizeInts = bytes / sizeof(int);
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Description:  Free memory created by alloc_buffers
 * =====================================================================================
 */
void dealloc_buffers(void)
{
  free(Buffer);
  free(Ngbs);
}


/* 
 * ===  FUNCTION  ======================================================================
 *  Description:  Allocate memory for the tree
 * =====================================================================================
 */
void alloc_tree(void)
{
  size_t bytes;
  if(!(Root = malloc(bytes = (int) sizeof(struct node) * (Params.tree_fraction * NPartWorld[GAS]))))
  {
    pprintf("Failed to allocate %g MiB of memory for nodes.\n",bytes/(1024.0*1024.0));
    kill(ERROR_MEM);
  }
  TreeSize=bytes/sizeof(struct node);
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Description:  Free memory for the tree
 * =====================================================================================
 */
void dealloc_tree(void)
{
  //Because nodes may allocate memory of their own.
  destroy_tree();
  free(Root);
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Description:  Allocate memory for particles of a given type.  Also allocate space
 *                for any shadow domains that are required.
 * =====================================================================================
 */
void alloc_particles(enum part_type type)
{
  size_t bytes;
  int ndown,nup,i;
  int tmp[NTask];
  int all[NTask*NTask];
  int lshadows[NTask];
  int count,j;
  switch(type)
  {
    case GAS:
      //These should be evenly distributed across all
      //processors.
      if(!(G = malloc(bytes = (int) NPartWorld[GAS] * sizeof(struct gas) * (Params.particle_excess_fraction / NTask) )))
      {
        pprintf("Failed to allocate %g MiB of memory for gas particles.\n",bytes/(1024.0*1024.0));
        kill(ERROR_MEM);
      }
      MaxGas = bytes/sizeof(struct gas);
      NGas = malloc(sizeof(int)*NTask);
      //Ideally, we'd put this many up/down of current
      ndown=(Params.nshadows/2);
      nup = Params.nshadows-ndown;
      //But if we can't fit all them below, add them at the top
      if(ndown>Task)
      {
        nup += ndown - Task;
        ndown = Task;
      }
      //If we can't fit them at the top, add them at the bottom
      if(nup > (NTask-1-Task))
      {
        ndown += nup - (NTask-1-Task);
        nup = NTask-1-Task;
      }
      //If we've still got an excess at the bottom, we're storing it all, so cut them off
      if(ndown>Task)
        ndown=Task;
      //NShadows <= Params.nshadows
      NShadows = ndown+nup;
      //Allocate trivial space for shadows
      Shadows = malloc(NTask * sizeof(struct shadow));
      //Now allocate the bulk space
      for(i=0;i<NTask;i++)
      {
        //Is it one of the ones to allocate space for?
        if((i>=Task-ndown) && (i!=Task) && (i<=Task+nup))
        {
          if(!(Shadows[i].G = malloc(bytes = (int) NPartWorld[GAS] * sizeof(struct gas) * (Params.particle_excess_fraction / NTask) )))
          {
            pprintf("Failed to allocate %g MiB of memory for shadow gas particles.\n",bytes/(1024.0*1024.0));
            kill(ERROR_MEM);
          }
          lshadows[i]=1;
        }
        else if(i==Task)
        {
          //Just an alias for G
          Shadows[i].G = G;
          lshadows[i]=1;
        }
        else
        {
          //Nothing to see here...
          Shadows[i].G = NULL;
          lshadows[i]=0;
        }
      }
      //Make special communicators for communicating amongst shadow domains
      MPI_Allgather(lshadows,NTask,MPI_INT,&all,NTask,MPI_INT,MPI_COMM_WORLD);
      MPI_Comm_group(MPI_COMM_WORLD,&World);
      for(i=0;i<NTask;i++)
      {
        lshadows[i]=all[i*NTask+Task];
        count=0;
        for(j=0;j<NTask;j++)
        {
          if(all[j*NTask+i])
          {
            tmp[count]=j;
            count++;
          }
        }
        MPI_Group_incl(World,count,tmp,&Shadows[i].group);
        MPI_Comm_create(MPI_COMM_WORLD,Shadows[i].group,&Shadows[i].comm);
        MPI_Group_translate_ranks(World,1,&i,Shadows[i].group,&Shadows[i].owner_rank_in_comm);
      }
      break;
    case SINK:
      //Sinks should be stored on all processors, 
      //with processing happening on all of them
      if(!(S = malloc(bytes = NPartWorld[SINK] * sizeof(struct sink))))
      {
        pprintf("Failed to allocate %g MiB of memory for sink particles.\n",bytes/(1024.0*1024.0));
        kill(ERROR_MEM);
      }
      break;
  }
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Description:  Free memory for particles of specified type
 * =====================================================================================
 */
void dealloc_particles(enum part_type type)
{
  int i;
  switch(type)
  {
    case GAS:
      for(i=0;i<NTask;i++)
      {
        if(i!=Task && Shadows[i].G)
        {
          free(Shadows[i].G);
        }
      }
      free(G);
      free(NGas);
      break;
    case SINK:
      free(S);
      break;
  }
}
