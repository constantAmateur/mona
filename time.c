/*
 * =====================================================================================
 *
 *       Filename:  time.c
 *
 *    Description:  Functions to do with time evolution.
 *
 *        Version:  0.6.3
 *        Created:  19/08/13 13:55:57
 *
 *         Author:  Matthew D. Young, my304@ast.cam.ac.uk
 *        Company:  Institute of Astronomy, Cambridge, UK
 *        License:  GNU General Public License
 *      Copyright:  Copyright (c) 2014, Matthew D. Young
 *
 * =====================================================================================
 */

#include "variables.h"
#include "functions.h"


void drift_particles(double dt)
{
  int i;
  for(i=0;i<NPart[GAS];i++)
  {
    G[i].x += dt * G[i].vx;
    G[i].y += dt * G[i].vy;
    G[i].z += dt * G[i].vz;
    //Drift the estimates of h as well...
    G[i].hsml *= exp((dt*G[i].divv) / NDIM);
    if(G[i].hsml==0)
    {
      printf("[%d] dt=%g,divv=%g,i=%d.\n",Task,dt,G[i].divv,i);
      kill(333);
    }
    //if(G[i].hsml>1.0)
    //  printf("[%d] New h = %g is huge at %d.\n",Task,G[i].hsml,i);
  }
  for(i=0;i<NPart[SINK];i++)
  {
    S[i].x += dt * S[i].vx;
    S[i].y += dt * S[i].vy;
    S[i].z += dt * S[i].vz;
  }
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  predict_quantities
 *  Description:  Predict values of quantities at a time dt in the future.
 * =====================================================================================
 */
void predict_quantities(double dt)
{
  int i;
  for(i=0;i<NPart[GAS];i++)
  {
    G[i].vpredx = G[i].vx + dt*G[i].ax;
    G[i].vpredy = G[i].vy + dt*G[i].ay;
    G[i].vpredz = G[i].vz + dt*G[i].az;
    G[i].Kpred = G[i].K + dt*G[i].dK;
  }
}

void kick_particles(double dt)
{
  int i;
  for(i=0;i<NPart[GAS];i++)
  {
    G[i].vx += dt*G[i].ax;
    G[i].vy += dt*G[i].ay;
    G[i].vz += dt*G[i].az;
    G[i].K += dt*G[i].dK;
  }
  for(i=0;i<NPart[SINK];i++)
  {
    S[i].vx += dt * S[i].ax;
    S[i].vy += dt * S[i].ay;
    S[i].vz += dt * S[i].az;
  }
}

void compute_timestep(void)
{
  double dt,dt_glob,dt_accel,dt_cour;
  double ac;
  int i;
#if HYDRO>=1
  double cs;
#endif
  dt = Params.max_tstep;
  for(i=0;i<NPart[GAS];i++)
  {
    ac = sqrt (G[i].ax*G[i].ax+G[i].ay*G[i].ay+G[i].az*G[i].az);
    if(ac==0)
      ac=1e-20;
    //Limitation based on pure acceleration
    //dt_accel = sqrt( Params.courant * G[i].hsml / ac);
    dt_accel = sqrt( .025 *Params.courant* G[i].hsml /ac);
    //Set this to do nothing in case we can't calculate it otherwise
    dt_cour = dt;
    //Artificial viscosity limiter
#if HYDRO==3
    dt_cour = Params.courant * G[i].hsml / G[i].vsig;
#endif
#if HYDRO>=1
    cs = sqrt(Params.gamma * G[i].pressure / G[i].density);
    if((Params.courant*G[i].hsml/cs) < dt_cour) dt_cour = (Params.courant * G[i].hsml / cs);
#endif

    //printf("[%d] fact = %g dt_accel/cour = %g,%g pressure,density,ac,h = %g,%g,%g,%g.\n",Task,Params.courant,dt_accel,dt_cour,G[i].pressure,G[i].density,ac,G[i].hsml);
    if(dt==0)
      kill(ERROR_SANITY);
    if(dt_accel < dt) dt = dt_accel;
    if(dt_cour < dt) dt = dt_cour;
  }
  //printf("[%d] After gas particles dt=%g.\n",Task,dt);
  //The same business for the sinks
  for(i=0;i<NPart[SINK];i++)
  {
    ac = sqrt(S[i].ax * S[i].ax + S[i].ay*S[i].ay + S[i].az*S[i].az);
    if(ac==0)
      ac=1e-20;
    dt_accel = sqrt(.125*Params.courant * Params.sink_soft / ac);
    if(dt_accel < dt) dt = dt_accel;
  }
  //printf("[%d] After sink particles dt=%g.\n",Task,dt);
  MPI_Allreduce(&dt,&dt_glob,1,MPI_DOUBLE,MPI_MIN,MPI_COMM_WORLD);
  Dt = dt_glob;
}
