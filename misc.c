/*
 * =====================================================================================
 *
 *       Filename:  misc.c
 *
 *    Description:  Functions that don't fit in anywhere else.
 *
 *        Version:  0.6.3
 *        Created:  20/07/13 11:18:30
 *
 *         Author:  Matthew D. Young, my304@ast.cam.ac.uk
 *        Company:  Institute of Astronomy, Cambridge, UK
 *        License:  GNU General Public License
 *      Copyright:  Copyright (c) 2014, Matthew D. Young
 *
 * =====================================================================================
 */

#include "variables.h"
#include "functions.h"

int imax(int x, int y)
{
  if(x>y)
    return x;
  return y;
}

int imin(int x, int y)
{
  if(x<y)
    return x;
  return y;
}

int isort(const void* a,const void *b)
{
  return (*((int *) a)) - (*((int *)b));
}

int fsort(const void* a,const void *b)
{
  float fa = *(float*) a;
  float fb = *(float*) b;
  return (fa >fb) - (fa<fb);
}

int dsort(const void* a,const void *b)
{
  double fa = *(double*) a;
  double fb = *(double*) b;
  return (fa >fb) - (fa<fb);
}

int gsort_radius(const void* a,const void *b)
{
  //Cast it as a pointer to type gas, then evaluate the value, then get the member R
  if( (*(struct gas*) a).R < (*(struct gas *) b).R) return -1;
  if( (*(struct gas*) a).R == (*(struct gas *) b).R) return 0;
  return 1;
}

int gsort_phi(const void* a,const void *b)
{
  //Cast it as a pointer to type gas, then evaluate the value, then get the member R
  //FLOAT fa = (*(struct gas *) a).phi;
  //FLOAT fb = (*(struct gas *) b).phi;
  //return (fa >fb) - (fa<fb);
  if( (*(struct gas*) a).phi < (*(struct gas *) b).phi) return -1;
  if( (*(struct gas*) a).phi < (*(struct gas *) b).phi) return 0;
  return 1;
}

int gsort_z(const void* a,const void *b)
{
  if( (*(struct gas*) a).z < (*(struct gas *) b).z) return -1;
  if( (*(struct gas*) a).z < (*(struct gas *) b).z) return 0;
  return 1;
}


//Given a pointer to an index of G, sort by R
int domsort_radius(const void *a,const void *b)
{
  if( G[*((int*)a)].R < G[*((int*)b)].R) return -1;
  if( G[*((int*)a)].R == G[*((int*)b)].R) return 0;
  return 1;
}

//Given a pointer to an index of G, sort by phi
int domsort_phi(const void *a,const void *b)
{
  if( G[*((int*)a)].phi < G[*((int*)b)].phi) return -1;
  if( G[*((int*)a)].phi == G[*((int*)b)].phi) return 0;
  return 1;
}

//Given a pointer to an index of G, sort by z
int domsort_z(const void *a,const void *b)
{
  if( G[*((int*)a)].z < G[*((int*)b)].z) return -1;
  if( G[*((int*)a)].z == G[*((int*)b)].z) return 0;
  return 1;
}



/*
 *  This Quickselect routine is based on the algorithm described in
 *  "Numerical recipes in C", Second Edition,
 *  Cambridge University Press, 1992, Section 8.5, ISBN 0-521-43108-5
 *  This code by Nicolas Devillard - 1998. Public domain.
 */


//#define ELEM_SWAP(a,b) { FLOAT elem_type t=(a);(a)=(b);(b)=t; }
//
//FLOAT quick_select(FLOAT arr[], int n) 
//{
//    int low, high ;
//    int median;
//    int middle, ll, hh;
//
//    low = 0 ; high = n-1 ; median = (low + high) / 2;
//    for (;;) {
//        if (high <= low) /* One element only */
//            return arr[median] ;
//
//        if (high == low + 1) {  /* Two elements only */
//            if (arr[low] > arr[high])
//                ELEM_SWAP(arr[low], arr[high]) ;
//            return arr[median] ;
//        }
//
//    /* Find median of low, middle and high items; swap into position low */
//    middle = (low + high) / 2;
//    if (arr[middle] > arr[high])    ELEM_SWAP(arr[middle], arr[high]) ;
//    if (arr[low] > arr[high])       ELEM_SWAP(arr[low], arr[high]) ;
//    if (arr[middle] > arr[low])     ELEM_SWAP(arr[middle], arr[low]) ;
//
//    /* Swap low item (now in position middle) into position (low+1) */
//    ELEM_SWAP(arr[middle], arr[low+1]) ;
//
//    /* Nibble from each end towards middle, swapping items when stuck */
//    ll = low + 1;
//    hh = high;
//    for (;;) {
//        do ll++; while (arr[low] > arr[ll]) ;
//        do hh--; while (arr[hh]  > arr[low]) ;
//
//        if (hh < ll)
//        break;
//
//        ELEM_SWAP(arr[ll], arr[hh]) ;
//    }
//
//    /* Swap middle item (in position low) back into correct position */
//    ELEM_SWAP(arr[low], arr[hh]) ;
//
//    /* Re-set active partition */
//    if (hh <= median)
//        low = ll;
//        if (hh >= median)
//        high = hh - 1;
//    }
//}
//
//#undef ELEM_SWAP

void calc_conserved(void)
{
  double mom[3],amom[3],energy;
  int i;
  for(i=0;i<3;i++)
  {
    mom[i]=amom[i]=energy=0;
  }
  for(i=0;i<NPart[GAS];i++)
  {
    mom[0] += PcleMass * G[i].vx;
    mom[1] += PcleMass * G[i].vy;
    mom[2] += PcleMass * G[i].vz;
    amom[0] += PcleMass * (G[i].y*G[i].vz-G[i].z*G[i].vy);
    amom[1] += PcleMass * (G[i].z*G[i].vx-G[i].x*G[i].vz);
    amom[2] += PcleMass * (G[i].x*G[i].vy-G[i].y*G[i].vx);
    //Kinetic
    energy += 0.5 * PcleMass * (G[i].vx*G[i].vx+G[i].vy*G[i].vy+G[i].vz*G[i].vz);
    //Thermal
#if HYDRO>=1
    energy += (PcleMass*G[i].K*pow(G[i].density,Params.gamma-1)/(Params.gamma-1));
#else
    energy += PcleMass*G[i].K;
#endif
    //Potential (add half because of double counting)
    energy += 0.5 * PcleMass * G[i].pot;
  }
  mom[0] += AccreteMom[0];
  mom[1] += AccreteMom[1];
  mom[2] += AccreteMom[2];
  amom[0] += AccreteAngMom[0];
  amom[1] += AccreteAngMom[1];
  amom[2] += AccreteAngMom[2];
  energy += AccreteEnergy;
  MPI_Allreduce(mom,Mom,3,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  MPI_Allreduce(amom,AngMom,3,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  MPI_Allreduce(&energy,&Energy,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  for(i=0;i<NPart[SINK];i++)
  {
    Mom[0] += S[i].m*S[i].vx;
    Mom[1] += S[i].m*S[i].vy;
    Mom[2] += S[i].m*S[i].vz;
    AngMom[0] += S[i].m * (S[i].y*S[i].vz-S[i].z*S[i].vy);
    AngMom[1] += S[i].m * (S[i].z*S[i].vx-S[i].x*S[i].vz);
    AngMom[2] += S[i].m * (S[i].x*S[i].vy-S[i].y*S[i].vx);
    Energy += 0.5*S[i].m * (S[i].vx*S[i].vx+S[i].vy*S[i].vy+S[i].vz*S[i].vz);
    Energy += 0.5*S[i].m * S[i].pot;
  }
}

//Calculate the centre of mass of the system and store it in the meta object
void calc_com(void)
{
  double sum[3],sumTot[3],M;
  int i;
  //Calculate the sum on each processor
  sum[0]=sum[1]=sum[2]=0.0;
  for(i=0;i<NPart[GAS];i++)
  {
    sum[0]=sum[0]+G[i].x;
    sum[1]=sum[1]+G[i].y;
    sum[2]=sum[2]+G[i].z;
  }
  //printf("[%d] Local sum is (%g,%g,%g).\n",Task,sum[0],sum[1],sum[2]);
  //Sum over all processors
  MPI_Reduce(&sum[0],&sumTot[0],3,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
  if(Task==0)
  {
    //multiply by particle mass
    sumTot[0]*=PcleMass;
    sumTot[1]*=PcleMass;
    sumTot[2]*=PcleMass;
    //Now add the sinks
    M=NPartWorld[GAS]*PcleMass;
    for(i=0;i<NPart[SINK];i++)
    {
      sumTot[0]+=S[i].x*S[i].m;
      sumTot[1]+=S[i].y*S[i].m;
      sumTot[2]+=S[i].z*S[i].m;
      M+=S[i].m;
    }
    //Divide by total mass
    COM[0]=sumTot[0]/M;
    COM[1]=sumTot[1]/M;
    COM[2]=sumTot[2]/M;
  }
  MPI_Bcast(&COM, 3*sizeof(double), MPI_BYTE, 0, MPI_COMM_WORLD);
}

void calc_derived_coordinates(void)
{
  int i;
  double phi;
  calc_com();
  gpprintf("Center of mass is now (%g,%g,%g).\n",COM[0],COM[1],COM[2]);
  for(i=0;i<NPart[GAS];i++)
  {
    G[i].R=sqrt((G[i].x-COM[0])*(G[i].x-COM[0])+(G[i].y-COM[1])*(G[i].y-COM[1]));
    phi=atan2((G[i].y-COM[1]),(G[i].x-COM[0]));
    if(phi<0)
      phi=phi+PI2;
    G[i].phi=phi;
  }
  //Calculate total momentum, angular momentum and energy
  //calc_conserved();
}

//SPH kernel (excluding factor of h^-NDIM so it's a function of q only)
double kernel(double q)
{
  if( q < 1.0 )
  {
    return NORM_CONST*(1+.75*q*q*(q-2));
  }
  else if( q < 2.0 ) 
  {
    q=(2.0-q);
    return NORM_CONST*.25*q*q*q;
  }
  else
  {
    return 0.0;
  }
}

double dkernel(double q)
{
  //Includes normalisation constant
  if( q < 1.0 ) return NORM_CONST*.75 * q * (3*q-4);
  if( q < 2.0 ) return NORM_CONST*-.75* (q-2)*(q-2);
  return 0.0;
}

//Only prints on process 0.  Prepends [Global] 
void gpprintf(const char *format,...)
{
  char buffer[MAX_MSG_SIZE] = "";

  va_list args;
  if(Task)
    return;
  va_start(args,format);
  vsnprintf(buffer,MAX_MSG_SIZE,format,args);
  va_end(args);

  printf("[Global] %s",buffer);
}
//A parallel print function.  Just prepends [%d] to the start...
void pprintf(const char *format,...)
{
  char buffer[MAX_MSG_SIZE] = "";

  va_list args;

  va_start(args,format);
  vsnprintf(buffer,MAX_MSG_SIZE,format,args);
  va_end(args);

  printf("[%d] %s",Task,buffer);
}

//An ordered version of the parallel print function.  Ensures that 
//all prints are done in ascending process order using global blocking
void opprintf(const char *format,...)
{
  char buffer[MAX_MSG_SIZE] = "";
  int i;

  va_list args;

  va_start(args,format);
  vsnprintf(buffer,MAX_MSG_SIZE,format,args);

  for(i=0;i<NTask;i++)
  {
    MPI_Barrier(MPI_COMM_WORLD);
    if(i==Task)
    {
      pprintf("%s",buffer);
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }
}

//Should only be used with the Accel Datatype
//void kick_reduce(struct gas *in,struct gas *inout,int *len, MPI_Datatype *dtype)
//{
//  int i;
//  for(i=0;i<*len;i++)
//  {
//    //printf("in/inout[%d] A,K,vsig = (%g/%g,%g/%g,%g/%g),%g/%g,%g/%g.\n",i,
//    //    in[sizeof(FLOAT)*(i*5+0)],inout[sizeof(FLOAT)*(i*5+0)],
//    //    in[sizeof(FLOAT)*(i*5+1)],inout[sizeof(FLOAT)*(i*5+1)],
//    //    in[sizeof(FLOAT)*(i*5+2)],inout[sizeof(FLOAT)*(i*5+2)],
//    //    in[sizeof(FLOAT)*(i*5+3)],inout[sizeof(FLOAT)*(i*5+3)],
//    //    in[sizeof(FLOAT)*(i*5+4)],inout[sizeof(FLOAT)*(i*5+4)]);
//    //printf("in/inout[%d] A,K,vsig = (%g/%g,%g/%g,%g/%g),%g/%g,%g/%g.\n",i,in[i].ax,inout[i].ax,in[i].ay,inout[i].ay,in[i].az,inout[i].az,in[i].K,inout[i].K,in[i].vsig,inout[i].vsig);
//    inout->ax = inout->ax + in->ax;
//    inout->ay = inout->ay + in->ay;
//    inout->az = inout->az + in->az;
//    inout->K = inout->K + in->K;
//    if(in->vsig > inout->vsig)
//      inout->vsig = in->vsig;
//    in++;
//    inout++;
//    //inout[i].ax += in[i].ax;
//    //inout[i].ay += in[i].ay;
//    //inout[i].az += in[i].az;
//    //inout[i].K += in[i].K;
//    //if(in[i].vsig > inout[i].vsig)
//    //  inout[i].vsig = in[i].vsig;
//  }
//}
//

int get_bit(int i,char *array)
{
  return (int) (array[i/CHAR_BIT] & (1 << (i%CHAR_BIT)));
}

void set_bit(int i,char *array)
{
  array[i/CHAR_BIT] |= (1 <<(i%CHAR_BIT));
}

void ref_limits(void)
{
  int avail_bits;
  int max_pcle;
  int needed_bits;
  MaxLVL=0;
  //How many bits do we have available?
  avail_bits=sizeof(TREE_REF)*CHAR_BIT;
  while(avail_bits>=needed_bits_tree(MaxLVL)) ++MaxLVL;
  MaxLVL--;
  MaxLID = 1;
  MaxLID <<= 3*MaxLVL;
  //Basically need to be able to store from 0 to the total number of particles, times the
  //excess fraction, with a little give because there might be a gap at the end of each
  //processor
  avail_bits=sizeof(PCLE_REF)*CHAR_BIT;
  max_pcle = (int) (NPartWorld[GAS]* Params.particle_excess_fraction);
  //1 bit for sign, 2 bits for minimum task and within task id and task id,
  //1 bit for worst-case splitting of particles between levels
  needed_bits=4;
  while (max_pcle >>=1) ++needed_bits;
  if(needed_bits > avail_bits)
  {
    gpprintf("Particle reference type is %d bits long, but we require at least %d bits to construct valid references to the specified processor number.\n\t  Either reduce the number of gas particles from %d, reduce the particle excess fraction from %g or use a larger type for the particle reference type PCLE_REF in variables.h\n",avail_bits,needed_bits,NPartWorld[GAS],Params.particle_excess_fraction);
    kill(ERROR_SETUP);
  }
  MaxID = 1;
  MaxID <<= avail_bits-4;
}

//How many bits do we need to store a tree to level n
int needed_bits_tree(int n)
{
  int targetlevel = 3*n+1;
  while (n >>= 1) ++targetlevel;
  return targetlevel;
}

