/* alloc.c */
void freedom(void);
void alloc_misc(void);
void dealloc_misc(void);
void alloc_buffers(void);
void dealloc_buffers(void);
void alloc_tree(void);
void dealloc_tree(void);
void alloc_particles(enum part_type type);
void dealloc_particles(enum part_type type);
/* comm.c */
void ngb_search(void *send, void *recv, size_t object_size, int buffer_size, void (*work_function)(void *, void *, int), void (*export_return)(void *), int (*compar)(const void *, const void *));
void ngb_search_async(void *send, void *recv, void *buffer, size_t object_size, int buffer_size, void (*work_function)(void *, void *, int), void (*export_return)(void *), int (*compar)(const void *, const void *));
/* core.c */
int main(int argc, char **argv);
void position_dependent_accel(void);
void velocity_dependent_accel(void);
/* density.c */
void density(void);
void density_eval(void);
void density_export_return(void *data);
void density_ngbs(void *index, void *export, int mode);
int den_buffer_sort(const void *a, const void *b);
/* domain.c */
void domain_decomp(void);
int sort_balancer(const void *a, const void *b);
void drift_domain_walls(void);
void exchange_particles(void);
void cast_shadows(void);
void cast_pressure_shadows(void);
void cast_density_shadows(void);
void cast_hydro_shadows(void);
void cast_kicked_shadows(void);
void gather_costs(void);
void gather_acceleration_shadows(void);
void gather_shadows_for_kick(void);
void update_counters(void);
void add_remove_particles(void);
int arrange_for_comm(const void *a, const void *b);
int sort_by_id(const void *a, const void *b);
/* gravity.c */
void gravity(void);
void self_gravity(void);
void gravity_export_return(void *data);
void gravity_eval(void *index, void *export, int mode);
void calc_grav_props(void);
double set_rmax(struct node *cnode);
void set_com(struct node *cnode);
double get_rmax(struct node *cell);
void direct_com(struct node *cell, double com[3]);
void exact_gas(void);
void self_gravity_slow(void);
void sink_gravity(void);
double softened_grav(double r, double h);
double softened_pot(double r, double h);
double dphi_dh(double q);
double deriv_grav_greens_0(double R);
double deriv_grav_greens_1(double R);
double deriv_grav_greens_2(double R);
double deriv_grav_greens_3(double R);
int grav_buffer_sort(const void *a, const void *b);
/* hydro.c */
void hydro(void);
void hydro_export_return(void *data);
void hydro_ngbs(void *index, void *export, int mode);
int hydro_buffer_sort(const void *a, const void *b);
void calc_pressure(void);
double eos(double density, double K);
/* io.c */
void write_snap(int snap_number);
void read_ic(void);
hid_t get_native_type(hid_t dtype);
void read_parameters(char *params);
enum part_type type_name_to_number(char *buff);
enum part_prop property_name_to_number(char *buff);
int prop_map(enum part_prop prop);
int part_map(enum part_type type);
/* kill.c */
void kill(int error_code);
/* misc.c */
int imax(int x, int y);
int imin(int x, int y);
int isort(const void *a, const void *b);
int fsort(const void *a, const void *b);
int dsort(const void *a, const void *b);
int gsort_radius(const void *a, const void *b);
int gsort_phi(const void *a, const void *b);
int gsort_z(const void *a, const void *b);
int domsort_radius(const void *a, const void *b);
int domsort_phi(const void *a, const void *b);
int domsort_z(const void *a, const void *b);
void calc_conserved(void);
void calc_com(void);
void calc_derived_coordinates(void);
double kernel(double q);
double dkernel(double q);
void gpprintf(const char *format, ...);
void pprintf(const char *format, ...);
void opprintf(const char *format, ...);
int get_bit(int i, char *array);
void set_bit(int i, char *array);
void ref_limits(void);
int needed_bits_tree(int n);
/* setup.c */
void startup(void);
int output_array_sort(const void *a, const void *b);
void initialise_variables(void);
int nlocal_particles(enum part_type type);
void prop_to_buffer(enum part_type type, enum part_prop prop, int offset, int count);
int store_prop(enum part_type type, enum part_prop prop, int offset, int count, int mode);
/* tests.c */
int test_valid_decomp(int verbose);
int test_ngb_finder(int ntest, double h, int verbose);
int count_ngbs_fast(double point[3], double d, int extra);
int count_ngbs_slow(double point[3], double d, int extra);
int test_tree_build(int verbose);
/* time.c */
void drift_particles(double dt);
void predict_quantities(double dt);
void kick_particles(double dt);
void compute_timestep(void);
/* tree.c */
void destroy_tree(void);
void set_bounding_box(void);
void build_tree(void);
void print_tree(struct node *cnode);
void mergeTree(void);
int lvl_lid_sort(const void *a, const void *b);
void refineTree(struct node *parent);
int sortLeafTable(const void *a, const void *b);
void splitter(int offset, int count, int axis, double split, int *counts);
void build_base(struct node *parent);
int find_ngbs(double point[3], double d, int *startnode, int strong_locality);
/* variables.c */
/* viscosity.c */
void viscosity(void);
void viscosity_export_return(void *data);
void visc_ngbs(void *index, void *export, int mode);
int visc_buffer_sort(const void *a, const void *b);
