/*
 * =====================================================================================
 *
 *       Filename:  io.c
 *
 *    Description:  Functions concerned with reading in and outputting infromation
 *
 *        Version:  0.6.3
 *        Created:  12/07/13 14:42:32
 *
 *         Author:  Matthew D. Young, my304@ast.cam.ac.uk
 *        Company:  Institute of Astronomy, Cambridge, UK
 *        License:  GNU General Public License
 *      Copyright:  Copyright (c) 2014, Matthew D. Young
 *
 * =====================================================================================
 */

#include "variables.h"
#include "functions.h"



//Writes the current state of the system to file
//Although this only produces one file, it happens 
//in parallel (like magic!)
void write_snap(int snap_number)
{
  hid_t file_id;
  hid_t group_id;
  hid_t plist_id;
  hid_t filespace,memspace;
  hid_t dset_id;
  hid_t attr_id;
  hsize_t count[1];
  hsize_t offset[1];
  hsize_t dimsf[1];
  hsize_t dimsa[] = {1};
  //This must be FLOAT not double because it is a pointer to the actual
  //particle data, the only things that are precision variable
  FLOAT *fdata;
  int *idata;
  int i,j,new;
  int prop_idx;
  int nparts[NTask];
  int glob_left,left;
  enum part_prop prop;
  enum part_type type;
  char buff[300];
  group_id=0;

  //Create the file in parallel
  plist_id = H5Pcreate(H5P_FILE_ACCESS);
  //The magical commands to set up parallel write
  H5Pset_fapl_mpio(plist_id, MPI_COMM_WORLD, MPI_INFO_NULL);
  //Make a new file globally
  sprintf(buff,"output_%04d",snap_number);
  gpprintf("Writing snapshot %s.\n",buff);
  file_id = H5Fcreate(buff,H5F_ACC_TRUNC,H5P_DEFAULT,plist_id);
  H5Pclose(plist_id);
  //Just a guess, won't matter if it's wrong
  type=GAS;
  new=1;
  for(i=0;i<Output_num;i++)
  {
    if(Output[i].type!=type)
    {
      type=Output[i].type;
      new=1;
    }
    //If we're starting a new group, create the relevant stuff
    if(new)
    {
      //Don't want to close anything the first time around...
      if(i!=0)
      {
        H5Gclose(group_id);
      }
      group_id=H5Gcreate(file_id,IO_particles[part_map(type)].name,H5P_DEFAULT);
      //Add the mass attribute if it's gas
      if(type == GAS)
      {
        //Create gas attributes
        filespace = H5Screate_simple(1,dimsa,NULL);
        attr_id = H5Acreate(group_id,"Mass",H5T_NATIVE_DOUBLE,filespace,H5P_DEFAULT);
        H5Awrite(attr_id,H5T_NATIVE_DOUBLE,&PcleMass);
        H5Aclose(attr_id);
        H5Sclose(filespace);
      }
      //Need this to get the offset write
      MPI_Allgather(&NPart[type],1,MPI_INT,nparts,1,MPI_INT,MPI_COMM_WORLD);
      dimsf[0] = NPartWorld[type];
      //printf("[%d] Preparing to write out %d particles of type %s.\n",Task,(int) dimsf[0],IO_particles[part_map(type)].name);
      new=0;
    }
    //Now write the property we're currently inspecting...
    prop = Output[i].prop;
    prop_idx = prop_map(prop);
    offset[0]=0;
    for(j=0;j<Task;j++)
    {
      offset[0]+=nparts[j];
    }
    count[0] = NPart[type];

    filespace = H5Screate_simple(1,dimsf,NULL);
    dset_id = H5Dcreate(group_id,IO_properties[prop_idx].name,IO_properties[prop_idx].hdf_type,filespace,H5P_DEFAULT);
    H5Sclose(filespace);
    fdata = Buffer;
    idata = Buffer;
    left = NPart[type];
    //Because we use collective write, all processors must traverse
    //the loop and call H5DWrite, even when they have nothing to write
    if(type==SINK && Task)
    {
      left=0;
    }
    //Only write anything on process 0 for globally stored sinks
    filespace = H5Dget_space(dset_id);
    do
    {
      //How many of this type can we fit into the comm buffer?
      switch(IO_properties[prop_idx].type)
      {
        case TYPE_FLOAT:
          count[0] = imin(BufferSizeFloats,left);
          break;
        case TYPE_INT:
          count[0] = imin(BufferSizeInts,left);
          break;
        case TYPE_STRING:
          count[0] = imin(BufferSizeInts,left);
          break;
      }
      //Load the relevant property from the struct into the comm buffer
      prop_to_buffer(type,prop,NPart[type]-left,count[0]);
      //Define the local block
      if(!left)
      {
        //We have nothing to write, HDF5 needs its hand held in
        //this case
        memspace = H5Screate(H5S_NULL);
        H5Sselect_none(memspace);
        H5Sselect_none(filespace);
      }
      else
      {
        //Select the bit of memory to write to
        memspace = H5Screate_simple(1,count,NULL);
        //Select the part in the file to write to
        H5Sselect_hyperslab(filespace,H5S_SELECT_SET,offset,NULL,count,NULL);
      }

      //This is all that's needed to setup the parallel transfer.  Amazeballs!
      plist_id = H5Pcreate(H5P_DATASET_XFER);
      //Note that because we're using "Collective" all processors must participate
      //in this loop every time, even if it's to write nothing
      H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_COLLECTIVE);
      //The writing happens here
      switch(IO_properties[prop_idx].type)
      {
        case TYPE_FLOAT:
          H5Dwrite(dset_id,IO_properties[prop_idx].hdf_type, memspace,filespace,plist_id,fdata);
          break;
        case TYPE_INT:
          H5Dwrite(dset_id,IO_properties[prop_idx].hdf_type, memspace,filespace,plist_id,idata);
          break;
        case TYPE_STRING:
          H5Dwrite(dset_id,IO_properties[prop_idx].hdf_type, memspace,filespace,plist_id,idata);
          break;
      }
      //Prep next lot
      offset[0] += count[0];
      left -= count[0];
      //Because we need to keep in this loop until all processors are done writing...
      MPI_Allreduce(&left,&glob_left,1,MPI_INT,MPI_MAX,MPI_COMM_WORLD);
      //Close off things
      H5Sclose(memspace);
      H5Pclose(plist_id);
    }while(glob_left);
    //Close the dataset and filespace
    H5Dclose(dset_id);
    H5Sclose(filespace);
  }
  //Close the last group if we've opened any
  if(Output_num)
    H5Gclose(group_id);
  //Close the file
  H5Fclose(file_id);

}

//Each processor independently opens the input file (in read only mode) and reads
//in its portion of the file
void read_ic(void)
{
  hid_t fh, ptype, dset, dtype,fspace,mspace,attr;
  herr_t status;
  hsize_t mdims[2],moff[2],foff[2],count[2];
  H5G_info_t info,ptinfo;
  int nattr;
  size_t dsize;
  int npart,offset,moffset,nleft,nread;
  int i,j;
  char buff[300];
  enum part_type type;
  enum part_prop property;

  //printf("Preparing to open file %s\n",Params.Init_Cond_File);
  //Open the file for reading
  fh = H5Fopen(Params.init_cond,H5F_ACC_RDONLY, H5P_DEFAULT);
  //Check how many particle types there are
  status = H5Gget_info(fh,&info);
  //Loop through the particle types, checking their names as we go
  for(i=0;i<info.nlinks;i++)
  {
    //Get the particle name
    H5Lget_name_by_idx(fh,".",H5_INDEX_NAME,H5_ITER_INC,i,buff,sizeof(buff),H5P_DEFAULT);
    //Convert it to a number
    type = type_name_to_number(buff);
    //Do we want to be loading this one?
    //if(!load_types[type])
    //  continue;
    //printf("[%d] Loading particles: %s\n",Task,buff);
    //Open the group and get info about the children
    ptype=H5Gopen(fh,buff);
    //Read the mass into PcleMass if it exists
    nattr = H5Aget_num_attrs(ptype);
    for(j=0;j<nattr;j++)
    {
      attr = H5Aopen_idx(ptype,j);
      H5Aget_name(attr,300,buff);
      if(!strcmp(buff,"Mass"))
      {
        status = H5Aread(attr,H5T_NATIVE_DOUBLE,&PcleMass);
        if(status<0)
          kill(ERROR_IO);
        //printf("[%d] The per particle mass is %g.\n",Task,PcleMass);
      }
      status = H5Aclose(attr);
    }
    //status = H5Aget_info_by_name(ptype,"/","Mass",&ainfo,NULL);
    //if(status>=0)
    //  printf("Found property!!!\n");
    status = H5Gget_info(ptype,&ptinfo);
    //printf("[%d] There are %d properties to read.\n",Task, ptinfo.nlinks);
    //Loop over all the datasets for this type
    npart=0;
    for(j=0;j<ptinfo.nlinks;j++)
    {
      //Get dataset name
      H5Lget_name_by_idx(ptype,".",H5_INDEX_NAME,H5_ITER_INC,j,buff,sizeof(buff),H5P_DEFAULT);
      //printf("[%d] Link is called %s.\n",Task,buff);
      //Convert dataset name to property number
      property = property_name_to_number(buff);
      //printf("[%d] Considering property %s\n",Task,buff);
      //printf("[%d] Type is %d, property %d\n",Task,type,property);
      //Do we need to load this property into memory?
      if(store_prop(type,property,0,0,1))
      {
        //Open the dataset
        dset = H5Dopen(ptype,buff);
        //Determine its type
        dtype = H5Dget_type(dset);
        //Determine the size of the type
        dsize = H5Tget_size(dtype);
        //And how many of these objects we can fit in the buffer
        mdims[0] = BufferSizeBytes/dsize;
        mdims[1] = 1;
        //Get the dataspace for the file
        fspace = H5Dget_space(dset);
        //Create a dataspace for the memory (for the buffer)
        mspace = H5Screate_simple(1,mdims,NULL);
        //If this is the first property for this type, allocate memory and metadata
        if(!npart)
        {
          npart = H5Sget_simple_extent_npoints(fspace);
          //Store the meta-data
          NPartWorld[type]=npart;
          //Allocate the permanent memory
          alloc_particles(type);
        }
        else
        {
          //Check that we have the right number of data
          if(npart!=H5Sget_simple_extent_npoints(fspace))
          {
            kill(1);
          }
        }
        //The offset and the number of particles to store locally
        offset=nlocal_particles(type);
        //Always want to use up all the memory in the buffer
        moff[0]=0;
        moff[1]=0;
        //But can't always read the whole file in one go
        foff[0]=offset;
        foff[1]=0;
        nleft=NPart[type];
        //Everything is initialised, now read things one buffer width at a time
        moffset=0;
        while(nleft)
        {
          foff[0]=offset;
          //Can we fit everything in the buffer?
          nread=imin(nleft,mdims[0]);
          count[0]=nread;
          count[1]=1;
          //Select memory space
          status = H5Sselect_hyperslab(mspace, H5S_SELECT_SET,moff,NULL,count,NULL);
          //Select correct part of the file
          status = H5Sselect_hyperslab(fspace, H5S_SELECT_SET,foff,NULL,count,NULL);
          //Read it in
          //status = H5Dread(dset,H5Tget_class(dtype),fspace,mspace,H5P_DEFAULT,Buffer);
          //printf("[%d] About to try reading %d pcles with m-offset = %d and f-offset = %d,count=%d.  Part %d Prop %d\n",Task,nread,moffset,foff[0],count[0],type,property);
          //status = H5Dread(dset,IO_properties[prop_map(type)].hdf_type,mspace,fspace,H5P_DEFAULT,Buffer);
          status = H5Dread(dset,get_native_type(dtype),mspace,fspace,H5P_DEFAULT,Buffer);
          //Move it to the appropriate location
          //Confusingly named moffset just says how many particles to skip when
          //reading from the buffer into the internal storage
          store_prop(type,property,moffset,nread,0);
          //printf("Stored.\n");
          //Update the counters
          nleft-=nread;
          offset+=nread;
          moffset+=nread;
        }
        H5Sclose(fspace);
        H5Sclose(mspace);
        H5Dclose(dset);
      }
    }
    H5Gclose(ptype);
  }
  //Close the file
  H5Fclose(fh);
}

//Probes the dataset and determines the appropriate native type to attempt to read
//data as.
hid_t get_native_type(hid_t dtype)
{
  switch(H5Tget_class(dtype))
  {
    case H5T_INTEGER:
      return H5T_NATIVE_INT;
    case H5T_FLOAT:
      return HFLOAT;
    default:
      return HFLOAT;
  }
}

void read_parameters(char *params)
{
  FILE *fp;
  char line[MAX_STRING_LEN],key[MAX_STRING_LEN],value[MAX_STRING_LEN];
  //char keys[MAX_NO_PARAMS][MAX_STRING_LEN];
  //void *addrs[MAX_NO_PARAMS];
  //enum io_type types[MAX_NO_PARAMS];
  int found[MAX_NO_PARAMS];
  int i,j;
  int cnt=0,error=0;
  //Error codes
  //1 - Unknown parameter
  //2 - Duplicate parameter
  //3 - Parameter not found
  //4 - Couldn't open file
  //5 - Parameter has invalid value

  //Task 0 reads the actual file, other processors just receive from process 0
  if(Task==0)
  {
    //Copy the GADGET way of doing this.  keys is an array with the names of the 
    //keys (i.e., the thing that appears on the left column of the param file).
    //addr is an array of pointers to the location of a variable to store the value.
    //Finally, types is an array which says what the variable type is.
    //strcpy(keys[cnt], "Init_Cond_File");
    //addrs[cnt] = Params.init_cond;
    //types[cnt++] = TYPE_STRING;

    //strcpy(keys[cnt], "ParticleExcessFraction");
    //addrs[cnt] = &Params.particle_excess_fraction;
    //types[cnt++] = TYPE_FLOAT;

    //strcpy(keys[cnt], "TreeFraction");
    //addrs[cnt] = &Params.tree_fraction;
    //types[cnt++] = TYPE_FLOAT;

    //strcpy(keys[cnt], "DomainDelta");
    //addrs[cnt] = &Params.domain_delta;
    //types[cnt++] = TYPE_FLOAT;

    //strcpy(keys[cnt], "PclesPerNode");
    //addrs[cnt] = &Params.pcles_per_node;
    //types[cnt++] = TYPE_INT;

    //strcpy(keys[cnt], "BufferSize");
    //addrs[cnt] = &Params.buffer_size;
    //types[cnt++] = TYPE_INT;

    //strcpy(keys[cnt], "NgbBufferSize");
    //addrs[cnt] = &Params.ngb_buffer_size;
    //types[cnt++] = TYPE_INT;

    //strcpy(keys[cnt], "eta");
    //addrs[cnt] = &Params.eta;
    //types[cnt++] = TYPE_FLOAT;

    //strcpy(keys[cnt], "Tollerance");
    //addrs[cnt] = &Params.toll;
    //types[cnt++] = TYPE_FLOAT;

    //strcpy(keys[cnt], "NewtonsConstantG");
    //addrs[cnt] = &Params.G;
    //types[cnt++] = TYPE_FLOAT;

    //strcpy(keys[cnt], "GravSoftSink");
    //addrs[cnt] = &Params.sink_soft;
    //types[cnt++] = TYPE_FLOAT;

    //strcpy(keys[cnt], "RatioOfSpecificHeats");
    //addrs[cnt] = &Params.gamma;
    //types[cnt++] = TYPE_FLOAT;

    //strcpy(keys[cnt], "CourantFactor");
    //addrs[cnt] = &Params.courant;
    //types[cnt++] = TYPE_FLOAT;
    //cnt=0;
    cnt = ParameterDefinitions_num;
    //while(ParameterDefinitions[cnt].addr)
    //  cnt++;

    //Open the file, checking for failure
    if((fp = fopen(params,"r")))
    {
      //printf("Parameter file is %s\n",params);
      //Initialise found
      for(i=0;i<MAX_NO_PARAMS;i++)
      {
        found[i]=0;
      }
      //Loop through the file until we reach the end
      while(!feof(fp))
      {
        //Read one line into the line variable
        //When NULL, this is the last line and has already been read
        if(fgets(line,MAX_STRING_LEN,fp) == NULL)
          continue;
        //Ignore commented lines
        if(line[0] == '#')
          continue;
        //Store the key/value pair appropriately
        if(sscanf(line,"%s%s",key,value)<2)
          continue;
        //See if the key matches anything defined above
        for(i=0,j=-1;i<cnt;i++)
        {
          if(strcmp(key,ParameterDefinitions[i].key) == 0)
          {
            j = i;
            //Keep track of how many times this has been found
            found[i]++;
            if(found[i]>1)
            {
              gpprintf("Parameter '%s' multiply defined.\n",key);
              error = 2;
              //Skip to the next line now
              i=-1;
            }
            break;
          }
        }
        if(i<0)
          continue;
        //If we found something valid, store it
        if(j >= 0)
        {
          switch(ParameterDefinitions[j].type)
          {
            case TYPE_INT:
              *((int *) ParameterDefinitions[j].addr) = atoi(value);
              gpprintf("Read parameter %s as %d\n",key,*((int *) ParameterDefinitions[j].addr));
              break;
            case TYPE_FLOAT:
              *((double *) ParameterDefinitions[j].addr) = atof(value);
              gpprintf("Read parameter %s as %g\n",key,*((double *) ParameterDefinitions[j].addr));
              break;
            case TYPE_STRING:
              strcpy(ParameterDefinitions[j].addr,value);
              gpprintf("Read parameter %s as %s\n",key,value);
          }
        }
        else
        {
          gpprintf("Parameter '%s' not expected, so will be ignored.\n",key);
          error = 1;
        }
      }
      fclose(fp);
      //Make sure we read in all the parameters
      for(i=0; i<cnt;i++)
      {
        if(found[i]<1)
        {
          gpprintf("Parameter '%s' was expected, but not found.\n",ParameterDefinitions[i].key);
          error = 3;
        }
      }
    }
    //The file didn't open properly :(
    else
    {
      gpprintf("Parameter file %s couldn't be opened.\n",params);
      error = 4;
    }
    //Check the parameters have valid values
  }
  //Process 0 done reading, communicate it back
  MPI_Bcast(&error,1,MPI_INT,0,MPI_COMM_WORLD);

  //Error = 1 means unrecognised parameter, which is not terminal
  if(error>1)
  {
    kill(ERROR_SETUP);
  }
  //Now send the read data to everyone
  MPI_Bcast(&Params, sizeof(struct parameters),MPI_BYTE,0,MPI_COMM_WORLD);
}


enum part_type type_name_to_number(char *buff)
{
  int i;
  for(i=0;i<IO_particles_num;i++)
  {
    if(!strcmp(buff,IO_particles[i].name))
    {
      return i;
    }
  }
  return -1;
}

enum part_prop property_name_to_number(char *buff)
{
  int i;
  for(i=0;i<IO_properties_num;i++)
  {
    if(!strcmp(buff,IO_properties[i].name))
    {
      return i;
    }
  }
  return -1;
}

//Slow, but we don't have to do it often, so who cares...
int prop_map(enum part_prop prop)
{
  int i;
  for(i=0;i<IO_properties_num;i++)
  {
    if(prop == IO_properties[i].number)
      return i;
  }
  return -1;
}

int part_map(enum part_type type)
{
  int i;
  for(i=0;i<IO_particles_num;i++)
  {
    if(type == IO_particles[i].number)
      return i;
  }
  return -1;
}
