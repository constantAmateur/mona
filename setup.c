/*
 * =====================================================================================
 *
 *       Filename:  setup.c
 *
 *    Description:  Initialisation functions and information.  This is run first at 
 *                  start-up as the name implies.
 *
 *        Version:  0.6.3
 *        Created:  12/07/13 14:43:18
 *
 *         Author:  Matthew D. Young, my304@ast.cam.ac.uk
 *        Company:  Institute of Astronomy, Cambridge, UK
 *        License:  GNU General Public License
 *      Copyright:  Copyright (c) 2014, Matthew D. Young
 *
 * =====================================================================================
 */

#include "variables.h"
#include "functions.h"

/*
 * Parameter definition
 */

/* 
This defines which parameters are required for the simulation to begin.
The first entry is the name looked for in the parameter file.
The second entry is the location in memory to store the value read in.
The third entry is the variable type of the parameter.
*/

struct parameter_definitions ParameterDefinitions[] = {
  {"InitCondFile",Params.init_cond,TYPE_STRING},
  {"ParticleExcessFraction",&Params.particle_excess_fraction,TYPE_FLOAT},
  {"TreeFraction",&Params.tree_fraction,TYPE_FLOAT},
  {"PclesPerNode",&Params.pcles_per_node,TYPE_INT},
  {"BufferSize",&Params.buffer_size,TYPE_INT},
  {"NgbBufferSize",&Params.ngb_buffer_size,TYPE_INT},
  {"Eta",&Params.eta,TYPE_FLOAT},
  {"GravSoftSink",&Params.sink_soft,TYPE_FLOAT},
  {"GravSoftGas",&Params.gas_soft,TYPE_FLOAT},
  {"GravSoftReducedDims",&Params.dim_grav_soft,TYPE_FLOAT},
  {"Tollerance",&Params.toll,TYPE_FLOAT},
  {"NewtonsConstantG",&Params.G,TYPE_FLOAT},
  {"RatioOfSpecificHeats",&Params.gamma,TYPE_FLOAT},
  {"CourantFactor",&Params.courant,TYPE_FLOAT},
  {"MaxTimeStep",&Params.max_tstep,TYPE_FLOAT},
  {"AlphaSPH",&Params.alpha_sph,TYPE_FLOAT},
  {"GravityOpeningTheta",&Params.grav_opening,TYPE_FLOAT},
  {"CoolingBeta",&Params.beta_cool,TYPE_FLOAT},
  {"AccretionRadius",&Params.accretion_radius,TYPE_FLOAT},
  {"EscapeRadius",&Params.escape_radius,TYPE_FLOAT},
  {"StopTime",&Params.stop_time,TYPE_FLOAT},
  {"NumberShadowDomains",&Params.nshadows,TYPE_INT},
  {"OutputEveryNTimesteps",&Params.output_n_tsteps,TYPE_INT},
  {"OutputSeparation",&Params.output_delta,TYPE_FLOAT},

};

/*
 * IO_Labels
 */

/* 
These two blocks define information about reading in/writing out
particles and their properties to/from HDF5 files.  
The first entry is the internal symbol used to represent the 
  particle or property in the code.
The second entry is the name used to refer to this particle
  or property in the HDF5 file.
The IO_properties array has two extra entries, the variable type
internally and the HDF5 variable type.  The latter of these is 
set by a switch statement in the startup() function because of
weird issues with setting it at declaration.
*/

struct io_types IO_particles[] = {
  {GAS,"Gas"},
  {SINK,"Sink"}
};

struct io_props IO_properties[] = {
  {IO_ID,"ID",TYPE_INT},
  {IO_X,"X",TYPE_FLOAT},
  {IO_Y,"Y",TYPE_FLOAT},
  {IO_Z,"Z",TYPE_FLOAT},
  {IO_VEL_X,"Vel_X",TYPE_FLOAT},
  {IO_VEL_Y,"Vel_Y",TYPE_FLOAT},
  {IO_VEL_Z,"Vel_Z",TYPE_FLOAT},
  {IO_ACC_X,"Acc_X",TYPE_FLOAT},
  {IO_ACC_Y,"Acc_Y",TYPE_FLOAT},
  {IO_ACC_Z,"Acc_Z",TYPE_FLOAT},
  {IO_HSML,"Smoothing_Length",TYPE_FLOAT},
  {IO_DEN,"Density",TYPE_FLOAT},
  {IO_UINT,"Internal_Energy",TYPE_FLOAT},
  {IO_MASS,"Mass",TYPE_FLOAT}
};

/*
 * Output definition
 */

/*
This block tells the code which properties of which
particles to write to file when a snapshot is written.
All particle/property pairs listed here are written.
The first entry is a particle type.
The second entry is a particle property.
*/

struct output Output[] = {
  {GAS,IO_X},
  {GAS,IO_Y},
  {GAS,IO_Z},
  {GAS,IO_UINT},
  {GAS,IO_HSML},
  {GAS,IO_DEN},
  {SINK,IO_X},
  {SINK,IO_Y},
  {SINK,IO_Z},
  {SINK,IO_MASS}
};



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  startup
 *  Description:  Because the above arrays are defined at declaration here, we can only 
 *                use the sizeof function to determine their size within this file.  To 
 *                get around this, each array has an associated <NAME>_num global integer 
 *                which makes its length globally available.  We set these variables 
 *                here and also assign the HDF5 variable type for particle properties.
 *                Finally, any other declarations that are needed prior to memory 
 *                allocation appear here also.
 * =====================================================================================
 */
void startup(void)
{
  int i;
  //How many elements?
  ParameterDefinitions_num = sizeof(ParameterDefinitions)/sizeof(struct parameter_definitions);
  IO_particles_num = sizeof(IO_particles)/sizeof(struct io_types);
  IO_properties_num = sizeof(IO_properties)/sizeof(struct io_props);
  //Make sure the output array is sorted by type
  Output_num = sizeof(Output)/sizeof(struct output);
  qsort(Output,Output_num,sizeof(struct output),output_array_sort);
  //Populate the hdf type property, since it can't be set at initialisation for some
  //bizarre reason to do with constants I don't quite understand
  for(i=0;i<IO_properties_num;i++)
  {
    switch(IO_properties[i].type)
    {
      case TYPE_FLOAT:
        //HFLOAT is defined in variables.h and depends on if the DOUBLE flag is set
        IO_properties[i].hdf_type = HFLOAT;
        break;
      case TYPE_INT:
        IO_properties[i].hdf_type = H5T_NATIVE_INT;
        break;
      case TYPE_STRING:
        IO_properties[i].hdf_type = H5T_NATIVE_CHAR;
        break;
    }
  }
  //Need to specify this here before memory allocation
  //The size of the boolean array (in bytes) for task membership
  MaskSize = (NTask/CHAR_BIT)+1;
  //The gas communication data types
  //MPI_Datatype tmp;
  //MPI_Datatype types[5] = {MFLOAT,MFLOAT,MFLOAT,MFLOAT,MFLOAT};
  //int counts[5] = {1,1,1,1,1};
  //MPI_Aint disps[5];
  ////
  //disps[0] = offsetof(struct gas,density);
  //disps[1] = offsetof(struct gas,dhsml);
  //disps[2] = offsetof(struct gas,zeta);
  //disps[3] = offsetof(struct gas,pressure);
  //disps[4] = offsetof(struct gas,hsml);
  //MPI_Type_create_struct(5,counts,disps,types,&tmp);
  //MPI_Type_create_resized(tmp,0,sizeof(struct gas),&Density_props);
  //MPI_Type_commit(&Density_props);
  //MPI_Type_free(&tmp);
  ////
  //disps[0] = offsetof(struct gas,vx);
  //disps[1] = offsetof(struct gas,vy);
  //disps[2] = offsetof(struct gas,vz);
  //disps[3] = offsetof(struct gas,K);
  //MPI_Type_create_struct(4,counts,disps,types,&tmp);
  //MPI_Type_create_resized(tmp,0,sizeof(struct gas),&Kicked_props);
  //MPI_Type_commit(&Kicked_props);
  //MPI_Type_free(&tmp);
  ////
  //disps[0] = offsetof(struct gas,ax);
  //disps[1] = offsetof(struct gas,ay);
  //disps[2] = offsetof(struct gas,az);
  //disps[3] = offsetof(struct gas,dK);
  //disps[4] = offsetof(struct gas,vsig);
  //MPI_Type_create_struct(5,counts,disps,types,&tmp);
  //MPI_Type_create_resized(tmp,0,sizeof(struct gas),&Accel);
  //MPI_Type_commit(&Accel);
  //MPI_Type_free(&tmp);
  ////
  //disps[0] = offsetof(struct gas,divv);
  //MPI_Type_create_struct(1,counts,disps,types,&tmp);
  //MPI_Type_create_resized(tmp,0,sizeof(struct gas),&Hydro_props);
  //MPI_Type_commit(&Hydro_props);
  //MPI_Type_free(&tmp);
  //
  //MPI_Op_create(kick_reduce,1,&Kick_reduce);
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  output_array_sort
 *  Description:  Comparison function so that the output array is sorted by type.
 * =====================================================================================
 */
int output_array_sort(const void* a, const void* b)
{
  if( (*((struct output *) a)).type < (*((struct output *) b)).type) return -1;
  if( (*((struct output *) a)).type > (*((struct output *) b)).type) return 1;
  return 0;
}

void initialise_variables(void)
{
  int i;
  double zmean,zsd;
  //Allocate the bit masks
  for(i=0;i<MaskSize;i++)
    NullMask[i] = (NullMask[i] & 0);
  //memmove(ShadowMask,NullMask,MaskSize);
  //for(i=0;i<NTask;i++)
  //{
  //  if(Shadows[i].G)
  //  {
  //    set_bit(i,ShadowMask);
  //  }
  //}
  //Internally, we use the square of theta...
  Params.grav_opening*=Params.grav_opening;
  //Similarly, we only use the square of dim_grav_soft
  Params.dim_grav_soft *= Params.dim_grav_soft;
  //Work out the origin of our coordinate system (and any other time variable coordinates)
  calc_derived_coordinates();
  gpprintf("Centre of Mass initially at (%g,%g,%g).\n",COM[0],COM[1],COM[2]);
  for(i=0;i<NPart[SINK];i++)
  {
    gpprintf("Sink particle size %g at (%g,%g,%g).\n",S[i].m,S[i].x,S[i].y,S[i].z);
  }
  

  //Magical incantation to determine lowest power of 2>=NTask
  i=NTask;
  PTask=0;
  while(i>>=1)
    PTask++;
  PTask = NTask == (1<<(PTask)) ? NTask : 1<<(PTask+1);


  //Time 
  Timings = NullTimings;
  Timings.total = MPI_Wtime();
  Timings.domain=MPI_Wtime();
  //Decompose domain so that coincident particles are all on the one processor
  domain_decomp();
  exchange_particles();
  Timings.domain = MPI_Wtime() - Timings.domain;
  //Start time
  Time=0;
  //Set the accretion counters
  AccreteMom[0]=AccreteMom[1]=AccreteMom[2]=0;
  AccreteAngMom[0]=AccreteAngMom[1]=AccreteAngMom[2]=0;
  AccreteEnergy=0;
  //Set the maximum ID
  NextID = 0;
  for(i=0;i<MAX_NO_PART_TYPES;i++)
    NextID += NPartWorld[i];
  //Make a first guess at the SPH smoothing lengths
  zmean=0;
  zsd=0;
  for(i=0;i<NPart[GAS];i++)
    zmean += G[i].z;
  //These operations are needed to enforce strong processor number independence
  MPI_Allreduce(MPI_IN_PLACE,&zmean,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  zmean /= NPartWorld[GAS];
  for(i=0;i<NPart[GAS];i++)
    zsd += (G[i].z-zmean)*(G[i].z-zmean);
  MPI_Allreduce(MPI_IN_PLACE,&zsd,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  zsd /= NPartWorld[GAS];
  zsd = sqrt(zsd);
  //This is actually our estimate of h
  zsd = Params.eta*pow(PI*(DomainWalls[NTask]*DomainWalls[NTask]-DomainWalls[0]*DomainWalls[0])*zsd/NPartWorld[GAS],1.0/3.0);
  //Initialise per/particle quantities
  for(i=0;i<NPart[GAS];i++)
  {
    G[i].hsml = zsd;
    //G[i].hsml = 0.1;
    G[i].dK = 0;
    //Initialise predictor variables
    G[i].vpredx = G[i].vx;
    G[i].vpredy = G[i].vy;
    G[i].vpredz = G[i].vz;
    G[i].Kpred = G[i].K;
    //How many operations have been done by this particle since last
    //domain decomposition
    G[i].nops = 0;
  }
}

/*
 * Sets the global variable NPart[type] to the number of particles of this
 * type that are to be stored locally.  The function also returns the offset
 * within a global list (effectively the cumulative sum of local particles),
 * assuming each processor is reading sequentially from a giant array containing
 * all particles.
 */

int nlocal_particles(enum part_type type)
{
  int i;
  int nleft,offset;
  switch(type)
  {
    case SINK:
      //Store the number of local particles of this type
      NPart[type]=NPartWorld[type];
      return 0;
    case GAS:
      //Determine how many bits of data to read for this processor
      //and where to start reading them from
      nleft=0;
      offset=0;
      for(i=0;i<=Task;i++)
      {
        offset+=nleft;
        //On average we have this many particles per processor
        nleft=NPartWorld[type]/NTask;
        //But there'll be some left over, read one extra per processor at the start
        //to make up the difference
        if(i<NPartWorld[type] % NTask)
          nleft++;
      }
      //Store it
      NPart[type]=nleft;
      return offset;
  }
  return 0;
}


//Moves data from the relevant struct to the comm buffer in preparation for writing
void prop_to_buffer(enum part_type type,enum part_prop prop,int offset,int count)
{
  int i;
  FLOAT *fptr;
  fptr = Buffer;
  switch(type)
  {
    case GAS:
      switch(prop)
      {
        case IO_ID:
          for(i=0;i<count;i++)
            fptr[i] = G[i+offset].id;
        case IO_X:
          for(i=0;i<count;i++)
            fptr[i] = G[i+offset].x;
          break;
        case IO_Y:
          for(i=0;i<count;i++)
            fptr[i] = G[i+offset].y;
          break;
        case IO_Z:
          for(i=0;i<count;i++)
            fptr[i] = G[i+offset].z;
          break;
        case IO_HSML:
          for(i=0;i<count;i++)
            fptr[i] = G[i+offset].hsml;
          break;
        case IO_DEN:
          for(i=0;i<count;i++)
            fptr[i] = G[i+offset].density;
          break;
        case IO_UINT:
          //Convert entropy back to internal energy and store
          for(i=0;i<count;i++)
            fptr[i] = G[i+offset].K*pow(G[i+offset].density,Params.gamma-1)/(Params.gamma-1);
          break;
        default:
          break;
      }
      break;
    case SINK:
      switch(prop)
      {
        case IO_ID:
          for(i=0;i<count;i++)
            fptr[i] = S[i+offset].id;
          break;
        case IO_X:
          for(i=0;i<count;i++)
            fptr[i] = S[i+offset].x;
          break;
        case IO_Y:
          for(i=0;i<count;i++)
            fptr[i] = S[i+offset].y;
          break;
        case IO_Z:
          for(i=0;i<count;i++)
            fptr[i] = S[i+offset].z;
          break;
        default:
          break;
      }
      break;
  }
}

//Either return an integer saying if we should load a property or actually store it somewhere
int store_prop(enum part_type type,enum part_prop prop,int offset,int count,int mode)
{
  int i;
  FLOAT *fptr;
  int *iptr;
  //Cast the data pointed to by void *ptr to the two types
  fptr = Buffer;
  iptr = Buffer;
  //mode=0 -> store data in void *ptr
  //mode=1 -> return true/false
  switch(type)
  {
    case GAS:
      switch(prop)
      {
        case IO_ID:
          if(mode==0)
          {
            for(i=0;i<count;i++)
              G[i+offset].id=*iptr++;
          }
          return 1;
        case IO_X:
          if(mode==0)
          {
            for(i=0;i<count;i++)
            {
              G[i+offset].x=*fptr++;
              //printf("[%d] Set G[%d].x=%g.\n",Task,i+offset,G[i+offset].x);
            }
          }
          return 1;
        case IO_Y:
          if(mode==0)
          {
            for(i=0;i<count;i++)
              G[i+offset].y=*fptr++;
          }
          return 1;
        case IO_Z:
          if(mode==0)
          {
            for(i=0;i<count;i++)
              G[i+offset].z=*fptr++;
          }
          return 1;
        case IO_VEL_X:
          if(mode==0)
          {
            for(i=0;i<count;i++)
              G[i+offset].vx=*fptr++;
          }
          return 1;
        case IO_VEL_Y:
          if(mode==0)
          {
            for(i=0;i<count;i++)
              G[i+offset].vy=*fptr++;
          }
          return 1;
        case IO_VEL_Z:
          if(mode==0)
          {
            for(i=0;i<count;i++)
              G[i+offset].vz=*fptr++;
          }
          return 1;
        case IO_UINT:
          if(mode==0)
          {
            for(i=0;i<count;i++)
              G[i+offset].K=*fptr++;
          }
          return 1;
        case IO_ACC_X:
        case IO_ACC_Y:
        case IO_ACC_Z:
        case IO_HSML:
        case IO_DEN:
        case IO_MASS:
          return 0;
      }
      break;
    case SINK:
      switch(prop)
      {
        case IO_ID:
          if(mode==0)
          {
            for(i=0;i<count;i++)
              S[i+offset].id = *iptr++;
          }
          return 1;
        case IO_X:
          if(mode==0)
          {
            for(i=0;i<count;i++)
              S[i+offset].x=*fptr++;
          }
          return 1;
        case IO_Y:
          if(mode==0)
          {
            for(i=0;i<count;i++)
              S[i+offset].y=*fptr++;
          }
          return 1;
        case IO_Z:
          if(mode==0)
          {
            for(i=0;i<count;i++)
              S[i+offset].z=*fptr++;
          }
          return 1;
        case IO_MASS:
          if(mode==0)
          {
            for(i=0;i<count;i++)
              S[i+offset].m=*fptr++;
          }
          return 1;
        case IO_VEL_X:
          if(mode==0)
          {
            for(i=0;i<count;i++)
              S[i+offset].vx=*fptr++;
          }
          return 1;
        case IO_VEL_Y:
          if(mode==0)
          {
            for(i=0;i<count;i++)
              S[i+offset].vy=*fptr++;
          }
          return 1;
        case IO_VEL_Z:
          if(mode==0)
          {
            for(i=0;i<count;i++)
              S[i+offset].vz=*fptr++;
          }
          return 1;
        case IO_ACC_X:
        case IO_ACC_Y:
        case IO_ACC_Z:
        case IO_HSML:
        case IO_DEN:
        case IO_UINT:
          return 0;
      }
      break;
  }
  return 0;
}
