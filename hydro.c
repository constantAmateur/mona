/*
 * =====================================================================================
 *
 *       Filename:  hydro.c
 *
 *    Description:  Calculation of the *inviscid* hydrodynamic forces.
 *
 *        Version:  0.6.3
 *        Created:  16/08/13 13:46:47
 *
 *         Author:  Matthew D. Young, my304@ast.cam.ac.uk
 *        Company:  Institute of Astronomy, Cambridge, UK
 *        License:  GNU General Public License
 *      Copyright:  Copyright (c) 2014, Matthew D. Young
 *
 * =====================================================================================
 */

#include "variables.h"
#include "functions.h"



//The simple GADGET-2 alternative
void hydro(void)
{
  int i;
#ifdef ACCURATE_DIVV
  double h;
  double inv_a,inv_b,inv_c,inv_e,inv_f,inv_i,inv_det;
#endif
  //Initialise things we're calculating
  for(i=0;i<NPart[GAS];i++)
  {
    G[i].divv = 0;
#ifdef ACCURATE_DIVV
    G[i].chi[0] = 0;
    G[i].chi[1] = 0;
    G[i].chi[2] = 0;
    G[i].chi[3] = 0;
    G[i].chi[4] = 0;
    G[i].chi[5] = 0;
#endif
  }
 
  //Do the hard work with default comm routine
  ngb_search(HydroSend,HydroRecv,
      sizeof(union hydro_comm),BufferSizeHydro,
      hydro_ngbs,
      hydro_export_return,
      hydro_buffer_sort);

#ifdef ACCURATE_DIVV
  for(i=0;i<NPart[GAS];i++)
  {
    //The symmetric tensor we need to calculate divv to second order
    inv_a=G[i].chi[3]*G[i].chi[5]-G[i].chi[4]*G[i].chi[4];
    inv_b=G[i].chi[2]*G[i].chi[4]-G[i].chi[1]*G[i].chi[5];
    inv_c=G[i].chi[1]*G[i].chi[4]-G[i].chi[2]*G[i].chi[3];
    inv_e=G[i].chi[0]*G[i].chi[5]-G[i].chi[2]*G[i].chi[2];
    inv_f=G[i].chi[1]*G[i].chi[2]-G[i].chi[0]*G[i].chi[4];
    inv_i=G[i].chi[0]*G[i].chi[3]-G[i].chi[1]*G[i].chi[1];
    inv_det = (G[i].chi[0]*inv_a+G[i].chi[1]*inv_b+G[i].chi[2]*inv_c);
    //Might as well save the inverse matrix, we don't have any need for chi itself
    G[i].chi[0] = inv_a / inv_det;
    G[i].chi[1] = inv_b / inv_det;
    G[i].chi[2] = inv_c / inv_det;
    G[i].chi[3] = inv_e / inv_det;
    G[i].chi[4] = inv_f / inv_det;
    G[i].chi[5] = inv_i / inv_det;
    //Can estimate d/dt(divv) now...
    //Multiply by matrix D and calculate trace
    G[i].divv = (G[i].matD[0]*G[i].chi[0] + G[i].matD[1]*G[i].chi[1] + G[i].matD[2]*G[i].chi[2] +
                 G[i].matD[3]*G[i].chi[1] + G[i].matD[4]*G[i].chi[3] + G[i].matD[5]*G[i].chi[4] +
                 G[i].matD[6]*G[i].chi[2] + G[i].matD[7]*G[i].chi[4] + G[i].matD[8]*G[i].chi[5] );
    //pprintf("First order divv for %d is %g second order is %g\n",i,-G[i].divv,G[i].divv* inv_det*(inv_a+inv_e+inv_i));
    //Finally, store the second order estimate of div.v
    //G[i].divv *= (inv_a+inv_e+inv_i)/inv_det;
    //h=G[i].hsml;
    //G[i].divv *= (-PcleMass/(G[i].dhsml*H_TO_THE_DPLUS1));
    //G[i].divv *= -1;
  }
#endif
}

void hydro_export_return(void *data)
{
  union hydro_comm *part = data;
  int k;
  k=part->out.what;
  G[k].ax += part->out.ax;
  G[k].ay += part->out.ay;
  G[k].az += part->out.az;
  G[k].divv += part->out.divv;
  G[k].nops += part->out.nops;
#ifdef ACCURATE_DIVV
  int i;
  G[k].chi[0] += part->out.chi[0];
  G[k].chi[1] += part->out.chi[1];
  G[k].chi[2] += part->out.chi[2];
  G[k].chi[3] += part->out.chi[3];
  G[k].chi[4] += part->out.chi[4];
  G[k].chi[5] += part->out.chi[5];
  for(i=0;i<9;i++)
    G[k].matD[i] += part->out.matD[i];
#endif
}

void hydro_ngbs(void *index,void *export, int mode)
{
  double h_a,rho_a,dhsml_a,pressure_a;
  double pos_a[3];
  double h2_a,fact_a,ax,ay,az;
  double dw_ab;
  double dx,dy,dz;
  double sum,r,r2;
  double vx_a,vy_a,vz_a,dvx,dvy,dvz;
  double divv;
  double q_ab;
  double h4_inv;
  double h;
#if GRAV>=2 && !defined CONSTANT_GAS_SOFT
  double zeta_a;
#endif
  int current;
  int nngbs,i,j,k;
  PCLE_REF *ngbs;
  int owner,mob,count;
  int reget=0;
  int nops=0;
  int export_offset[NTask];
  const struct node *cnode;
  //Cast the memory locations of the particle to search for (index/remote) and 
  //where to store any exported particles (storage)
  struct gas *local = NULL;
  union hydro_comm *remote;
  union hydro_comm *storage = NULL;
#ifdef ACCURATE_DIVV
  double chi[6]={0};
  double matD[9]={0};
#endif

  if(mode)
  {
    //Cast pointer as needed
    remote = index;
    pos_a[0] = remote->in.x;
    pos_a[1] = remote->in.y;
    pos_a[2] = remote->in.z;
    vx_a = remote->in.vx;
    vy_a = remote->in.vy;
    vz_a = remote->in.vz;
    h_a = remote->in.hsml;
    rho_a = remote->in.density;
    dhsml_a = remote->in.dhsml;
    pressure_a = remote->in.pressure;
    nngbs = remote->in.nnodes;
    //pprintf("Received %d nodes from remote\n",nngbs);
    ngbs = remote->in.nodes;
#if GRAV>=2 && !defined CONSTANT_GAS_SOFT
    zeta_a = remote->in.zeta;
#endif
  }
  else
  {
    //Cast index pointer to appropriate type
    local = index;
    storage = export;
    pos_a[0] = local->x;
    pos_a[1] = local->y;
    pos_a[2] = local->z;
    vx_a = local->vpredx;
    vy_a = local->vpredy;
    vz_a = local->vpredz;
    h_a = local->hsml;
    rho_a = local->density;
    dhsml_a = local->dhsml;
    pressure_a = local->pressure;
    nngbs = local->nngbs;
    ngbs = local->ngbs;
    for(i=0;i<NTask;i++)
      export_offset[i]=-1;
#if GRAV>=2 && !defined CONSTANT_GAS_SOFT
    zeta_a = local->zeta;
#endif
  }

  h2_a = h_a*h_a*KER_SUPPORT*KER_SUPPORT;
  h=h_a;
  h4_inv = 1.0/H_TO_THE_DPLUS1;
  fact_a = PcleMass * h4_inv / (dhsml_a );
  current = 0;
  ax = ay = az=0;
  divv = 0;
  mob=owner=count=0;
  //printf("[%d] Starting the game with %d ngbs.\n",Task,nngbs);
  //Outer loop in case we over-fill buffer
  do
  {
    //printf("[%d] Hey we're going around the loop with current=%d.\n",Task,current);
    //Do we already have a nice list of nodes/ngbs?
    //Second check is if we're looping over find_ngbs we 
    //don't accidentally end up skipping subsequent calls on overflow
    if(!nngbs || ngbs == Ngbs)
    {
      //printf("[%d] Mode=%d, Don't have ngbs, so calculating them...\n",Task,mode);
      //No we don't, so get one...
      nngbs = find_ngbs(pos_a,KER_SUPPORT*h_a,&current,mode);
      //Can we store it?
      //Not if on export, we don't have room, find_ngbs overflowed or 
      //we're not in the first run through the do loop
      if(!mode && nngbs<NGB_LIST_LEN && current==-1 && !reget)
      {
        //printf("[%d] Storing %d ngbs.\n",Task,nngbs);
        //So store it then
        memmove(local->ngbs,Ngbs,sizeof(int)*nngbs);
        local->nngbs=nngbs;
      }
      //Set pointer for work now
      ngbs = Ngbs;
      reget=1;
    }
    else
    {
      //So we exit the loop OK.
      current=-1;
    }
    ////printf("[%d] Now looping through %d neighbours.\n",Task,nngbs);
    //ngbs now has a list of nodes/neighbours, do stuff with it...
    for(i=0;i<nngbs;i++)
    {
      //OK.  The ngb buffer is assumed to have some special properties we're going to
      //exploit.  Firstly, if it's a particle, then we must have a copy of it 
      //(strongly local if on export), otherwise how did we find it?  If it's a node
      //and we're local, it must be exported.If it's a node and we're on export, it 
      //must be opened up (otherwise why was it sent to us)?  The debug section tests
      //these assumptions.
#if DEBUG>=1
      if(ngbs[i]<0)
      {
        cnode=Root-ngbs[i];
        if(mode)
        {
          //If it's on export, this must be strongly local
          if((cnode->owner>0 && cnode->owner!=Task) ||
             (cnode->owner<0 && !get_bit(Task,cnode->owners)))
          {
            pprintf("Neighbour list contains element %d = %d which is not local even though we're in export mode.\n",i,ngbs[i]);
            kill(ERROR_SANITY);
          }
        }
        else
        {
          //Can't possibly be entirely locally owned, otherwise we should have 
          //its contents rather than the node
          j=0;
          //Flag that we've found a foreigner
          k=0;
          do
          {
            owner = cnode->owner<0 ? (int) (cnode->extra[j].mob/MaxGas) : cnode->owner;
            //This is foreign, as at least one of them must be
            if(!Shadows[owner].G)
              k=1;
            j++;
          }while(j<(-cnode->owner) && !k);
          if(!k)
          {
            pprintf("Neighbour %d was %d, which is a node that we have entirely locally and so should have been opened up given we're in local mode.\n",i,ngbs[i]);
          kill(ERROR_SANITY);
          }
        }
      }
      else
      {
        //It's a particle, it had better be local 
        //It must be strongly local on export
        owner=ngbs[i]/MaxGas;
        if(owner!=Task && (mode || !Shadows[owner].G))
        {
          pprintf("With mode %d neighbour %d was particle %d, which is not local.\n",mode,i,ngbs[i]);
          kill(ERROR_SANITY);
        }
      }
#endif
      //Is it a particle or a node?
      if(ngbs[i]>=0)
      {
        cnode = NULL;
        owner = ngbs[i]/MaxGas;
        mob = ngbs[i]%MaxGas;
        count=1;
      }
      else
      {
        cnode = Root-ngbs[i];
        //Only chance I'll process it is if we're on export
        if(mode)
        {
          owner=Task;
          //Get the local bit
          if(cnode->owner<0)
          {
            j=0;
            while(cnode->extra[j].mob/MaxGas!=Task)
              j++;
            mob = (int) (cnode->extra[j].mob%MaxGas);
            count = cnode->extra[j].count;
          }
          else
          {
            mob = cnode->mob;
            count = cnode->count;
          }
        }
      }
      //Now process if needed
      if(!cnode || mode)
      {
        for(k=mob;k<mob+count;k++)
        {
          nops++;
          //pprintf("owner=%d,k=%d,i=%d,ngbs=%d,reget=%d\n",owner,k,i,ngbs[i],reget);
          dx = pos_a[0] - Shadows[owner].G[k].x;
          dy = pos_a[1] - Shadows[owner].G[k].y;
          dz = pos_a[2] - Shadows[owner].G[k].z;
  
          r2 = dx*dx + dy*dy + dz*dz;
          if(r2<h2_a && r2)
          {
            //It's a real neighbour, do stuff with it...
            r=sqrt(r2);
            q_ab = r /h_a;
            dw_ab = dkernel(q_ab);
            dvx = vx_a - Shadows[owner].G[k].vpredx;
            dvy = vy_a - Shadows[owner].G[k].vpredy;
            dvz = vz_a - Shadows[owner].G[k].vpredz;
            //Add the normal h_a contribution to particle a
            sum = fact_a * dw_ab / r;
            //Add the correction due to adaptive gravitational softening
#if GRAV>=2 && !defined CONSTANT_GAS_SOFT
            sum *= (pressure_a/(rho_a*rho_a))+0.5*Params.G*zeta_a;
#else
            sum *= (pressure_a/(rho_a*rho_a));
#endif
            ax -= sum*dx;
            ay -= sum*dy;
            az -= sum*dz;
            //The divv calculation...
#ifdef ACCURATE_DIVV
            //This is not actually divv, but is used to solve for it
            dw_ab /= (r*Shadows[owner].G[k].density);
            //divv += dw_ab * (dvx*dx+dvy*dy+dvz*dz);
            //The symmetric tensor we need to calculate divv to second order
            chi[0] += dx*dx*dw_ab;
            chi[1] += dx*dy*dw_ab;
            chi[2] += dx*dz*dw_ab;
            chi[3] += dy*dy*dw_ab;
            chi[4] += dy*dz*dw_ab;
            chi[5] += dz*dz*dw_ab;
            matD[0] += dvx*dx*dw_ab;
            matD[1] += dvx*dy*dw_ab;
            matD[2] += dvx*dz*dw_ab;
            matD[3] += dvy*dx*dw_ab;
            matD[4] += dvy*dy*dw_ab;
            matD[5] += dvy*dz*dw_ab;
            matD[6] += dvz*dx*dw_ab;
            matD[7] += dvz*dy*dw_ab;
            matD[8] += dvz*dz*dw_ab;
#else
            divv -= dw_ab  *fact_a* (dvx*dx+dvy*dy+dvz*dz)/ (r*Shadows[owner].G[k].density);
#endif
            //Rather than trying to gather all the additions to 
            //particle a due to the h_b terms, we instead add 
            //all the h_a terms to particle b now
            //Essentially we're just explicitly satisfying newton's
            //second law here
            Shadows[owner].G[k].ax += sum*dx;
            Shadows[owner].G[k].ay += sum*dy;
            Shadows[owner].G[k].az += sum*dz;
          }
        }
      }
      //Otherwise it's a local node that we need to export
      else
      {
        //Export this node to all its owners
        j=0;
        do
        {
          //We only really need to know where to send it...
          if(cnode->owner<0)
          {
            owner = (int) (cnode->extra[j].mob/MaxGas);
            //Skip locally owned bits, they've already been seen as particles
            if(Shadows[owner].G)
            {
              j++;
              if(j<(-cnode->owner))
                continue;
              break;
            }
          }
          else
          {
            owner = cnode->owner;
          }
          //Establish export of this node to processor whatever
          if(export_offset[owner]<0)
          {
            //pprintf("For particle %d, exporting node %d to %d with lvl/lid %d/%d count %d owner %d.\n",(int) (local-G),ngbs[i],owner,(int) (cnode->lvl_lid/MaxLID),(int) (cnode->lvl_lid%MaxLID),cnode->count,cnode->owner);
            export_offset[owner]=N_Export;
            storage[N_Export].in.x = pos_a[0];
            storage[N_Export].in.y = pos_a[1];
            storage[N_Export].in.z = pos_a[2];
            storage[N_Export].in.vx = vx_a;
            storage[N_Export].in.vy = vy_a;
            storage[N_Export].in.vz = vz_a;
            storage[N_Export].in.hsml = h_a;
            storage[N_Export].in.density = rho_a;
            storage[N_Export].in.dhsml = dhsml_a;
            storage[N_Export].in.pressure = pressure_a;
#if GRAV>=2 && !defined CONSTANT_GAS_SOFT
            storage[N_Export].in.zeta = zeta_a;
#endif
            storage[N_Export].in.what = (int) (local-G);
            storage[N_Export].in.where = owner;
            storage[N_Export].in.from = Task;
            storage[N_Export].in.nodes[0] = ngbs[i];
            storage[N_Export].in.nnodes = 1;
            N_Export++;
            N_Send_Local[owner]++;
          }
          else
          {
            if(storage[export_offset[owner]].in.nnodes == NGB_LIST_LEN)
            {
              //printf("[%d] Too many to export :(\n",Task);
              storage[export_offset[owner]].in.nnodes = 0;
              //pprintf("For particle %d abandoning export of nodes.\n",(int) (local-G));
            }
            //Don't add anything if we can't fit it all
            else if(storage[export_offset[owner]].in.nnodes)
            {
              //pprintf("For particle %d, adding in export of node %d to %d with lvl/lid %d/%d count %d owner %d.\n",(int) (local-G),ngbs[i],owner,(int) (cnode->lvl_lid/MaxLID),(int) (cnode->lvl_lid%MaxLID),cnode->count,cnode->owner);
              //printf("[%d] Sup!  Adding in extra node to send, up to nnodes = %d.\n",Task,storage[export_offset[owner]].in.nnodes);
              //Add in the new node
              storage[export_offset[owner]].in.nodes[storage[export_offset[owner]].in.nnodes] = ngbs[i];
              storage[export_offset[owner]].in.nnodes++;
            }
          }
          j++;
        }while(j<(-cnode->owner));
      }
    }
  }while(current!=-1);

  if(mode)
  {
    mode = remote->in.where;
    i = remote->in.from;
    nngbs = remote->in.what;
    remote->out.what = nngbs;
    remote->out.where = mode;
    remote->out.from = i;
    remote->out.ax = ax;
    remote->out.ay = ay;
    remote->out.az = az;
    remote->out.divv = divv;
    remote->out.nops = nops;
#ifdef ACCURATE_DIVV
    remote->out.chi[0] = chi[0];
    remote->out.chi[1] = chi[1];
    remote->out.chi[2] = chi[2];
    remote->out.chi[3] = chi[3];
    remote->out.chi[4] = chi[4];
    remote->out.chi[5] = chi[5];
    for(i=0;i<9;i++)
      remote->out.matD[i] = matD[i];
#endif
  }
  else
  {
    local->ax += ax;
    local->ay += ay;
    local->az += az;
    local->divv += divv;
    local->nops += nops;
#ifdef ACCURATE_DIVV
    local->chi[0] += chi[0];
    local->chi[1] += chi[1];
    local->chi[2] += chi[2];
    local->chi[3] += chi[3];
    local->chi[4] += chi[4];
    local->chi[5] += chi[5];
    for(i=0;i<9;i++)
      local->matD[i]=matD[i];
#endif
  }
}


int hydro_buffer_sort(const void* a,const void* b)
{
  if((*((union hydro_comm*) a)).in.where < (*((union hydro_comm*) b)).in.where) return -1;
  if((*((union hydro_comm*) a)).in.where > (*((union hydro_comm*) b)).in.where) return 1;
  return 0;
}

void calc_pressure(void)
{
  int i,j;
  //Assume that this is a simple calculation and that it's faster to do on all
  //shadow domains than to execute a broadcast from local to shadows
  for(j=0;j<NTask;j++)
  {
    if(Shadows[j].G)
    {
      for(i=0;i<NGas[j];i++)
      {
        Shadows[j].G[i].pressure = eos(Shadows[j].G[i].density,Shadows[j].G[i].Kpred);
#if DEBUG>=1
        if(Shadows[j].G[i].pressure!=Shadows[j].G[i].pressure || Shadows[j].G[i].pressure<=0)
        {
          pprintf("G[%d].(density,K,pressure) = (%g,%g,%g) has nan pressure Gamma = %g.\n",i,Shadows[j].G[i].density,Shadows[j].G[i].Kpred,Shadows[j].G[i].pressure,Params.gamma);
          kill(ERROR_SANITY);
        }
#endif
      }
    }
  }
}

double eos(double density,double K)
{
  return K*pow(density,Params.gamma);
}

