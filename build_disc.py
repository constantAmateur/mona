import h5py as h
import numpy as np
from scipy.special import erf
from numpy import sqrt,exp,log,pi




dyn_range=5
q=.2
density_power = -2
Npart = 1e6
epsilon = 0.00
sym = True
twoD = False
file_name = "Test_1e6.hdf5"

### ARBITRARY PARAMETERS ###

R_i = 1.0
M = 1.0
Q_i = 2.0
gamma = 5/3.0
mu = 2.3
temp_power = 3 + 2*density_power

### UNITS ###

#Unit Mass in grams
M_unit=1.989e33 #Solar mass
#Unit Length in cm
L_unit=1.496e13 #An AU
#We want the unit of time to be one outer rotation periods, so we set that and let the velocity unit follow...
#That is t_unit=2pisqrt(R^3/GM), giving t in units of seconds
#t_unit=2*pi*sqrt(((25*1.496e13)^3)/(6.673e-8*1.989e33))
#This unit choice gives G=1 (or at least it should).  It sets the time unit to be 1/Omega at R=1
t_unit=sqrt((L_unit**3)/(6.673e-08*M_unit))
#Infer the velocity unit
v_unit=L_unit/t_unit
##Unit Velocity in cm/sec
#v_unit=1e5
##Infer unit time
#t_unit=L_unit/v_unit
#Define some other units we'll use
#Boltzman constant in grams, cm, seconds, then converted to internal units
k_b=1.38e-16
k_b=k_b*(1/v_unit)**2*(1/M_unit)
#Mass of hydrogen in grams and internal units
m_H = 1.67e-24
m_H = m_H * (1/M_unit)
#Newton's Gravitational constant in grams,cm,seconds
ginternal=6.673e-08
#Convert to internal units...
ginternal = ginternal*(1/L_unit)**3*(1/M_unit)**-1*(1/t_unit)**-2

### Dependent variables #####

M_disc = M*q
R_o = R_i * dyn_range
#First calculate the Sigma normalization constant
if density_power==-2.0:
  Sigma_norm=(1-epsilon)*(M_disc/(2*pi*log(dyn_range)))
else:
  Sigma_norm=((1-epsilon)*M_disc * (density_power+2))/(2*pi*(R_o**(density_power+2)-R_i**(density_power+2)))
#Normailised so it's Q_i at the lowest point...
Q_pow=-1.5+(temp_power/2.0)-density_power
if Q_pow<=0:
  RR=R_o
else:
  RR=R_i

Temp_norm = (RR**(-2.0*Q_pow))*((Q_i*Q_i*mu*m_H*ginternal*pi*pi*Sigma_norm*Sigma_norm)/(gamma*k_b*M))



#############################
# BUILD INITIAL CONDITIONS  #
#############################

#We need to pick the exponential decay in such a way that the surface density remains continuous, its 1st derivative remains continuous and the mass enclosed is equal to epsilon*M.  The root to the following function are what are needed to meet these requirements
def outpeach(b,eps,Ri,r,alpha=density_power):
  return (Ri*(Ri-b)*(1/alpha)*(exp((alpha*(Ri-b))/(2*Ri))-exp(-(alpha*b*b)/(2*Ri*(b-Ri))))+b*sqrt((pi*Ri*(b-Ri))/(2*alpha))*(erf(sqrt(((b-Ri)*alpha*.5)/Ri))-erf(sqrt((alpha*b*b)/(2*Ri*(b-Ri)))))+((eps*M_disc*Ri**(-alpha)*exp((alpha*.5*(Ri-b))/Ri))/(2*pi*Sigma_norm)))

epsilon=0
if epsilon!=0:
  #Determine where to look for root
  samp=np.linspace(0,R_i,10000)
  samp=samp[samp!=1]
  tmp=outpeach(samp,eps=epsilon,Ri=R_i,r=dyn_range,alpha=density_power)
  lower=samp[max(np.where(tmp<0)[0])]
  print "searching for root above R=%g"%lower
  #The mean of the distribution
  #b=uniroot(outpeach,eps=epsilon,Ri=R_i,r=dyn_range,alpha=density_power,lower=lower,upper=R_i,f.upper=(R_i^(-density_power)*epsilon*log(dyn_range))/(1-epsilon))
  #b=b$root
  ##it's standard deviation
  #c=sqrt(R_i*(b-R_i)/density_power)
  #print(paste("Root finder settled on standard deviation =",c,"mean=",b))
  ##Not really needed, but for completeness...
  #a=Sigma_norm*R_i^density_power*exp(density_power*(b-R_i)/(2*R_i))
else:
  a=1
  b=1
  c=1

def pdf(R):
  ret=(a*R*exp(-(R-b)**2/(2*c**2)))
  ret[R>=R_i]=(((1-epsilon)*M_disc*R**(density_power+1))/(2*pi*log(dyn_range)))[R>=R_i]
  return ret

def rejectionSample(no,pdf,xmin=0,xmax=1,ymin=None,ymax=None,sample=100,safteyFact=2):
  if ymin is None or ymax is None:
    samp=np.linspace(xmin,xmax,sample)
    tmp=pdf(samp)
    ymin=np.min(tmp)/safteyFact
    ymax=np.max(tmp)*safteyFact
  res=np.array([])
  #Keep sampling till we have enough points
  while len(res)<no:
    #We'll make too many this way, but who cares...
    tmp_x=(xmax-xmin)*np.random.rand(no)+xmin
    tmp_y=(ymax-ymin)*np.random.rand(no)+ymin
    true_y=pdf(tmp_x)
    res = np.r_[res,tmp_x[tmp_y<true_y]]
  #Only keep the first no
  res=res[1:no]
  return res


def cyl2cart(R,th,z):
  x=R*np.cos(th)
  y=R*np.sin(th)
  return np.c_[x,y,z]

if epsilon==0:
  start=R_i
else:
  start=0
radi = rejectionSample(Npart,pdf,xmax=R_o,xmin=start)
if sym:
  radi = np.tile(radi[np.random.permutation(len(radi))[:len(radi)/2]],2)
  Npart=len(radi)

print "Effective inner radius:"
#Theta probably should never be anything but uniform...
if sym:
  theta = np.random.rand(Npart/2)*(2*np.pi)
  tmp=(theta + pi)%(2*pi)
  theta = np.r_[theta,tmp]
else:
  theta= np.random.rand(Npart)*2*np.pi
#The temperature
temp = Temp_norm*radi**temp_power
#Calculate the speed of sound for each particle
cs = sqrt(gamma*k_b*temp/(mu*m_H))
#Calculate the velocities (just the usual 1/sqrt(r) keplerian)
vphi=sqrt((ginternal * M)/(radi**3))
#Add the pressure modification? This saves time when settling into marginal stability
vphi=sqrt((ginternal * M)/(radi))
vphi=vphi*sqrt(1-density_power*cs*cs/(vphi*vphi))
vphi=vphi/radi
#This determines the vertical scale of the disk, it goes as r^(3/2-temppower/2)
scale_H = (cs /vphi)
#The z coordinate
if sym:
  z = np.random.normal(size=Npart/2)*scale_H[:(Npart/2)]
  z = np.r_[z,-z]
else:
  z= np.random.normal(size=Npart)*scale_H
#An array of masses...
mass=np.tile(M_disc/Npart,Npart)
#We've calculated everything, now put it in the final variables which are cartesian...
pos=cyl2cart(radi,theta,z)
if twoD:
  pos[:,2]=0
vel=np.c_[-pos[:,1]*vphi,pos[:,0]*vphi,np.tile(0,len(vphi))]
#We need to convert the temperature to internal energy per unit mass
u=temp*k_b/(mu*m_H*(gamma-1))
#Add on the star...  It doesn't need an internal energy as it's a different "type"
pos=np.r_[pos,np.zeros((1,3))]
vel=np.r_[vel,np.zeros((1,3))]
mass=np.r_[mass,M]

print "The number of particles is %d"%Npart

with h.File(file_name) as out:
  #Create the gas particles
  out.create_group("Gas")
  out["Gas"]["ID"]=np.arange(Npart)
  out["Gas"]["Internal_Energy"]=u
  out["Gas"]["Vel_X"]=vel[:-1,0]
  out["Gas"]["Vel_Y"]=vel[:-1,1]
  out["Gas"]["Vel_Z"]=vel[:-1,2]
  out["Gas"]["X"]=pos[:-1,0]
  out["Gas"]["Y"]=pos[:-1,1]
  out["Gas"]["Z"]=pos[:-1,2]
  out["Gas"].attrs["Mass"]=M_disc/Npart
  #Create the sinks
  out.create_group("Sink")
  out["Sink"]["ID"]=np.array([Npart])
  out["Sink"]["Vel_X"]=np.array([0.0])
  out["Sink"]["Vel_Y"]=np.array([0.0])
  out["Sink"]["Vel_Z"]=np.array([0.0])
  out["Sink"]["X"]=np.array([0.0])
  out["Sink"]["Y"]=np.array([0.0])
  out["Sink"]["Z"]=np.array([0.0])
  out["Sink"]["Mass"]=np.array([1.0])
